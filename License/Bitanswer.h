/**
 *
 * BitAnswer Client Library.
 *
 * BitAnswer Ltd. (C) 2009 - ?. All rights reserved.
 *
 */

#include<QLibrary>
#ifndef __BITANSWER_CLIENT_API_H__
    #define __BITANSWER_CLIENT_API_H__

#if defined(_MSC_VER) || defined(__BORLANDC__)
    typedef unsigned __int64   BIT_UINT64;
    typedef signed __int64     BIT_INT64;
#else
    typedef unsigned long long BIT_UINT64;
    typedef signed long long   BIT_INT64;
#endif
#if defined(_MSC_VER)
    typedef unsigned long      BIT_UINT32;
    typedef signed long        BIT_INT32;
#else
    typedef unsigned int       BIT_UINT32;
    typedef signed int         BIT_INT32;
#endif
typedef unsigned short         BIT_USHORT;
typedef signed short           BIT_SHORT;

typedef unsigned short         BIT_UINT16;
typedef short                  BIT_INT16;

typedef unsigned char          BIT_UINT8;
typedef char                   BIT_INT8;

typedef unsigned char          BIT_UCHAR;
typedef char                   BIT_CHAR;
typedef const char *           BIT_PCSTR;
typedef int                    BIT_BOOL;

typedef void *                 BIT_HANDLE;
typedef void                   BIT_VOID;

#if defined(_WIN32) || defined(WIN32) || defined(_MSC_VER) || defined(__BORLANDC__)
    #define BIT_CALLCONV __stdcall
#else
    #define BIT_CALLCONV
#endif



//#if defined(_WIN32) || defined(WIN32)
//#ifdef _MT
//#ifdef _DLL
//#pragma comment(lib, "00018917_0000000a_md.lib")
//#else
//#pragma comment(lib, "00018917_0000000a_mt.lib")
//#endif
//#else
//#pragma comment(lib, "00018917_0000000a_mt.lib")
//#endif
//#endif

#ifdef __cplusplus
extern "C" {
#endif

///** The Application Data is confidential information, please keep it carefully! */
//static BIT_UCHAR application_data[] = {
//    0x40,0x80,0xbc,0x80,0x7e,0xbf,0x51,0x1d,0xf9,0x67,0xc9,0xcc,0x22,0xf4,0x73,0x15,
//    0x11,0x17,0x46,0xd6,0xeb,0x71,0x2a,0xa2,0xc1,0x95,0x27,0xac,0x8d,0xe8,0xce,0xf6,
//    0x29,0x46,0x7f,0xf6,0x53,0x60,0x64,0x89,0x0d,0x0f,0xc9,0x34,0xf3,0x1e,0x1b,0xa5,
//    0x62,0x28,0x68,0x84,0xbc,0x5e,0x56,0x0c,0xb7,0xff,0xed,0x9b,0xa5,0x53,0x72,0xb4,
//    0xe8,0x91,0x7e,0x15,0x22,0xdf,0x4b,0x6c,0xca,0x1b,0x86,0x84,0x81,0x22,0x84,0xe2,
//    0x5a,0x5c,0xe3,0x0b,0x45,0xc0,0x81,0x8c,0xe4,0x6d,0x64,0x6b,0xdf,0x13,0xa8,0x05,
//    0x82,0x3c,0x4b,0x5b,0xe3,0x67,0xaf,0x9d,0xc8,0xc9,0xe2,0xb9,0x03,0x31,0x57,0xcd,
//    0x1f,0xf7,0x6b,0x50,0x6b,0x52,0xff,0x5a,0xd1,0x7c,0xd6,0x88,0x35,0xbc,0xb3,0x38,
//    0x4c,0xe5,0x6f,0xa0,0x2e,0x71,0x35,0x27,0xd6,0xac,0xa1,0x2b,0x67,0x8c,0x54,0xea,
//    0x79,0xad,0x1f,0xed,0xdb,0xfc,0x70,0xa4,0xd1,0xb2,0xe0,0x8d,0x0f,0x47,0x46,0x57,
//    0xbd,0xad,0x7a,0x0e,0x55,0x3e,0xa1,0x6e,0xcf,0xe3,0xbc,0x89,0xe9,0x97,0xf3,0x54,
//    0x93,0xd4,0xdd,0x25,0xa3,0xc6,0x7e,0x40,0x08,0xac,0xb4,0x43,0x77,0x2f,0x83
//};

typedef enum _FEATURE_TYPE
{
   BIT_FEATURE_CONVERT         = 0x03,
   BIT_FEATURE_READ_ONLY       = 0x04,
   BIT_FEATURE_READ_WRITE      = 0x05,
   BIT_FEATURE_CRYPTION        = 0x09,
   BIT_FEATURE_USER            = 0x0a,
   BIT_FEATURE_UNIFIED         = 0x0b,
   BIT_FEATURE_NONE            = 0xFFFFFFFF,
} FEATURE_TYPE;

typedef enum _LOGIN_MODE
{
    BIT_MODE_LOCAL             = 0x01,
    BIT_MODE_REMOTE            = 0x02,
    BIT_MODE_AUTO              = 0x03
} LOGIN_MODE;

#define LOGIN_MODE_CACHE       4
#define LOGIN_MODE_USB         8

typedef enum _BINDING_TYPE
{
    BINDING_EXISTING           = 0,
    BINDING_LOCAL              = 1,
    BINDING_USB_STORAGE        = 2
} BINDING_TYPE;

typedef enum _BIT_INFO_TYPE
{
   BIT_INFO_SERVER_ADDRESS     = 0,
   BIT_INFO_SN                 = 1,
   BIT_INFO_SN_FEATURE         = 2,
   BIT_INFO_SN_LICENSE         = 3,
   BIT_INFO_UPDATE_ERROR       = 4
} BIT_INFO_TYPE;

/** Structures */
#pragma pack(1)

typedef struct _BIT_DATE_TIME
{
   BIT_USHORT  year;
   BIT_UCHAR   month;
   BIT_UCHAR   dayOfMonth;
   BIT_UCHAR   hour;
   BIT_UCHAR   minute;
   BIT_UCHAR   second;
   BIT_UCHAR   unused;
} BIT_DATE_TIME, *PBIT_DATE_TIME;

typedef struct _BIT_FEATURE_INFO
{
   BIT_UINT32      featureId;
   FEATURE_TYPE    type;
   BIT_CHAR        featureName[128];
   BIT_DATE_TIME   endDateTime;
   BIT_UINT32      expirationDays;
   BIT_UINT32      users;
} BIT_FEATURE_INFO, *PBIT_FEATURE_INFO;

#pragma pack()

/**
 * @defgroup Run-time API Status Codes
 *
 * @{
 */
enum BIT_ERROR_CODES
{
    BIT_SUCCESS                       = 0x0000,

    /** client side error */
    BIT_ERR_NETWORK                   = 0x0101,
    BIT_ERR_WRONG_HANDLE              = 0x0102,
    BIT_ERR_INVALID_PARAMETER         = 0x0103,
    BIT_ERR_BUFFER_SMALL              = 0x0104,
    BIT_ERR_APPLICATION_DATA          = 0x0105,
    BIT_ERR_HEARTBEAT_THREAD          = 0x0106,
    BIT_ERR_SERVER_BUSY               = 0x0107,
    BIT_ERR_SERVER_DOWN               = 0x0108,
    BIT_ERR_RETURN_NULL               = 0x0109,
    BIT_ERR_CMD_TYPE                  = 0x010A,
    BIT_ERR_BADSTRING                 = 0x010B,
    BIT_ERR_HANDLE_NULL               = 0x010C,
    BIT_ERR_INVALID_OFF_LINE_SN       = 0x010E,
    BIT_ERR_OFF_LINE_SN_DENIED        = 0x010F,
    BIT_ERR_SN_FILE_NOT_FOUND         = 0x0110,
    BIT_ERR_OFF_LINE_SN_SIGNATURE     = 0x0111,
    BIT_ERR_RUN_TIME_ERROR            = 0x0112,
    BIT_ERR_CHECK_THREAD              = 0x0113,
    BIT_ERR_LICENSE_NOT_FOUND         = 0x0114,
    BIT_ERR_UNEXPECTED                = 0x0115,
    BIT_ERR_DOWNLOAD_UNCOMPLETE       = 0x0116,
    BIT_ERR_POINTS_OVER               = 0x0117,
    BIT_ERR_TLV_BUF_OVER              = 0x0118,
    BIT_ERR_MORE_USB_DISK             = 0x0119,
    BIT_ERR_NO_USB_DISK               = 0x011A,
    BIT_ERR_GET_MID                   = 0x011B,
    BIT_ERR_SN_INVALID                = 0x011C,
    BIT_ERR_TIME_TAMPER               = 0x011D,
    BIT_ERR_NOT_ENCRYPTED             = 0x011E,
    BIT_ERR_KEY_NOT_MATCH             = 0x011F,
    BIT_ERR_BINDINGPOLICY_NOT_MATCHED = 0x0120,
    BIT_ERR_LOAD_EXTENSION_FAILED     = 0x0121,
    BIT_ERR_MALLOC_FAILED             = 0x0122,
    BIT_ERR_REVOKED                   = 0x0123,
    BIT_ERR_EXTENSION_EXIST           = 0x0124,
    BIT_ERR_INIT_DBPATH               = 0x0125,
    BIT_ERR_LOCAL_HOST_NULL           = 0x0126,
    BIT_ERR_WRONG_SERVER_TYPE         = 0x0127,
    BIT_ERR_SOAP                      = 0x0128,
    BIT_ERR_UDP                       = 0x0129,
    BIT_ERR_UPDATE_CODE               = 0x012A,
    BIT_ERR_UPDATE_INFO_VERSION       = 0x012B,
    BIT_ERR_TYPE_NOT_MATCHED          = 0x012C,
    BIT_ERR_INVALID_URL               = 0x012D,
    BIT_ERR_UPDATE_INFO_FORMAT        = 0x012E,
    BIT_ERR_SN_EMPTY                  = 0x012F,

    BIT_ERR_SN_ENTRY_NOT_FOUND        = 0x0131,
    BIT_ERR_FUNCTION_NOT_SUPPORT      = 0x0132,

    BIT_ERR_ACCESS_ERROR              = 0x0151,
    BIT_ERR_ACCESS_DENIED             = 0x0152,
    BIT_ERR_ACCESS_NOT_FOUND          = 0x0153,

    BIT_ERR_TOKEN_NOT_SUPPORT         = 0x0181,
    BIT_ERR_TOKEN_NOT_FOUND           = 0x0182,
    BIT_ERR_TOKEN_ACCESS_FAILED       = 0x0183,
    BIT_ERR_TOKEN_READ_FAILED         = 0x0184,
    BIT_ERR_TOKEN_WRITE_FAILED        = 0x0185,
    BIT_ERR_TOKEN_NOT_INIT            = 0x0186,
    BIT_ERR_TOKEN_ALREADY_INIT        = 0x0187,

    /** server side error */
    BIT_ERR_NEED_LOGIN                = 0x0201,
    BIT_ERR_NULL_PARAMETER            = 0x0202,
    BIT_ERR_SRV_INVALID_PARAMETER     = 0x0203,
    BIT_ERR_UNKNOWN_EXCEPTION         = 0x0204,
    BIT_ERR_UNKNOWN_VAR               = 0x0205,
    BIT_ERR_PRODUCT_DISABLED          = 0x0206,
    BIT_ERR_DEVELOPER_DISABLED        = 0x0207,
    BIT_ERR_WEB_SERVICE_STOP          = 0x0208,
    BIT_ERR_PRODUCT_NOT_MATCH         = 0x020A,
    BIT_ERR_SERVER_KICKOUT            = 0x0212,
    BIT_ERR_SERVER_DENY               = 0x0213,
    BIT_ERR_OVER_USERS_IN_IP          = 0x0214,

    BIT_ERR_STRING_LENGH              = 0x0301,
    BIT_ERR_BADCOMMAND                = 0x0302,
    BIT_ERR_CMD_LENGTH                = 0x0303,

    BIT_ERR_FEATURE_ADDR              = 0x0501,
    BIT_ERR_FEATURE_ZERO              = 0x0502,
    BIT_ERR_FEATURE_NOT_FOUND         = 0x0503,
    BIT_ERR_FEATURE_TYPE_NOT_MATCH    = 0x0504,
    BIT_ERR_FEATURE_TYPE_PARAM        = 0x0505,
    BIT_ERR_FEATURE_EXPIRATION        = 0x0509,

    BIT_ERR_DATA_NOT_FOUND            = 0x0601,
    BIT_ERR_DATA_INDEX_OVER_LENGTH    = 0x0602,
    BIT_ERR_DATA_OVER_MAX_SIZE        = 0x0603,
    BIT_ERR_DATA_OVER_MAX_COUNT       = 0x0604,

    BIT_ERR_SN_LICENSE_EXPIRATION     = 0x0701,
    BIT_ERR_SN_USER_NUMBER_OVER       = 0x0702,
    BIT_ERR_SN_VOLUME_NUMBER_OVER     = 0x0703,
    BIT_ERR_SN_EXECUTION_COUNTER_OVER = 0x0704,
    BIT_ERR_SN_DISABLED               = 0x0705,
    BIT_ERR_SN_NOT_FOUND              = 0x0706,
    BIT_ERR_SN_OVER_MAX_SESSION_COUNT = 0x0707,
    BIT_ERR_SN_EXECUTE_MORE_COMMAND   = 0x0708,
    BIT_ERR_SN_DISABLED_FOR_ATTACK    = 0x0709,
    BIT_ERR_MISSING_USB               = 0x070D,
    BIT_ERR_SN_INVALID_FORMAT         = 0x070E,
    BIT_ERR_CAPACITY_EXHAUSTED        = 0x0712,
    BIT_ERR_CAPACITY_NOT_USED         = 0x0713,
    BIT_ERR_SN_TYPE_NOT_MATCH         = 0x0714,
    BIT_ERR_SN_OFFLINE_EXPIRED        = 0x0780,
    BIT_ERR_SN_MID_CHECK              = 0x0781,
    BIT_ERR_MACHINE_DISABLED          = 0x0782,
    BIT_ERR_NO_AVAILABLE_SN           = 0x0783,
    BIT_ERR_NO_AVAILABLE_MODULE       = 0x0784,
    BIT_ERR_DEMO_UPDATE               = 0x0785,
    BIT_ERR_VM_DISABLED               = 0x0786,
    BIT_ERR_SN_INVALID_STATUS         = 0x0787,

    BIT_ERR_OFF_LINE_ERR              = 0x0801,
    BIT_ERR_OFF_LINE                  = 0x0802,
    BIT_ERR_DOWNLOAD_OVER             = 0x0803,
    BIT_ERR_SIGNATURE                 = 0x0804,
    BIT_ERR_OFF_LINE_UPDATE_AGAIN     = 0x0806,
    BIT_ERR_WRONG_LOCAL_TIME          = 0x0807,
    BIT_ERR_NO_AVAILABLE_UPDATE       = 0x0808,
    BIT_ERR_LOCAL_SERVICE_UPDATE      = 0x0809,

    BIT_ERR_PRODUCT_NOT_FOUND         = 0x0901,
    BIT_ERR_DEVELOPER_NOT_FOUND       = 0x0902,
    BIT_ERR_INVALID_MID_FROMAT        = 0x0903,
    BIT_ERR_COMMAND_NOT_SUPPORT       = 0x0904,
    BIT_ERR_VERSION_NOT_SUPPORT       = 0x0905,
    BIT_ERR_BINDING_INFO              = 0x0906,
    BIT_ERR_CMD_DATA_LENGTH           = 0x0907,
    BIT_ERR_TABLE_SIGN                = 0x090A,
    BIT_ERR_NOT_INIT                  = 0x090B,

    /* Local service */
    BIT_ERR_BIT_SERVICER_SIGN         = 0x0A01,
    BIT_ERR_CONFIG_FILE               = 0x0A03,
    BIT_ERR_INVALID_USER              = 0x0A04,
    BIT_ERR_OLD_PASSWORD_WRONG        = 0x0A06,
    BIT_ERR_PASSWORD_NOT_MATCHED      = 0x0A07,
    BIT_ERR_UPLOAD_FAILURE            = 0x0A09,
    BIT_ERR_LOAD_EXTENTION            = 0x0A11,
    BIT_ERR_PAGE_NOT_FOUND            = 0x0A12,
    BIT_ERR_SN_EXISTED                = 0x0A13,
    BIT_ERR_OLD_EXTENTION             = 0x0A14,
    BIT_ERR_SRV_ROLE_INVALID          = 0x0A15,
    BIT_ERR_SRV_ROLE_EXIST            = 0x0A16
};

#define BIT_DATA_ITEM_NAME_MAX_LEN   128
#define BIT_DATA_ITEM_MAX_LEN        1024
#define BIT_CIPH_DATA_MAX_LEN        256


/**
 * @defgroup typedefs and macros
 *
 * @{
 */

/** A status code */
typedef enum BIT_ERROR_CODES BIT_STATUS;

/**
* @defgroup SESSION_TYPE of session information in XML or plain format
*
* @{
*/
typedef enum _SESSION_TYPE
{
    XML_TYPE_SN_INFO          = 3,
    XML_TYPE_FEATURE_INFO     = 4,
    BIT_SERVER_ADDRESS        = 0x101,
    BIT_SERVER_TIME           = 0x201,
    BIT_CONTROL_TYPE          = 0x302,
    BIT_VOLUME_NUMBER         = 0x303,
    BIT_START_DATE            = 0x304,
    BIT_END_DATE              = 0x305,
    BIT_EXPIRATION_DAYS       = 0x306,
    BIT_USAGE_NUMBER          = 0x307,
    BIT_CONSUMED_USAGE_NUMBER = 0x308,
    BIT_ACTIVATE_DATE         = 0x30A,
    BIT_USER_LIMIT            = 0x30B,
    BIT_LAST_UPDATE_DATE      = 0x30C,
    BIT_MAX_OFFLINE_MINUTES   = 0x30D,
    BIT_NEXT_CONNECT_DATE     = 0x30E
} SESSION_TYPE;

/**
 * @}
 */
  
/**
 * @defgroup The Basic Run-time API
 *
 * @{
 */
BIT_STATUS BIT_CALLCONV Bit_Login(
      BIT_PCSTR       szURL,
      BIT_PCSTR       szSN,
      BIT_UCHAR *     pApplicationData,
      BIT_HANDLE *    pHandle,
      LOGIN_MODE      mode);

BIT_STATUS BIT_CALLCONV Bit_LoginEx(
      BIT_PCSTR       szURL,
      BIT_PCSTR       szSN,
      BIT_UINT32      featureId,
      BIT_PCSTR       szReserved,
      BIT_UCHAR *     pApplicationData,
      BIT_HANDLE *    pHandle,
      LOGIN_MODE      mode);

BIT_STATUS BIT_CALLCONV Bit_Logout(
      BIT_HANDLE      handle); 

BIT_STATUS BIT_CALLCONV Bit_ReadFeature(
      BIT_HANDLE      handle,
      BIT_UINT32      featureId,
      BIT_UINT32 *    pFeatureValue);

BIT_STATUS BIT_CALLCONV Bit_WriteFeature(
      BIT_HANDLE      handle,
      BIT_UINT32      featureId,
      BIT_UINT32      featureValue);

BIT_STATUS BIT_CALLCONV Bit_ConvertFeature(
      BIT_HANDLE      handle,
      BIT_UINT32      featureId,
      BIT_UINT32      para1,
      BIT_UINT32      para2,
      BIT_UINT32      para3,
      BIT_UINT32      para4,
      BIT_UINT32 *    pResult);

BIT_STATUS BIT_CALLCONV Bit_EncryptFeature(
      BIT_HANDLE      handle,
      BIT_UINT32      featureId,
      BIT_VOID *      pPlainBuffer,
      BIT_VOID *      pCipherBuffer,
      BIT_UINT32      dataBufferSize);

BIT_STATUS BIT_CALLCONV Bit_DecryptFeature(
      BIT_HANDLE      handle,
      BIT_UINT32      featureId,
      BIT_VOID *      pCipherBuffer,
      BIT_VOID *      pPlainBuffer,
      BIT_UINT32      dataBufferSize);

BIT_STATUS BIT_CALLCONV Bit_QueryFeature(
     BIT_HANDLE       handle,
     BIT_UINT32       featureId,
     BIT_UINT32       *pCapacity);

BIT_STATUS BIT_CALLCONV Bit_ReleaseFeature(
     BIT_HANDLE       handle,
     BIT_UINT32       featureId,
     BIT_UINT32       *pCapacity);

BIT_STATUS BIT_CALLCONV Bit_SetDataItem(
      BIT_HANDLE      handle,
      BIT_PCSTR       szDataItemName,
      BIT_VOID *      pDataItemValue,
      BIT_UINT32      dataItemValueSize);

BIT_STATUS BIT_CALLCONV Bit_RemoveDataItem(
      BIT_HANDLE      handle,
      BIT_PCSTR       szDataItemName);

BIT_STATUS BIT_CALLCONV Bit_GetDataItem(
      BIT_HANDLE      handle,
      BIT_PCSTR       szDataItemName,
      BIT_VOID *      pDataItemValue,
      BIT_UINT32 *    pDataItemValueSize);

BIT_STATUS BIT_CALLCONV Bit_GetDataItemNum(
      BIT_HANDLE      handle,
      BIT_UINT32 *    pNum);

BIT_STATUS BIT_CALLCONV Bit_GetDataItemName(
      BIT_HANDLE      handle,
      BIT_UINT32      index,
      BIT_CHAR *      pDataItemName,
      BIT_UINT32 *    pDataItemNameSize);

BIT_STATUS BIT_CALLCONV Bit_GetFeatureInfo(
      BIT_HANDLE         handle,
      BIT_UINT32         featureId,
      BIT_FEATURE_INFO * pFeatureInfo);

BIT_STATUS BIT_CALLCONV Bit_GetSessionInfo(
      BIT_HANDLE      handle,
      SESSION_TYPE    type,
      BIT_CHAR *      pSessionInfo,
      BIT_UINT32 *    pSessionInfoSize);

BIT_STATUS BIT_CALLCONV Bit_GetRequestInfo(
      BIT_PCSTR       szSN,
      BIT_UCHAR *     pApplicationData,
      BINDING_TYPE    type,
      BIT_CHAR *      pRequestInfo,
      BIT_UINT32 *    pRrequestInfoSize);

BIT_STATUS BIT_CALLCONV Bit_Revoke(
      BIT_PCSTR       szURL,
      BIT_PCSTR       szSN,
      BIT_UCHAR *     pApplicationData,
      BIT_CHAR *      pRevocationInfo,
      BIT_UINT32 *    pRevocationInfoSize);

BIT_STATUS BIT_CALLCONV Bit_GetUpdateInfo(
      BIT_PCSTR       szURL,
      BIT_PCSTR       szSN,
      BIT_UCHAR *     pApplicationData,
      BIT_PCSTR       pRequestInfo,
      BIT_CHAR *      pUpdateInfo,
      BIT_UINT32 *    pUpdateInfoSize);

BIT_STATUS BIT_CALLCONV Bit_ApplyUpdateInfo(
      BIT_UCHAR *     pApplicationData,
      BIT_PCSTR       pUpdateInfo,
      BIT_CHAR *      pReceiptInfo,
      BIT_UINT32 *    pReceiptInfoSize);

BIT_STATUS BIT_CALLCONV Bit_UpdateOnline(
      BIT_PCSTR       szURL,
      BIT_PCSTR       szSN,
      BIT_UCHAR *     pApplicationData);

BIT_STATUS BIT_CALLCONV Bit_SetRootPath(
      BIT_PCSTR       szPath);

BIT_STATUS BIT_CALLCONV Bit_GetProductPath(
      BIT_UCHAR       *pApplicationData,
      BIT_CHAR *      buf,
      BIT_UINT32      lenBuf);

BIT_STATUS BIT_CALLCONV Bit_SetLocalServer(
      BIT_UCHAR       *pApplicationData,
      BIT_PCSTR       szHostName,
      BIT_UINT32      nPort,
      BIT_UINT32      nTimeoutSeconds);

BIT_STATUS BIT_CALLCONV Bit_SetProxy(
      BIT_UCHAR       *pApplicationData,
      BIT_PCSTR       szHostName,
      BIT_UINT32      nPort,
      BIT_PCSTR       szUserID,
      BIT_PCSTR       szPassword);

BIT_STATUS BIT_CALLCONV Bit_GetInfo(
      BIT_PCSTR       szSN,
      BIT_UCHAR       *pApplicationData,
      BIT_INFO_TYPE   type,
      BIT_CHAR        *pInfo,
      BIT_UINT32      *pInfoSize);

BIT_STATUS BIT_CALLCONV Bit_GetVersion(
      BIT_UINT32      *pVersion);

BIT_STATUS BIT_CALLCONV Bit_RemoveSn(
      BIT_PCSTR       szSN,
      BIT_UCHAR       * pApplicationData);

/**
 * @}
 */
#ifdef __cplusplus
} // extern "C"
#endif

#endif /* __BITANSWER_CLIENT_API_H__ */
