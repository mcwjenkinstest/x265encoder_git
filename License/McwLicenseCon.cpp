#include <string.h>
#include "stdio.h"
#include <time.h>
#include <atltime.h>
#include "shlobj.h"
#include <direct.h>
#include <QDebug>
#include "McwLicenseCon.h"

#if defined(_WIN32) || defined(WIN32)
typedef BIT_STATUS (*MyBitLoginCon)(BIT_PCSTR, BIT_PCSTR, BIT_UCHAR* , BIT_HANDLE*, LOGIN_MODE);
typedef BIT_STATUS (*MyBitLogoutCon)(BIT_HANDLE);
typedef BIT_STATUS (*MyBitReadFeatureCon)(BIT_HANDLE, BIT_UINT32, BIT_UINT32 *);
typedef BIT_STATUS (*MyBitConvertFeatureCon)(BIT_HANDLE, BIT_UINT32, BIT_UINT32, BIT_UINT32, BIT_UINT32, BIT_UINT32, BIT_UINT32 *);
typedef BIT_STATUS (*MyBitQueryFeatureCon)(BIT_HANDLE, BIT_UINT32, BIT_UINT32*);
typedef BIT_STATUS (*MyBitReleaseFeatureCon)(BIT_HANDLE,BIT_UINT32,BIT_UINT32*);
typedef BIT_STATUS (*MyBitGetDataItemCon)(BIT_HANDLE,BIT_PCSTR ,BIT_VOID *,BIT_UINT32 *);
typedef BIT_STATUS (*MyBitGetDataItemNameCon)(BIT_HANDLE, BIT_UINT32, BIT_CHAR *, BIT_UINT32 *);
typedef BIT_STATUS (*MyBitUpdateOnlineCon)(BIT_PCSTR, BIT_PCSTR,BIT_UCHAR *);
typedef BIT_STATUS (*MyBitRemoveSnCon)(BIT_PCSTR,BIT_UCHAR*);
typedef BIT_STATUS (*MyBitGetSessionInfoCon)(BIT_HANDLE, SESSION_TYPE, BIT_CHAR * , BIT_UINT32 * );

QLibrary conLib("XCFT389IHBM67C");
MyBitLoginCon MyBit_LoginCon = (MyBitLoginCon) conLib.resolve("Bit_Login");
MyBitLogoutCon MyBit_LogoutCon = (MyBitLogoutCon) conLib.resolve("Bit_Logout");
MyBitReadFeatureCon MyBit_ReadFeatureCon = (MyBitReadFeatureCon) conLib.resolve("Bit_ReadFeature");
MyBitConvertFeatureCon MyBit_ConvertFeatureCon = (MyBitConvertFeatureCon) conLib.resolve("Bit_ConvertFeature");
MyBitQueryFeatureCon MyBit_QueryFeatureCon = (MyBitQueryFeatureCon) conLib.resolve("Bit_QueryFeature");
MyBitReleaseFeatureCon MyBit_ReleaseFeatureCon = (MyBitReleaseFeatureCon) conLib.resolve("Bit_ReleaseFeature");
MyBitGetDataItemCon MyBit_GetDataItemCon = (MyBitGetDataItemCon) conLib.resolve("Bit_GetDataItem");
MyBitGetDataItemNameCon MyBit_GetDataItemNameCon = (MyBitGetDataItemNameCon) conLib.resolve("Bit_GetDataItemName");
MyBitUpdateOnlineCon MyBit_UpdateOnlineCon = (MyBitUpdateOnlineCon) conLib.resolve("Bit_UpdateOnline");
MyBitRemoveSnCon MyBit_RemoveSnCon = (MyBitRemoveSnCon) conLib.resolve("Bit_RemoveSn");
MyBitGetSessionInfoCon MyBit_GetSessionInfoCon = (MyBitGetSessionInfoCon) conLib.resolve("Bit_GetSessionInfo");
#endif

BIT_UINT32 gFeatureAlgorithmValue[16][5] =
{
    0, 0, 0, 0,   0xa96bc963,
    0, 0, 0, 1,   0x45fb03a9,
    0, 0, 1, 0,   0xbf360cdf,
    0, 0, 1, 1,   0xc598b604,
    0, 1, 0, 0,   0xe1f75ce3,
    0, 1, 0, 1,   0x61d1210f,
    0, 1, 1, 0,   0xea1da13b,
    0, 1, 1, 1,   0x8091c403,
    1, 0, 0, 0,   0xc374fdd,
    1, 0, 0, 1,   0x697bc33d,
    1, 0, 1, 0,   0x22bc483f,
    1, 0, 1, 1,   0x6bccb8f3,
    1, 1, 0, 0,   0x5fa3dcdb,
    1, 1, 0, 1,   0x2316938a,
    1, 1, 1, 0,   0x918441a0,
    1, 1, 1, 1,   0xe9515638
};

//BIT_UINT32     dwfeatureDataPos = 0;
//BIT_UINT32     i, DataItemNum      = 0;


//BIT_UCHAR      plainbuffer[256] = { 0 };
//BIT_UCHAR      cipherbuffer[256] = { 0 };
//BIT_CHAR       requestInfo[REQUEST_INFO_SIZE];
//BIT_CHAR       updateInfo[UPDATE_INFO_SIZE];
//BIT_CHAR       receiptInfo[RECEIPT_INFO_SIZE];//
//BIT_UINT32     requestInfoSize = REQUEST_INFO_SIZE;
//BIT_UINT32     updateInfoSize = UPDATE_INFO_SIZE;
//BIT_UINT32     receiptInfoSize = RECEIPT_INFO_SIZE;

time_t FormatTime(char* szTime)
{
    struct tm tm1;

    time_t time1;

    sscanf_s(szTime, "%4d-%2d-%2d %2d:%2d:%2d",
        &tm1.tm_year,
        &tm1.tm_mon,
        &tm1.tm_mday,
        &tm1.tm_hour,
        &tm1.tm_min,
        &tm1.tm_sec);

    tm1.tm_year -= 1900;
    tm1.tm_mon --;
    tm1.tm_isdst = 0;

    time1 = mktime(&tm1);

    return time1;
}


void DumpError(BIT_PCSTR d_data, size_t len)
{
    static int t = 0;
    static int line = 0;

    char out_name[256];
    sprintf_s(out_name, "dump_%d_Dec.txt", t);    //save to current path

    FILE *hFile = NULL;
    errno_t err;
    if( (err  = fopen_s( &hFile, out_name, "a" )) !=0 )
    {
        printf("Could not create dump file\n");
    }

    fwrite("\r\n", sizeof("\r\n")+1, 1, hFile);
    fseek(hFile, 0, SEEK_END);

    int count = (int)fwrite(d_data, len, 1, hFile);
    fclose(hFile);

    ++line;
    if(line > 1000)
    {
        line = 0;
        ++t;
    }
    //t = t + 1;
}


CLicenseManager::CLicenseManager()
{
	m_Result     = BIT_SUCCESS;
	m_Handle     = NULL;
    m_MembufferLen = DEMO_MEMBUFFER_SIZE;

	memset(m_Sn, 0, SN_BUFFER_LEN);
	memset(m_Membuffer, 0, DEMO_MEMBUFFER_SIZE);
	memset(m_DataItemName, 0, DEMO_MEMBUFFER_SIZE);

	m_DataLen = 0;
}

CLicenseManager::~CLicenseManager()
{
	UserReleaseFeature();
	Logout();
    conLib.unload();
}

BIT_UCHAR CLicenseManager::application_data[191]= {
    0x40,0x80,0xbc,0x8b,0xc5,0x17,0x01,0x8e,0x6c,0x9f,0xd5,0x3a,0x1d,0x01,0x47,0x10,
    0xf8,0xa6,0xc1,0x35,0x24,0x4d,0x26,0x95,0x71,0x13,0x2a,0xd7,0xac,0x98,0xd2,0x20,
    0x3c,0xfa,0xaa,0xa1,0xf0,0x47,0x4e,0xcc,0x8f,0xf8,0xbb,0xd1,0x1a,0x6d,0xa6,0xc4,
    0x18,0xce,0xcf,0xb3,0x63,0x0d,0xf8,0xe6,0x8c,0xfd,0x04,0x51,0x21,0x98,0x62,0xc0,
    0xfb,0x5f,0x87,0xea,0x6f,0x09,0x97,0x62,0xe1,0x3b,0x35,0xc3,0xf1,0x4d,0x60,0x23,
    0x01,0x93,0x15,0xb4,0x65,0xb2,0x1e,0x29,0x96,0xec,0xd4,0x1e,0xf8,0x70,0x05,0x25,
    0x15,0xc0,0xe1,0x28,0x94,0x33,0xf5,0xe2,0xe0,0xd8,0xe2,0x3c,0x20,0xca,0x46,0x5b,
    0x2d,0x00,0xb7,0xc1,0x08,0xc1,0x49,0x75,0xfe,0x67,0x1b,0x2a,0xca,0x92,0xb9,0x76,
    0x9d,0x79,0x7e,0x9e,0x82,0x1f,0xb4,0x8c,0x81,0x2b,0xe5,0xa1,0xf6,0x7e,0xc2,0x44,
    0x55,0xf1,0xc1,0x2e,0x8a,0x6a,0x2d,0xf6,0x4b,0x91,0x9a,0xd7,0x50,0xa9,0x20,0xe5,
    0x78,0x5c,0xac,0x89,0x7f,0xb3,0x88,0x6f,0xdd,0x90,0xaa,0x9e,0xdd,0x7b,0x4d,0x9c,
    0xc4,0x79,0x6a,0x5e,0xa0,0x77,0x34,0x16,0xfc,0xac,0x27,0x7a,0xea,0xca,0xcf
};

//Read Data item
BIT_STATUS CLicenseManager::ReadDataItem(BIT_UINT32 nItemId)
{
	memset(m_DataItemName, 0, sizeof(m_DataItemName));
	memset(m_Membuffer, 0, sizeof(m_Membuffer));
    //m_Result = Bit_GetDataItemName(m_Handle, nItemId, m_DataItemName, &m_DataLen);
    if(MyBit_GetDataItemNameCon)
        m_Result = MyBit_GetDataItemNameCon(m_Handle, nItemId, m_DataItemName, &m_DataLen);
	if(m_Result)
	{
		return m_Result;
	}

	m_MembufferLen = DEMO_MEMBUFFER_SIZE;
    //m_Result = Bit_GetDataItem(m_Handle, m_DataItemName, m_Membuffer, &m_MembufferLen);
    if(MyBit_GetDataItemCon)
        m_Result = MyBit_GetDataItemCon(m_Handle, m_DataItemName, m_Membuffer, &m_MembufferLen);
	if(m_Result)
	{
		return m_Result;
	}

	return m_Result;
}

//Read feature item
BIT_STATUS CLicenseManager::ReadFeature(BIT_UINT32 nFeatureId)
{
    // read Feature
    //m_Result = Bit_ReadFeature(m_Handle, nFeatureId, &m_FeatureValue);
    if(MyBit_ReadFeatureCon)
        m_Result = MyBit_ReadFeatureCon(m_Handle, nFeatureId, &m_FeatureValue);
    return m_Result;
}


//Verify Feature item
BIT_INT32 CLicenseManager::VerifyFeatureValue(BIT_UINT32 val)
{
	if(m_FeatureValue == val)
	{
		return HEVC_NO_ERROR;
	}
	else
	{
		return HEVC_ERR_FEATURE_VALUE;
	}
}

//convert feature item
BIT_STATUS CLicenseManager::ConvertFeature(
	BIT_UINT32 nFeatureId, 
	BIT_UINT32 para1,
	BIT_UINT32 para2,
	BIT_UINT32 para3,
	BIT_UINT32 para4
	)
{
	// convert Feature
//	m_Result = Bit_ConvertFeature(
//		m_Handle,
//		nFeatureId,
//		para1,
//		para2,
//		para3,
//		para4,
//		&m_ConvertResult);
    if(MyBit_ConvertFeatureCon)
        m_Result = MyBit_ConvertFeatureCon(
            m_Handle,
            nFeatureId,
            para1,
            para2,
            para3,
            para4,
            &m_ConvertResult);

	return m_Result;
}


BOOL CLicenseManager::Logout(VOID)
{
	// log out
	if(NULL == m_Handle)
	{
		return FALSE;
	}
	else
	{
        //m_Result = Bit_Logout(m_Handle);
        if(MyBit_LogoutCon)
            m_Result = MyBit_LogoutCon(m_Handle);
	}
	
	if(m_Result)
	{
		return HEVC_ERR_LOGOUT;
	}
	else
	{
		return HEVC_NO_ERROR;
	}
}

//====================================================


//Get Local license status
BIT_STATUS CLicenseManager::UserGetLicenseStatus()
{
    //BOOL hr = FALSE;

    // log in
    //m_Result = Bit_Login(NULL, NULL, application_data, &m_Handle, BIT_MODE_AUTO);
    if (MyBit_LoginCon)
        m_Result = MyBit_LoginCon(NULL, NULL, application_data, &m_Handle, BIT_MODE_AUTO);
	return m_Result;
}

//Set SN
BOOL CLicenseManager::UserSetSn(BIT_PCSTR sn)
{
	UINT32 len = strlen((const char*)sn);
	if(0 == len)
	{
		return FALSE;
	}

	if(len <= SN_BUFFER_LEN)
	{
		strncpy_s(m_Sn, sn, len * sizeof(BIT_CHAR));
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

//Apply for License
BIT_STATUS CLicenseManager::UserApplyForLicense()
{
	if(0 == strlen(m_Sn))
	{
		//SN is invalid.
		return BIT_ERR_SN_INVALID; 
	}

	// log in
    //m_Result = Bit_Login(NULL, m_Sn, application_data, &m_Handle, BIT_MODE_AUTO);

    if (MyBit_LoginCon)
        m_Result = MyBit_LoginCon(NULL, m_Sn, application_data, &m_Handle, BIT_MODE_AUTO);
	return m_Result;
}

// Verify Feature value by converting
BIT_INT32 CLicenseManager::UserVerifyFeature(BIT_UINT32 nFeatureId, BIT_UINT32 val)
{
    m_Result = ReadFeature(nFeatureId);
	if(BIT_SUCCESS != m_Result)
	{
		return HEVC_ERR_READ_FEATURE;
	}

	if(!VerifyFeatureValue(val))
	{
		//right
		return HEVC_NO_ERROR;
	}
	else
	{
		return HEVC_ERR_FEATURE_VALUE;
	}
}

// Verify Feature value by converting
BIT_INT32 CLicenseManager::UserVerifyAlgorithmFeature(
	BIT_UINT32 nFeatureId,
	BIT_UINT32 index /* it should less than 16 for array size */
	)
{
	if(index > 16)
	{
		return HEVC_ERR_CONVERT_FEATURE;
	}

	BIT_UINT32 para1 = gFeatureAlgorithmValue[index][0];
	BIT_UINT32 para2 = gFeatureAlgorithmValue[index][1];
	BIT_UINT32 para3 = gFeatureAlgorithmValue[index][2];
	BIT_UINT32 para4 = gFeatureAlgorithmValue[index][3];

	// convert Feature
//	m_Result = Bit_ConvertFeature(
//		m_Handle,
//		nFeatureId,
//		para1,
//		para2,
//		para3,
//		para4,
//		&m_ConvertResult);
    if(MyBit_ConvertFeatureCon)
        m_Result = MyBit_ConvertFeatureCon(
            m_Handle,
            nFeatureId,
            para1,
            para2,
            para3,
            para4,
            &m_ConvertResult);

	if(m_Result)
	{
		return HEVC_ERR_CONVERT_FEATURE;
	}
	else
	{
		if(gFeatureAlgorithmValue[index][4] != m_ConvertResult)
		{
			return HEVC_ERR_FEATURE_VALUE;
		}

		return HEVC_NO_ERROR;
	}
}

BIT_STATUS CLicenseManager::UserQueryFeature(void)
{
	BIT_UINT32 *pCapacity = NULL;

//	m_Result = Bit_QueryFeature(
//		m_Handle,
//		3,
//		pCapacity
//		);
    if(MyBit_QueryFeatureCon)
        m_Result = MyBit_QueryFeatureCon(
            m_Handle,
            3,
            pCapacity
            );
	return m_Result;
}

BIT_STATUS CLicenseManager::UserReleaseFeature(void)
{
	BIT_UINT32 *pCapacity = NULL;
//	m_Result = Bit_ReleaseFeature(
//		m_Handle,
//		3,
//		pCapacity
//		);
    if(MyBit_ReleaseFeatureCon)
        m_Result = MyBit_ReleaseFeatureCon(
            m_Handle,
            3,
            pCapacity
            );
	return m_Result;
}

BIT_STATUS CLicenseManager::UpdateLicense(BIT_PCSTR szSN)
{
	// update license
    //m_Result = Bit_UpdateOnline(NULL, szSN, application_data);
    if(MyBit_UpdateOnlineCon)
        m_Result = MyBit_UpdateOnlineCon(NULL, szSN, application_data);
	return m_Result;
}


// Delete local SN for test or problem.
BIT_STATUS CLicenseManager::UserRemoveSN(BIT_PCSTR szSN)
{
    //m_Result = Bit_RemoveSn(szSN, application_data);
    if(MyBit_RemoveSnCon)
        m_Result = MyBit_RemoveSnCon(szSN, application_data);
	return m_Result;
}

BIT_STATUS CLicenseManager::GetStatus(void)
{
	return m_Result;
}

void CLicenseManager::DumpStatus(void)
{
	char pDestStr[SN_BUFFER_LEN];
	itoa(m_Result, pDestStr, 10);
	//DumpError(pDestStr, strlen(pDestStr));	//if our software is not registered in microsoft,we can't create dump. windows will think it's un safe.
}

BIT_INT32 CLicenseManager::UserGetRemainDays(void)
{
    memset(m_Membuffer, 0, sizeof(m_Membuffer));
    m_MembufferLen = DEMO_MEMBUFFER_SIZE;
    int m_ExpirationDays;
    if(MyBit_GetSessionInfoCon)
        m_Result = MyBit_GetSessionInfoCon(m_Handle, BIT_END_DATE, m_Membuffer, &m_MembufferLen);
    if(BIT_SUCCESS == m_Result)
    {
        if('U' == m_Membuffer[0])
        {
            m_ExpirationDays = 9999;
        }
        else
        {
            time_t endTime = FormatTime(m_Membuffer);
            time_t now = time(NULL);
            double time = difftime(endTime, now);

            m_ExpirationDays = time / (60*60*24) + 0.99;
        }
    }

    return m_ExpirationDays;
}

//Get current License Type
BIT_INT32 CLicenseManager::UserGetLicenseType(void)
{
    m_nLicenseType = 0;

    memset(m_Membuffer, 0, sizeof(m_Membuffer));
    m_MembufferLen = DEMO_MEMBUFFER_SIZE;
    if(MyBit_GetSessionInfoCon)
           m_Result = MyBit_GetSessionInfoCon(m_Handle, BIT_CONTROL_TYPE, m_Membuffer, &m_MembufferLen);

    if(BIT_SUCCESS == m_Result)
    {
        if(strncmp(m_Membuffer + 7, "Smart", strlen("Smart")) == 0)
        {
            m_nLicenseType = 1;
        }

        if(strncmp(m_Membuffer + 7, "Group", strlen("Group")) == 0)
        {
            m_nLicenseType = 2;
        }

        if(strncmp(m_Membuffer + 7, "Demo", strlen("Demo")) == 0)
        {
            m_nLicenseType = 3;
        }
    }

    return m_nLicenseType;
}
//Check wether the folder is exists.If not, create it.
//0 : Folder exists.
//-1: Failed to create folder.
//1 : Success to create folder.
int CheckDir(char* Dir)
{
    FILE *fp = NULL;
    char TempDir[200];
    memset(TempDir,'\0',sizeof(TempDir));
    sprintf(TempDir,Dir);
    strcat(TempDir,"\\");
    strcat(TempDir,".temp.fortest");
    fp = fopen(TempDir,"w");
    if (!fp)
    {
        if(_mkdir(Dir)==0)
        {
            return 1;	//success to create folder;
        }
        else
        {
            return -1;	//can not make a dir;
        }
    }
    else
    {
        fclose(fp);
    }
    return 0;
}

//return TRUE : It's the first time today.
//       FALSE: It's not the first time today.
BOOL CLicenseManager::isOnceToday(void)
{
    BOOL isOnce = TRUE;

    //Get AppData path
    TCHAR   szPath[MAX_PATH];
    SHGetSpecialFolderPath(NULL,szPath,CSIDL_APPDATA,FALSE);

    //Make MCW path
    TCHAR szMcwFolder[MAX_PATH] = L"\\MCW";
    lstrcat(szPath, szMcwFolder);

    char szString[MAX_PATH];
    DWORD dwNum = WideCharToMultiByte(CP_OEMCP, NULL, szPath, -1, NULL, 0, NULL, FALSE);
    WideCharToMultiByte(CP_OEMCP, NULL, szPath, -1, szString, dwNum, NULL, FALSE);


    CTime t = CTime::GetCurrentTime();	//Get system time
    int day = t.GetDay();				//Get day
    int dayRecord = 0;


    int info = CheckDir(szString);

    errno_t err;
    FILE *fp = NULL;
    strcat_s(szString, "\\x265 HEVC Upgrade.txt");


    switch(info)
    {
    case 0://open
    case 1://create

        if( (err  = fopen_s( &fp, szString, "a+" )) !=0 )
        {
            //printf("Could not create file\n");
            //exit(-1);
            return TRUE;
        }

        if(0 == fread(&dayRecord, sizeof(int), 1, fp))
        {//no record
            //fwrite(&day, sizeof(int), 2, fp); /* write struct s to file */
        }

        //compare
        if(day != dayRecord)
        {
            fclose(fp);

            isOnce = TRUE;

            //Record today
            if((err  = fopen_s( &fp, szString, "w")) !=0 )
            {
                printf("Could not create file\n");
                return TRUE;
            }

            fwrite(&day, sizeof(int), 1, fp); /* write struct s to file */
        }
        else
        {
            isOnce = FALSE;
        }

        fclose(fp);
        break;

    case -1:
        isOnce = TRUE;
        break;

    default:
        isOnce = TRUE;
        break;
    }

    return isOnce;
}
