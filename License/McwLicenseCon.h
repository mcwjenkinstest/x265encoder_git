#pragma once
#include <windows.h>
#include "Bitanswer.h"
#include "Utils.h"

class CLicenseManager
{
public:
	CLicenseManager();
	~CLicenseManager();

public:
	BIT_STATUS UserGetLicenseStatus();
	BOOL UserSetSn(BIT_PCSTR sn);
	BIT_STATUS UserApplyForLicense();
	BIT_INT32 UserVerifyFeature(BIT_UINT32 nFeatureId, BIT_UINT32 val);
	BIT_INT32 UserVerifyAlgorithmFeature(BIT_UINT32 nFeatureId, BIT_UINT32 index);
	BIT_STATUS UserQueryFeature(void);
	BIT_STATUS UserReleaseFeature(void);
	BIT_STATUS UpdateLicense(BIT_PCSTR szSN);
	BIT_STATUS UserRemoveSN(BIT_PCSTR szSN);
    BIT_INT32  UserGetRemainDays(void);
    BIT_INT32 UserGetLicenseType(void);
	BIT_STATUS GetStatus(void);
	void DumpStatus(void);
	BOOL Logout(VOID);
    BOOL isOnceToday();

protected:
	BIT_STATUS ReadDataItem(BIT_UINT32 nItemId);
	BIT_STATUS ReadFeature(BIT_UINT32 nFeatureId);
	BIT_INT32 VerifyFeatureValue(BIT_UINT32 val);
	BIT_STATUS ConvertFeature(
		BIT_UINT32 nFeatureId, 
		BIT_UINT32 para1,
		BIT_UINT32 para2,
		BIT_UINT32 para3,
		BIT_UINT32 para4);
	
private:
    static BIT_UCHAR application_data[191];
	BIT_CHAR       m_Sn[SN_BUFFER_LEN];
	BIT_STATUS     m_Result;
	BIT_HANDLE     m_Handle;
	BIT_UINT32     m_FeatureValue;
	BIT_UINT32     m_ConvertResult;
	BIT_UINT32     m_MembufferLen;
	BIT_CHAR       m_Membuffer[DEMO_MEMBUFFER_SIZE];
	BIT_CHAR       m_DataItemName[BIT_DATA_ITEM_NAME_MAX_LEN];
	BIT_UINT32     m_DataLen;
    BIT_UINT32     m_nLicenseType;
    BIT_UINT32     m_ExpirationDays;
};
