#include <string.h>
#include "stdio.h"
#include "McwLicensePro.h"


#if defined(_WIN32) || defined(WIN32)
typedef BIT_STATUS (*MyBitLoginPro)(BIT_PCSTR, BIT_PCSTR, BIT_UCHAR* , BIT_HANDLE*, LOGIN_MODE);
typedef BIT_STATUS (*MyBitLogoutPro)(BIT_HANDLE);
typedef BIT_STATUS (*MyBitReadFeaturePro)(BIT_HANDLE, BIT_UINT32, BIT_UINT32 *);
typedef BIT_STATUS (*MyBitConvertFeaturePro)(BIT_HANDLE, BIT_UINT32, BIT_UINT32, BIT_UINT32, BIT_UINT32, BIT_UINT32, BIT_UINT32 *);
typedef BIT_STATUS (*MyBitQueryFeaturePro)(BIT_HANDLE, BIT_UINT32, BIT_UINT32*);
typedef BIT_STATUS (*MyBitReleaseFeaturePro)(BIT_HANDLE,BIT_UINT32,BIT_UINT32*);
typedef BIT_STATUS (*MyBitGetDataItemPro)(BIT_HANDLE,BIT_PCSTR ,BIT_VOID *,BIT_UINT32 *);
typedef BIT_STATUS (*MyBitGetDataItemNamePro)(BIT_HANDLE, BIT_UINT32, BIT_CHAR *, BIT_UINT32 *);
typedef BIT_STATUS (*MyBitUpdateOnlinePro)(BIT_PCSTR, BIT_PCSTR,BIT_UCHAR *);
typedef BIT_STATUS (*MyBitRemoveSnPro)(BIT_PCSTR,BIT_UCHAR*);

QLibrary proLib("FTYU389L654GYP");
MyBitLoginPro MyBit_LoginPro = (MyBitLoginPro) proLib.resolve("Bit_Login");
MyBitLogoutPro MyBit_LogoutPro = (MyBitLogoutPro) proLib.resolve("Bit_Logout");
MyBitReadFeaturePro MyBit_ReadFeaturePro = (MyBitReadFeaturePro) proLib.resolve("Bit_ReadFeature");
MyBitConvertFeaturePro MyBit_ConvertFeaturePro = (MyBitConvertFeaturePro) proLib.resolve("Bit_ConvertFeature");
MyBitQueryFeaturePro MyBit_QueryFeaturePro = (MyBitQueryFeaturePro) proLib.resolve("Bit_QueryFeature");
MyBitReleaseFeaturePro MyBit_ReleaseFeaturePro = (MyBitReleaseFeaturePro) proLib.resolve("Bit_ReleaseFeature");
MyBitGetDataItemPro MyBit_GetDataItemPro = (MyBitGetDataItemPro) proLib.resolve("Bit_GetDataItem");
MyBitGetDataItemNamePro MyBit_GetDataItemNamePro = (MyBitGetDataItemNamePro) proLib.resolve("Bit_GetDataItemName");
MyBitUpdateOnlinePro MyBit_UpdateOnlinePro = (MyBitUpdateOnlinePro) proLib.resolve("Bit_UpdateOnline");
MyBitRemoveSnPro MyBit_RemoveSnPro = (MyBitRemoveSnPro) proLib.resolve("Bit_RemoveSn");
#endif

BIT_UINT32 gFeatureAlgorithmValuePro[16][5] =
{
    0, 0, 0, 0,   0xa96bc963,
    0, 0, 0, 1,   0x45fb03a9,
    0, 0, 1, 0,   0xbf360cdf,
    0, 0, 1, 1,   0xc598b604,
    0, 1, 0, 0,   0xe1f75ce3,
    0, 1, 0, 1,   0x61d1210f,
    0, 1, 1, 0,   0xea1da13b,
    0, 1, 1, 1,   0x8091c403,
    1, 0, 0, 0,   0xc374fdd,
    1, 0, 0, 1,   0x697bc33d,
    1, 0, 1, 0,   0x22bc483f,
    1, 0, 1, 1,   0x6bccb8f3,
    1, 1, 0, 0,   0x5fa3dcdb,
    1, 1, 0, 1,   0x2316938a,
    1, 1, 1, 0,   0x918441a0,
    1, 1, 1, 1,   0xe9515638
};

//BIT_UINT32     dwfeatureDataPos = 0;
//BIT_UINT32     i, DataItemNum      = 0;


//BIT_UCHAR      plainbuffer[256] = { 0 };
//BIT_UCHAR      cipherbuffer[256] = { 0 };
//BIT_CHAR       requestInfo[REQUEST_INFO_SIZE];
//BIT_CHAR       updateInfo[UPDATE_INFO_SIZE];
//BIT_CHAR       receiptInfo[RECEIPT_INFO_SIZE];//
//BIT_UINT32     requestInfoSize = REQUEST_INFO_SIZE;
//BIT_UINT32     updateInfoSize = UPDATE_INFO_SIZE;
//BIT_UINT32     receiptInfoSize = RECEIPT_INFO_SIZE;


void DumpErrorPro(BIT_PCSTR d_data, size_t len)
{
    static int t = 0;
    static int line = 0;

    char out_name[256];
    sprintf_s(out_name, "dump_%d_Dec.txt", t);    //save to current path

    FILE *hFile = NULL;
    errno_t err;
    if( (err  = fopen_s( &hFile, out_name, "a" )) !=0 )
    {
        printf("Could not create dump file\n");
    }

    fwrite("\r\n", sizeof("\r\n")+1, 1, hFile);
    fseek(hFile, 0, SEEK_END);

    int count = (int)fwrite(d_data, len, 1, hFile);
    fclose(hFile);

    ++line;
    if(line > 1000)
    {
        line = 0;
        ++t;
    }
    //t = t + 1;
}


CLicenseManagerPro::CLicenseManagerPro()
{
	m_Result     = BIT_SUCCESS;
	m_Handle     = NULL;
	m_MembufferLen = DEMO_MEMBUFFER_SIZE;

	memset(m_Sn, 0, SN_BUFFER_LEN);
	memset(m_Membuffer, 0, DEMO_MEMBUFFER_SIZE);
	memset(m_DataItemName, 0, DEMO_MEMBUFFER_SIZE);

	m_DataLen = 0;
}

CLicenseManagerPro::~CLicenseManagerPro()
{
	UserReleaseFeature();
	Logout();
    proLib.unload();
}

BIT_UCHAR CLicenseManagerPro::application_data[] = {
    0x40,0x80,0xbc,0x96,0x7d,0x9f,0x80,0xfd,0x4a,0xff,0x14,0x18,0x74,0x9a,0xb3,0xa9,
    0x66,0xac,0xa9,0xa9,0x9b,0x2d,0x0f,0x64,0xd9,0x3f,0x51,0x04,0xc8,0x66,0x4f,0x5d,
    0xb4,0xf8,0xd2,0x51,0xc8,0xc8,0x9b,0x72,0x45,0x89,0x99,0x98,0xdb,0xd5,0x37,0xd8,
    0x4c,0x71,0x6c,0x30,0xec,0xd0,0xb2,0xb2,0xbe,0x49,0xc0,0x22,0x2e,0x0c,0x2f,0x4d,
    0x6a,0x3e,0xe0,0xa5,0xbf,0xa8,0x2a,0x2c,0x3c,0x0b,0xc9,0x19,0x9f,0xf6,0x51,0xcc,
    0xf0,0x46,0xf2,0xa2,0x8d,0x9c,0x39,0x12,0x8a,0x02,0x21,0xff,0x8e,0xc1,0xb7,0xb7,
    0xfe,0xad,0xf5,0x35,0x4e,0x18,0x10,0xf9,0xba,0x22,0x04,0xb4,0x63,0x39,0x5b,0xa5,
    0xb9,0x0f,0xf8,0xa9,0x8d,0x6b,0x66,0x49,0x45,0x6c,0x12,0x0f,0xcd,0xb6,0xaf,0x46,
    0xe4,0x05,0x0a,0x94,0xc2,0x0c,0xc7,0x99,0xbf,0x44,0x31,0x96,0xad,0xd4,0x3e,0xaa,
    0xaf,0x4c,0xa1,0x1f,0x10,0x82,0x3a,0x06,0x4e,0x10,0xaf,0xb1,0x55,0x3b,0x7f,0x15,
    0x72,0x26,0xaf,0x55,0xd4,0x9e,0x53,0x85,0x40,0x55,0x50,0x0e,0x94,0x1c,0x06,0x0d,
    0xac,0xde,0xa1,0xde,0x61,0x7a,0x8f,0x35,0xa4,0xf9,0x9f,0xe1,0x8c,0x36,0xf2
};

//Read Data item
BIT_STATUS CLicenseManagerPro::ReadDataItem(BIT_UINT32 nItemId)
{
	memset(m_DataItemName, 0, sizeof(m_DataItemName));
	memset(m_Membuffer, 0, sizeof(m_Membuffer));
    //m_Result = Bit_GetDataItemName(m_Handle, nItemId, m_DataItemName, &m_DataLen);
    if(MyBit_GetDataItemNamePro)
        m_Result = MyBit_GetDataItemNamePro(m_Handle, nItemId, m_DataItemName, &m_DataLen);
	if(m_Result)
	{
		return m_Result;
	}

	m_MembufferLen = DEMO_MEMBUFFER_SIZE;
    //m_Result = Bit_GetDataItem(m_Handle, m_DataItemName, m_Membuffer, &m_MembufferLen);
    if(MyBit_GetDataItemPro)
        m_Result = MyBit_GetDataItemPro(m_Handle, m_DataItemName, m_Membuffer, &m_MembufferLen);
	if(m_Result)
	{
		return m_Result;
	}

	return m_Result;
}

//Read feature item
BIT_STATUS CLicenseManagerPro::ReadFeature(BIT_UINT32 nFeatureId)
{
    // read Feature
    //m_Result = Bit_ReadFeature(m_Handle, nFeatureId, &m_FeatureValue);
    if(MyBit_ReadFeaturePro)
        m_Result = MyBit_ReadFeaturePro(m_Handle, nFeatureId, &m_FeatureValue);
    return m_Result;
}


//Verify Feature item
BIT_INT32 CLicenseManagerPro::VerifyFeatureValue(BIT_UINT32 val)
{
	if(m_FeatureValue == val)
	{
		return HEVC_NO_ERROR;
	}
	else
	{
		return HEVC_ERR_FEATURE_VALUE;
	}
}

//convert feature item
BIT_STATUS CLicenseManagerPro::ConvertFeature(
	BIT_UINT32 nFeatureId, 
	BIT_UINT32 para1,
	BIT_UINT32 para2,
	BIT_UINT32 para3,
	BIT_UINT32 para4
	)
{
	// convert Feature
//	m_Result = Bit_ConvertFeature(
//		m_Handle,
//		nFeatureId,
//		para1,
//		para2,
//		para3,
//		para4,
//		&m_ConvertResult);
    if(MyBit_ConvertFeaturePro)
        m_Result = MyBit_ConvertFeaturePro(
            m_Handle,
            nFeatureId,
            para1,
            para2,
            para3,
            para4,
            &m_ConvertResult);

	return m_Result;
}


BOOL CLicenseManagerPro::Logout(VOID)
{
	// log out
	if(NULL == m_Handle)
	{
		return FALSE;
	}
	else
	{
        //m_Result = Bit_Logout(m_Handle);
        if(MyBit_LogoutPro)
            m_Result = MyBit_LogoutPro(m_Handle);
	}
	
	if(m_Result)
	{
		return HEVC_ERR_LOGOUT;
	}
	else
	{
		return HEVC_NO_ERROR;
	}
}

//====================================================


//Get Local license status
BIT_STATUS CLicenseManagerPro::UserGetLicenseStatus()
{
    //BOOL hr = FALSE;

	// log in
    //m_Result = Bit_Login(NULL, NULL, application_data, &m_Handle, BIT_MODE_AUTO);
    if (MyBit_LoginPro)
        m_Result = MyBit_LoginPro(NULL, NULL, application_data, &m_Handle, BIT_MODE_AUTO);
	return m_Result;
}

//Set SN
BOOL CLicenseManagerPro::UserSetSn(BIT_PCSTR sn)
{
	UINT32 len = strlen((const char*)sn);
	if(0 == len)
	{
		return FALSE;
	}

	if(len <= SN_BUFFER_LEN)
	{
		strncpy_s(m_Sn, sn, len * sizeof(BIT_CHAR));
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

//Apply for License
BIT_STATUS CLicenseManagerPro::UserApplyForLicense()
{
	if(0 == strlen(m_Sn))
	{
		//SN is invalid.
		return BIT_ERR_SN_INVALID; 
	}

	// log in
    //m_Result = Bit_Login(NULL, m_Sn, application_data, &m_Handle, BIT_MODE_AUTO);
    if (MyBit_LoginPro)
        m_Result = MyBit_LoginPro(NULL, m_Sn, application_data, &m_Handle, BIT_MODE_AUTO);
	return m_Result;
}

// Verify Feature value by converting
BIT_INT32 CLicenseManagerPro::UserVerifyFeature(BIT_UINT32 nFeatureId, BIT_UINT32 val)
{
	m_Result = ReadFeature(nFeatureId);
	if(BIT_SUCCESS != m_Result)
	{
		return HEVC_ERR_READ_FEATURE;
	}

	if(!VerifyFeatureValue(val))
	{
		//right
		return HEVC_NO_ERROR;
	}
	else
	{
		return HEVC_ERR_FEATURE_VALUE;
	}
}

// Verify Feature value by converting
BIT_INT32 CLicenseManagerPro::UserVerifyAlgorithmFeature(
	BIT_UINT32 nFeatureId,
	BIT_UINT32 index /* it should less than 16 for array size */
	)
{
	if(index > 16)
	{
		return HEVC_ERR_CONVERT_FEATURE;
	}

    BIT_UINT32 para1 = gFeatureAlgorithmValuePro[index][0];
    BIT_UINT32 para2 = gFeatureAlgorithmValuePro[index][1];
    BIT_UINT32 para3 = gFeatureAlgorithmValuePro[index][2];
    BIT_UINT32 para4 = gFeatureAlgorithmValuePro[index][3];

	// convert Feature
//	m_Result = Bit_ConvertFeature(
//		m_Handle,
//		nFeatureId,
//		para1,
//		para2,
//		para3,
//		para4,
//		&m_ConvertResult);

    if(MyBit_ConvertFeaturePro)
        m_Result = MyBit_ConvertFeaturePro(
            m_Handle,
            nFeatureId,
            para1,
            para2,
            para3,
            para4,
            &m_ConvertResult);

	if(m_Result)
	{
		return HEVC_ERR_CONVERT_FEATURE;
	}
	else
	{
        if(gFeatureAlgorithmValuePro[index][4] != m_ConvertResult)
		{
			return HEVC_ERR_FEATURE_VALUE;
		}

		return HEVC_NO_ERROR;
	}
}

BIT_STATUS CLicenseManagerPro::UserQueryFeature(void)
{
	BIT_UINT32 *pCapacity = NULL;

//	m_Result = Bit_QueryFeature(
//		m_Handle,
//		3,
//		pCapacity
//		);
    if(MyBit_QueryFeaturePro)
        m_Result = MyBit_QueryFeaturePro(
            m_Handle,
            3,
            pCapacity
            );
	return m_Result;
}

BIT_STATUS CLicenseManagerPro::UserReleaseFeature(void)
{
	BIT_UINT32 *pCapacity = NULL;
//	m_Result = Bit_ReleaseFeature(
//		m_Handle,
//		3,
//		pCapacity
//		);
    if(MyBit_ReleaseFeaturePro)
        m_Result = MyBit_ReleaseFeaturePro(
            m_Handle,
            3,
            pCapacity
            );
	return m_Result;
}

BIT_STATUS CLicenseManagerPro::UpdateLicense(BIT_PCSTR szSN)
{
	// update license
    //m_Result = Bit_UpdateOnline(NULL, szSN, application_data);
    if(MyBit_UpdateOnlinePro)
        m_Result = MyBit_UpdateOnlinePro(NULL, szSN, application_data);
	return m_Result;
}


// Delete local SN for test or problem.
BIT_STATUS CLicenseManagerPro::UserRemoveSN(BIT_PCSTR szSN)
{
    //m_Result = Bit_RemoveSn(szSN, application_data);
    if(MyBit_RemoveSnPro)
        m_Result = MyBit_RemoveSnPro(szSN, application_data);
	return m_Result;
}

BIT_STATUS CLicenseManagerPro::GetStatus(void)
{
	return m_Result;
}

void CLicenseManagerPro::DumpStatus(void)
{
	char pDestStr[SN_BUFFER_LEN];
	itoa(m_Result, pDestStr, 10);
    //DumpErrorPro(pDestStr, strlen(pDestStr));	//if our software is not registered in microsoft,we can't create dump. windows will think it's un safe.
}
