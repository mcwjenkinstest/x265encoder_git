#pragma once


/**
 * Predefined Features.
 *
 * feature id 1 is for encryption and decription
 * feature id 2 is for read and write
 * feature id 3 is for convert
 */
#define DEMO_FEATURE_CRYPTO    1
#define DEMO_FEATURE_RW        2
#define DEMO_FEATURE_CONVERT   3

#define REQUEST_INFO_SIZE      10240
#define UPDATE_INFO_SIZE       10240
#define RECEIPT_INFO_SIZE      10240

#define DEMO_MEMBUFFER_SIZE   1024


#define SN_BUFFER_LEN            100

#define HEVC_NO_ERROR             0                            
#define HEVC_ERR_SN              (HEVC_NO_ERROR+1)             
#define HEVC_ERR_LOGIN           (HEVC_ERR_SN+1)               
#define HEVC_ERR_GET_ITEMNAME    (HEVC_ERR_LOGIN+1)            
#define HEVC_ERR_GET_DATAITEM    (HEVC_ERR_GET_ITEMNAME+1)     
#define HEVC_ERR_READ_FEATURE    (HEVC_ERR_GET_DATAITEM+1)     
#define HEVC_ERR_CONVERT_FEATURE (HEVC_ERR_READ_FEATURE+1)     
#define HEVC_ERR_FEATURE_VALUE   (HEVC_ERR_CONVERT_FEATURE+1)  

#define HEVC_ERR_LOGOUT          (HEVC_ERR_FEATURE_VALUE+1)  

