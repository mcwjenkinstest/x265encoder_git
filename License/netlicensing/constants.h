#ifndef __CONSTANTS_H__
#define __CONSTANTS_H__

#include <list>
#include <string>

namespace netlicensing {

  static const char* const ID      = "id";
  static const char* const ACTIVE  = "active";
  static const char* const NUMBER  = "number";
  static const char* const NAME    = "name";
  static const char* const VERSION = "version";
  static const char* const LICENSEE_AUTOCREATE = "licenseeAutoCreate";
  static const char* const DESCRIPTION = "description";
  static const char* const LICENSING_INFO = "licensingInfo";
  static const char* const DELETED = "deleted";
  static const char* const CASCADE = "forceCascade";
  static const char* const PRICE   = "price";
  static const char* const DISCOUNT = "discount";
  static const char* const CURRENCY = "currency";
  static const char* const IN_USE  = "inUse";
  static const char* const FILTER  = "filter";
  static const char* const BASE_URL = "baseUrl";
  static const char* const USERNAME = "username";
  static const char* const PASSWORD = "password";
  static const char* const SECURITY_MODE = "securityMode";
  static const char* const PROP_ID     = "ID";
  static const char* const PROP_TTL    = "TTL";

}  // namespace netlicensing

#endif  // __CONSTANTS_H__
