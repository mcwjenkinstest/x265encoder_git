x265Encoder
-------------

This is a GUI for MCW's x265 HEVC Encoder

Build Instruction
-------------------

open x265Encoder.pro file with qt creator and build the project. Qt version
5.3 or higher recommended.
To build a Demo version, change the argument of DEFINES to DEMO in x265Encoder.pro, then run 
qmake and rebuld the project.

Running Instruction
---------------------
copy the Addons Folder where the executable is present.
copy the dlls inside libs folder and put then where executable is present

Installation
---------------
1. put Addons Folder, x265Encoder.exe and x265Encoder.exe.embed.manifest in _build folder.
2. run the installer script. (Inno setup is required)
