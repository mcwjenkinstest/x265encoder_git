#define MyAppName "x265 Encoder"
#define MyAppVersion "1.0"
#define MyAppPublisher "x265"
#define MyAppURL "(link)"
#define MyAppExeName "x265Encoder.exe"
#define SubFolder "UHDcode With x265 Encoder"
#define StartMenuFolder "HEVC Upgrade Pack"
#define LblWidth 250
#define DesktopIconPos 120
#define wpSelectDir 6
#define wpLiscence 2
#define Height50 50

[Setup]
AppId= {#MyAppName}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
UsePreviousAppDir=no
DefaultDirName={pf64}\{#SubFolder}\{#MyAppName}
DisableProgramGroupPage=yes
DefaultGroupName={#StartMenuFolder}
;AllowNoIcons = yes

ArchitecturesInstallIn64BitMode=x64

SourceDir=_build
LicenseFile=..\installer\LICENSE
SetupIconFile=..\installer\x265Logo_128alpha.ico

Compression=lzma2/ultra64
SolidCompression=yes

WizardImageFile=..\installer\x265Logo.bmp
WizardSmallImageFile=..\installer\x265Logosmall.bmp

OutputDir=D:\Setup\
OutputBaseFilename=x265Encoder-x64-setup
DisableReadyMemo=yes
DisableReadyPage=yes
PrivilegesRequired=admin


[Icons]
; Create a desktop icon for all the users
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; WorkingDir: "{app}"; Check: desktopicon
; Create a start menu entry for all the users
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; WorkingDir: "{app}"; Comment: "x265 GUI Encoder";
;Name: "{group}\{cm:ProgramOnTheWeb,{#MyAppName}}"; Filename: "{#MyAppURL}"
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}";

[Files]
Source: "*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent unchecked

[CustomMessages]
FinsihedMsg = Installed

[Code]
var
   desktopCheckBox:TNewCheckBox;
   FLabel, LLabel: TLabel;
   NameTop: Integer;
function desktopicon(): Boolean;
begin
    Result := desktopCheckBox.Checked;
end;
function GetUninstallString(): String;
var
  sUnInstPath: String;
  sUnInstallString: String;
begin
  sUnInstPath := ExpandConstant('Software\Microsoft\Windows\CurrentVersion\Uninstall\{#emit SetupSetting("AppId")}_is1');
  sUnInstallString := '';
  if not RegQueryStringValue(HKLM, sUnInstPath, 'UninstallString', sUnInstallString) then
    RegQueryStringValue(HKCU, sUnInstPath, 'UninstallString', sUnInstallString);
  Result := sUnInstallString;
end;
procedure ExitProcess(exitCode:integer);
  external 'ExitProcess@kernel32.dll stdcall';
procedure InitializeWizard;
var
   uinstallOK : Integer ;
   sUnInstallString : String;
   iResultCode : Integer;
begin
  if RegValueExists(HKEY_LOCAL_MACHINE,ExpandConstant('Software\Microsoft\Windows\CurrentVersion\Uninstall\{#MyAppName}_is1'), 'UninstallString') then
  begin
     uinstallOK := MsgBox('The current version of the x265 Encoder must be uninstalled before the new version can be installed. OK to uninstall x265 Encoder?', mbinformation, MB_OKCANCEL);
     if uinstallOK <> IDOK   then
         ExitProcess(0);
     sUnInstallString := GetUninstallString();
     sUnInstallString :=  RemoveQuotes(sUnInstallString);
     Exec(ExpandConstant(sUnInstallString), '', '', SW_SHOW, ewWaitUntilTerminated, iResultCode) ;
     if RegValueExists(HKEY_LOCAL_MACHINE,ExpandConstant('Software\Microsoft\Windows\CurrentVersion\Uninstall\{#MyAppName}_is1'), 'UninstallString') then
     begin
           ExitProcess(0);
     end;     
  end;
  desktopCheckBox := TNewCheckBox.Create(WizardForm);
  desktopCheckBox.Parent := WizardForm.SelectDirpage;
  desktopCheckBox.Top := {#DesktopIconPos};
  desktopCheckBox.Width := {#LblWidth};
  desktopCheckBox.Caption := 'Create Desktop Shortcut';
  desktopCheckBox.Checked :=  True;
  WizardForm.LicenseAcceptedRadio.Checked := True;
  WizardForm.LicenseAcceptedRadio.Hide;
  WizardForm.LicenseNotAcceptedRadio.Hide;
  WizardForm.LicenseMemo.Top :=  WizardForm.LicenseLabel1.Top - 5;
  WizardForm.LicenseMemo.Top :=  WizardForm.LicenseLabel1.Top + WizardForm.LicenseLabel1.Height - 10;
  WizardForm.LicenseLabel1.Height := WizardForm.LicenseLabel1.Height - 10;
  WizardForm.LicenseMemo.Height := WizardForm.LicenseMemo.Height + {#Height50};
  WizardForm.WelcomeLabel1.Caption := ExpandConstant('{#MyAppName} Setup');
  WizardForm.WelcomeLabel1.Font.Size := 22;
  WizardForm.FinishedHeadingLabel.Font.Size := 22;
  WizardForm.FinishedHeadingLabel.Caption := ExpandConstant('{#MyAppName} Setup');
  WizardForm.WelcomeLabel1.Height := 80;
  WizardForm.FinishedHeadingLabel.Height := 80;
  WizardForm.WelcomeLabel2.Caption := ExpandConstant('This will install {#MyAppName} {#MyAppVersion} on your computer.' + #13#10 + #13#10
                                                     'We recommend that you close all other applications before' + #13#10 +
                                                     'continuing.');
  WizardForm.LicenseLabel1.Caption :='Please read the following. Scroll down to read the entire Agreement.';
  WizardForm.WelcomeLabel2.Top :=    WizardForm.WelcomeLabel1.Top +  WizardForm.WelcomeLabel1.Height ;
  LLabel := TLabel.Create(WizardForm);
  LLabel.Top := WizardForm.LicenseMemo.Top + WizardForm.LicenseMemo.Height + 3;
  LLabel.Parent := WizardForm.LicenseLabel1.Parent; 
  LLabel.Caption := 'If you accept the agreement, please Click I Agree to continue.'
  WizardForm.FinishedLabel.Hide;
  FLabel := TLabel.Create(WizardForm);
  FLabel.Left := WizardForm.FinishedLabel.Left;
  FLabel.Top := WizardForm.FinishedHeadingLabel.Top + WizardForm.FinishedHeadingLabel.Height;
  FLabel.Width := WizardForm.FinishedLabel.Width;
  FLabel.Height := WizardForm.FinishedLabel.Height;
  FLabel.Font.Size := WizardForm.FinishedLabel.Font.Size;
  FLabel.Font.Color := WizardForm.FinishedLabel.Font.Color;
  FLabel.Parent := WizardForm.FinishedLabel.Parent;
  FLabel.Caption := ExpandConstant('Finished installing {#MyAppName} {#MyAppVersion} on your computer.');
  WizardForm.WizardBitmapImage.Left := 20;
  WizardForm.WizardBitmapImage2.Left := 20;
  WizardForm.SelectDirBrowseLabel.Caption := 'Browse to change the destination location.';
  WizardForm.DiskSpaceLabel.Caption := WizardForm.DiskSpaceLabel.Caption + ' Click Install to proceed.';
  NameTop := WizardForm.PageNameLabel.Top;

end; 
procedure CurPageChanged(CurPageID: Integer);
begin
  if CurPageID = {#wpLiscence} then
  begin
    WizardForm.NextButton.Caption := 'I Agree';
    WizardForm.PageDescriptionLabel.Hide;
    WizardForm.PageNameLabel.Top := NameTop + 10;    
  end;
  if CurPageID = {#wpSelectDir} then
  begin
    WizardForm.PageNameLabel.Top := NameTop;
    WizardForm.PageDescriptionLabel.Show;
    WizardForm.NextButton.Caption := SetupMessage(msgButtonInstall); 
  end;
end;