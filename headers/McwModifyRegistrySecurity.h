#pragma once

#include <windows.h>

LRESULT RegModifySecurity(
    HKEY   rootkey,
    LPTSTR subkey,
    LPTSTR strSecurity
	);


// Misc Registry helpers
HRESULT CreateRegistryKey(
	HKEY hKey, 
	LPCTSTR subkey, 
	HKEY *phNewKey
	);

