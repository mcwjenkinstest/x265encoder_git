#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include "filecontainer.h"
#include "downloaddialog.h"
namespace Ui {
class AboutDialog;
}

class AboutDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AboutDialog(QWidget *parent = 0);
    QString  HevcVersion;
    Ui::AboutDialog *ui;
    ~AboutDialog();

private slots:
    void on_btnClosed_clicked();

    void on_btnUpdate_clicked();

private:
    FileContainer *fileContainer;
    DownloadDialog *downloadDialog;


protected:
     void closeEvent(QCloseEvent *event);
};

#endif // DIALOG_H
