#ifndef ADDONS_H
#define ADDONS_H

#include <QString>
#include <QDir>
#include <QApplication>
#include <QStandardPaths>

class Path
{
public:
    Path() {}
    static QString getPath()
    {
        QString Folder = QDir(qApp->applicationDirPath()).absolutePath();
        return Folder;
    }
    static QString getTempPath()
    {
        QString Folder = QStandardPaths::writableLocation(QStandardPaths::TempLocation);
        return Folder;
    }
};

class Addons
{
public:
    Addons();
    static QString MediaInfoExe()
    {
        return QString("\"") + Path::getPath() + "/Addons/MediaInfo/MediaInfo.exe" + QString("\"");
    }
    static QString MediaInfoArgGeneral()
    {
        return QString("\"") + Path::getPath() + "/Addons/MediaInfo/General" + QString("\"");
    }
    static QString MediaInfoArgVideo()
    {
        return QString("\"") + Path::getPath() + "/Addons/MediaInfo/Video" + QString("\"");
    }
    static QString MediaInfoArgAudio()
    {
        return QString("\"") + Path::getPath() + "/Addons/MediaInfo/Audio" + QString("\"");
    }
    static QString MediaInfoArgSub()
    {
        return QString("\"") + Path::getPath() + "/Addons/MediaInfo/Sub" + QString("\"");
    }
    static QString FFmpegExe()
    {
        return QString("\"") + Path::getPath() + "/Addons/FFmpeg/ffmpeg.exe" + QString("\"");
    }
    static QString x2658BitExe()
    {
        return QString("\"") + Path::getPath() + "/Addons/x265/x265-8bpp.exe" + QString("\"");
    }
    static QString x26510BitExe()
    {
        return QString("\"") + Path::getPath() + "/Addons/x265/x265-10bpp.exe" + QString("\"");
    }
    static QString x26512BitExe()
    {
        return QString("\"") + Path::getPath() + "/Addons/x265/x265-12bpp.exe" + QString("\"");
    }
    static QString mp4boxExe()
    {
        return QString("\"") + Path::getPath() + "/Addons/mp4box/mp4box.exe" + QString("\"");
    }
    static bool allAddonsPresent();
};

#endif // ADDONS_H
