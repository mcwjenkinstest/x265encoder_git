#ifndef DOWNLOADDIALOG_H
#define DOWNLOADDIALOG_H

#include <QDialog>
#include <QUrl>
#include <QNetworkReply>
#include <QDesktopServices>
#include <QNetworkAccessManager>
#include <QTimer>

namespace Ui {
class DownloadDialog;
}

class DownloadDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DownloadDialog(QWidget *parent = 0);
    void beginDownload (const QUrl& url, QString Version, QString checksum);
    ~DownloadDialog();

private slots:
    void openDownload (void);
    void installUpdate (void);
    void cancelDownload (void);
    void downloadFinished (void);
    void updateProgress (qint64 received, qint64 total);
    void timeout();
private:
    Ui::DownloadDialog *ui;
    QString path, Version, checksum;
    QNetworkReply *reply;
    QNetworkAccessManager *manager;
    uint start_time;
    QTimer *timeoutTimer;
    float roundNumber (const float& input);

};

#endif // DOWNLOADDIALOG_H
