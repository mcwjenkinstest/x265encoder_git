#ifndef BASIC_H
#define BASIC_H

#include <QMainWindow>
#include <QProgressDialog>
#include <QProcess>
#include <QWinTaskbarButton>
#include <QWinTaskbarProgress>
#include <QNetworkReply>
#include <QSignalMapper>
#include <QMovie>
#include "filecontainer.h"
#include "progressdialog.h"
#include "aboutdialog.h"
#include "settingswidget.h"
#include "settingutils.h"
#include "clickablelabel.h"

namespace Ui {
class Encoder;
}

class Encoder : public QMainWindow
{
    Q_OBJECT

public:
    ~Encoder();
    void switchWindow();
    void encode();
    void setFileContainer();
    static Encoder *instance();
    bool fileExists();
    void displayMessage(QString message, QMessageBox::Icon);
    void deleteTemp();
    void displayError();
    QList<SettingsWidget *> setList;
    Ui::Encoder * getUI() { return ui;}
    SettingsWidget *dummySetting;
    QWinTaskbarProgress *taskbarProgress;
    QStringList outputDestination;
    QStringList outputFile;
    QStringList outputEncoded;
    QStringList DefBasicSettings;
    int outContainer = 0;
    void getBanner();
    void loadBanner();

private slots:
    void on_btnAdd_clicked();
    void on_btnRemove_clicked();
    void on_btnClear_clicked();
    void on_btnRowDown_clicked();
    void on_btnRowUp_clicked();
    void on_btnEncode_clicked();
    void on_currentRowChanged(int currentRow);
    void encodeVideo();
    void mergeAudio();
    void muxMkv();
    void deleteTempFiles();
    void btnStopClicked();
    void btnPauseClicked();
    void btnResumeClicked();
    void btnOutputClicked();
    void redirectOutput();
    void advancedEncodeVideo();
    void secondPass();
    void askOnEsc();
    void deleteFfmpegVideo();
    void on_btnHelp_clicked();
    void on_btnAbout_clicked();
    void onError(QProcess::ProcessError);
    void onProgCancel();
    void on_btnLoad_clicked();
    void bannerClicked();
    void on_btnSave_clicked();
    void openFolder(int currentRow);
    void playVideo(int currentRow);

private:
    enum State { NULL_STATE, DEMUX, VIDEO_ENCODE, SECOND_PASS, MUX, DEL_TEMP };
    Encoder(QWidget *parent = 0);
    Ui::Encoder *ui;
    FileContainer *fileContainer;
    int fileIndex;
    int outputQueueIndex;
    bool firstTranscode;
    bool pauseClicked;
    bool stopClicked;
    bool resumeClicked;
    bool ffmpegVideoActive;
    ProgressDialog *progDial;
    QProcess *ffmpegAudio;
    QProcess *ffmpegSub;
    QProcess *ffmpegVideo;
    QProcess *ffmpegVideoSec;
    QProcess *x265Sec;
    QProcess *x265;
    QProcess *mp4box;
    QProcess *ffmpegMux;
    State currentState;
    bool audioAvailability;
    bool subAvailability;
    bool framesAvailable;
    int progPrev;
    int progPrevSingle;
    void setProgressBar(QString);
    void killCurrentProcess();
    AboutDialog *aboutDialog;
    QString lastOpenDirectory;
    QString encodedFrames;
    QWinTaskbarButton *taskbarButton;
    settingutils settutils;
    bool progCancelClicked;
    ClickableLabel lblBanner;
    QString externalUrl;
    bool loadBanner(QString filname);
    void setBanner(QString filename);
    QString btnEncodeStyle, btnOtherStyle;
    QString btnEncDisStyle, btnOthDisStyle;
    QGraphicsDropShadowEffect *encodeEffect, *addEffect, *removeEffect;
    QGraphicsDropShadowEffect *clearEffect, *upEffect, *downEffect;
    QGraphicsDropShadowEffect *loadEffect, *saveEffect, *aboutEffect, *helpEffect;
    void prepareOutputQueue();
    QSignalMapper *folderMapper, *playMapper;
    QMovie *movie;

protected:
     void closeEvent(QCloseEvent *event);

};

#endif // BASIC_H
