#ifndef FILECONTAINER_H
#define FILECONTAINER_H

#include <QWidget>
#include <QListWidget>
#include <QDragEnterEvent>
#include <QMimeData>
#include <QDrag>
#include <QDropEvent>
#include <QStringList>
#include <QMessageBox>
#include <QCheckBox>
#include <QGraphicsDropShadowEffect>
#include <QLabel>
#include <QFrame>
#include <QPropertyAnimation>
#include <QPainter>
#include <QStyledItemDelegate>
#include <QProgressDialog>
#include <QMargins>
#include "mediainfo.h"
#include "settingswidget.h"

class SelectionFrame : public QFrame
{
    Q_OBJECT
private:
    QString label;
public:
    SelectionFrame(QWidget *parent = 0)
        : QFrame(parent)
    {
    }
    void setText(QString label)
    {
        this->label = label;
    }
    void paintEvent(QPaintEvent *event)
    {
        event->accept();
        QPainter painter(this);
        QPointF pt(10, this->height()/1.5);
        painter.drawText(pt, label);
    }
};

class RemoveSelectionDelegate : public QStyledItemDelegate
{
public:
    RemoveSelectionDelegate(QObject *parent = 0)
        : QStyledItemDelegate(parent)
    {
    }

    void paint(QPainter *painter, const QStyleOptionViewItem &option,
               const QModelIndex &index) const
    {
        // Call the original paint method with the selection state cleared
        // to prevent painting the original selection background
        QStyleOptionViewItemV4 optionV4 =
            *qstyleoption_cast<const QStyleOptionViewItemV4 *>(&option);
        optionV4.state &= ~QStyle::State_Selected;
        QStyledItemDelegate::paint(painter, optionV4, index);
    }
};

class FileContainer : public QListWidget
{
    Q_OBJECT

public:
    static FileContainer *instance();
    ~FileContainer();
    void queueAdd(QString fileName, bool addSett = true);
    void queueRemove();
    void deQueue(int index);
    void queueClear();
    void rowUp();
    void rowDown();
    void updateOutputFolder(QString);
    QStringList totalFrames;
    QStringList audioFormat;
    QStringList totalAudioFrames;
    QStringList subFormat;
    QStringList frameRate;
    QStringList bitRate;
    QStringList videoHeight;
    QStringList videoWidth;
    QStringList videoDepth;
    QStringList fps;
    QStringList videoColorSpace;
    QStringList subSampling;
    QString outputFolder;
    int curIndex;
    bool loadxml;


private slots:
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
    void dragMoveEvent(QDragMoveEvent *event);
    void dragLeaveEvent(QDragLeaveEvent *event);
    void setSelectedItemSettings();
    void leaveEvent(QEvent *);
    void enterEvent(QEvent *);
    void resizeEvent(QResizeEvent *e);
    void updateSelection(int duration = 0);
    void onDropProgCancel();

private:
    FileContainer(QWidget *parent = 0);
    void setBackground();
    void swapRow(int i, int j);
    void recursiveAdd(QString);
    int getY4MCount(QString);
    void setSettings(bool);
    QStringList generalInfo;
    QStringList videoInfo;
    QStringList audioInfo;
    QStringList subInfo;
    QMessageBox *y4mWarnMsg;
    QCheckBox *y4mWarnMsgCheck;
    //int curIndex;
    QFont selectedFont;
    QFont font;
    SelectionFrame selectionFrame;
    QPropertyAnimation animation;
    QGraphicsDropShadowEffect itemShadow, settingShadow;
    bool dropProgCancelClicked;
    QProgressDialog dlg;
};

#endif // FILECONTAINER_H
