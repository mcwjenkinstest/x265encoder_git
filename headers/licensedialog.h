#ifndef LICENSEDIALOG_H
#define LICENSEDIALOG_H

#include <QDialog>
#include <QString>
#include "clickablelabel.h"

namespace Ui {
class LicenseDialog;
}

class LicenseDialog : public QDialog
{
    Q_OBJECT


public:
    explicit LicenseDialog(QString str = "", QWidget *parent = 0);
    ~LicenseDialog();
     static LicenseDialog *instance(QString );
     void getBanner();
     void loadBanner();


private slots:
    void on_Ignore_clicked();
    void on_Buy_clicked();
    void on_Activate_clicked();
    void bannerClicked();

private:
    Ui::LicenseDialog *ui;
    ClickableLabel lblBanner;
    QString externalUrl;
    bool flag = false;
    bool loadBanner(QString filname);
    void setBanner(QString filename);


protected:
     void closeEvent(QCloseEvent *event);

};

#endif // LICENSEDIALOG_H
