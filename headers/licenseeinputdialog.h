#ifndef LICENSEEINPUTDIALOG_H
#define LICENSEEINPUTDIALOG_H

#include <QDialog>
//#include <QString>
//#include "clickablelabel.h"

namespace Ui {
class LicenseeInputDialog;
}

class LicenseeInputDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LicenseeInputDialog(QWidget *parent = 0);
    ~LicenseeInputDialog();

    QString GetTextLine();

private:
   Ui::LicenseeInputDialog *ui;
};

#endif // LICENSEEINPUTDIALOG_H
