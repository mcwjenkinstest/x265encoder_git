#ifndef MAXBITRATEDIALOG_H
#define MAXBITRATEDIALOG_H

#include <QDialog>
#include <QSettings>

namespace Ui {
class MaxBitRateDialog;
}

class MaxBitRateDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MaxBitRateDialog(QWidget *parent = 0);
    ~MaxBitRateDialog();
    QSettings capSettings;

private slots:
    void on_btnSave_clicked();

    void on_btnCancel_clicked();

    void on_btnReset_clicked();

private:
    Ui::MaxBitRateDialog *ui;
    void readCapValues();
    void writeCapValues();
};

#endif // MAXBITRATEDIALOG_H
