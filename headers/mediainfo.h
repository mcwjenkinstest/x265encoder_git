#ifndef MEDIAINFO_H
#define MEDIAINFO_H
#include <QStringList>

class MediaInfo
{
   public:
     MediaInfo(){}
     static QStringList getGeneralDetails(QString fileName);
     static QStringList getVideoDetails(QString fileName);
     static QStringList getAudioDetails(QString fileName);
     static QStringList getSubDetails(QString fileName);
};

#endif // MEDIAINFO_H
