#ifndef PROGRESSDIALOG_H
#define PROGRESSDIALOG_H

#include <QDialog>
namespace Ui {
class ProgressDialog;
}

class ProgressDialog : public QDialog
{
    Q_OBJECT

public:
    Ui::ProgressDialog *ui;
    explicit ProgressDialog(QWidget *parent = 0);
    void setMin(int min);
    void setMax(int max);
    void setVal(int val);
    int getVal();
    void increment();
    void setMaxSingle(int max);
    void setValSingle(int val);
    int getValSingle();
    void incrementSingle();
    void printOutput(QString);
    void showPause(bool);
    bool isPaused();
    void setTitleBar(QString);
    ~ProgressDialog();

private slots:
    void on_checkDetails_toggled(bool checked);

private:
    //Ui::ProgressDialog *ui;

protected:
     void closeEvent(QCloseEvent *event);
};

#endif // PROGRESSDIALOG_H
