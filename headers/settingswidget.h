#ifndef SETTINGSWIDGET_H
#define SETTINGSWIDGET_H

#include <QWidget>
#include <QGraphicsDropShadowEffect>
#include <QLabel>
#include <QMouseEvent>
#include <QMessageBox>
#include <QCheckBox>
#include "clickablelabel.h"

namespace Ui {
class SettingsWidget;
}

class SettingsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SettingsWidget(QWidget *parent = 0);
    ~SettingsWidget();
     Ui::SettingsWidget *ui;
     QString mapPreset(int value)  const;
     int sldBitRateInvertedValue(int value) const;
     int coresAvailable;
     int numaNodesCount;
     QStringList stepList;
     QString getFfmpegArgs(int fileIndex);
     QString getX265Args(int pass = 1,QString frames = "");
     void setSelectedSetting(int);
     ClickableLabel txtBasic, txtAdvanced;
     void DefLoadSet(SettingsWidget * widget);
     void DefSaveSet(SettingsWidget * widget);

private:
    void initCpuUsage();
    void defaultSettings();
    void defaultAdvSettings();
    void writeSettings();
    void setCropArea(bool );
    QString getBasicFfmpegArgs(int fileIndex);
    QString getAdvFfmpegArgs(int fileIndex);
    QString getBasicX265Args();
    QString getAdvX265ArgsPass1();
    QString getAdvX265ArgsPass2(QString frames = "");
    void defaultSettingsUltrafast();
    void defaultSettingsSuperfast();
    void defaultSettingsVeryfast();
    void defaultSettingsFaster();
    void defaultSettingsFast();
    void defaultSettingsMedium();
    void defaultSettingsSlow();
    void defaultSettingsSlower();
    void defaultSettingsVeryslow();
    void defaultSettingsPlacebo();
    void toggleSettings();
    int getMaxRate(int height);
    QString currX265Args;
    QString selectedStyle;
    QString unselectedStyle;
    int currPresetIndex;
    int currTuneIndex;
    bool enableBluRay;
    enum Setting;
    enum Settings selecetedSettings;
    QMessageBox *presetWarnMsg;
    QCheckBox *presetWarnMsgCheck;
    QMessageBox *tuneWarnMsg;
    QCheckBox *tuneWarnMsgCheck;
    static bool presetChecked;
    static bool tuneChecked;
    QString lastOpenDir;

private slots:
    void on_btnDestination_clicked();
    void on_sldPreset_valueChanged(int value);
    void on_sldBitRate_valueChanged(int value);
    //void on_sldCpuUtilization_valueChanged(int value);
    void on_btnLogDest_clicked();
    void on_chkTransSkip_stateChanged(int value);
    void on_spinConsBFrame_valueChanged(int value);
    void on_checkLossless_clicked();
    void on_comboRateControl_currentIndexChanged(int);
    void on_comboOverScan_currentIndexChanged(int);
    void on_checkTwoPass_clicked();
    void on_checkCustomSAR_clicked();
    void on_btnQPFile_clicked();
    void on_btnScalingFile_clicked();
    void on_btnLambdaFile_clicked();
    void basicClicked();
    void advancedClicked();
    void on_spinAQStrength_valueChanged(double value);
    void on_chkRect_clicked();
    void comboPresetAdv_currentIndexChanged(int index);
    void comboTuneAdv_currentIndexChanged(int index);
    void on_comboProfileAdv_currentIndexChanged(int index);
    void on_helpPreset_clicked();
    void on_helpTune_clicked();
    void on_helpLevel_clicked();
    void on_helpProfile_clicked();
    void on_helpTier_clicked();
    void on_helpLogLevel_clicked();
    void on_helpLogFile_clicked();
    void on_helpEmitCu_clicked();
    void on_helpPME_clicked();
    void on_helpPMA_clicked();
    void on_helpThreads_clicked();
    void on_helpFrameThreads_clicked();
    void on_helpWPP_clicked();
    void on_helpFrames_clicked();
    void on_helpSeek_clicked();
    void on_helpMaxIntra_clicked();
    void on_helpMinGOP_clicked();
    void on_helpScenecut_clicked();
    void on_helpLookAhead_clicked();
    void on_helpMaxB_clicked();
    void on_helpBBias_clicked();
    void on_helpMaxReference_clicked();
    void on_helpAdaptiveB_clicked();
    void on_helpBRef_clicked();
    void on_helpEnableGop_clicked();
    void on_helpISlicePenalty_clicked();
    void on_helpTransformSkip_clicked();
    void on_helpConstrainedIntra_clicked();
    void on_helpStrongIntra_clicked();
    void on_helpFastTransformSkip_clicked();
    void on_helpUseIntaB_clicked();
    void on_helpCRF_clicked();
    void on_helpBitRate_clicked();
    void on_helpQP_clicked();
    void on_helpIPRatio_clicked();
    void on_helpPBRatio_clicked();
    void on_helpEnableTwoPass_clicked();
    void on_helpSlowFirstPass_clicked();
    void on_helpVBVMaxBitRate_clicked();
    void on_helpVBVBuffSize_clicked();
    void on_helpVBVInitial_clicked();
    void on_helpLossless_clicked();
    void on_helpAdaptQuant_clicked();
    void on_helpQuantStrength_clicked();
    void on_helpChromaOffsetCb_clicked();
    void on_helpChromaOffsetCb_2_clicked();
    void on_helpNoiseRedStrength_clicked();
    void on_helpNoiseRedInter_clicked();
    void on_helpQPFile_clicked();
    void on_helpScalingFile_clicked();
    void on_helpLambdaFile_clicked();
    void on_helpMotionSearch_clicked();
    void on_helpMotionSearchRange_clicked();
    void on_helpSubPixelRef_clicked();
    void on_helpRect_clicked();
    void on_helpAssemetric_clicked();
    void on_helpMaxMerge_clicked();
    void on_helpRateDisortion_clicked();
    void on_helpRDOQLevel_clicked();
    void on_helpEarlySkip_clicked();
    void on_helpWeightP_clicked();
    void on_helpWeightB_clicked();
    void on_helpCULossless_clicked();
    void on_helpHideSign_clicked();
    void on_helpCUTree_clicked();
    void on_helpMaxCU_clicked();
    void on_helpTUIntra_clicked();
    void on_helpInter_clicked();
    void on_helpRDO_clicked();
    void on_helpRDOQuant_clicked();
    void on_helpSAR_clicked();
    void on_helpVideoFormat_clicked();
    void on_helpRange_clicked();
    void on_helpColorPrim_clicked();
    void on_helpTransChar_clicked();
    void on_helpColorMatrix_clicked();
    void on_helpChromaSamp_clicked();
    void on_helpOverScan_clicked();
    void on_helpCrop_clicked();
    void on_helpRepeatHeaders_clicked();
    void on_helpHRDSignalling_clicked();
    void on_helpSEI_clicked();
    void on_helpAUDSignalling_clicked();
    void on_helpDeblockLoopFilter_clicked();
    void on_helpSampleAdapOffset_clicked();
    void on_helpSAONonDeblock_clicked();
    void on_helpSAOSkip_clicked();
    //void on_helpAnalysisMode_clicked();
    //void on_helpAnalysisFile_clicked();
    void on_helpBasicRateFactor_clicked();
    void on_helpBasicPreset_clicked();
    void on_btnCap_clicked();
    void on_btnLoadSet_clicked();
    void on_btnSaveSet_clicked();
    void on_spinVbvBufferSize_valueChanged(int arg1);
    void on_helpEnableStrictCbr_clicked();
    void on_helpMinCU_clicked();
    void on_helpLookAheadSlices_clicked();
    void on_helpMaxTU_clicked();
    void on_helpTemporalLayer_clicked();
    void on_helpLimitReference_clicked();
    void on_CropLeft_valueChanged(int arg1);
    void on_CropRight_valueChanged(int arg1);
    void on_CropTop_valueChanged(int arg1);
    void on_CropBottom_valueChanged(int arg1);
    void on_preset_currentIndexChanged(int index);
    void on_Width_valueChanged(int arg1);
    void on_chkResize_stateChanged(int arg1);
    void on_chkAspectRatio_stateChanged(int arg1);
    void on_Width_editingFinished();
    void on_Height_editingFinished();
    void on_helpIntraRefresh_clicked();
    void on_helpQcomp_clicked();
    void on_helpQpstep_clicked();
    void on_helpQblur_clicked();
    void on_helpCplxblur_clicked();
    void on_helpMasterDisplay_clicked();
    void on_helpLightLevel_clicked();
    void on_chkLoopFilter_clicked(bool checked);
    void on_chkUhdBd_clicked(bool checked);
    void on_chkqgSize_clicked(bool checked);
    void on_helpqgSize_clicked();
    void on_helpMaxLuma_clicked();
    void on_helpMinLuma_clicked();
    void on_helpLimitModes_clicked();
    void on_helpUhdBd_clicked();
    void on_sldCpuUtilization_sliderMoved(int position);
    void on_sldCpuUtilization_actionTriggered(int action);
    void on_comboContainer_currentIndexChanged(int index);
    void on_helpRSkip_clicked();
    void on_helpQpMin_clicked();
    void on_helpQpMax_clicked();
    void on_helpLimitTU_clicked();
};

#endif // SETTINGSWIDGET_H
