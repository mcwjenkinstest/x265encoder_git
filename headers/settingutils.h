#ifndef SETTINGUTILS_H
#define SETTINGUTILS_H
#include <QXmlStreamWriter>
#include <QXmlStreamReader>

#include "settingswidget.h"
#include "filecontainer.h"

class settingutils
{
private:
    FileContainer *filecontainer;
    void writeSettings(SettingsWidget *, int all);
    void readSettings(SettingsWidget *, QString filename);
    QXmlStreamReader reader;
    QXmlStreamWriter writer;
    QMap<QString, int> SettingsMap;

public:
    settingutils();
    void loadsettings(QList<SettingsWidget *>, QString, int all = 1);
    void saveSettings(QList<SettingsWidget *>, QString, int all = 1);
};

#endif // SETTINGUTILS_H
