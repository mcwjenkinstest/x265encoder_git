#ifndef SYSTEMINFORMATION
#define SYSTEMINFORMATION

#include <Windows.h>

typedef BOOL (WINAPI *LPFN_GLPI)(
    PSYSTEM_LOGICAL_PROCESSOR_INFORMATION,
    PDWORD);

class SystemInformation
{
    private:
        DWORD logicalProcessorCount = 0;
        DWORD numaNodeCount = 0;
        DWORD processorCoreCount = 0;
        DWORD processorL1CacheCount = 0;
        DWORD processorL2CacheCount = 0;
        DWORD processorL3CacheCount = 0;
        DWORD processorPackageCount = 0;
        int result;
        DWORD CountSetBits(ULONG_PTR bitMask);
        int setAllInfo();
    public:
        int getNumaNodeCount();
        int getProcessorCores();
        int getLogicalProcessorCount();
        SystemInformation();
        ~SystemInformation();
};

#endif // SYSTEMINFORMATION
