#ifndef UPDATE_H
#define UPDATE_H
#include <QString>
#include <QStringList>
#include <QApplication>

class Update
{
public:
    Update();
    ~Update();
    static Update *instance();
    int chkUpdate(QString * , QString * , QString *);
    bool chkDownload();
    bool chkRedundant();
    void installUpdate();
//private:
     //Update();
};

#endif // UPDATE_H
