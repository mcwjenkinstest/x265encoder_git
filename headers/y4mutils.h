#ifndef Y4MUTILS_H
#define Y4MUTILS_H
#include <QString>

class Y4MUtils
{
    private:
          Y4MUtils();
    public:
          typedef struct
          {
              int planes;
              int width[3];
              int height[3];
          } x265_cli_csp;
         int getColorSpaceIndex(QString);
         typedef unsigned long long int int64_t;
         static Y4MUtils *instance();
         int frameCount(int width, int height, int64_t size,int depth, QString colorSpace);
         int frameCount(int width, int height, int64_t size);
         int frameCount(int width, int height, int64_t size, QString colorSpace);
         int frameCount(int width, int height, int64_t size, int depth);
};

#endif // Y4MUTILS_H
