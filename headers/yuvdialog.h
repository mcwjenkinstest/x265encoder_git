#ifndef YUVDIALOG_H
#define YUVDIALOG_H

#include <QDialog>
#include <QString>

namespace Ui {
class YUVDialog;
}

class YUVDialog : public QDialog
{
    Q_OBJECT

public:
    explicit YUVDialog(QWidget *parent = 0);
    ~YUVDialog();
    static YUVDialog *instance();
    Ui::YUVDialog *ui;
    int subSampling = 0;
    bool Cancel = false;
    bool Ok = false;

private slots:

    void on_Ok_clicked();



    void on_Subsampling_currentIndexChanged(int index);



    void on_Resolution_currentIndexChanged(int index);

    void on_Cancel_clicked();

    void on_Width_textChanged(const QString &arg1);

    void on_Height_textChanged(const QString &arg1);

private:
    //Ui::YUVDialog *ui;
protected:
     void closeEvent(QCloseEvent *event);

};

#endif // YUVDIALOG_H
