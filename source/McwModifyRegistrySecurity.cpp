
#include <assert.h>
#include <tchar.h>
#include "Sddl.h"
#include "Aclapi.h"
#include "McwModifyRegistrySecurity.h"

///////////////////////////////////////////////////////////////////////
// Name: CreateRegistryKey
// Desc: Creates a new registry key. (Thin wrapper just to encapsulate
//       all of the default options.)
///////////////////////////////////////////////////////////////////////

HRESULT CreateRegistryKey(HKEY hKey, LPCTSTR subkey, HKEY *phNewKey)
{
	assert(phNewKey != NULL);

	LONG lreturn = RegCreateKeyEx(
		hKey,                 // parent key
		subkey,               // name of subkey
		0,                    // reserved
		NULL,                 // class string (can be NULL)
		REG_OPTION_NON_VOLATILE,
		KEY_ALL_ACCESS,
		NULL,                 // security attributes
		phNewKey,
		NULL                  // receives the "disposition" (is it a new or existing key)
		);

	return HRESULT_FROM_WIN32(lreturn);
}



BOOL SetPrivilege(
    HANDLE hToken,          // access token handle
    LPCTSTR lpszPrivilege,  // name of privilege to enable/disable
    BOOL bEnablePrivilege   // to enable or disable privilege
)
{
    TOKEN_PRIVILEGES tp;
    LUID luid;

    if(!LookupPrivilegeValue(
           NULL,            // lookup privilege on local system
           lpszPrivilege,   // privilege to lookup
           &luid))         // receives LUID of privilege
    {
        return FALSE;
    }

    tp.PrivilegeCount = 1;
    tp.Privileges[0].Luid = luid;
    if(bEnablePrivilege)
    {
        tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
    }
    else
    {
        tp.Privileges[0].Attributes = 0;
    }

    // Enable the privilege or disable all privileges.
    if(!AdjustTokenPrivileges(
           hToken,
           FALSE,
           &tp,
           sizeof(TOKEN_PRIVILEGES),
           (PTOKEN_PRIVILEGES) NULL,
           (PDWORD) NULL))
    {
        return FALSE;
    }

    if(GetLastError() == ERROR_NOT_ALL_ASSIGNED)
    {
        return FALSE;
    }
    return TRUE;
}



LRESULT SetSecurity(LPTSTR str_name, SE_OBJECT_TYPE e_type, LPTSTR geneic_str)
{
    long bRetval                          = -1;
    HANDLE hToken                         = NULL;
    PSID pSIDAdmin                        = NULL;
    PSID pSIDEveryone                     = NULL;
    PACL pNewDACL                         = NULL;
    PACL pOldDACL                         = NULL;
    SID_IDENTIFIER_AUTHORITY SIDAuthWorld = SECURITY_WORLD_SID_AUTHORITY;
    SID_IDENTIFIER_AUTHORITY SIDAuthNT    = SECURITY_NT_AUTHORITY;
    const int NUM_ACES                    = 2;
    EXPLICIT_ACCESS ea[NUM_ACES];

    // Acquire operation right
    BOOL bFlag = OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES, &hToken);
    bFlag = SetPrivilege(hToken, SE_TAKE_OWNERSHIP_NAME, TRUE);

    // Acquire right for setting
    bFlag = AllocateAndInitializeSid(&SIDAuthWorld, 1,
            SECURITY_WORLD_RID,
            0,
            0, 0, 0, 0, 0, 0,
            &pSIDEveryone);

    bFlag = AllocateAndInitializeSid(&SIDAuthNT, 2,
            SECURITY_BUILTIN_DOMAIN_RID,
            DOMAIN_ALIAS_RID_ADMINS,
            0, 0, 0, 0, 0, 0,
            &pSIDAdmin);

    DWORD dwRes = SetNamedSecurityInfo(
            str_name,                    // name of the object
            e_type,                      // type of object
            OWNER_SECURITY_INFORMATION,  // change only the object's owner
            pSIDAdmin,                   // SID of Administrator group
            NULL,
            NULL,
            NULL);

    ///////Set New Right/////////////////////////////

    ZeroMemory(&ea, NUM_ACES * sizeof(EXPLICIT_ACCESS));

    DWORD dwPermission = 0;
    ACCESS_MODE e_am = SET_ACCESS;

    if(0 == lstrcmpi(geneic_str, _T("ACCESS_READONLY")))
    {
        dwPermission = GENERIC_READ | GENERIC_EXECUTE;
        e_am = SET_ACCESS;
    }
    else if(0 == lstrcmpi(geneic_str, _T("ACCESS_DENYALL")))
    {
        dwPermission = GENERIC_ALL;
        e_am         = DENY_ACCESS;
    }
    else if(0 == lstrcmpi(geneic_str, _T("ACCESS_GENERICALL")))
    {
        dwPermission = GENERIC_ALL;
        e_am         = SET_ACCESS;
    }

    ea[0].grfAccessPermissions = dwPermission;
    ea[0].grfAccessMode = e_am;
    ea[0].grfInheritance = NO_INHERITANCE;
    ea[0].Trustee.TrusteeForm = TRUSTEE_IS_SID;
    ea[0].Trustee.TrusteeType = TRUSTEE_IS_WELL_KNOWN_GROUP;
    ea[0].Trustee.ptstrName = (LPTSTR) pSIDEveryone;
    ea[1].grfAccessPermissions = dwPermission;
    ea[1].grfAccessMode = e_am;
    ea[1].grfInheritance = NO_INHERITANCE;
    ea[1].Trustee.TrusteeForm = TRUSTEE_IS_SID;
    ea[1].Trustee.TrusteeType = TRUSTEE_IS_WELL_KNOWN_GROUP;
    ea[1].Trustee.ptstrName = (LPTSTR) pSIDAdmin;


    dwRes = SetEntriesInAcl(NUM_ACES, ea, NULL, &pNewDACL);

    ////////Set Security////////////////////////
    bRetval = SetNamedSecurityInfo(
            str_name,                    // name of the object
            e_type,                      // type of object
            DACL_SECURITY_INFORMATION,   // change only the object's DACL
            NULL,
            NULL,                        // do not change owner or group
            pNewDACL,                    // DACL specified
            NULL);                       // do not change SACL

    if(hToken)
    {
        CloseHandle(hToken);
    }

    if(pSIDEveryone)
    {
        FreeSid(pSIDEveryone);
    }

    if(pSIDAdmin)
    {
        FreeSid(pSIDAdmin);
    }

    if(pNewDACL)
    {
        LocalFree(pNewDACL);
    }

    return bRetval;
}


LRESULT RegModifySecurity(HKEY rootKey, LPTSTR subkey, LPTSTR strSecurity)
{
    long ret            = -1;
    TCHAR keyname[1024] = {0};

    if(rootKey == HKEY_CLASSES_ROOT)
    {
        lstrcpy(keyname, _T("CLASSES_ROOT\\"));
    }
    else if(rootKey == HKEY_LOCAL_MACHINE)
    {
        lstrcpy(keyname, _T("MACHINE\\"));
    }
    else if(rootKey == HKEY_CURRENT_USER)
    {
        lstrcpy(keyname, _T("CURRENT_USER\\"));
    }
    else if(rootKey == HKEY_USERS)
    {
        lstrcpy(keyname, _T("USERS\\"));
    }

    lstrcat(keyname, subkey);

    ret = SetSecurity(keyname, SE_REGISTRY_KEY, strSecurity);

    return ret;

}


//int _tmain(int argc, _TCHAR* argv[])
//{
//  HKEY rootKey       = HKEY_LOCAL_MACHINE;
//  LPTSTR SubKey      = TEXT("Software\\Microsoft\\Windows Media Foundation\\ByteStreamHandlers\\.avi");
//  LPTSTR strSecurity = TEXT("ACCESS_GENERICALL");
//
//  RegModifySecurity(rootKey, SubKey, strSecurity);
//}

