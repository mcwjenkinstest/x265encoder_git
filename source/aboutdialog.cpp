#include "aboutdialog.h"
#include "ui_aboutdialog.h"
#include <QFile>
#include <QDir>
#include <QUrl>
#include <QUrlQuery>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>
#include <QStandardPaths>
#include <QSettings>
#include <QDomDocument>
#include "update.h"
#include "ui_downloaddialog.h"
#include "downloaddialog.h"

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);
    QFile file(":/docs/version.txt");
    file.open(QFile::ReadOnly);
    QString version = "Version : " + QString(file.readAll());
    file.close();
    ui->lblVersion->setText(version);
    downloadDialog = new DownloadDialog(this);
    ui->btnChecking->setVisible(false);
}

AboutDialog::~AboutDialog()
{
    delete ui;
}

void AboutDialog::on_btnClosed_clicked()
{
    this->close();
}

void AboutDialog::closeEvent(QCloseEvent *event)
{
    this->fileContainer = FileContainer::instance();
    this->fileContainer->setAcceptDrops(true);
    event->accept();
}



void AboutDialog::on_btnUpdate_clicked()
{
    ui->btnUpdate->setVisible(false);
    ui->btnChecking->setVisible(true);
    ui->btnClosed->setEnabled(false);

    if (Update::instance()->chkDownload())
    {

        ui->btnChecking->setVisible(false);
        ui->btnUpdate->setVisible(true);
        ui->btnUpdate->setEnabled(false);
        QMessageBox messagebox;
        messagebox.setIcon(QMessageBox::Information);
        messagebox.setWindowTitle("Update available");
        messagebox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        messagebox.setText("There's an update to HEVC Upgrade that is ready to be installed.");
        messagebox.setInformativeText("Do you want to install it now?");
        if (messagebox.exec() == QMessageBox::Yes)
        {
            Update::instance()->installUpdate();
        }
        else
            this->close();
    }
    else
    {
        int update;
        QString downloadVersion, downloadUrl, checksum;
        update = Update::instance()->chkUpdate(&downloadVersion, &downloadUrl, &checksum);
        checksum = checksum.trimmed();
        if (update == 1)
        {
            ui->btnChecking->setVisible(false);
            ui->btnUpdate->setVisible(true);
            ui->btnUpdate->setEnabled(false);
            QMessageBox messagebox;
            messagebox.setIcon(QMessageBox::Information);
            messagebox.setWindowTitle(tr("Update available"));
            messagebox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            messagebox.setText("There's an update available!");
            messagebox.setInformativeText("do you want to download it?");

            // If the user clicks "yes" open the download dialog
            if (messagebox.exec() == QMessageBox::Yes)
            {

                downloadDialog->show();
                downloadDialog->beginDownload(QUrl(downloadUrl), downloadVersion, checksum);
            }
            ui->btnClosed->setEnabled(true);
            ui->btnUpdate->setEnabled(true);
            this->close();

        }
        else if (update == 0)
        {
            ui->btnClosed->setEnabled(true);
            ui->btnChecking->setVisible(false);
            ui->btnUpdate->setVisible(true);
            ui->btnUpdate->setEnabled(true);
            QMessageBox::information(this, "No updates available",
                                     "You are running the latest version of the application!");
            this->close();
        }
        else if (update == 2)
        {
            ui->btnClosed->setEnabled(true);
            ui->btnChecking->setVisible(false);
            ui->btnUpdate->setVisible(true);
            ui->btnUpdate->setEnabled(true);
            QMessageBox::information(this, "Network Error",
                                           "Please check your network connection and try again");
            this->close();
        }
    }
}
