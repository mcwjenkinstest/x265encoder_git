#include "addons.h"

Addons::Addons()
{
}

/* returns true if all addons are present in the addons folder */
bool Addons::allAddonsPresent()
{
    QFile fileMediaExe(Path::getPath() + "/Addons/MediaInfo/MediaInfo.exe");
    QFile fileMediaGeneral(Path::getPath() + "/Addons/MediaInfo/General");
    QFile fileMediaAudio(Path::getPath() + "/Addons/MediaInfo/Audio");
    QFile fileMediaSub(Path::getPath() + "/Addons/MediaInfo/Sub");
    QFile fileMediaVideo(Path::getPath() + "/Addons/MediaInfo/Video");
    QFile fileFFmpegExe(Path::getPath() + "/Addons/FFmpeg/ffmpeg.exe");
    QFile filex2658BitExe(Path::getPath() + "/Addons/x265/x265-8bpp.exe");
    QFile filex26510BitExe(Path::getPath() + "/Addons/x265/x265-10bpp.exe");
    QFile filex26512BitExe(Path::getPath() + "/Addons/x265/x265-12bpp.exe");
    QFile filemp4Exe(Path::getPath() + "/Addons/mp4box/mp4box.exe");
    QFile fileConDll(Path::getPath() + "/XCFT389IHBM67C.dll");
    QFile fileProDll(Path::getPath() + "/FTYU389L654GYP.dll");
    if(!fileMediaExe.exists() || !fileMediaGeneral.exists() || !fileMediaAudio.exists() || !fileMediaSub.exists() || !fileMediaVideo.exists()
            || !fileFFmpegExe.exists() || !filex2658BitExe.exists() || !filex26510BitExe.exists()|| !filex26512BitExe.exists()|| !filemp4Exe.exists()
            || !fileProDll.exists()|| !fileConDll.exists())
    {
        return false;
    }
    return true;
}
