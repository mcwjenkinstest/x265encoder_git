#include "clickablelabel.h"

ClickableLabel::ClickableLabel():QLabel()
{
}
ClickableLabel::~ClickableLabel(){}
void ClickableLabel::mousePressEvent(QMouseEvent *event)
{
       event->accept();
       emit clicked();
}
