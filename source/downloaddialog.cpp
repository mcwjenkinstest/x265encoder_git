#include "downloaddialog.h"
#include "ui_downloaddialog.h"
#include <QFile>
#include <QDir>
#include <QMessageBox>
#include <QDesktopServices>
#include <QCryptographicHash>

DownloadDialog::DownloadDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DownloadDialog)
{
    ui->setupUi(this);
    QIcon blank;
    setWindowIcon (blank);
    setWindowModality (Qt::WindowModal);
    setWindowFlags (Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint);
    connect (ui->btnStop, SIGNAL (clicked()), this, SLOT (cancelDownload()));
    connect (ui->btnOpen, SIGNAL (clicked()), this, SLOT (installUpdate()));
    ui->btnOpen->setEnabled (false);
    ui->btnOpen->setVisible (false);
    manager = new QNetworkAccessManager (this);
}

DownloadDialog::~DownloadDialog()
{
    delete ui;
}


void DownloadDialog::beginDownload (const QUrl &url, QString Version, QString checksum)
{
   this->timeoutTimer = new QTimer(this);
   this->timeoutTimer->setSingleShot(true);
   this->timeoutTimer->setInterval(60000);
   connect(this->timeoutTimer, SIGNAL(timeout()), this, SLOT(timeout()));
   this->Version = Version;
   this->checksum = checksum;
   ui->progressBar->setValue (0);
   ui->btnStop->setText ("Stop");
   ui->downloadLabel->setText ("Downloading updates");
   ui->timeLabel->setText ("Time remaining : unknown");

   // Start Downloading
   reply = manager->get (QNetworkRequest (url));
   start_time = QDateTime::currentDateTime().toTime_t();

   // Update the progress bar value
   connect (reply, SIGNAL (downloadProgress (qint64, qint64)), this,
            SLOT (updateProgress (qint64, qint64)));

   // Write the file to the temp directory after the download is finished
   connect (reply, SIGNAL (finished()), this, SLOT (downloadFinished()));

   //timeout when network fails
   connect (reply, SIGNAL (downloadProgress (qint64, qint64)), this->timeoutTimer, SLOT (start()));
   this->timeoutTimer->start();
}

void DownloadDialog::timeout()
{
    if(!reply->isFinished())
    {
        reply->abort();
        QMessageBox::information(this, "Network Error",
                                 "Please check your network connection and try again");
        this->close();
        return;
    }
}

void DownloadDialog::downloadFinished (void)
{
    if (reply->errorString().compare("Connection closed", Qt::CaseInsensitive) == 0)
    {
        QMessageBox::information(this, "Network Error",
                                 "Please check your network connection and try again");
        this->close();
        return;

    }
    ui->btnStop->setText ("Close");
    ui->downloadLabel->setText ("Download complete!");
    ui->timeLabel->setText ("The installer will open in a separate window");

    QByteArray data = reply->readAll();

    if (!data.isEmpty())
    {
        QFile file (QDir::tempPath() + "/x265HEVCUpgrade_update.exe");
        QString downloadChecksum = QString(QCryptographicHash::hash((data),QCryptographicHash::Sha1).toHex());
        if (file.exists())
        {
            QString Path = QDir::tempPath();
            QDir dir(Path);
            dir.remove(file.fileName());
        }

        QFile updateFile (QDir::tempPath() + "/x265HEVCUpgrade_update.txt");
        if (updateFile.exists())
        {
            QString Path = QDir::tempPath();
            QDir dir(Path);
            dir.remove(updateFile.fileName());
        }

        if(QString::compare(this->checksum, downloadChecksum, Qt::CaseInsensitive))
        {
            QMessageBox::information(this, "Network Error",
                                     "Something went wrong with your download.Please try again");
            this->close();
            return;

        }
        if (file.open (QIODevice::WriteOnly))
        {
            file.write (data);
            path = file.fileName();
        }
        file.close();
        if (updateFile.open (QIODevice::WriteOnly))
        {
            QTextStream outStream(&updateFile);
            outStream << this->Version;
        }
        updateFile.close();
        installUpdate();
    }
}

void DownloadDialog::updateProgress (qint64 received, qint64 total)
{

    if (total > 0 && received > 0) {
        ui->progressBar->setMinimum (0);
        ui->progressBar->setMaximum (100);

        int progress = (int) ((received * 100) / total);
        ui->progressBar->setValue (progress);

        QString total_string;
        QString received_string;

        float _total = total;
        float _received = received;

        if (_total < 1024)
            total_string = QString("%1 bytes").arg (_total);

        else if (_total < 1024 * 1024) {
            _total = roundNumber (_total / 1024);
            total_string = QString("%1 KB").arg (_total);
        }

        else {
            _total = roundNumber (_total / (1024 * 1024));
            total_string = QString("%1 MB").arg (_total);
        }

        if (_received < 1024)
            received_string = QString("%1 bytes").arg (_received);

        else if (received < 1024 * 1024) {
            _received = roundNumber (_received / 1024);
            received_string = QString("%1 KB").arg (_received);
        }

        else {
            _received = roundNumber (_received / (1024 * 1024));
            received_string = QString("%1 MB").arg (_received);
        }

        ui->downloadLabel->setText ("Downloading updates (" + received_string + " of " + total_string + ")");

        uint diff = QDateTime::currentDateTime().toTime_t() - start_time;

        if (diff > 0) {
            QString time_string;
            float time_remaining = (total - received) / (received / diff);

            if (time_remaining > 7200) {
                time_remaining /= 3600;
                time_string = QString("About %1 hours").arg (int (time_remaining + 0.5));
            }

            else if (time_remaining > 60) {
                time_remaining /= 60;
                time_string = QString("About %1 minutes").arg (int (time_remaining + 0.5));
            }

            else if (time_remaining <= 60)
                time_string = QString("%1 seconds").arg (int (time_remaining + 0.5));

            ui->timeLabel->setText ("Time remaining : " + time_string);
        }
    }


    else {
        ui->progressBar->setValue (-1);
        ui->progressBar->setMinimum (0);
        ui->progressBar->setMaximum (0);
        ui->downloadLabel->setText ("Downloading updates");
        ui->timeLabel->setText ("Time remaining : Unknown");
    }
}

float DownloadDialog::roundNumber (const float& input)
{
    return roundf (input * 100) / 100;
}

void DownloadDialog::installUpdate (void)
{
    QMessageBox msg;
    msg.setIcon (QMessageBox::Question);
    msg.setText ("<b>" +
                  QString("To apply the update, you must first quit %1")
                 .arg (qApp->applicationName()) +
                 "</b>");
    msg.setInformativeText (QString("Do you want to quit %1 now?").arg (qApp->applicationName()));
    msg.setStandardButtons (QMessageBox::Yes | QMessageBox::No);

    if (msg.exec() == QMessageBox::Yes) {
        openDownload();
        qApp->closeAllWindows();
    }

    else {
        ui->btnOpen->setEnabled (true);
        ui->btnOpen->setVisible (true);
        ui->timeLabel->setText ("Click the \"Open\" button to apply the update");
    }
}

void DownloadDialog::openDownload (void)
{
    if (!path.isEmpty()) {
        QString url = path;

        if (url.startsWith ("/"))
            url = "file://" + url;

        else
            url = "file:///" + url;

        QDesktopServices::openUrl (url);
    }
}

void DownloadDialog::cancelDownload (void)
{
    if (!reply->isFinished()) {
        QMessageBox message;
        message.setWindowTitle ("Update");
        message.setIcon (QMessageBox::Question);
        message.setStandardButtons (QMessageBox::Yes | QMessageBox::No);
        message.setText ("Are you sure you want to cancel the download?");

        if (message.exec() == QMessageBox::Yes) {
            hide();
            reply->abort();
        }
    }

    else
        hide();
}
