#include <QStringList>
#include <QFileDialog>
#include <QMessageBox>
#include <QProgressDialog>
#include <QProcess>
#include <QStandardPaths>
#include <QThread>
#include <QSignalMapper>
#include <QDesktopServices>
#include <QRegExp>
#include <QShortcut>
#include <Windows.h>
#include <QUrl>
#include <QUrlQuery>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>
#include <QStandardPaths>
#include <QSettings>
#include <QDomDocument>
#include <QImageReader>
#include <QPixmap>
#include <QDesktopWidget>
#include <QtTest/QTest>
#include "update.h"
#include "encoder.h"
#include "ui_encoder.h"
#include "addons.h"
#include "mediainfo.h"
#include "ui_aboutdialog.h"
#include "ui_settingswidget.h"
#include "ui_progressdialog.h"
#include "settingutils.h"

#define DEFAULT_PRESET 22
#define DEFAULT_BIT_RATE 28
#define DEFAULT_UTILIZATION 100
#define MinCoreRequired 4

Encoder::Encoder(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Encoder)
{
    ui->setupUi(this);
    setFileContainer();
    ui->lblHeader1->setStyleSheet("color:rgb(90,90,90);padding-bottom:20px;/*font-size:33px;*/");
    ui->lblHeader3->setStyleSheet("color:rgb(140,140,140);padding-bottom:20px;/*font-size:33px;*/");

    // set Encode button height according to system DPI and change x265 Image
    // pretty hacky, but do work for now
    int lDpiX = QApplication::desktop()->logicalDpiX();
    int lDpiY = QApplication::desktop()->logicalDpiY();
    if(lDpiX >= 192 && lDpiY >= 192)
    {
        ui->btnEncode->setMinimumHeight(150);
        ui->bannerLbl->setPixmap(QPixmap(":/Images/x265LogoSmall.png"));
        ui->bannerLbl->setStyleSheet("padding-bottom:20px;");
    }
    btnEncodeStyle = "QPushButton {\
                         border: 1px solid #999;\
                         border-radius: 20px;\
                         background:none;\
                         background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\
                                                           stop: 0 #F4F5F5, stop: 1 #DEDADA);\
                         /*min-height:20px;*/\
                         font-weight:bold;\
                         color:#333;\
                      }\
                      QPushButton:pressed {\
                         background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\
                                                           stop: 0 #dadbde, stop: 1 #f6f7fa);\
                      }\
                      QPushButton:flat {\
                         border: none;\
                      }\
                      QPushButton:default {\
                         border-color: navy;\
                      }";
    btnEncDisStyle = "QPushButton {\
                        border: 1px solid #aaa;\
                        border-radius: 20px;\
                        background:none;\
                        background-color: #ddd;\
                        /*height:20px;*/\
                        font-weight:bold;\
                        color:#999;\
                     }";
    btnOthDisStyle = "QPushButton {\
                        border: 1px solid #aaa;\
                        border-radius: 5px;\
                        background:none;\
                        background-color: #ddd;\
                        /*font-size:11px;*/\
                        min-height:25px;\
                        min-width:25px;\
                     }";
    btnOtherStyle = "QPushButton {\
                        border: 1px solid #999;\
                        border-radius: 5px;\
                        background:none;\
                        background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\
                                                    stop: 0 #f6f7fa, stop: 1 #dadbde);\
                        /*font-size:11px;*/\
                        min-height:25px;\
                        min-width:25px;\
                     }\
                     QPushButton:pressed {\
                        background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\
                                                    stop: 0 #dadbde, stop: 1 #f6f7fa);\
                     }\
                     QPushButton:flat {\
                        border: none; \
                     }\
                     QPushButton:default {\
                        border-color: navy; \
                     }";
    encodeEffect = new QGraphicsDropShadowEffect();
    encodeEffect->setBlurRadius(4);
    encodeEffect->setOffset(2,2);
    addEffect = new QGraphicsDropShadowEffect();
    addEffect->setBlurRadius(1);
    addEffect->setOffset(1,1);
    removeEffect = new QGraphicsDropShadowEffect();
    removeEffect->setBlurRadius(1);
    removeEffect->setOffset(1,1);
    clearEffect = new QGraphicsDropShadowEffect();
    clearEffect->setBlurRadius(1);
    clearEffect->setOffset(1,1);
    upEffect = new QGraphicsDropShadowEffect();
    upEffect->setBlurRadius(1);
    upEffect->setOffset(1,1);
    downEffect = new QGraphicsDropShadowEffect();
    downEffect->setBlurRadius(1);
    downEffect->setOffset(1,1);
    loadEffect = new QGraphicsDropShadowEffect();
    loadEffect->setBlurRadius(1);
    loadEffect->setOffset(1,1);
    saveEffect = new QGraphicsDropShadowEffect();
    saveEffect->setBlurRadius(1);
    saveEffect->setOffset(1,1);
    aboutEffect = new QGraphicsDropShadowEffect();
    aboutEffect->setBlurRadius(1);
    aboutEffect->setOffset(1,1);
    helpEffect = new QGraphicsDropShadowEffect();
    helpEffect->setBlurRadius(1);
    helpEffect->setOffset(1,1);
    ui->btnEncode->setStyleSheet(btnEncDisStyle);
    ui->btnAdd->setStyleSheet(btnOtherStyle);
    ui->btnRemove->setStyleSheet(btnOthDisStyle);
    ui->btnClear->setStyleSheet(btnOthDisStyle);
    ui->btnRowUp->setStyleSheet(btnOthDisStyle);
    ui->btnRowDown->setStyleSheet(btnOthDisStyle);
    ui->btnLoad->setStyleSheet(btnOtherStyle);
    ui->btnSave->setStyleSheet(btnOthDisStyle);
    ui->btnAbout->setStyleSheet(btnOtherStyle);
    ui->btnHelp->setStyleSheet(btnOtherStyle);
    ui->btnEncode->setGraphicsEffect(encodeEffect);
    ui->btnAbout->setGraphicsEffect(aboutEffect);
    ui->btnHelp->setGraphicsEffect(helpEffect);
    ui->btnAdd->setGraphicsEffect(addEffect);
    ui->btnRemove->setGraphicsEffect(removeEffect);
    ui->btnClear->setGraphicsEffect(clearEffect);
    ui->btnRowUp->setGraphicsEffect(upEffect);
    ui->btnRowDown->setGraphicsEffect(downEffect);
    ui->btnLoad->setGraphicsEffect(loadEffect);
    ui->btnSave->setGraphicsEffect(saveEffect);
    progDial = NULL;
    currentState = NULL_STATE;
    mp4box = NULL;
    ffmpegAudio = NULL;
    ffmpegSub = NULL;
    ffmpegVideo = NULL;
    x265 = NULL;
    ffmpegVideoSec = NULL;
    x265Sec = NULL;
    pauseClicked = false;
    resumeClicked = false;
    QShortcut *shortcut = new QShortcut(QKeySequence("Del"), this);
    QObject::connect(shortcut, SIGNAL(activated()), this, SLOT(on_btnRemove_clicked()));
    QShortcut *esc = new QShortcut(QKeySequence("Esc"), this);
    QObject::connect(esc, SIGNAL(activated()), this, SLOT(askOnEsc()));
    taskbarButton = NULL;
    progCancelClicked = false;
    lblBanner.setCursor(QCursor(Qt::PointingHandCursor));
    setStyleSheet("QWidget{background-image: url(:/Images/background.png);}\
                    QMessageBox QPushButton{background:none;}");
    //loadBanner();
}

Encoder::~Encoder()
{
    delete ui;
}

void Encoder::askOnEsc()
{
    QMessageBox::StandardButton reply;
      reply = QMessageBox::question(this, "Confirm", "Do you want to close x265 Encoder?",
                                    QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::No) return;
    close();
}

void Encoder::closeEvent(QCloseEvent *event)
{
    QFile DefSetFile (QDir::tempPath() + "/x265Encoder_DefSet.xml");
    if (DefSetFile.exists())
    {
        QString Path = QDir::tempPath();
        QDir dir(Path);
        dir.remove(DefSetFile.fileName());
    }
    event->accept();
}

/* opens file dialog for adding files and add them to the file queue */
void Encoder::on_btnAdd_clicked()
{
    fileContainer->loadxml = false;
    if(lastOpenDirectory.isEmpty())
        lastOpenDirectory = QStandardPaths::writableLocation(QStandardPaths::MoviesLocation);
    QStringList fileNames = QFileDialog::getOpenFileNames(
                             this,
                             "Add Files",
                             lastOpenDirectory,
                             "Videos (*.mp4 *.mov *.avi *.y4m *.mkv *.yuv *.h264);;\
                             All Files (*.*)");
    int count = fileNames.count();
    if(count == 0)
        return;
    lastOpenDirectory = fileNames.at(0).left(fileNames.at(0).lastIndexOf('/') + 1);
    progCancelClicked = false;
    QProgressDialog dlg;
    connect(&dlg, SIGNAL(canceled()), this, SLOT(onProgCancel()));
    dlg.setWindowFlags(dlg.windowFlags() & ~Qt::WindowCloseButtonHint);
    dlg.setMinimum(0);
    if(count == 1) count = 0;
    dlg.setMaximum(count);
    dlg.show();
    int i = 0;
    foreach (QString fileName, fileNames)
    {
        QApplication::processEvents();
        if(progCancelClicked)
            return;
        fileContainer->queueAdd(fileName);
        dlg.setValue(++i);
    }
    dlg.close();
    on_currentRowChanged(fileContainer->currentRow());
}

void Encoder::on_btnRemove_clicked()
{
    fileContainer->queueRemove();
    on_currentRowChanged(fileContainer->currentRow());
}

void Encoder::on_btnClear_clicked()
{
    QMessageBox::StandardButton reply;
      reply = QMessageBox::question(this, "Confirm", "Are you sure you want to clear entire list?",
                                    QMessageBox::Yes|QMessageBox::No);
    if(reply == QMessageBox::No) return;
    fileContainer->queueClear();
    on_currentRowChanged(fileContainer->currentRow());
}

void Encoder::on_btnRowUp_clicked()
{
    fileContainer->rowUp();
}

void Encoder::on_btnRowDown_clicked()
{
    fileContainer->rowDown();
}

/* checks for empty queue, checks for empty destination and starts encoding */
void Encoder::on_btnEncode_clicked()
{
//    if (!bannerRecieved)
//        this->getBanner();
    if(fileContainer->count() == 0)
    {
        displayMessage("No input files to encode", QMessageBox::Information);
        return;
    }
    if(setList.at(0)->ui->txtDest->text().isEmpty() || setList.at(0)->ui->txtDest->text().isNull())
    {
        displayMessage("Output folder must be specified", QMessageBox::Information);
        return;
    }
    outContainer = setList.at(0)->ui->comboContainer->currentIndex();
    if (setList.at(0)->ui->stackedWidget->currentIndex() == 0)
    {
        DefBasicSettings.clear();
        DefBasicSettings << QString::number(setList.at(0)->ui->sldBitRate->value());
        DefBasicSettings << QString::number(setList.at(0)->ui->sldPreset->value());
        DefBasicSettings << QString::number(setList.at(0)->ui->sldCpuUtilization->value());
        DefBasicSettings << setList.at(0)->ui->lblCpuUtilization->text();
    }
    else
    {
        DefBasicSettings.clear();
        SettingsWidget *widget ;
        widget = new SettingsWidget();
        widget->DefSaveSet(setList.at(0));
    }
    firstTranscode = true;
    fileIndex = 0;
    fileContainer->setCurrentRow(fileIndex);
    encode();
}


/* starts the encoding process if alteast one file is in the queue
 * If all the files are encoded it closes the progress dialog and outputs message "Encoding completed"
 */
void Encoder::encode()
{

    QString file;
    QString ffmpegArgs1, ffmpegArgs2;
    int rowCount = fileContainer->count();
    if (!progDial)
        progDial = new ProgressDialog(this);
    if (firstTranscode && !resumeClicked)
    {
        if(!taskbarButton)
        {
            taskbarButton = new QWinTaskbarButton(this);
            taskbarButton->setWindow(this->windowHandle());
        }
        taskbarProgress = taskbarButton->progress();
        progDial->setMin(0);
        progDial->setMax(rowCount * 53);
        progDial->setMaxSingle(1 * 53);
        if(rowCount == 1)
        {
            progDial->ui->progressEncFile->setVisible(false);
            progDial->ui->lblBatchEnc->setVisible(false);
        }
        else
        {
            progDial->ui->progressEncFile->setVisible(true);
            progDial->ui->lblBatchEnc->setVisible(true);
            progDial->ui->lblBatchEnc->setText("Batch Encoding Progress.........");
        }
        taskbarProgress->setRange(0, rowCount * 53);
        progDial->setVal(0);
        progDial->setValSingle(0);
        progDial->setModal(true);
        progDial->setWindowFlags(progDial->windowFlags() & ~Qt::WindowCloseButtonHint);
        progDial->showPause(true);
        progDial->ui->btnStop->show();
        progDial->ui->btnOutputFolder->hide();
        prepareOutputQueue();
        progDial->setMinimumWidth(this->width()/1.5);
        progDial->resize(this->width()/1.5, this->height()/2);
        progDial->adjustSize();
        progDial->show();
        taskbarProgress->setVisible(true);
        taskbarProgress->show();
        this->fileContainer->setAcceptDrops(false);
        outputQueueIndex = 0;
    }

    if (rowCount >= 1)
    {
        if (!fileExists())
        {
            currentState = NULL_STATE;
            displayMessage("File Not Present", QMessageBox::Critical);
            progDial->close();
            taskbarProgress->setVisible(false);
            this->fileContainer->setAcceptDrops(true);
            return;
        }
        stopClicked = false;
        currentState = DEMUX;
        progDial->setTitleBar("Extracting Audio from " +  fileContainer->item(0)->text());
        progDial->increment();
        progDial->incrementSingle();
        file = QString("\"") +  fileContainer->item(0)->text() + QString("\"");
        audioAvailability = false;
        subAvailability = false;
        framesAvailable = true;
        if(fileContainer->totalFrames.at(0).isEmpty())
            framesAvailable = false;
        if(!fileContainer->audioFormat.at(0).isEmpty() && !fileContainer->audioFormat.at(0).isNull())
            audioAvailability = true;
        if(!fileContainer->subFormat.at(0).isEmpty() && !fileContainer->subFormat.at(0).isNull())
            subAvailability = true;

        QString audioFrames;
        if(audioAvailability && setList.at(0)->ui->spinSeek->value() > 0)
        {

            float seekSeconds;
            if(!FileContainer::instance()->frameRate.at(0).isEmpty())
            {
                seekSeconds = setList.at(0)->ui->spinSeek->value() / FileContainer::instance()->frameRate.at(0).toFloat();
                audioFrames += " -ss " + QString::number(seekSeconds);
            }
        }
        if(audioAvailability && setList.at(0)->ui->stackedWidget->currentIndex() && setList.at(0)->ui->spinFrames->value() > 0)
        {
            float totalTime;
            if(!FileContainer::instance()->frameRate.at(0).isEmpty())
            {
                totalTime = setList.at(0)->ui->spinFrames->value() / FileContainer::instance()->frameRate.at(0).toFloat();
                audioFrames += " -t " + QString::number(totalTime);
            }
        }
        QString outFile = setList.at(0)->ui->txtDest->text() + "/" + setList.at(0)->ui->txtFile->text();
        if(audioAvailability && subAvailability)
        {
            if (QString::compare(fileContainer->subFormat.at(0), "Timed Text", Qt::CaseInsensitive) == 0 && outFile.endsWith(".mp4",Qt::CaseInsensitive))
            {
                if(QString::compare(fileContainer->audioFormat.at(0), "PCM", Qt::CaseInsensitive) == 0)
                    ffmpegArgs1 = Addons::FFmpegExe() + " -i " + file + audioFrames + " -acodec copy -vn " + QString("\"") + Path::getTempPath() + "/x265Encoder_audio_out.mov\" -y";
                else if(QString::compare(fileContainer->audioFormat.at(0), "FLAC", Qt::CaseInsensitive) == 0)
                    ffmpegArgs1 = Addons::FFmpegExe() + " -i " + file + audioFrames + " -acodec copy -vn -scodec copy -map 0:a -map 0:s " + QString("\"") + Path::getTempPath() + "/x265Encoder_audio_out.mkv\" -y";
                else
                    ffmpegArgs1 = Addons::FFmpegExe() + " -i " + file + audioFrames + " -acodec copy -vn -scodec copy -map 0:a -map 0:s " + QString("\"") + Path::getTempPath() + "/x265Encoder_audio_out.mp4\" -y";
            }

            else if (outFile.endsWith(".mp4",Qt::CaseInsensitive))
            {
                if(QString::compare(fileContainer->audioFormat.at(0), "PCM", Qt::CaseInsensitive) == 0)
                    ffmpegArgs1 = Addons::FFmpegExe() + " -i " + file + audioFrames + " -acodec copy -vn " + QString("\"") + Path::getTempPath() + "/x265Encoder_audio_out.mov\" -y";
                else if(QString::compare(fileContainer->audioFormat.at(0), "FLAC", Qt::CaseInsensitive) == 0)
                    ffmpegArgs1 = Addons::FFmpegExe() + " -i " + file + audioFrames + " -acodec copy -vn -map 0:a " + QString("\"") + Path::getTempPath() + "/x265Encoder_audio_out.mkv\" -y";
                else
                    ffmpegArgs1 = Addons::FFmpegExe() + " -i " + file + audioFrames + " -acodec copy -vn -map 0:a " + QString("\"") + Path::getTempPath() + "/x265Encoder_audio_out.mp4\" -y";
            }

            else
            {
                if(QString::compare(fileContainer->audioFormat.at(0), "PCM", Qt::CaseInsensitive) == 0)
                    ffmpegArgs1 = Addons::FFmpegExe() + " -i " + file + audioFrames + " -acodec copy -vn " + QString("\"") + Path::getTempPath() + "/x265Encoder_audio_out.mov\" -y";
                else if(QString::compare(fileContainer->audioFormat.at(0), "FLAC", Qt::CaseInsensitive) == 0)
                    ffmpegArgs1 = Addons::FFmpegExe() + " -i " + file + audioFrames + " -acodec copy -vn -map 0:a " + QString("\"") + Path::getTempPath() + "/x265Encoder_audio_out.mkv\" -y";
                else
                    ffmpegArgs1 = Addons::FFmpegExe() + " -i " + file + audioFrames + " -acodec copy -vn -map 0:a " + QString("\"") + Path::getTempPath() + "/x265Encoder_audio_out.mp4\" -y";

                ffmpegArgs2 = Addons::FFmpegExe() + " -i " + file + audioFrames + " -an -vn -scodec copy -map 0:s " + QString("\"") + Path::getTempPath() + "/x265Encoder_sub_out.mkv\" -y";
                ffmpegSub = new QProcess(this);
                ffmpegSub->setProcessChannelMode(QProcess::MergedChannels);
                ffmpegSub->start(ffmpegArgs2);
            }

        }
        else if(audioAvailability)
        {
            if(QString::compare(fileContainer->audioFormat.at(0), "PCM", Qt::CaseInsensitive) == 0)
                ffmpegArgs1 = Addons::FFmpegExe() + " -i " + file + audioFrames + " -acodec copy -vn " + QString("\"") + Path::getTempPath() + "/x265Encoder_audio_out.mov\" -y";
            else if(QString::compare(fileContainer->audioFormat.at(0), "FLAC", Qt::CaseInsensitive) == 0)
                ffmpegArgs1 = Addons::FFmpegExe() + " -i " + file + audioFrames + " -acodec copy -vn -map 0:a " + QString("\"") + Path::getTempPath() + "/x265Encoder_audio_out.mkv\" -y";
            else
                ffmpegArgs1 = Addons::FFmpegExe() + " -i " + file + audioFrames + " -acodec copy -vn -map 0:a " + QString("\"") + Path::getTempPath() + "/x265Encoder_audio_out.mp4\" -y";
        }
        else
            ffmpegArgs1 = Addons::FFmpegExe();
        ffmpegAudio = new QProcess(this);
        ffmpegAudio->setProcessChannelMode(QProcess::MergedChannels);
        connect(ffmpegAudio, SIGNAL(readyReadStandardOutput()), this, SLOT(redirectOutput()));
        connect(ffmpegAudio, SIGNAL(error(QProcess::ProcessError)), this, SLOT(onError(QProcess::ProcessError)));
        if (setList.at(0)->ui->stackedWidget->currentIndex())
            connect(ffmpegAudio, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(advancedEncodeVideo()));
        else
            connect(ffmpegAudio, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(encodeVideo()));
        ffmpegAudio->start(ffmpegArgs1);
        movie = new QMovie(":/Images/encoding.gif");
        QLabel *processLabel = new QLabel(this);
        processLabel->setMovie(movie);
        processLabel->setAlignment(Qt::AlignCenter);
        movie->start();
        progDial->ui->outputQueue->setCellWidget(outputQueueIndex, 0, processLabel);
        pauseClicked = false;

    }
    else
    {
        //progDial->close();
        int outputCount = progDial->ui->outputQueue->rowCount();
        for(int i = 0; i < outputCount; i++)
        {
            progDial->ui->outputQueue->cellWidget(i, 2)->setEnabled(true);
            progDial->ui->outputQueue->cellWidget(i, 3)->setEnabled(true);
        }
        progDial->ui->btnPause->hide();
        progDial->ui->btnStop->hide();
        progDial->ui->btnOutputFolder->show();
        progDial->ui->lblBatchEnc->setVisible(false);
        progDial->ui->lblProcess->setText("Done!");
        taskbarProgress->setVisible(false);
        progDial->ui->progressEncFile->setVisible(false);
        this->fileContainer->setAcceptDrops(true);
        currentState = NULL_STATE;
        progDial->setVal(progDial->ui->progressEnc->maximum());
        progDial->setValSingle(progDial->ui->progressEncFile->maximum());
        QMessageBox::StandardButton reply;
        reply = QMessageBox::information(this, "Done!", "Encoding Complete.", QMessageBox::Ok);
        progDial->setWindowFlags(Qt::Dialog);
        progDial->setModal(true);
        progDial->show();
        return;
    }

}

void Encoder::advancedEncodeVideo()
{
    QProcess *p = (QProcess *)sender();
    if(!pauseClicked && framesAvailable)
    {
        if(audioAvailability && p->exitCode() != 0)
        {
            displayError();
            p->deleteLater();
            return;
        }
        p->deleteLater();
        progPrev = progDial->getVal();
        progPrevSingle = progDial->getValSingle();
    }
    else if(!pauseClicked && !framesAvailable && setList.at(0)->ui->checkTwoPass->isChecked())
    {
        if(audioAvailability && p->exitCode() != 0)
        {
            displayError();
            p->deleteLater();
            return;
        }
        p->deleteLater();
        progDial->setVal(progDial->getVal() + 12);
        progDial->setValSingle(progDial->getValSingle() + 12);
    }
    else if(!pauseClicked && !framesAvailable)
    {
        if(audioAvailability && p->exitCode() != 0)
        {
            displayError();
            p->deleteLater();
            return;
        }
        p->deleteLater();
        progDial->setVal(progDial->getVal() + 25);
        progDial->setValSingle(progDial->getValSingle() + 25);
    }
    ffmpegAudio = NULL;
    if(!fileExists())
    {
        currentState = NULL_STATE;
        displayMessage("File Not Present", QMessageBox::Critical);
        progDial->close();
        taskbarProgress->setVisible(false);
        this->fileContainer->setAcceptDrops(true);
        return;
    }
    currentState = VIDEO_ENCODE;
    progDial->setTitleBar("Encoding Video " +  fileContainer->item(fileIndex)->text());

    ffmpegVideo = new QProcess(this);
    x265 = new QProcess(this);
    ffmpegVideo->setStandardOutputProcess(x265);
    x265->setProcessChannelMode(QProcess::MergedChannels);
    connect(ffmpegVideo, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(deleteFfmpegVideo()));
    connect(ffmpegVideo, SIGNAL(error(QProcess::ProcessError)), this, SLOT(onError(QProcess::ProcessError)));
    connect(x265, SIGNAL(readyReadStandardOutput()), this, SLOT(redirectOutput()));
    connect(x265, SIGNAL(error(QProcess::ProcessError)), this, SLOT(onError(QProcess::ProcessError)));

    // for 2 pass
    if(setList.at(0)->ui->checkTwoPass->isChecked())
    {
        connect(x265, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(secondPass()));
    }
    else
    {
        connect(x265, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(mergeAudio()));
    }

    ffmpegVideoActive = true;
    ffmpegVideo->start(setList.at(0)->getFfmpegArgs(fileIndex));
    while(!ffmpegVideo->waitForStarted());
    x265->start(setList.at(0)->getX265Args());
    pauseClicked = false;
}

void Encoder::secondPass()
{
    QProcess *p = (QProcess *)sender();
    if(!pauseClicked && framesAvailable)
    {
        if(p->exitCode() != 0)
        {
            displayError();
            p->deleteLater();
            return;
        }
        p->deleteLater();
        progPrev = progDial->getVal();
        progPrevSingle = progDial->getValSingle();
    }
    else if(!pauseClicked && !framesAvailable)
    {
        if(p->exitCode() != 0)
        {
            displayError();
            p->deleteLater();
            return;
        }
        p->deleteLater();
        progDial->setVal(progDial->getVal() + 13);
        progDial->setValSingle(progDial->getValSingle() +13);
    }
    ffmpegVideo = NULL;
    x265 = NULL;
    if(!fileExists())
    {
        currentState = NULL_STATE;
        displayMessage("File Not Present", QMessageBox::Critical);
        progDial->close();
        taskbarProgress->setVisible(false);
        this->fileContainer->setAcceptDrops(true);
        return;
    }
    currentState = SECOND_PASS;
    progDial->setTitleBar("Second Pass " +  fileContainer->item(fileIndex)->text());
    ffmpegVideoSec = new QProcess(this);
    x265Sec = new QProcess(this);
    ffmpegVideoSec->setStandardOutputProcess(x265Sec);
    x265Sec->setProcessChannelMode(QProcess::MergedChannels);
    connect(ffmpegVideoSec, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(deleteFfmpegVideo()));
    connect(ffmpegVideoSec, SIGNAL(error(QProcess::ProcessError)), this, SLOT(onError(QProcess::ProcessError)));
    connect(x265Sec, SIGNAL(readyReadStandardOutput()), this, SLOT(redirectOutput()));
    connect(x265Sec, SIGNAL(error(QProcess::ProcessError)), this, SLOT(onError(QProcess::ProcessError)));
    connect(x265Sec, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(mergeAudio()));
    ffmpegVideoActive = true;
    ffmpegVideoSec->start(setList.at(0)->getFfmpegArgs(fileIndex));
    while(!ffmpegVideoSec->waitForStarted());
    x265Sec->start(setList.at(0)->getX265Args(2, encodedFrames));
    pauseClicked = false;
}


/* Deletes the instance of sender process,
 * Starts a piped process of ffmpeg and x265 to decode and encode the original video
 */
void Encoder::encodeVideo()
{
    QProcess *p = (QProcess *)sender();
    if(!pauseClicked && framesAvailable)
    {
        if(audioAvailability && p->exitCode() != 0)
        {
            displayError();
            p->deleteLater();
            return;
        }
        p->deleteLater();
        progPrev = progDial->getVal();
        progPrevSingle = progDial->getValSingle();
    }
    else if(!pauseClicked && !framesAvailable)
    {
        if(audioAvailability && p->exitCode() != 0)
        {
            displayError();
            p->deleteLater();
            return;
        }
        p->deleteLater();
        progDial->setVal(progDial->getVal() + 25);
        progDial->setValSingle(progDial->getValSingle() + 25);
    }
    ffmpegAudio = NULL;
    if(!fileExists())
    {
        currentState = NULL_STATE;
        displayMessage("File Not Present", QMessageBox::Critical);
        progDial->close();
        taskbarProgress->setVisible(false);
        this->fileContainer->setAcceptDrops(true);
        return;
    }
    currentState = VIDEO_ENCODE;
    progDial->setTitleBar("Encoding Video " +  fileContainer->item(fileIndex)->text());

    ffmpegVideo = new QProcess(this);
    x265 = new QProcess(this);
    ffmpegVideo->setStandardOutputProcess(x265);
    x265->setProcessChannelMode(QProcess::MergedChannels);
    connect(ffmpegVideo, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(deleteFfmpegVideo()));
    connect(ffmpegVideo, SIGNAL(error(QProcess::ProcessError)), this, SLOT(onError(QProcess::ProcessError)));
    connect(x265, SIGNAL(readyReadStandardOutput()), this, SLOT(redirectOutput()));
    connect(x265, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(mergeAudio()));
    connect(x265, SIGNAL(error(QProcess::ProcessError)), this, SLOT(onError(QProcess::ProcessError)));
    ffmpegVideoActive = true;
    ffmpegVideo->start(setList.at(0)->getFfmpegArgs(fileIndex));
    while(!ffmpegVideo->waitForStarted());
    x265->start(setList.at(0)->getX265Args());
    pauseClicked = false;
}

/* Deletes the instance of sender process, and starts the process for merging audio to the encoded video
 * in a mp4 container
 */
void Encoder::mergeAudio()
{
    QProcess *p = (QProcess *)sender();
    if(!pauseClicked && framesAvailable)
    {
        if(p->exitCode() != 0)
        {
            displayError();
            p->deleteLater();
            return;
        }
        p->deleteLater();
        progDial->increment();
        progDial->incrementSingle();
    }
    else if(!pauseClicked && !framesAvailable)
    {
        if(p->exitCode() != 0)
        {
            displayError();
            p->deleteLater();
            return;
        }
        p->deleteLater();
        progDial->setVal(progDial->getVal() + 25);
        progDial->setValSingle(progDial->getValSingle() + 25);
    }
    ffmpegVideo = NULL;
    x265 = NULL;
    if(!fileExists())
    {
        currentState = NULL_STATE;
        displayMessage("File Not Present", QMessageBox::Critical);
        progDial->close();
        taskbarProgress->setVisible(false);
        this->fileContainer->setAcceptDrops(true);
        return;
    }
    currentState = MUX;
    progDial->setTitleBar("Merging Audio...");
    QString outputFile(setList.at(0)->ui->txtDest->text() + "/" + setList.at(0)->ui->txtFile->text());
    mp4box = new QProcess(this);
    QString mp4boxArgs;
    if(outputFile.endsWith(".mp4",Qt::CaseInsensitive))
    {
        if(audioAvailability)
        {
            if(QString::compare(fileContainer->audioFormat.at(0), "PCM", Qt::CaseInsensitive) == 0)
                mp4boxArgs = Addons::mp4boxExe() + " -inter 500 -add " + QString("\"") + Path::getTempPath() + "/x265Encoder_video_out.hevc\"" + ":forcesync -par 1=1:1 -add " + QString("\"") + Path::getTempPath() + "/x265Encoder_audio_out.mov\" " + QString("\"")  + outputFile + QString("\"") + " -new";
            else
                mp4boxArgs = Addons::mp4boxExe() + " -inter 500 -add " + QString("\"") + Path::getTempPath() + "/x265Encoder_video_out.hevc\"" + ":forcesync -par 1=1:1 -add " + QString("\"") + Path::getTempPath() + "/x265Encoder_audio_out.mp4\" " + QString("\"") +  outputFile + QString("\"") + " -new";
        }
        else
             mp4boxArgs = Addons::mp4boxExe() + " -inter 500 -add " + QString("\"") + Path::getTempPath() + "/x265Encoder_video_out.hevc\"" + ":forcesync -par 1=1:1 " + QString("\"")  + outputFile + QString("\"") + " -new";
        mp4box->setProcessChannelMode(QProcess::MergedChannels);
        connect(mp4box, SIGNAL(readyReadStandardOutput()), this, SLOT(redirectOutput()));
        connect(mp4box, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(deleteTempFiles()));
        connect(mp4box, SIGNAL(error(QProcess::ProcessError)), this, SLOT(onError(QProcess::ProcessError)));
        mp4box->start(mp4boxArgs);
        pauseClicked = false;
    }
    else if(outputFile.endsWith(".mkv",Qt::CaseInsensitive))
    {
        if(audioAvailability)
        {
            if(QString::compare(fileContainer->audioFormat.at(0), "PCM", Qt::CaseInsensitive) == 0)
                mp4boxArgs = Addons::mp4boxExe() + " -inter 500 -add " + QString("\"") + Path::getTempPath() + "/x265Encoder_video_out.hevc\"" + ":forcesync -par 1=1:1 -add " + QString("\"") + Path::getTempPath() + "/x265Encoder_audio_out.mov\" " + QString("\"")  + Path::getTempPath() + "/x265Encoder_video_out.mp4\" "  + " -new";
            else if(QString::compare(fileContainer->audioFormat.at(0), "FLAC", Qt::CaseInsensitive) == 0)
                mp4boxArgs = Addons::mp4boxExe() + " -inter 500 -add " + QString("\"") + Path::getTempPath() + "/x265Encoder_video_out.hevc\" "  + QString("\"") +  Path::getTempPath() + "/x265Encoder_video_out.mp4\" "  + " -new";
            else
                mp4boxArgs = Addons::mp4boxExe() + " -inter 500 -add " + QString("\"") + Path::getTempPath() + "/x265Encoder_video_out.hevc\"" + ":forcesync -par 1=1:1 -add " + QString("\"") + Path::getTempPath() + "/x265Encoder_audio_out.mp4\" " + QString("\"") +  Path::getTempPath() + "/x265Encoder_video_out.mp4\" " +  " -new";
        }
        else
             mp4boxArgs = Addons::mp4boxExe() + " -inter 500 -add " + QString("\"") + Path::getTempPath() + "/x265Encoder_video_out.hevc\"" + ":forcesync -par 1=1:1 "  + QString("\"")  +  Path::getTempPath() + "/x265Encoder_video_out.mp4\" "  + " -new";
        mp4box->setProcessChannelMode(QProcess::MergedChannels);
        connect(mp4box, SIGNAL(readyReadStandardOutput()), this, SLOT(redirectOutput()));
        connect(mp4box, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(muxMkv()));
        connect(mp4box, SIGNAL(error(QProcess::ProcessError)), this, SLOT(onError(QProcess::ProcessError)));
        mp4box->start(mp4boxArgs);
        pauseClicked = false;
    }
}

/* Remux output into mkv container */
void Encoder::muxMkv()
{
   QString outputFile(setList.at(0)->ui->txtDest->text() + "/" + setList.at(0)->ui->txtFile->text());
   QString ffmpegArgs;
   if(subAvailability && QString::compare(fileContainer->audioFormat.at(0), "FLAC", Qt::CaseInsensitive) == 0 && QString::compare(fileContainer->subFormat.at(0), "Timed Text", Qt::CaseInsensitive) != 0)
      ffmpegArgs = Addons::FFmpegExe() + " -i " + QString("\"") + Path::getTempPath() + "/x265Encoder_video_out.mp4\" " + " -i " + QString("\"") + Path::getTempPath() + "/x265Encoder_audio_out.mkv\" " + " -i " + QString("\"") + Path::getTempPath() + "/x265Encoder_sub_out.mkv\" " + " -acodec copy -vcodec copy -scodec copy -map 0 -map 1 -map 2 " + QString("\"")  + outputFile + QString("\"") + " -y";
   else if (QString::compare(fileContainer->audioFormat.at(0), "FLAC", Qt::CaseInsensitive) == 0)
      ffmpegArgs = Addons::FFmpegExe() + " -i " + QString("\"") + Path::getTempPath() + "/x265Encoder_video_out.mp4\" " + " -i " + QString("\"") + Path::getTempPath() + "/x265Encoder_audio_out.mkv\" " + " -acodec copy -vcodec copy -map 0 -map 1 " + QString("\"")  + outputFile + QString("\"") + " -y";
   else
      ffmpegArgs = Addons::FFmpegExe() + " -i " + QString("\"") + Path::getTempPath() + "/x265Encoder_video_out.mp4\" " + " -acodec copy -vcodec copy -map 0 " + QString("\"")  + outputFile + QString("\"") + " -y";
   ffmpegMux = new QProcess(this);
   ffmpegMux->setProcessChannelMode(QProcess::MergedChannels);
   connect(ffmpegMux, SIGNAL(readyReadStandardOutput()), this, SLOT(redirectOutput()));
   connect(ffmpegMux, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(deleteTempFiles()));
   connect(ffmpegMux, SIGNAL(error(QProcess::ProcessError)), this, SLOT(onError(QProcess::ProcessError)));
   ffmpegMux->start(ffmpegArgs);
   pauseClicked = false;
}

/* Deletes the instance of sender process, temporary files generated, dequeue completed file from the file queue and starts encoding process again */
void Encoder::deleteTempFiles()
{
    QProcess *p = (QProcess *)sender();
    if(!pauseClicked)
    {
        if(p->exitCode() != 0)
        {
            displayError();
            p->deleteLater();
            return;
        }
        p->deleteLater();
        progDial->increment();
        progDial->incrementSingle();
    }
    mp4box = NULL;
    currentState = DEL_TEMP;
    deleteTemp();
    firstTranscode = false;
    fileContainer->deQueue(fileIndex);
    pauseClicked = false;
    resumeClicked = false;
    fileContainer->totalFrames.removeAt(0);
    fileContainer->audioFormat.removeAt(0);
    fileContainer->subFormat.removeAt(0);
    fileContainer->totalAudioFrames.removeAt(0);
    fileContainer->frameRate.removeAt(0);
    fileContainer->bitRate.removeAt(0);
    fileContainer->videoHeight.removeAt(0);
    fileContainer->videoWidth.removeAt(0);
    fileContainer->videoDepth.removeAt(0);
    fileContainer->subSampling.removeAt(0);
    fileContainer->fps.removeAt(0);
    movie->stop();
    QLabel *processLabel = new QLabel(this);
    processLabel->setPixmap(QPixmap(":/Images/encode_success.png"));
    processLabel->setAlignment(Qt::AlignCenter);
    progDial->ui->outputQueue->setCellWidget(outputQueueIndex, 0, processLabel);
    progDial->setValSingle(0);
    outputEncoded.append(QString::number(outputQueueIndex));
    outputQueueIndex++;
    encode();
}

void Encoder::killCurrentProcess()
{
    switch (currentState)
    {
    case DEMUX:
        disconnect(ffmpegAudio, SIGNAL(finished(int,QProcess::ExitStatus)), 0, 0);
        ffmpegAudio->kill();
        ffmpegAudio = NULL;
        break;
    case MUX:
        disconnect(mp4box, SIGNAL(finished(int,QProcess::ExitStatus)), 0, 0);
        mp4box->kill();
        mp4box = NULL;
        break;
    case VIDEO_ENCODE:
        if(ffmpegVideoActive)
        {
            disconnect(ffmpegVideo, SIGNAL(finished(int,QProcess::ExitStatus)), 0, 0);
            ffmpegVideo->kill();
        }
        disconnect(x265, SIGNAL(finished(int,QProcess::ExitStatus)), 0, 0);
        if(x265)
            x265->kill();
        ffmpegVideo = NULL;
        x265 = NULL;
        break;
    case SECOND_PASS:
        if (ffmpegVideoActive)
        {
            disconnect(ffmpegVideoSec, SIGNAL(finished(int,QProcess::ExitStatus)), 0, 0);
            ffmpegVideoSec->kill();
        }
        disconnect(x265Sec, SIGNAL(finished(int,QProcess::ExitStatus)), 0, 0);
        x265Sec->kill();
        ffmpegVideoSec = NULL;
        x265Sec = NULL;
        break;
    default:
        break;
    }
}


/* Checks for the current state of the encoder and stops corresponding process and delete temporary files */
void Encoder::btnStopClicked()
{
    QMessageBox::StandardButton reply;
      reply = QMessageBox::question(this, "Confirm", "Do You Want to Stop Encoding?",
                                    QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::No || !fileContainer->count()) return;
    stopClicked = true;
    if (!progDial->isPaused())
        killCurrentProcess();
    deleteTemp();
    resumeClicked = false;
    currentState = NULL_STATE; // set the state of the encoder to NULL_STATE
    progDial->ui->btnPause->hide();
    progDial->ui->btnResume->hide();
    progDial->ui->btnStop->hide();
    progDial->setWindowFlags(Qt::Dialog);
    progDial->setModal(true);
    progDial->show();
    progDial->setTitleBar("Stopped");
    for(int i = outputQueueIndex; i < progDial->ui->outputQueue->rowCount(); i++)
    {
        QLabel *processLabel = new QLabel(this);
        processLabel->setPixmap(QPixmap(":/Images/encode_failed.png"));
        processLabel->setAlignment(Qt::AlignCenter);
        progDial->ui->outputQueue->setCellWidget(i, 0, processLabel);
    }
    if(outputEncoded.count() >= 1)
        progDial->ui->btnOutputFolder->show();
    for(int i = 0; i < outputEncoded.count(); i++)
    {
        progDial->ui->outputQueue->cellWidget(i, 2)->setEnabled(true);
        progDial->ui->outputQueue->cellWidget(i, 3)->setEnabled(true);
    }
    if(taskbarProgress->isPaused())
        taskbarProgress->setPaused(false);
    taskbarProgress->setVisible(false);
    this->fileContainer->setAcceptDrops(true);
    QFile::remove(setList.at(0)->ui->txtLogFile->text()); // delete log file, if present
}

/* Checks for the current state of the encoder and stops corresponding process */
void Encoder::btnPauseClicked()
{
    progDial->setTitleBar("Paused");
    taskbarProgress->setPaused(true);
    killCurrentProcess();
    if(currentState == VIDEO_ENCODE || currentState == SECOND_PASS)
    {
        progDial->setVal(progPrev);
        progDial->setValSingle(progPrevSingle);
    }
    pauseClicked = true;
    progDial->showPause(false);
    if(movie)
        movie->setPaused(true);
    if(outputEncoded.count() >= 1)
        progDial->ui->btnOutputFolder->show();
    for(int i = 0; i < outputEncoded.count(); i++)
    {
        progDial->ui->outputQueue->cellWidget(i, 2)->setEnabled(true);
        progDial->ui->outputQueue->cellWidget(i, 3)->setEnabled(true);
    }
}

/* Checks for the current state of the encoder and starts corresponding process */
void Encoder::btnResumeClicked()
{
    // Delete the contents of csv file, before appending
    resumeClicked = true;
    if(currentState == VIDEO_ENCODE || currentState == SECOND_PASS)
    {
        QFile csvFile;
        csvFile.setFileName(setList.at(0)->ui->txtLogFile->text());
        if(csvFile.exists())
        {
            csvFile.resize(0);
            csvFile.close();
        }
    }

    progDial->showPause(true);
    taskbarProgress->setPaused(false);
    if(movie)
        movie->setPaused(false);
    if(!progDial->ui->btnOutputFolder->isHidden())
        progDial->ui->btnOutputFolder->hide();
    for(int i = 0; i < outputEncoded.count(); i++)
    {
        progDial->ui->outputQueue->cellWidget(i, 2)->setEnabled(false);
        progDial->ui->outputQueue->cellWidget(i, 3)->setEnabled(false);
    }
    switch (currentState)
    {
    case DEMUX:
        encode();
        break;
    case MUX:
        mergeAudio();
        break;
    case VIDEO_ENCODE:
        if (setList.at(0)->ui->stackedWidget->currentIndex())
            advancedEncodeVideo();
        else
            encodeVideo();
        break;
    case SECOND_PASS:
        secondPass();
        break;
    default:
        break;
    }

}

void Encoder::btnOutputClicked()
{
    if(!QDesktopServices::openUrl(QUrl::fromLocalFile(outputDestination.at(0)))){
        displayMessage("Could not open folder", QMessageBox::Critical);
    }
}

void Encoder::setFileContainer()
{
    this->fileContainer = FileContainer::instance();
    ui->verticalLayout1->addWidget(this->fileContainer);

    // TODO: dont use fixed values
    this->fileContainer->setMinimumWidth(450);
    connect(this->fileContainer, SIGNAL(currentRowChanged(int)), this, SLOT(on_currentRowChanged(int)));
    dummySetting = new SettingsWidget();
    dummySetting->setEnabled(false);
    QString bgColor = palette().color(QPalette::Normal, QPalette::Window).name();
    dummySetting->setStyleSheet("#grpOutputFolder{color:#777;background:" + bgColor + ";}\
                                #frame{border:2px solid #ccc}");
    QString sldDisableStyle = "QSlider::groove:horizontal {\
                border: 1px solid #bbb;\
                height: 3px; \
                background-color: #ddd;\
                margin: 2px 0;\
                }\
            QSlider::handle:horizontal {\
                background: #ddd;\
                border: 1px solid #bbb;\
                width: 8px;\
                margin: -8px 0;\
                border-radius: 2px;\
            }";
    dummySetting->ui->sldBitRate->setStyleSheet(sldDisableStyle);
    dummySetting->ui->sldPreset->setStyleSheet(sldDisableStyle);
    dummySetting->ui->sldCpuUtilization->setStyleSheet(sldDisableStyle);
    dummySetting->txtBasic.setStyleSheet("QLabel{\
                                         font-weight:bold;\
                                         /*font-size:12px;*/\
                                         border: 1px solid #bbb;\
                                         border-bottom:none;\
                                         background:#fff;\
                                         border-bottom:0px;\
                                         color:#999;\
                                         padding: 7px 0px;\
                                         qproperty-alignment:AlignCenter;}");
    dummySetting->ui->btnCap->setStyleSheet("QPushButton {\
                                            border: 1px solid #aaa;\
                                            border-radius: 5px;\
                                            background:none;\
                                            background-color: #ddd;\
                                            /*font-size:11px;*/\
                                            min-height:25px;\
                                            min-width:95px;\
                                         }");
//    QGraphicsDropShadowEffect *capEffect = new QGraphicsDropShadowEffect();
//    capEffect->setBlurRadius(1);
//    capEffect->setOffset(1,1);
//    dummySetting->ui->btnCap->setGraphicsEffect(capEffect);
    ui->verticalLayout2->addWidget(dummySetting);
    ui->btnRemove->setDisabled(true);
    ui->btnRowDown->setDisabled(true);
    ui->btnRowUp->setDisabled(true);
    ui->btnClear->setDisabled(true);
    ui->btnSave->setDisabled(true);
    ui->btnEncode->setDisabled(true);
}

void Encoder::on_currentRowChanged(int currentRow)
{
    ui->btnClear->setEnabled(currentRow >= 0 && fileContainer->count());
    ui->btnRemove->setEnabled(currentRow >= 0 && fileContainer->count());
    ui->btnRowDown->setEnabled(currentRow >= 0 && fileContainer->count() >= 2);
    ui->btnRowUp->setEnabled(currentRow >= 0 && fileContainer->count() >= 2);
    ui->btnSave->setEnabled(currentRow >= 0 && fileContainer->count());
    ui->btnEncode->setEnabled(currentRow >= 0 && fileContainer->count());
    if(currentRow >= 0 && fileContainer->count())
    {
        ui->lblHeader3->setStyleSheet("color:rgb(90,90,90);padding-bottom:20px;/*font-size:33px;*/");
        ui->btnEncode->setStyleSheet(btnEncodeStyle);
        ui->btnRemove->setStyleSheet(btnOtherStyle);
        ui->btnClear->setStyleSheet(btnOtherStyle);
        ui->btnRowUp->setStyleSheet(btnOtherStyle);
        ui->btnRowDown->setStyleSheet(btnOtherStyle);
        ui->btnSave->setStyleSheet(btnOtherStyle);
    }
    else
    {
        ui->lblHeader3->setStyleSheet("color:rgb(140,140,140);padding-bottom:20px;/*font-size:33px;*/");
        ui->btnEncode->setStyleSheet(btnEncDisStyle);
        ui->btnRemove->setStyleSheet(btnOthDisStyle);
        ui->btnClear->setStyleSheet(btnOthDisStyle);
        ui->btnRowUp->setStyleSheet(btnOthDisStyle);
        ui->btnRowDown->setStyleSheet(btnOthDisStyle);
        ui->btnSave->setStyleSheet(btnOthDisStyle);
    }
    if(currentRow >= 0 && fileContainer->count() >= 2)
    {
        ui->btnRowUp->setStyleSheet(btnOtherStyle);
        ui->btnRowDown->setStyleSheet(btnOtherStyle);
    }
    else
    {
        ui->btnRowUp->setStyleSheet(btnOthDisStyle);
        ui->btnRowDown->setStyleSheet(btnOthDisStyle);
    }
}

/*
 * If instance of Encoder class is not created, it Creates an instance and returns pointer to it.
 */
Encoder *Encoder::instance()
{
    static Encoder *enc = NULL;
    if(!enc)
        enc = new Encoder();
    return enc;
}

bool Encoder::fileExists()
{
    QFile inFile(fileContainer->item(fileIndex)->text());
    return inFile.exists();
}

/*
 * Sends the standard output of the processes for printing.
 */
void Encoder::redirectOutput()
{
    QProcess *p = (QProcess *)sender();
    QString consoleOutput(p->readAllStandardOutput());
    progDial->printOutput(consoleOutput);
    if((currentState == VIDEO_ENCODE || currentState == SECOND_PASS) && framesAvailable)
        setProgressBar(consoleOutput);
}

void Encoder::setProgressBar(QString consoleOutput)
{
    if(pauseClicked)
        return;
    QRegExp regex;
    regex.setPattern("\\[\\d{1,3}(\\.\\d{1,2})?%\\]");
    float val;
    int pos = 0;
    bool flag = false;
    while((pos = regex.indexIn(consoleOutput,pos)) != -1)
    {
        int length = regex.matchedLength();
        QString tempString = consoleOutput.mid(pos + 1,length - 3);
        try
        {
            val = tempString.toFloat();
            flag = true;
            break;
        }
        catch(...)
        {
            return;
        }
        pos += length;
    }
    if(flag && setList.at(0)->ui->stackedWidget->currentIndex() && setList.at(0)->ui->checkTwoPass->isChecked())
    {
         progDial ->setVal(ceil(progPrev + (val/4.0)));
         progDial ->setValSingle(ceil(progPrevSingle +(val/4.0)));
    }
    else if (flag)
    {
         progDial ->setVal(ceil(progPrev + (val/2.0)));
         progDial ->setValSingle(ceil(progPrevSingle + (val/2.0)));
    }
    else{
       pos = 0;
       regex.setPattern("encoded \\d{1,} frames");
       pos = regex.lastIndexIn(consoleOutput);

       if (pos != -1)
       {
           regex.setPattern("\\d{1,}");
           pos = regex.indexIn(consoleOutput, pos);
           int length = regex.matchedLength();
           encodedFrames = consoleOutput.mid(pos, length);
       }
   }
}

void Encoder::deleteFfmpegVideo()
{
    QProcess *p = (QProcess *)sender();
    if(p->exitCode() != 0)
    {
        displayError();
    }
    p->deleteLater();
    ffmpegVideoActive = false;
}

void Encoder::on_btnHelp_clicked()
{
    QString link = "https://x265.com/create-hevc-video/";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void Encoder::on_btnAbout_clicked()
{
    aboutDialog= new AboutDialog(this);
    aboutDialog->layout()->setSizeConstraint(QLayout::SetFixedSize);
    aboutDialog->setModal(true);
    aboutDialog->show();
    this->fileContainer->setAcceptDrops(false);
}

void Encoder::onError(QProcess::ProcessError)
{
    if(!pauseClicked && !stopClicked)
    {
        displayError();
        QProcess *p = (QProcess *)sender();
        disconnect(p, SIGNAL(finished(int,QProcess::ExitStatus)), 0, 0);
    }
}

void Encoder::onProgCancel()
{
    progCancelClicked = true;
}

void Encoder::displayMessage(QString message, QMessageBox::Icon icon)
{
    QMessageBox msg;
    msg.setText(message);
    msg.setIcon(icon);
    msg.exec();
}

/* Delete temporary files */
void Encoder::deleteTemp()
{
    QString path = Path::getTempPath();
    QDir dir(path);
    dir.setNameFilters(QStringList() << "x265Encoder_video_out.hevc" << "x265Encoder_audio_out.mp4" << "x265Encoder_audio_out.mkv" << "x265Encoder_sub_out.mkv" <<"x265Encoder_audio_out.mov" << "x265Encoder_video_out.mp4" << "x265_2pass.log" << "x265_2pass.log.cutree" );
    dir.setFilter(QDir::Files);
    foreach (QString dirFile, dir.entryList())
    {
        dir.remove(dirFile);
    }
}

void Encoder::displayError()
{
    currentState = NULL_STATE;
    displayMessage("Sorry! Some Error Occurred", QMessageBox::Critical);
    progDial->close();
    taskbarProgress->setVisible(false);
    this->fileContainer->setAcceptDrops(true);
    deleteTemp();
}

void Encoder::getBanner()
{
    QString banner_url_str = "http://localhost/banner.php";
    QSettings settings;
    int timeStamp = settings.value("BannerTimeStamp",0).toInt();
    QNetworkAccessManager NAManager(this);
    QUrl url(banner_url_str);
    QNetworkRequest request(url);
    QUrlQuery data;
    data.addQueryItem("timestamp",QString::number(timeStamp));
    request.setHeader(QNetworkRequest::ContentTypeHeader,
                      "application/x-www-form-urlencoded");
    QNetworkReply *reply = NAManager.post(request, data.toString(QUrl::FullyEncoded).toUtf8());
    QEventLoop eventLoop;
    QObject::connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    eventLoop.exec();
    if (reply->error())
        return;
    QString response = reply->readAll();
    if ( response.length() == 0)
        return;
    QDomDocument dom;
    dom.setContent(response);
    QDomElement document = dom.documentElement();
    QDomNodeList nodelist = document.elementsByTagName("file");
    if (nodelist.count() == 0)
        return;
    QDomElement ele = nodelist.at(0).toElement();
    QString imgPath = ele.text();
    nodelist = document.elementsByTagName("timestamp");
    if (nodelist.count() == 0)
        return;
    ele = nodelist.at(0).toElement();
    timeStamp = ele.text().toInt();
    settings.setValue("BannerTimeStamp", timeStamp);
    QUrl image_url(imgPath);
    QNetworkRequest imagerequest(image_url);
    reply->deleteLater();
    QNetworkReply  *imageReply = NAManager.get(imagerequest);
    QEventLoop imageLoop;
    QObject::connect(imageReply, SIGNAL(finished()), &imageLoop, SLOT(quit()));
    imageLoop.exec();
    QByteArray imageArray = imageReply->readAll();
    QPixmap pixmap;
    pixmap.loadFromData(imageArray);
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    if (!dir.exists())
        dir.mkdir(dir.absolutePath());
    QFile imageFile(dir.absolutePath()+ "/banner_new.png");
    imageFile.open(QIODevice::WriteOnly);
    pixmap.save(&imageFile);
    imageFile.close();
    QFile xmlFile(dir.absolutePath()+"/banner_new.xml");
    xmlFile.open(QIODevice::WriteOnly);
    QTextStream out(&xmlFile);
    out << response;
    xmlFile.close();
}

bool Encoder::loadBanner(QString filename)
{
    QFile imageFile(filename + ".png");
    if (!imageFile.exists())
        return false;
    QImageReader imageReader(&imageFile);
    if (imageReader.error())
        return false;
    QFile xmlFile(filename + ".xml");
    if (!xmlFile.exists())
        return false;
    QDomDocument dom;
    dom.setContent(&xmlFile);
    QDomElement document = dom.documentElement();
    QDomNodeList nodelist = document.elementsByTagName("url");
    if (nodelist.count() == 0)
        return false;
    QDomElement ele = nodelist.at(0).toElement();
    externalUrl = ele.text();
    imageFile.close();
    xmlFile.close();
    return true;
}

void Encoder::setBanner(QString filename)
{
    QPixmap pixmap;
    pixmap.load(filename);
    connect(&lblBanner, SIGNAL(clicked()), this, SLOT(bannerClicked()));
    lblBanner.setPixmap(pixmap);
    lblBanner.setMaximumWidth(158);
    lblBanner.setMaximumHeight(200);
    ui->verticalLayout3->insertWidget(3,&lblBanner);
}

void Encoder::prepareOutputQueue()
{
    folderMapper = new QSignalMapper(this);
    playMapper = new QSignalMapper(this);

    progDial->ui->outputQueue->setColumnCount(4);
    progDial->ui->outputQueue->horizontalHeader()->hide();
    for(int i = 0; i < fileContainer->count(); i++)
    {
        QTableWidgetItem *item = new QTableWidgetItem();
        QPushButton *btnFolder = new QPushButton("");
        QPushButton *btnPlay = new QPushButton("");
        btnFolder->setIcon(QIcon(":/Images/folder-icon.png"));
        btnPlay->setIcon(QIcon(":/Images/play-icon.png"));
        btnFolder->setToolTip("Locate file");
        btnPlay->setToolTip("Play file");
        btnPlay->setEnabled(false);
        btnFolder->setEnabled(false);
        connect(btnFolder, SIGNAL(clicked()), folderMapper, SLOT(map()));
        connect(btnPlay, SIGNAL(clicked()), playMapper, SLOT(map()));
        folderMapper->setMapping(btnFolder, i);
        playMapper->setMapping(btnPlay, i);
        if(setList.at(i)->ui->txtFile->text().contains(".mp4"))
        {
           QString txtFile = setList.at(i)->ui->txtFile->text();
           txtFile.remove(".mp4");
           setList.at(i)->ui->txtFile->setText(txtFile);
        }
        else if (setList.at(i)->ui->txtFile->text().contains(".mkv"))
        {
            QString txtFile = setList.at(i)->ui->txtFile->text();
            txtFile.remove(".mkv");
            setList.at(i)->ui->txtFile->setText(txtFile);
        }
        if(setList.at(i)->ui->comboContainer->currentIndex() == 0)
        {
            setList.at(i)->ui->txtFile->setText(setList.at(i)->ui->txtFile->text() + ".mp4");
        }
        else
        {
            setList.at(i)->ui->txtFile->setText(setList.at(i)->ui->txtFile->text() + ".mkv");
        }
        outputDestination.append(setList.at(i)->ui->txtDest->text());
        outputFile.append(setList.at(i)->ui->txtFile->text());
        item->setText(outputFile.at(i));
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        QLabel *processLabel = new QLabel(this);
        processLabel->setPixmap(QPixmap(":/Images/file_add.png"));
        processLabel->setAlignment(Qt::AlignCenter);
        progDial->ui->outputQueue->insertRow(i);
        progDial->ui->outputQueue->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
        progDial->ui->outputQueue->setCellWidget(i, 0, processLabel);
        progDial->ui->outputQueue->setItem(i, 1, item);
        progDial->ui->outputQueue->setCellWidget(i, 2, btnFolder);
        progDial->ui->outputQueue->setCellWidget(i, 3, btnPlay);
        progDial->ui->outputQueue->resizeColumnsToContents();
    }
    connect(folderMapper, SIGNAL(mapped(int)), this, SLOT(openFolder(int)));
    connect(playMapper, SIGNAL(mapped(int)), this, SLOT(playVideo(int)));
}

void Encoder::openFolder(int clickedRow)
{
    QString expArgs = "Explorer /select,";
    expArgs += QDir::toNativeSeparators(outputDestination.at(clickedRow) + "/" +
                            outputFile.at(clickedRow));
    if(!QProcess::startDetached(expArgs)){
        displayMessage("Could not open the folder", QMessageBox::Critical);
    }
}

void Encoder::playVideo(int clickedRow)
{
    if(!QDesktopServices::openUrl(QUrl::fromLocalFile(outputDestination.at(clickedRow) + "/" +
                              outputFile.at(clickedRow)))){
        displayMessage("Could not play the file", QMessageBox::Critical);
    }
}

void Encoder::loadBanner()
{
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    if (!dir.exists())
        return;
    if (loadBanner(dir.absolutePath() + "/banner_new")) {
       setBanner(dir.absolutePath() + "/banner_new.png");
       QFile imageFile(dir.absolutePath() + "/banner.png");
       QFile imageFileNew(dir.absolutePath() + "/banner_new.png");
       QFile xmlFile(dir.absolutePath() + "/banner.xml");
       QFile xmlFileNew(dir.absolutePath() + "/banner_new.xml");
       imageFile.remove();
       xmlFile.remove();
       imageFileNew.rename(imageFile.fileName());
       xmlFileNew.rename(xmlFile.fileName());
       imageFileNew.close();
        xmlFileNew.close();
    }
    else if (loadBanner(dir.absolutePath()+ "/banner"))
        setBanner(dir.absolutePath()+ "/banner.png");
    else
        externalUrl = "";
}

void Encoder::on_btnLoad_clicked()
{
    if(lastOpenDirectory.isEmpty())
        lastOpenDirectory = QStandardPaths::writableLocation(QStandardPaths::MoviesLocation);
    QString fileName = QFileDialog::getOpenFileName(
                             this,
                             "Load Settings",
                             lastOpenDirectory,
                             "x265 Settings (*.xml);;");
    if (fileName.length() == 0)
        return;
    settutils.loadsettings(setList,fileName);
    lastOpenDirectory = fileName.left(fileName.lastIndexOf('/') + 1);
}

void Encoder::bannerClicked()
{
    QDesktopServices::openUrl(QUrl(externalUrl,QUrl::TolerantMode));
}

void Encoder::on_btnSave_clicked()
{
    QString filePath = QFileDialog::getSaveFileName(this, tr("Save Settings"),
                                             lastOpenDirectory,
                                             tr("XML(*.xml)"));
    if(filePath.length()==0)
        return;
    settutils.saveSettings(setList,filePath);
    lastOpenDirectory = filePath.left(filePath.lastIndexOf('/') + 1);
}

