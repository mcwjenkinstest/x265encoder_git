#include <QPainter>
#include <QDir>
#include <iostream>
#include <QListWidgetItem>
#include <QMessageBox>
#include <QFont>
#include <QFileInfo>
#include <QRgb>
#include <QHeaderView>
#include <QDateTime>
#include <QStorageInfo>
#include "y4mutils.h"
#include "encoder.h"
#include "ui_encoder.h"
#include "ui_settingswidget.h"
#include "ui_yuvdialog.h"
#include "filecontainer.h"
#include "mediainfo.h"
#include "yuvdialog.h"

using namespace std;

/* Constructor */
FileContainer::FileContainer(QWidget *parent) : QListWidget(parent), selectionFrame(this)
  , animation(&selectionFrame, "geometry")
{
    setAcceptDrops(true);
    y4mWarnMsg = new QMessageBox(parent);
    y4mWarnMsgCheck = new QCheckBox();
    y4mWarnMsg->setCheckBox(y4mWarnMsgCheck);
    y4mWarnMsg->setWindowTitle("Warning");
    y4mWarnMsg->setIcon(QMessageBox::Warning);
    y4mWarnMsg->setText("Headers are missing for y4m file. Assuming bit depth 8 and subsampling YUV(4:2:0)");
    y4mWarnMsg->setStandardButtons(QMessageBox::Ok);
    y4mWarnMsgCheck->setText("Don't show this again.");
    y4mWarnMsg->setModal(true);
    connect(this, SIGNAL(itemSelectionChanged()), this, SLOT(setSelectedItemSettings()));
    connect((QObject *)this->verticalScrollBar(), SIGNAL(valueChanged(int)), this,SLOT(updateSelection()));
    connect((QObject *)this->horizontalScrollBar(), SIGNAL(valueChanged(int)), this,SLOT(updateSelection()));
    curIndex = -1;
    setBackground();
    setFrameShadow(QFrame::Raised);
    //font.setPixelSize(12);
    //selectedFont.setPixelSize(13);
    selectionFrame.setAttribute(Qt::WA_TransparentForMouseEvents);
    selectionFrame.setFocusPolicy(Qt::NoFocus);
    selectionFrame.setStyleSheet("background: solid #bbb;");
    selectionFrame.hide();
    itemShadow.setOffset(2, 2);
    itemShadow.setBlurRadius(15);
    settingShadow.setOffset(2, 2);
    settingShadow.setBlurRadius(15);
    selectionFrame.setGraphicsEffect(&itemShadow);
    selectionFrame.setFont(selectedFont);
    animation.setEasingCurve(QEasingCurve::InOutBack);
    setItemDelegate(new RemoveSelectionDelegate(this));
    outputFolder = "";
    dropProgCancelClicked = false;
    loadxml = false;
    connect(&dlg, SIGNAL(canceled()), this, SLOT(onDropProgCancel()));
}

/* Creates and returns an instance of the FileContainer class if not already cretaed */
FileContainer *FileContainer::instance()
{
    static FileContainer *fileContainer = NULL;
    if(!fileContainer)
        fileContainer = new FileContainer();
    return fileContainer;
}

FileContainer::~FileContainer()
{
}

/* Add files to the queue, if mediainfo recognizes the file as video */
void FileContainer::queueAdd(QString fileName, bool addSett)
{
    generalInfo = MediaInfo::getGeneralDetails(QString("\"") + fileName + QString("\""));
    float requiredSpace = 0.0;
    if (!generalInfo.at(2).isEmpty() && !generalInfo.at(2).isNull())
         requiredSpace = generalInfo.at(2).toDouble() /(1024*1024*1024) * 0.75;
    QStorageInfo storage(QDir::tempPath());
    if(storage.bytesAvailable()/1024/1024/1024 < requiredSpace)
    {
         Encoder::instance()->displayMessage("Insufficient storage space in the current temporary location. File is not added to the queue."
                                             " Please change the path of the User environment variables "
                                             + QString("\"") + "TEMP" + QString("\"") + " and " + QString("\"") + "TMP" + QString("\"") +
                                             " to a location with enough storage space.",
                                             QMessageBox::Information);
         return;
    }
    if(QString::compare(generalInfo.at(1), "mp4", Qt::CaseInsensitive) != 0 && QString::compare(generalInfo.at(1), "mkv", Qt::CaseInsensitive) != 0 && QString::compare(generalInfo.at(1), "mov", Qt::CaseInsensitive) != 0 && QString::compare(generalInfo.at(1), "avi", Qt::CaseInsensitive) != 0  && QString::compare(generalInfo.at(1), "y4m", Qt::CaseInsensitive) != 0  && QString::compare(generalInfo.at(1), "yuv", Qt::CaseInsensitive) != 0 && QString::compare(generalInfo.at(1), "h264", Qt::CaseInsensitive) != 0)
    {
        Encoder::instance()->displayMessage("Input File Format is not supported!",
                                            QMessageBox::Information);
        return;
    }


    videoInfo = MediaInfo::getVideoDetails(QString("\"") + fileName + QString("\""));

    if((videoInfo.at(0).isEmpty() || videoInfo.at(0).isNull()) && generalInfo.at(1).compare("YUV", Qt::CaseInsensitive) == 0)
    {
        if(!loadxml)
        {
            QStringList file = generalInfo.at(0).split( "/" );
            QString fileName = file.value( file.length() - 1);
            QStringList fileResolution = fileName.split( "x" );
            QStringList fileFrameRate = fileName.split( "fps" );
            int fileResolutionLength = fileResolution.length();
            int fileFrameRateLength = fileFrameRate.length();
            QString fileWidth, fileHeight, fileFps;
            if(fileResolutionLength == 2)
            {
               QStringList resWidth = fileResolution.at(0).split( "_" );
               QStringList resHeight = fileResolution.at(1).split( "_" );
               fileWidth = resWidth.value(resWidth.length() - 1);
               fileHeight = resHeight.value(0);
            }
            if(fileFrameRateLength == 2)
            {

               QStringList leftFps = fileFrameRate.at(0).split( "_");
               fileFps = leftFps.value(leftFps.length() - 1);
            }

            YUVDialog *YuvDialog = new YUVDialog();
            YuvDialog->setWindowTitle("Settings for " + fileName +".yuv");
            YuvDialog->layout()->setSizeConstraint(QLayout::SetFixedSize);

            if(!fileWidth.isNull() && !fileHeight.isNull())
            {
               YuvDialog->ui->Width->setText(fileWidth);
               YuvDialog->ui->Height->setText(fileHeight);
            }
            if(!fileFps.isNull())
               YuvDialog->ui->fps->setValue(fileFps.toDouble());

            YuvDialog->exec();
            if(YuvDialog->Cancel)
               return;

            int height = YuvDialog->ui->Height->text().toInt();
            int width =  YuvDialog->ui->Width->text().toInt();
            int depth =  YuvDialog->ui->bps->value();
            QString subSample = YuvDialog->ui->Subsampling->currentText();
            videoDepth  << QString::number(depth);
            videoColorSpace << YuvDialog->ui->ColorSpace->currentText();
            subSampling << YuvDialog->ui->Subsampling->currentText();
            fps << QString::number(YuvDialog->ui->fps->value());
            videoHeight << QString::number(height);
            videoWidth  << QString::number(width);
            unsigned long long int size = generalInfo.at(2).toLongLong();
            frameRate << QString::number(YuvDialog->ui->fps->value());
            totalFrames << QString::number(Y4MUtils::instance()->frameCount(width, height, size, depth , subSample));
        }
        bitRate << "";
        audioInfo = MediaInfo::getAudioDetails(QString("\"") + fileName + QString("\""));
        audioFormat << audioInfo.at(0);
        totalAudioFrames << audioInfo.at(1);
        subFormat << "";
    }
    else
    {
        if(QString::compare(videoInfo.at(0), "AVC", Qt::CaseInsensitive) != 0  && QString::compare(videoInfo.at(0), "YUV", Qt::CaseInsensitive) != 0
                && QString::compare(videoInfo.at(0), "RGB", Qt::CaseInsensitive) != 0 && QString::compare(videoInfo.at(0), "RGBA", Qt::CaseInsensitive) != 0)
        {
            Encoder::instance()->displayMessage("Unknown Format or Corrupted File!",
                                                QMessageBox::Information);
            return;
        }
        if((videoInfo.at(3).isEmpty() || videoInfo.at(3).isNull()) && videoInfo.at(0).compare("YUV", Qt::CaseInsensitive) == 0)
        {
            totalFrames << QString::number(getY4MCount(fileName));
            if(!y4mWarnMsgCheck->isChecked())
            {
                setAcceptDrops(false);
                y4mWarnMsg->exec();
                setAcceptDrops(true);
            }
        }
        else
            totalFrames << videoInfo.at(3);
        frameRate << videoInfo.at(6);
        bitRate << videoInfo.at(7);
        videoHeight << videoInfo.at(2);
        videoWidth  << videoInfo.at(1);
        videoDepth  << videoInfo.at(4);
        videoColorSpace << "";
        subSampling << videoInfo.at(5);
        fps << QString::number(0);
        audioInfo = MediaInfo::getAudioDetails(QString("\"") + fileName + QString("\""));
        subInfo = MediaInfo::getSubDetails(QString("\"") + fileName + QString("\""));
        subFormat << subInfo.at(0);
        audioFormat << audioInfo.at(0);
        totalAudioFrames << audioInfo.at(1);

    }

    /* Auto-generate default fileName */
    QListWidgetItem * listItem;
    QString file;
    QFile inFile(fileName);
    QFileInfo fileInfo(inFile.fileName());
    QString appendText = "-hevc";
    QString prevFiles;
    QString preFileName;
    int existCount = -1;
    if(outputFolder.length() == 0)
        outputFolder = fileInfo.path();

    if(outputFolder.endsWith('/'))
        outputFolder.truncate(outputFolder.length() - 1);
    preFileName = outputFolder + "/" + fileInfo.completeBaseName() + appendText;

    for(int i = 0; i < count(); ++i)
    {
        listItem = item(i);
        file = listItem->text();
        if(QString::compare(fileName, file, Qt::CaseInsensitive) == 0)
        {
            prevFiles = Encoder::instance()->setList.at(i)->ui->txtDest->text() + "/"  + Encoder::instance()->setList.at(i)->ui->txtFile->text();
            if(prevFiles.startsWith(preFileName))
            {
                int m = prevFiles.length();
                int n = preFileName.length();
                QString extra =prevFiles.mid(n, m - n - 4);
                try
                {
                    int count = extra.toInt();
                    if(count > existCount)
                    {
                        existCount = count;
                    }
                }
                catch(...)
                {
                }
            }
        }
    }
    if(existCount >= 0)
    {
        existCount++;
        appendText = QString("%1.mp4").arg(existCount);
    }
    else
        appendText = QString(".mp4");

    if(existCount == -1)
        existCount++;
    QFile outFile(preFileName + appendText);
    bool fileExist = outFile.exists();
    while(fileExist)
    {
        existCount++;
        appendText = QString("%1.mp4").arg(existCount);
        QFile outFile(preFileName + appendText);
        fileExist = outFile.exists();
    }
    SettingsWidget *widget ;
    if (addSett) {
        widget = new SettingsWidget();
        Encoder::instance()->setList.append(widget);
        QFile DefSetFile (QDir::tempPath() + "/x265Encoder_DefSet.xml");
        if (FileContainer::instance()->count() == 0)
        {
            Encoder::instance()->setList.at(FileContainer::instance()->count())->ui->comboContainer->setCurrentIndex(Encoder::instance()->outContainer);
            if(!Encoder::instance()->DefBasicSettings.isEmpty())
            {
                Encoder::instance()->setList.at(FileContainer::instance()->count())->ui->sldBitRate->setValue(
                            Encoder::instance()->DefBasicSettings.at(0).toInt());
                Encoder::instance()->setList.at(FileContainer::instance()->count())->ui->sldPreset->setValue(
                            Encoder::instance()->DefBasicSettings.at(1).toInt());
                Encoder::instance()->setList.at(FileContainer::instance()->count())->ui->sldCpuUtilization->setValue(
                            Encoder::instance()->DefBasicSettings.at(2).toInt());
                Encoder::instance()->setList.at(FileContainer::instance()->count())->ui->lblCpuUtilization->setText(
                                            Encoder::instance()->DefBasicSettings.at(3));
            }
            else if (DefSetFile.exists())
            {
                widget->DefLoadSet(Encoder::instance()->setList.at(FileContainer::instance()->count()));
                widget->setSelectedSetting(1);
            }

        }
        if (FileContainer::instance()->count() >=1 ) {
            Encoder::instance()->setList.at(FileContainer::instance()->count())->ui->comboContainer->setCurrentIndex(
                                    Encoder::instance()->setList.at(FileContainer::instance()->count() -1)->ui->comboContainer->currentIndex());
            if (Encoder::instance()->setList.at(FileContainer::instance()->count() -1)->ui->stackedWidget->currentIndex() == 0)
            {
                Encoder::instance()->setList.at(FileContainer::instance()->count())->ui->sldBitRate->setValue(
                            Encoder::instance()->setList.at(FileContainer::instance()->count() -1)->ui->sldBitRate->value());
                Encoder::instance()->setList.at(FileContainer::instance()->count())->ui->sldPreset->setValue(
                            Encoder::instance()->setList.at(FileContainer::instance()->count() -1)->ui->sldPreset->value());
                Encoder::instance()->setList.at(FileContainer::instance()->count())->ui->sldCpuUtilization->setValue(
                            Encoder::instance()->setList.at(FileContainer::instance()->count() -1)->ui->sldCpuUtilization->value());
                Encoder::instance()->setList.at(FileContainer::instance()->count())->ui->lblCpuUtilization->setText(
                                            Encoder::instance()->setList.at(FileContainer::instance()->count() -1)->ui->lblCpuUtilization->text());
            }
            else
            {
                widget->DefSaveSet(Encoder::instance()->setList.at(FileContainer::instance()->count() -1));
                widget->DefLoadSet(Encoder::instance()->setList.at(FileContainer::instance()->count()));
                widget->setSelectedSetting(Encoder::instance()->setList.at(FileContainer::instance()->count() -1)->ui->stackedWidget->currentIndex());
            }

        }
        widget->ui->txtDest->setText(outputFolder);
        appendText.remove(".mp4");
        if (QString::compare(audioFormat.at(count()), "FLAC", Qt::CaseInsensitive) == 0) {
            widget->ui->comboContainer->setCurrentIndex(1);
            widget->ui->comboContainer->setEnabled(false);
        }
        widget->ui->txtFile->setText(fileInfo.completeBaseName() +"-hevc" + appendText);
        //widget->ui->frame->setGraphicsEffect(&settingShadow);
        QString csvAppendText = existCount == 0 ? "-hevc_" : QString("-hevc%1_").arg(existCount);
        widget->ui->txtLogFile->setText(outputFolder + "/" + fileInfo.completeBaseName() + csvAppendText + QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm") + ".csv");
        widget->ui->txtLogFile->home(true);
        widget->ui->txtLogFile->setSelection(0,0);
        //Encoder::instance()->setList.append(widget);
    }
    addItem(fileName);
    if (addSett) {
        widget->ui->spinSeek->setMaximum(totalFrames.at(count() - 1).toInt());
        widget->ui->spinFrames->setMaximum(totalFrames.at(count() - 1).toInt());
        widget->ui->spinFrames->setValue(totalFrames.at(count() - 1).toInt());
    }
    item(count() - 1)->setFont(font);
    setCurrentRow(count() - 1);
    setBackground();
    int count = FileContainer::instance()->count();
    if (subSampling.at(count - 1).compare("4:2:2", Qt::CaseInsensitive) == 0 && videoDepth.at(count - 1).toInt() == 10)
    {
        Encoder::instance()->setList.at(count - 1)->ui->comboProfileAdv->setCurrentIndex(3);
    }
    else if (subSampling.at(count - 1).compare("4:4:4", Qt::CaseInsensitive) == 0 && videoDepth.at(count - 1).toInt() == 10)
    {
        Encoder::instance()->setList.at(count -1 )->ui->comboProfileAdv->setCurrentIndex(4);
    }
    if (Encoder::instance()->setList.at(count - 1)->ui->Width->text().isEmpty())
    {
        Encoder::instance()->setList.at(count - 1)->ui->Width->setValue(videoWidth.at(count - 1).toInt());
    }
    if (Encoder::instance()->setList.at(count - 1)->ui->Height->text().isEmpty())
    {
        Encoder::instance()->setList.at(count - 1)->ui->Height->setValue(videoHeight.at(count -1).toInt());
    }
    if (Encoder::instance()->setList.at(count - 1)->ui->frameRate->text().isEmpty())
    {
        Encoder::instance()->setList.at(count - 1)->ui->frameRate->setText(frameRate.at(count -1));
    }
    QStringList WidthPreset, HeightPreset;
    float aspectRatio;
    int height;
    WidthPreset << videoWidth.at(count -1);
    HeightPreset << videoHeight.at(count - 1);
    aspectRatio = videoWidth.at(count -1).toFloat() / videoHeight.at(count - 1).toFloat();
    while (WidthPreset.at(WidthPreset.size() -1).toInt() >= 320)
    {
        WidthPreset << QString::number(WidthPreset.at(WidthPreset.size() - 1).toInt() - 320);
        height = WidthPreset.at(WidthPreset.size() -1).toInt() / aspectRatio;
        height = (height % 2 == 0) ? height : (height + 1);
        HeightPreset << QString::number(height);
    }
    for (int i = 0; i < WidthPreset.size() -1; ++i)
    {
        Encoder::instance()->setList.at(count - 1)->ui->preset->addItem(WidthPreset.at(i) + " x " + HeightPreset.at(i) );
    }

    Encoder::instance()->setList.at(count - 1)->ui->preset->addItem("Custom");
    Encoder::instance()->setList.at(count - 1)->ui->Width->setMaximum(videoWidth.at(count - 1).toInt());
    Encoder::instance()->setList.at(count - 1)->ui->Height->setMaximum(videoHeight.at(count -1).toInt());
    Encoder::instance()->setList.at(count - 1)->ui->Width->setDisabled(true);
    Encoder::instance()->setList.at(count - 1)->ui->Height->setDisabled(true);
    Encoder::instance()->setList.at(count - 1)->ui->cross->setDisabled(true);
    Encoder::instance()->setList.at(count - 1)->ui->preset->setDisabled(true);

}

/* Recursively calls queueAdd when a folder is dropped in the queue  */
void FileContainer::recursiveAdd(QString filePath)
{
    if(QFileInfo(filePath).isFile())
    {
        queueAdd(filePath);
        return;
    }
    QDir dir(filePath);
    dir.setFilter(QDir::Files | QDir ::AllDirs |QDir::NoDotAndDotDot);
    foreach(QString file, dir.entryList())
    {
        recursiveAdd(filePath + "/" + file);
        dlg.setValue(1);
    }
}

/* Retrives the video info and invoke frameCount from y4mUtils */
int FileContainer::getY4MCount(QString fileName)
{
    generalInfo = MediaInfo::getGeneralDetails(QString("\"") + fileName + QString("\""));
    int width = videoInfo.at(1).toInt();
    int height = videoInfo.at(2).toInt();
    int depth ;
    try
    {
        depth = videoInfo.at(4).toInt();
    }
    catch(...)
    {
        depth = -1;
    }
    if(depth == 0)
        depth = -1;
    QString colorSpace = videoInfo.at(5);
    unsigned long long int size = generalInfo.at(2).toLongLong();
    if(depth == -1  && (colorSpace.isEmpty() || colorSpace.isNull()))
    {
        return Y4MUtils::instance()->frameCount(width, height, size);
    }
    else if(depth == -1)
        return Y4MUtils::instance()->frameCount(width, height, size, colorSpace);
    else if((colorSpace.isEmpty() || colorSpace.isNull()))
        return Y4MUtils::instance()->frameCount(width, height, size, depth);
    return Y4MUtils::instance()->frameCount(width, height, size, depth, colorSpace);
}

void FileContainer::setSettings(bool remove)
{
    if(curIndex >= 0)
    {
        SettingsWidget *widget = Encoder::instance()->setList.at(curIndex);
        Encoder::instance()->getUI()->verticalLayout2->removeWidget(widget);
        widget -> hide();
        if(remove)
        {
            Encoder::instance()->setList.removeAt(curIndex);
            if(curIndex >= count())
               curIndex = count() - 1;
        }
    }
    if(count() != 0)
    {
        setSelectedItemSettings();
        return;
    }
    Encoder::instance()->dummySetting->show();
    Encoder::instance()->dummySetting->ui->lblHeader2->setStyleSheet("color:rgb(140,140,140);padding-bottom:50px;/*font-size:33px;*/");
    Encoder::instance()->getUI()->verticalLayout2->addWidget(Encoder::instance()->dummySetting);
    curIndex = -1;
}


/* remove selected row from the queue */
void FileContainer::queueRemove()
{
    disconnect(this, SIGNAL(itemSelectionChanged()), 0, 0);
    if(count() == 0)
        return;
    if(!selectionModel()->isSelected(currentIndex()))
        return;
    int selIndx = selectionModel()->selectedRows().at(0).row();
    delete item(selIndx);
    totalFrames.removeAt(selIndx);
    audioFormat.removeAt(selIndx);
    subFormat.removeAt(selIndx);
    totalAudioFrames.removeAt(selIndx);
    frameRate.removeAt(selIndx);
    bitRate.removeAt(selIndx);
    videoHeight.removeAt(selIndx);
    videoWidth.removeAt(selIndx);
    videoDepth.removeAt(selIndx);
    videoColorSpace.removeAt(selIndx);
    subSampling.removeAt(selIndx);
    fps.removeAt(selIndx);
    setSettings(true);
    setBackground();
    connect(this,SIGNAL(itemSelectionChanged()),this,SLOT(setSelectedItemSettings()));
}

/* remove rows corresponding to the index */
void FileContainer::deQueue(int index)
{
    disconnect(this, SIGNAL(itemSelectionChanged()), 0, 0);
    if(count() >= 1)
    {
        curIndex = 0;
        delete item(index);
        setSettings(true);
        setBackground();
    }
    connect(this,SIGNAL(itemSelectionChanged()),this,SLOT(setSelectedItemSettings()));
}

/* Remove all rows from file queue */
void FileContainer::queueClear()
{
    disconnect(this, SIGNAL(itemSelectionChanged()), 0, 0);
    int num = count();
    if(num == 0)
        return;
    for (int i = num - 1; i >= 0; --i)
        delete item(i);
    totalFrames.clear();
    audioFormat.clear();
    subFormat.clear();
    totalAudioFrames.clear();
    frameRate.clear();
    bitRate.clear();
    videoHeight.clear();
    videoWidth.clear();
    videoDepth.clear();
    videoColorSpace.clear();
    subSampling.clear();
    fps.clear();
    setBackground();
    setSettings(false);
    Encoder::instance()->setList.clear();
    connect(this,SIGNAL(itemSelectionChanged()),this,SLOT(setSelectedItemSettings()));
}

/* Move selected row up the queue */
void FileContainer::rowUp()
{
    if(count() <= 1)
    return;
    int selIndx = selectionModel()->selectedRows().at(0).row();
    if(selIndx == 0)
      return;
    curIndex = selIndx - 1;
    swapRow(selIndx, curIndex);
    SettingsWidget *widget =  Encoder::instance()->setList.at(selIndx);
    Encoder::instance()->setList.removeAt(selIndx);
    Encoder::instance()->setList.insert(curIndex, widget);
}

/* Move selected row down the queue */
void FileContainer::rowDown()
{
   if(count() <= 1)
       return;
   int selIndx = selectionModel() -> selectedRows().at(0).row();
   if(selIndx == (count() - 1))
       return;
   curIndex = selIndx + 1;
   swapRow(selIndx, curIndex);
   SettingsWidget *widget =  Encoder::instance()->setList.at(selIndx);
   Encoder::instance()->setList.removeAt(selIndx);
   Encoder::instance()->setList.insert(curIndex, widget);
}

void FileContainer::updateOutputFolder(QString dest)
{
    int setCount = Encoder::instance()->setList.count();
    for(int i = 0; i < setCount; i++)
    {
        Encoder::instance()->setList.at(i)->ui->txtDest->setText(dest);
    }
}

void FileContainer::dragEnterEvent(QDragEnterEvent *event)
{
    event->acceptProposedAction();
}

/* Add files when drag and drop */
void FileContainer::dropEvent(QDropEvent *event)
{
    dropProgCancelClicked = false;
    dlg.setWindowFlags(dlg.windowFlags() & ~Qt::WindowCloseButtonHint);
    dlg.setMinimum(0);
    int count = event->mimeData()->urls().count();
    if(count == 1) count = 0;
    dlg.setMaximum(count);
    dlg.setMinimumDuration(0);
    dlg.show();
    int i = 0;
    foreach(QUrl url, event->mimeData()->urls())
    {
        QApplication::processEvents();
        if(dropProgCancelClicked)
            return;
        QString fileName = url.toLocalFile();
        event->acceptProposedAction();
        recursiveAdd(fileName);
        dlg.setValue(++i);
    }
    dlg.close();
}

void FileContainer::dragLeaveEvent(QDragLeaveEvent *event)
{
    event->accept();
}

void FileContainer::setSelectedItemSettings()
{
    SettingsWidget *widget;
    if(curIndex == -1)
    {
        Encoder::instance()->getUI()->verticalLayout2->removeWidget(Encoder::instance()->dummySetting);
        Encoder::instance()->dummySetting->hide();
    }
    else
    {
        widget = Encoder::instance()->setList.at(curIndex);
        Encoder::instance()->getUI()->verticalLayout2->removeWidget(widget);
        widget->hide();
        item(curIndex)->setFont(font);
    }
    curIndex = selectionModel()->selectedRows().at(0).row();
    widget = Encoder::instance()->setList.at(curIndex);
    Encoder::instance()->getUI()->verticalLayout2->addWidget(widget);
    widget->show();
    selectionFrame.setFont(selectedFont);
    //widget->ui->frame->setGraphicsEffect(&settingShadow);
    widget->ui->lblHeader2->setStyleSheet("color:rgb(90,90,90);padding-bottom:50px;/*font-size:33px;*/");
    item(curIndex)->setFont(selectedFont);
    updateSelection(250);
}

void FileContainer::leaveEvent(QEvent *)
{
    this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

void FileContainer::enterEvent(QEvent *)
{
    this->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
}

void FileContainer::dragMoveEvent(QDragMoveEvent *event)
{
    event->acceptProposedAction();
}

/* sets background if no files in the queue */
void FileContainer::setBackground()
{
    if (count() == 0)
    {
        setStyleSheet("QListWidget {\
                            background-color:white;\
                            background-image:url(':/Images/dragdrop.png');\
                            background-repeat:no-repeat;\
                            background-position:center;\
                            border-top: 2px solid #ccc;\
                            border-left: 2px solid #ccc;\
                            border-bottom: 2px solid #ccc;\
                     }");
    }
    else
    {
        setStyleSheet("QListWidget {\
                            background-color:white;\
                            background-image:none;\
                            background-repeat:no-repeat;\
                            background-position:center;\
                            background:none;\
                            padding:0px;\
                            margin:0px;\
                            border:none;\
                      }\
                      QListWidget::Item {\
                            padding-top:10px;\
                            padding-bottom:10px;\
                            margin:0px;\
                      }\
                      QListWidget::Item:hover {\
                            background:#ccc;\
                            margin:0px;\
                      }\
                      QListWidget::Item:selected:active {\
                            border:none;\
                            margin:0px;\
                            color:white;\
                      }\
                      QScrollBar{\
                            background:none;\
                      }\
                      QScrollBar::add-page:horizontal, QScrollBar::sub-page:horizontal {\
                            background: none;\
                      }\
                      QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {\
                            background: none;\
                      }\
                      QScrollBar:vertical {\
                            background:rgba(0,0,0,0);\
                            width:4px;  \
                            margin: 0px 0px 0px 0px;\
                      }\
                      QScrollBar::handle:vertical {\
                            background: qlineargradient(x1:0, y1:0, x2:1, y2:0,\
                                        stop: 0  #bbb, stop: 0.5 #bbb,  stop:1 #bbb);\
                            min-height: 0px;\
                      }\
                      QScrollBar::add-line:vertical {\
                            background: qlineargradient(x1:0, y1:0, x2:1, y2:0,\
                                        stop: 0  #bbb, stop: 0.5 #bbb,  stop:1 #bbb);\
                            height: px;\
                            subcontrol-position: bottom;\
                            subcontrol-origin: margin;\
                      }\
                      QScrollBar::sub-line:vertical {\
                            background: qlineargradient(x1:0, y1:0, x2:1, y2:0,\
                                        stop: 0  #bbb, stop: 0.5 #bbb,  stop:1 #bbb);\
                            height: 0px;\
                            subcontrol-position: top;\
                            subcontrol-origin: margin;\
                      }\
                      QScrollBar:horizontal {\
                            background:white;\
                            height:4px;  \
                            margin: 0px 0px 0px 0px;\
                      }\
                      QScrollBar::handle:horizontal {\
                            background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\
                                        stop: 0  #bbb, stop: 0.5 #bbb,  stop:1 #bbb);\
                            min-width: 0px;\
                      }\
                      QScrollBar::add-line:horizontal {\
                            background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\
                                        stop: 0  #bbb, stop: 0.5 #bbb,  stop:1 #bbb);\
                            width: px;\
                            subcontrol-position: bottom;\
                            subcontrol-origin: margin;\
                      }\
                      QScrollBar::sub-line:horizontal {\
                            background: qlineargradient(x1:0, y1:0, x2:0, y2:1,\
                                        stop: 0  #bbb, stop: 0.5 #bbb,  stop:1 #bbb);\
                            width: 0px;\
                            subcontrol-position: top;\
                            subcontrol-origin: margin;\
                      }");

    }
    //updateSelection(1);
}

/* Swap rows when up/down button is clicked */
void FileContainer::swapRow(int i, int j)
{
    disconnect(this, SIGNAL(itemSelectionChanged()), 0, 0);
    QListWidgetItem *newItem = new QListWidgetItem(item(i)->text());
    delete item(i);
    insertItem(j, newItem);
    item(j)->setFont(selectedFont);
    setCurrentRow(j);
    QString temp;
    temp = totalFrames.at(i);
    totalFrames.removeAt(i);
    totalFrames.insert(j, temp);
    temp = audioFormat.at(i);
    audioFormat.removeAt(i);
    audioFormat.insert(j, temp);
    temp = totalAudioFrames.at(i);
    totalAudioFrames.removeAt(i);
    totalAudioFrames.insert(j, temp);
    temp = subFormat.at(i);
    subFormat.removeAt(i);
    subFormat.insert(j, temp);
    temp = frameRate.at(i);
    frameRate.removeAt(i);
    frameRate.insert(j, temp);
    temp = bitRate.at(i);
    bitRate.removeAt(i);
    bitRate.insert(j, temp);
    temp = videoHeight.at(i);
    videoHeight.removeAt(i);
    videoHeight.insert(j, temp);
    temp = videoWidth.at(i);
    videoWidth.removeAt(i);
    videoWidth.insert(j, temp);
    temp = videoDepth.at(i);
    videoDepth.removeAt(i);
    videoDepth.insert(j, temp);
    temp = videoColorSpace.at(i);
    videoColorSpace.removeAt(i);
    videoColorSpace.insert(j, temp);
    temp = subSampling.at(i);
    subSampling.removeAt(i);
    subSampling.insert(j, temp);
    temp = fps.at(i);
    fps.removeAt(i);
    fps.insert(j, temp);
    updateSelection(250);
    connect(this,SIGNAL(itemSelectionChanged()),this,SLOT(setSelectedItemSettings()));
}

void FileContainer::resizeEvent(QResizeEvent *e)
{
    QListWidget::resizeEvent(e);
    updateSelection();
}

void FileContainer::updateSelection(int duration)
{
    QListWidgetItem* current = currentItem();
    animation.stop();
    if(!current)
    {
        selectionFrame.hide();
        return;
    }
    if(!selectionFrame.isVisible())
    {
        selectionFrame.setGeometry(visualItemRect(current));
        selectionFrame.show();
        return;
    }
    selectionFrame.setText(current->text());
    selectionFrame.update();
    animation.setDuration(duration);
    animation.setStartValue(selectionFrame.geometry());
    animation.setEndValue(visualItemRect(current));
    animation.start();
}

void FileContainer::onDropProgCancel()
{
    dropProgCancelClicked = true;
}
