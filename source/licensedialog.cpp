#include <QFile>
#include <QLayout>
#include <QStringList>
#include <QFileDialog>
#include <QMessageBox>
#include <QProgressDialog>
#include <QProcess>
#include <QStandardPaths>
#include <QThread>
#include <QSignalMapper>
#include <QDesktopServices>
#include <QRegExp>
#include <QShortcut>
#include <Windows.h>
#include <QUrl>
#include <QUrlQuery>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>
#include <QStandardPaths>
#include <QSettings>
#include <QDomDocument>
#include <QImageReader>
#include <QPixmap>
#include <QDesktopWidget>
#include "licensedialog.h"
#include "ui_licensedialog.h"
#include "encoder.h"

LicenseDialog::LicenseDialog(QString str,QWidget *parent ) :
    QDialog(parent),
    ui(new Ui::LicenseDialog)
{
    ui->setupUi(this);
    ui->licenseinfo->setStyleSheet("font-size:30px;");
    ui->licenseinfo->setText(str);
    this->setWindowTitle("Trial License");
    QShortcut *buy = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_B), this);
    QObject::connect(buy, SIGNAL(activated()), this, SLOT(on_Buy_clicked()));
    QShortcut *activate = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_A), this);
    QObject::connect(activate, SIGNAL(activated()), this, SLOT(on_Activate_clicked()));
    //loadBanner();

}

LicenseDialog::~LicenseDialog()
{
    delete ui;
}

void runLicenseManager()
{
    QString lmPath =  QCoreApplication::applicationDirPath() + "/../LicenseManager.exe";
    QFile lmFile(lmPath);
    if (lmFile.exists())
    {
        QString prepareLmExe = QString("\"") + lmPath + QString("\"");
        if (!QProcess::startDetached(prepareLmExe))
            QMessageBox::critical(0, "Failed To Start", "Could Not Start License Manager");
    }
    else
        QMessageBox::critical(0, "Error", "Could Not Find License Manager");
}


void LicenseDialog::on_Ignore_clicked()
{
    this->close();
}

void LicenseDialog::on_Buy_clicked()
{
    QString link = "https://x265.com/store/";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void LicenseDialog::on_Activate_clicked()
{
    this->flag = true;
    this->close();
    runLicenseManager();
}

LicenseDialog *LicenseDialog::instance(QString str)
{
    static LicenseDialog *ld = NULL;
    if(!ld) {
        ld = new LicenseDialog(str);

    }
    return ld;
}

void LicenseDialog::closeEvent(QCloseEvent *event)
{
    if (this->flag)
        event->accept();
    else
    {
        event->accept();
        Encoder *encoder = Encoder::instance();
        encoder->show();

    }
}

void LicenseDialog::getBanner()
{
    QString banner_url_str = "http://192.168.1.43/banner.php";
    QSettings settings;
    int timeStamp = settings.value("BannerTimeStamp",0).toInt();
    QNetworkAccessManager NAManager(this);
    QUrl url(banner_url_str);
    QNetworkRequest request(url);
    QUrlQuery data;
    data.addQueryItem("timestamp",QString::number(timeStamp));
    request.setHeader(QNetworkRequest::ContentTypeHeader,
                      "application/x-www-form-urlencoded");
    QNetworkReply *reply = NAManager.post(request, data.toString(QUrl::FullyEncoded).toUtf8());
    QEventLoop eventLoop;
    QObject::connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    eventLoop.exec();
    if (reply->error())
        return;
    QString response = reply->readAll();
    if ( response.length() == 0)
        return;
    QDomDocument dom;
    dom.setContent(response);
    QDomElement document = dom.documentElement();
    QDomNodeList nodelist = document.elementsByTagName("file");
    if (nodelist.count() == 0)
        return;
    QDomElement ele = nodelist.at(0).toElement();
    QString imgPath = ele.text();
    nodelist = document.elementsByTagName("timestamp");
    if (nodelist.count() == 0)
        return;
    ele = nodelist.at(0).toElement();
    timeStamp = ele.text().toInt();
    settings.setValue("BannerTimeStamp", timeStamp);
    QUrl image_url(imgPath);
    QNetworkRequest imagerequest(image_url);
    reply->deleteLater();
    QNetworkReply  *imageReply = NAManager.get(imagerequest);
    QEventLoop imageLoop;
    QObject::connect(imageReply, SIGNAL(finished()), &imageLoop, SLOT(quit()));
    imageLoop.exec();
    QByteArray imageArray = imageReply->readAll();
    QPixmap pixmap;
    pixmap.loadFromData(imageArray);
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    if (!dir.exists())
        dir.mkdir(dir.absolutePath());
    QFile imageFile(dir.absolutePath()+ "/banner_new.png");
    imageFile.open(QIODevice::WriteOnly);
    pixmap.save(&imageFile);
    imageFile.close();
    QFile xmlFile(dir.absolutePath()+"/banner_new.xml");
    xmlFile.open(QIODevice::WriteOnly);
    QTextStream out(&xmlFile);
    out << response;
    xmlFile.close();
}

bool LicenseDialog::loadBanner(QString filename)
{
    QFile imageFile(filename + ".png");
    if (!imageFile.exists())
        return false;
    QImageReader imageReader(&imageFile);
    if (imageReader.error())
        return false;
    QFile xmlFile(filename + ".xml");
    if (!xmlFile.exists())
        return false;
    QDomDocument dom;
    dom.setContent(&xmlFile);
    QDomElement document = dom.documentElement();
    QDomNodeList nodelist = document.elementsByTagName("url");
    if (nodelist.count() == 0)
        return false;
    QDomElement ele = nodelist.at(0).toElement();
    externalUrl = ele.text();
    imageFile.close();
    xmlFile.close();
    return true;
}

void LicenseDialog::setBanner(QString filename)
{

    int lDpiX = QApplication::desktop()->logicalDpiX();
    int lDpiY = QApplication::desktop()->logicalDpiY();
    QPixmap pixmap;
    pixmap.load(filename);
    if (lDpiX > 96 && lDpiX <= 144  && lDpiY > 96 && lDpiY <= 144)
    {
        pixmap = pixmap.scaled(pixmap.size().height() *(1.25),pixmap.size().width() * (1.25),Qt::KeepAspectRatio);
        connect(&lblBanner, SIGNAL(clicked()), this, SLOT(bannerClicked()));
        lblBanner.setPixmap(pixmap);
        lblBanner.setAlignment(Qt::AlignHCenter);
        //ui->bannerlayout->setSizeConstraint(QLayout::SetFixedSize);
        lblBanner.setMaximumWidth(pixmap.size().width() * (1.25) );
        lblBanner.setMaximumHeight(pixmap.size().height() * (1.25));
        ui->bannerlayout->insertWidget(0,&lblBanner);

    }
    else if (lDpiX > 144 && lDpiX <= 192  && lDpiY > 144 && lDpiY <= 192)
    {
        pixmap = pixmap.scaled(pixmap.size().height() * (1.5),pixmap.size().width() *(1.5),Qt::KeepAspectRatio);
        connect(&lblBanner, SIGNAL(clicked()), this, SLOT(bannerClicked()));
        lblBanner.setPixmap(pixmap);
        lblBanner.setAlignment(Qt::AlignHCenter);
        //ui->bannerlayout->setSizeConstraint(QLayout::SetFixedSize);
        lblBanner.setMaximumWidth(pixmap.size().width() *(1.5));
        lblBanner.setMaximumHeight(pixmap.size().height() *(1.5));
        ui->bannerlayout->insertWidget(0,&lblBanner);

    }
    else if (lDpiX > 192   &&  lDpiY > 192)
    {
        pixmap = pixmap.scaled(pixmap.size().height() *(2),pixmap.size().width() *(2),Qt::KeepAspectRatio);
        connect(&lblBanner, SIGNAL(clicked()), this, SLOT(bannerClicked()));
        lblBanner.setPixmap(pixmap);
        lblBanner.setAlignment(Qt::AlignHCenter);
        //ui->bannerlayout->setSizeConstraint(QLayout::SetFixedSize);
        lblBanner.setMaximumWidth(pixmap.size().width()*(2));
        lblBanner.setMaximumHeight(pixmap.size().height() *(2));
        ui->bannerlayout->insertWidget(0,&lblBanner);

    }
    else
    {
        pixmap = pixmap.scaled(pixmap.size().height(),pixmap.size().width(),Qt::KeepAspectRatio);
        connect(&lblBanner, SIGNAL(clicked()), this, SLOT(bannerClicked()));
        lblBanner.setPixmap(pixmap);
        lblBanner.setAlignment(Qt::AlignHCenter);
        //ui->bannerlayout->setSizeConstraint(QLayout::SetFixedSize);
        lblBanner.setMaximumWidth(pixmap.size().width());
        lblBanner.setMaximumHeight(pixmap.size().height());
        ui->bannerlayout->insertWidget(0,&lblBanner);

    }


}

void LicenseDialog::loadBanner()
{
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    if (!dir.exists())
        return;
    if (loadBanner(dir.absolutePath() + "/banner_new")) {
       setBanner(dir.absolutePath() + "/banner_new.png");
       QFile imageFile(dir.absolutePath() + "/banner.png");
       QFile imageFileNew(dir.absolutePath() + "/banner_new.png");
       QFile xmlFile(dir.absolutePath() + "/banner.xml");
       QFile xmlFileNew(dir.absolutePath() + "/banner_new.xml");
       imageFile.remove();
       xmlFile.remove();
       imageFileNew.rename(imageFile.fileName());
       xmlFileNew.rename(xmlFile.fileName());
       imageFileNew.close();
        xmlFileNew.close();
    }
    else if (loadBanner(dir.absolutePath()+ "/banner"))
        setBanner(dir.absolutePath()+ "/banner.png");
    else
        externalUrl = "";
}

void LicenseDialog::bannerClicked()
{
    QDesktopServices::openUrl(QUrl(externalUrl,QUrl::TolerantMode));
}
