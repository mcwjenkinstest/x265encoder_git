
#include "licenseeinputdialog.h"
#include "ui_licenseeinputdialog.h"

LicenseeInputDialog::LicenseeInputDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LicenseeInputDialog)
{
    ui->setupUi(this);
}

LicenseeInputDialog::~LicenseeInputDialog()
{
    delete ui;
}

QString LicenseeInputDialog::GetTextLine()
{
    return ui->lineEdit->text();
}