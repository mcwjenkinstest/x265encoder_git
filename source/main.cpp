#include <tchar.h>
#include <QApplication>
#include <QMessageBox>
#include <QPushButton>
#include <QSharedMemory>
#include <QProcess>
#include <QDesktopWidget>
#include <QLayout>
#include "encoder.h"
#include "filecontainer.h"
#include "addons.h"
#include "licensedialog.h"
#include "McwLicenseCon.h"
#include "McwLicensePro.h"
#include "update.h"
#include "licenseeinputdialog.h"
#include "McwModifyRegistrySecurity.h"
#include <strsafe.h>
#include <Nb30.h>
#pragma comment(lib,"netapi32.lib")

#ifdef LICENCE_BITANSWER
CLicenseManager g_LicenseManager;
CLicenseManagerPro g_LicenseManagerPro;

void LicenseManager()
{
    QString lmPath =  QCoreApplication::applicationDirPath() + "/../LicenseManager.exe";
    QFile lmFile(lmPath);
    if (lmFile.exists())
    {
        QString prepareLmExe = QString("\"") + lmPath + QString("\"");
        if (!QProcess::startDetached(prepareLmExe))
            QMessageBox::critical(0, "Failed To Start", "Could Not Start License Manager");
    }
    else
        QMessageBox::critical(0, "Error", "Could Not Find License Manager");
}
#endif

BOOL licenseMessageBox(void)
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("Error");
    msgBox.setText("x265 Encoder has detected an installation of x265 HEVC Upgrade that is not activated. Please activate your copy by clicking \"Activate\" button below, after purchasing a valid license from www.x265.com");
    QAbstractButton *btnActivate = msgBox.addButton("Activate", QMessageBox::YesRole);
    QAbstractButton *btnCancel = msgBox.addButton("Cancel", QMessageBox::NoRole);
    msgBox.setIcon(QMessageBox::Critical);
    msgBox.exec();

    if(msgBox.clickedButton() == btnActivate)
    {
        return TRUE;
        //LicenseManager();
    }
    else if(msgBox.clickedButton() == btnCancel)
        return FALSE;
}

#ifdef LICENCE_BITANSWER
bool checkLicensePro()
{
    BIT_STATUS hr = g_LicenseManagerPro.UserGetLicenseStatus();

    if (BIT_SUCCESS == hr)
    {
        //test encryption
        BIT_INT32 rrr = g_LicenseManagerPro.UserVerifyFeature(3/*FID*/, 220);
        if (0 != rrr)
        {
            //printf("Feature Value is wrong!");
            g_LicenseManagerPro.DumpStatus();
            return false;
        }

        BIT_UINT32 product_ID = 100;

        if (0 != g_LicenseManagerPro.UserVerifyFeature(5, product_ID))
        {
            //printf("Feature Value is wrong!");
            g_LicenseManagerPro.DumpStatus();
            return false;
        }

        return true;
    }
    g_LicenseManagerPro.DumpStatus();
    return false;
}

bool checkLicense()
{
    BIT_STATUS hr = g_LicenseManager.UserGetLicenseStatus();

    if (BIT_ERR_SN_LICENSE_EXPIRATION == hr || BIT_ERR_TIME_TAMPER == hr)
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Trial License Expired");
        msgBox.setText("x265 Encoder has detected an installation of x265 HEVC Upgrade that is not activated. Please activate your copy by clicking \"Activate\" button below, after purchasing a valid license from www.x265.com");
        QAbstractButton *btnActivate = msgBox.addButton("Activate", QMessageBox::YesRole);
        QAbstractButton *btnCancel = msgBox.addButton("Cancel", QMessageBox::NoRole);
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
        if(msgBox.clickedButton() == btnActivate)
           LicenseManager();

        exit(EXIT_FAILURE);

    }
    if (BIT_SUCCESS == hr)
    {
        //test encryption
        if (0 != g_LicenseManager.UserVerifyFeature(3/*FID*/, 220))
        {
            //printf("Feature Value is wrong!");
            g_LicenseManager.DumpStatus();
           return false;
        }

        BIT_UINT32 product_ID = 0;

        if (0 != g_LicenseManager.UserVerifyFeature(5/*FID*/, product_ID))
        {
            //printf("Feature Value is wrong!");
            g_LicenseManager.DumpStatus();
            return false;
        }

        return true;
    }

    g_LicenseManager.DumpStatus();
    return false;
}
#endif

int GetMac(char * mac)
{
    NCB ncb;
    typedef struct _ASTAT_
    {
        ADAPTER_STATUS   adapt;
        NAME_BUFFER   NameBuff[30];
    }ASTAT, *PASTAT;

    ASTAT Adapter;

    typedef struct _LANA_ENUM
    {
        UCHAR   length;
        UCHAR   lana[MAX_LANA];
    }LANA_ENUM;

    LANA_ENUM lana_enum;
    UCHAR uRetCode;
    memset(&ncb, 0, sizeof(ncb));
    memset(&lana_enum, 0, sizeof(lana_enum));
    ncb.ncb_command = NCBENUM;
    ncb.ncb_buffer = (unsigned char *)&lana_enum;
    ncb.ncb_length = sizeof(LANA_ENUM);
    uRetCode = Netbios(&ncb);

    if (uRetCode != NRC_GOODRET)
        return uRetCode;

    for (int lana = 0; lana<lana_enum.length; lana++)
    {
        ncb.ncb_command = NCBRESET;
        ncb.ncb_lana_num = lana_enum.lana[lana];
        uRetCode = Netbios(&ncb);
        if (uRetCode == NRC_GOODRET)
            break;
    }

    if (uRetCode != NRC_GOODRET)
        return uRetCode;

    memset(&ncb, 0, sizeof(ncb));
    ncb.ncb_command = NCBASTAT;
    ncb.ncb_lana_num = lana_enum.lana[0];
    strcpy_s((char*)ncb.ncb_callname, sizeof("*"), "*");
    ncb.ncb_buffer = (unsigned char *)&Adapter;
    ncb.ncb_length = sizeof(Adapter);
    uRetCode = Netbios(&ncb);

    if (uRetCode != NRC_GOODRET)
        return uRetCode;

    sprintf(mac, "%02X-%02X-%02X-%02X-%02X-%02X",
            Adapter.adapt.adapter_address[0],
            Adapter.adapt.adapter_address[1],
            Adapter.adapt.adapter_address[2],
            Adapter.adapt.adapter_address[3],
            Adapter.adapt.adapter_address[4],
            Adapter.adapt.adapter_address[5]);

    return 0;
}

#include <iostream>
#include <random>
#include <ctime>
#include <sstream>
#include <string>
#include "netlicensing/netlicensing.h"
#include "netlicensing/constants.h"

#include <Iphlpapi.h>
#pragma comment(lib, "Iphlpapi.lib")

using namespace netlicensing;
#define DEMO_MEMBUFFER_SIZE   1024
#define UN_LIMIT    9999
#define MIN_VALID_DAYS      5


void byte2Hex(unsigned char bData, char hex[])
{
    int high = bData / 16, low = bData % 16;
    hex[0] = (high <10) ? ('0' + high) : ('A' + high - 10);
    hex[1] = (low <10) ? ('0' + low) : ('A' + low - 10);
}

int getPhysicalMac(char *mac)
{
    ULONG ulSize = 0;
    PIP_ADAPTER_INFO pAdapter = NULL;
    int temp = 0;
    temp = GetAdaptersInfo(pAdapter, &ulSize);
    pAdapter = (PIP_ADAPTER_INFO)malloc(ulSize);
    temp = GetAdaptersInfo(pAdapter, &ulSize);


    int iCount = 0;
    while (pAdapter)
    {
        if (strstr(pAdapter->Description, "PCI") != NULL)
        {
            for (int i = 0; i<(int)pAdapter->AddressLength; i++)
            {
                byte2Hex(pAdapter->Address[i], &mac[iCount]);
                iCount += 2;
                if (i<(int)pAdapter->AddressLength - 1)
                {
                    mac[iCount++] = ':';
                }
                else
                {
                    mac[iCount++] = '#';
                }
            }
        }

        pAdapter = pAdapter->Next;
    }


    if (iCount >0)
    {
        mac[--iCount] = '\0';
        return iCount;
    }
    else return -1;
}

time_t FormatTime(const char* szTime)
{
    struct tm tm1;
    char membuffer[DEMO_MEMBUFFER_SIZE];
    memset(membuffer, 0, sizeof(membuffer));
    strcpy(membuffer, szTime);

    time_t time1;

    membuffer[10] = ' ';
    membuffer[19] = '\0';

    sscanf_s(membuffer, "%4d-%2d-%2d %2d:%2d:%2d",
             &tm1.tm_year,
             &tm1.tm_mon,
             &tm1.tm_mday,
             &tm1.tm_hour,
             &tm1.tm_min,
             &tm1.tm_sec);

    tm1.tm_year -= 1900;
    tm1.tm_mon--;
    tm1.tm_isdst = 0;

    time1 = mktime(&tm1);

    return time1;
}

int UserGetRemainDays(ValidationResult &vres)
{
    int expirationDays = 0;

    ////Get valid status
    std::map<std::string, Composition>  iter = vres.getValidations();
    std::map<std::string, Composition>::iterator it = iter.begin();

    netlicensing::Composition com = it->second;
    std::shared_ptr<std::string> aptr = com.get("evaluation");

    if (0 == strcmp(aptr->c_str(), "false"))
    {
        expirationDays = UN_LIMIT;
    }
    else
    {
        aptr->clear();
        aptr = com.get("evaluationExpires");

        time_t endTime = FormatTime(aptr->c_str());   //("2018-09-06T09:42:57.422Z")
        time_t now = time(NULL);
        double time = difftime(endTime, now);

        expirationDays = time / (60 * 60 * 24) + 0.99;
    }

    return expirationDays;
}

bool UserGetValidStatus(ValidationResult &vres)
{
    bool flag = false;
    std::map<std::string, Composition>  iter = vres.getValidations();
    std::map<std::string, Composition>::iterator it = iter.begin();

    netlicensing::Composition com = it->second;
    std::shared_ptr<std::string> aptr = com.get("valid");

    flag = strcmp(aptr->c_str(), "false");

    return flag;
}

bool checkNetlicensingLicense(std::string licensee, int &status)
{
    using namespace netlicensing;
    bool valid_flag = false;

    std::string licensee_number;

    if (licensee.length() > 0) 
    {
        licensee_number = licensee;
    }
    else
    {
        return false;
    }

    Context ctx;
    ctx.set_base_url("https://go.netlicensing.io/core/v2/rest/");  // NetLicensing base URL
    ctx.set_username("anshuarya");                                 // Vendor username at netlicensing.io
    ctx.set_password("mcwdemo");                                   // Vendor password at netlicensing.io

    // Licensee is a holder of licenses. This can be an individual or organisation, but not necessarily the end-user.
    // LicenseeNumber  - arbitrary sequence of printable ASCII characters that uniquely identify an licensee within NetLicensing
    // e.g. Licensee number can be email ID, hardware tocken, IMEI number, etc.
    {
        status = 0;
        ValidationResult vres;
        for (int i = 0; i < 3; i++)
        {
            try {
                //Get CPUID
                int deBuf[4];
                __cpuidex(deBuf, 1, 0);
                char cpu_info[32] = {0};
                memset(cpu_info, 0, 32);
                sprintf_s(cpu_info, "%.8X%.8X", deBuf[3], deBuf[0]);

                //Get mac address
                char   mac[256] = { 0 };
                getPhysicalMac(mac);

                //combine CPUID + MAC
                strcat(mac, cpu_info);

                ValidationParameters vParams = ValidationParameters();
                char *licensee_secret = mac;
                vParams.setLicenseeSecret(licensee_secret);

                vres = LicenseeService::validate(ctx, licensee_number, vParams);

            }
            catch (const RestException& e) {
                std::cerr << e.what() << " code " << e.http_code() << std::endl;
                status = e.http_code();

            }
            catch (const std::runtime_error& err) {
                std::cerr << err.what() << std::endl;
                std::cerr << "Bad network, can't connect to server, Please try more." << std::endl;
                status = 1;
            }

            if (0 == status)
            {
                break;
            }

            Sleep(100);
         }

        if (0 == status)
        {
            //Get valid status
            valid_flag = UserGetValidStatus(vres);
            if (valid_flag)
            {
                std::cout << "value = true" << std::endl;
            }
            else
            {
                std::cout << "value = false" << std::endl;
            }

            //Get expired remaining days
            int remain_days = UserGetRemainDays(vres);

            if ((remain_days > 0 && (remain_days < MIN_VALID_DAYS)) && (UN_LIMIT != remain_days) && valid_flag)
            {
                QString str;
                str.sprintf("Remaining no. of day(s) for expiry : %d day.", remain_days);
                QMessageBox msg;
                msg.setText(str);
                msg.setIcon(QMessageBox::Information);
                msg.exec();
            }

            if ((remain_days > 0) && (UN_LIMIT != remain_days) && (!valid_flag))
            {
                QString str;
                str.sprintf("This licence is already used !");
                QMessageBox msg;
                msg.setText(str);
                msg.setIcon(QMessageBox::Information);
                msg.exec();
            }

        }
     }

    return valid_flag;
}

#ifdef LICENCE_BITANSWER
bool showLicenceInfo()
{
     int days = g_LicenseManager.UserGetRemainDays();
     int licenseType = g_LicenseManager.UserGetLicenseType();
     if(g_LicenseManager.isOnceToday() && licenseType == 3)
     {
          if ( days > 0 )
          {
              if (days == 1)
              {
                  LicenseDialog *ld = LicenseDialog::instance(QString("Trial License expires in %1 day").arg(days));
                  ld->layout()->setSizeConstraint(QLayout::SetFixedSize);
                  ld->getBanner();
                  ld->loadBanner();
                  ld->show();
              }
              else
              {
                  LicenseDialog *ld = LicenseDialog::instance(QString("Trial License expires in %1 days").arg(days));
                  ld->layout()->setSizeConstraint(QLayout::SetFixedSize);
                  ld->getBanner();
                  ld->loadBanner();
                  ld->show();
              }
          }
          return true;
     }
     else
          return false;
}

void activateLicense()
{
    BIT_STATUS hr = g_LicenseManager.UserGetLicenseStatus();
       if (BIT_SUCCESS != hr)
       {//
           if (BIT_ERR_LICENSE_NOT_FOUND == hr)
               {//There is no license found in local machine.
                   //If this is not registered, a demo key will be registerd here.
                   g_LicenseManager.UserSetSn("BBEA6CCCXC32EPU2");
                   BOOL bAgain = TRUE;
                   while (bAgain)
                   {
                       hr = g_LicenseManager.UserApplyForLicense();

                       if (BIT_SUCCESS == hr)
                       {
                           bAgain = FALSE;
                       }
                       else if(BIT_ERR_SERVER_BUSY == hr)
                       {
                           bAgain = TRUE;
                       }
                       else if(BIT_ERR_NETWORK == hr || BIT_ERR_SERVER_DOWN == hr)
                       {
                           QMessageBox msgBox;
                           msgBox.setWindowTitle("Network Error");
                           msgBox.setText(QString("Please check your network connection and try again"));
                           msgBox.setIcon(QMessageBox::Information);
                           msgBox.exec();
                           exit(EXIT_FAILURE);
                       }
                       else if(BIT_ERR_WRONG_LOCAL_TIME == hr || BIT_ERR_SERVER_DENY == hr || BIT_ERR_SN_LICENSE_EXPIRATION == hr)
                       {
                           QMessageBox msgBox;
                           msgBox.setWindowTitle("Trial License Expired");
                           msgBox.setText("x265 Encoder has detected an installation of x265 HEVC Upgrade that is not activated. Please activate your copy by clicking \"Activate\" button below, after purchasing a valid license from www.x265.com");
                           QAbstractButton *btnActivate = msgBox.addButton("Activate", QMessageBox::YesRole);
                           QAbstractButton *btnCancel = msgBox.addButton("Cancel", QMessageBox::NoRole);
                           msgBox.setIcon(QMessageBox::Critical);
                           msgBox.exec();
                           if(msgBox.clickedButton() == btnActivate)
                              LicenseManager();

                           exit(EXIT_FAILURE);
                       }

                   }
               }
       }
}

#endif

// Registry location for bytestream handlers.
const TCHAR *REGKEY_MF_BYTESTREAM_HANDLERS
= TEXT("Software\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\x265 HEVC Upgrade");

// File extension for media files.
const TCHAR *psSubKey[] =
{
    TEXT("LICENSEE_KEY"),
    0
};

void CharToTchar(const char * _char, TCHAR * tchar)
{
    int iLength;

    iLength = MultiByteToWideChar(CP_ACP, 0, _char, strlen(_char) + 1, NULL, 0);
    MultiByteToWideChar(CP_ACP, 0, _char, strlen(_char) + 1, tchar, iLength);
}


HRESULT UserRegister(const char *strDescription)
{
    HKEY hKeyMachine = NULL;
    HKEY hSubKeyMachine = NULL;
    HKEY HKEY_MACHINE_KEY = HKEY_LOCAL_MACHINE;

    QString src = "ACCESS_GENERICALL";

    LPTSTR strSecurity = (LPTSTR)(src.toLatin1().constData());
    //char strDescription[100] = "jxf";
    TCHAR sDescription[100];
    CharToTchar(strDescription, sDescription);

    TCHAR szKeyMachine[MAX_PATH];
    memset(szKeyMachine, 0, MAX_PATH);

    // Create the subkey name.
    HRESULT hr = StringCchPrintf(
        szKeyMachine,
        MAX_PATH,
        TEXT("%s\\%s"),
        REGKEY_MF_BYTESTREAM_HANDLERS,
        psSubKey[0]);

    if (S_OK == hr)
    {
        hr = RegModifySecurity(HKEY_MACHINE_KEY, szKeyMachine, strSecurity);
    }

    // Delete a parent tree which has the subkey
    RegDeleteKeyValue(HKEY_MACHINE_KEY, REGKEY_MF_BYTESTREAM_HANDLERS, psSubKey[0]);

    hr = CreateRegistryKey(HKEY_LOCAL_MACHINE, REGKEY_MF_BYTESTREAM_HANDLERS, &hKeyMachine);

    if (S_OK == hr)
    {
        hr = RegSetValueEx(
            hKeyMachine,
            psSubKey[0],
            0,
            REG_SZ,
            (BYTE *)sDescription,
            (_tcslen(sDescription) + 1) *  sizeof(TCHAR)
            );
    }

    if (hKeyMachine != NULL)
    {
        RegCloseKey(hKeyMachine);
    }

    return hr;
}

void UserDeleteRegister(void)
{
    HKEY HKEY_MACHINE_KEY = HKEY_LOCAL_MACHINE;

    // Delete a parent tree which has the subkey
    RegDeleteKeyValue(HKEY_MACHINE_KEY, REGKEY_MF_BYTESTREAM_HANDLERS, psSubKey[0]);
}


BOOL TCHARtochar(const TCHAR *tchar, char *_char)
{
    int iLength = 0;
    iLength = WideCharToMultiByte(CP_ACP, 0, tchar, -1, NULL, 0, NULL, NULL);
    WideCharToMultiByte(CP_ACP, 0, tchar, -1, _char, iLength, NULL, NULL);

    return true;
}


//InstallLocation          //LICENSEE_KEY
BOOL GetLicensee(TCHAR *szLicensee)
{
    if (NULL == szLicensee)
        return FALSE;

    TCHAR path[512];
    HKEY key;
    const TCHAR *data_set = L"Software\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\\Uninstall\\x265 HEVC Upgrade";
    long hr = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, data_set, 0, KEY_WOW64_64KEY | KEY_READ, &key);
    if (ERROR_SUCCESS == hr)
    {
        TCHAR szValue[512];
        DWORD dwSZType = REG_SZ;
        DWORD dwSize = sizeof(szValue);
        hr = ::RegQueryValueEx(key, psSubKey[0], 0, &dwSZType, (LPBYTE)szValue, &dwSize);
        if (ERROR_SUCCESS == hr)
        {
            wcscpy_s(path, szValue);
        }
        else
            return FALSE;

        lstrcpy(szLicensee, path);

        //if (0 == _tcscmp(szValue, L"jxf"))
        //    MessageBox(NULL, L"jxf", L"information", MB_OK);
        //else
        //    MessageBox(NULL, L"different", L"different information", MB_OK);

        return TRUE;
    }
    else
        return FALSE;


}

int main(int argc, char *argv[])
{
    QSharedMemory sharedMemory;
    sharedMemory.setKey("4da7b8a28a378e7fa9004e7a95cf257e");
    QApplication a(argc, argv);
    if(!sharedMemory.create(1))
    {
        QMessageBox msg;
        msg.setText("Unable to launch. Instance of x265Encoder is already running.");
        msg.setIcon(QMessageBox::Information);
        msg.exec();
        return 0; // Exit already a process running
    }

    int lDpiX = QApplication::desktop()->logicalDpiX();
    int lDpiY = QApplication::desktop()->logicalDpiY();
    QApplication::setAttribute(Qt::AA_UseHighDpiPixmaps, true);
    QFont defaultFont;
    if(lDpiX >= 192 && lDpiY >= 192)
        defaultFont.setPointSize(8);
    else if(lDpiX == 144 && lDpiY == 144)
        defaultFont.setPointSize(8);
    else if(lDpiX == 120 && lDpiY == 120)
        defaultFont.setPointSize(9);
    else
        defaultFont.setPointSize(10);
    QApplication::setFont(defaultFont);
    /* check if all addons are present */
    if (!Addons::allAddonsPresent())
    {
        QMessageBox msg;
        msg.setText("Addons are missing. Please reinstall x265 Encoder");
        msg.setIcon(QMessageBox::Information);
        msg.exec();
        return 1;
    }

    QCoreApplication::setOrganizationName("MCW");
    QCoreApplication::setOrganizationDomain("x265.com");
    QCoreApplication::setApplicationName("x265Encoder");

#ifdef DEMO
    if(!checkLicensePro())
    {
        if(checkLicense())
        {
           if(showLicenceInfo()){}
           else
           {
               Encoder *encoder = Encoder::instance();
               encoder->show();
           }
        }
        else
        {
            activateLicense();
            if(showLicenceInfo()){}
            else
            {
                Encoder *encoder = Encoder::instance();
                encoder->show();
            }
        }
    }
    else
    {
        Encoder *encoder = Encoder::instance();
        encoder->show();
    }
   return a.exec();
#else
    ////Bitanswer
    //if (!checkLicensePro() && !checkLicense()) {
    //        licenseMessageBox();
    //        return 0;
    //}

    QString licensee_input;
    LicenseeInputDialog *licenseeDlg;
    licenseeDlg = new LicenseeInputDialog();


    TCHAR szLicensee[512];
    BOOL bExisted = GetLicensee(szLicensee);
    if (FALSE == bExisted)
    {
        if (licenseMessageBox())
        {
            licenseeDlg->setModal(true);
            licenseeDlg->exec();

            licensee_input = licenseeDlg->GetTextLine();
        }
        else
        {
            return 0;
        }

    }
    else
    {// there is licensee in local machine.
        char strLicensee[512] = {0};
        TCHARtochar(szLicensee, strLicensee);

        licensee_input = QString(QLatin1String(strLicensee));
    }


    //QMessageBox::information(NULL, "information", licensee_input);
    if (licensee_input.isEmpty())
        return 0;

    int status = 0;
    BOOL bDelete = FALSE;
    if (!checkNetlicensingLicense(licensee_input.toStdString(), status)) {
        if (status == 0){
            bDelete = TRUE;
            //licenseMessageBox();
        }
        else {
            QString str;
            switch (status)
            {
            case 1:
                str = "Bad network, can't connect to server, Please try more.";
                break;
            case 204:
                str = "Successful request";
                break;
            case 400:
                bDelete = TRUE;
                str = "Malformed or illegal request";
                break;
            case 403:
                str = "Access is denied";
                break;
            case 404:
                bDelete = TRUE;
                str = "Resource not found";
                break;
            case 500:
                bDelete = TRUE;
                str = "Internal service error";
                break;
            default:
                str = "Unknown error";
                break;
            }
            //for (auto det : e.get_details()) {
            //    std::cerr << det.to_string() << std::endl;
            //}
            QMessageBox msg;
            msg.setText(str);
            msg.setIcon(QMessageBox::Information);
            msg.exec();
        }
        if (bExisted && bDelete) {
            //QMessageBox msg;
            //msg.setText("Information");
            //msg.setIcon(QMessageBox::Information);
            //msg.exec();
            UserDeleteRegister();
        }
        return 0;
    }
    
    //save licensee
    std::string s = licensee_input.toStdString();
    UserRegister(s.c_str());


    if(Update::instance()->chkDownload())
    {
        QMessageBox messagebox;
        messagebox.setIcon(QMessageBox::Information);
        messagebox.setWindowTitle("Update available");
        messagebox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        messagebox.setText("There's an update to HEVC Upgrade that is ready to be installed.");
        messagebox.setInformativeText("Do you want to install it now?");
        if (messagebox.exec() == QMessageBox::Yes)
        {
            Update::instance()->installUpdate();
        }
        else
        {
            Encoder *encoder = Encoder::instance();
            encoder->show();
            int width = encoder->frameGeometry().width();
            int height = encoder->frameGeometry().height();
            QDesktopWidget wid;
            int screenWidth = wid.screen()->width();
            int screenHeight = wid.screen()->height();
            encoder->setGeometry((screenWidth/2)-(width/2),(screenHeight/2)-(height/2),width,height);
            return a.exec();
        }

    }
    else
    {
        QString path = Path::getTempPath();
        QDir dir(path);
        dir.setNameFilters(QStringList() << "x265HEVCUpgrade_update.exe" << "x265HEVCUpgrade_update.txt");
        dir.setFilter(QDir::Files);
        foreach (QString dirFile, dir.entryList())
        {
            dir.remove(dirFile);
        }
        Encoder *encoder = Encoder::instance();
        encoder->show();
        int width = encoder->frameGeometry().width();
        int height = encoder->frameGeometry().height();
        QDesktopWidget wid;
        int screenWidth = wid.screen()->width();
        int screenHeight = wid.screen()->height();
        encoder->setGeometry((screenWidth/2)-(width/2),(screenHeight/2)-(height/2),width,height);
        return a.exec();
    }
#endif
}
