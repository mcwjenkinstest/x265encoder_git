#include "maxbitratedialog.h"
#include "ui_maxbitratedialog.h"

MaxBitRateDialog::MaxBitRateDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MaxBitRateDialog)
{
    ui->setupUi(this);
    readCapValues();
}

MaxBitRateDialog::~MaxBitRateDialog()
{
    delete ui;
}

void MaxBitRateDialog::readCapValues()
{
    if(capSettings.value("spin3840").toInt())
        ui->spin3480->setValue(capSettings.value("spin3840").toInt());
    else
        capSettings.setValue("spin3840", ui->spin3480->value());

    if(capSettings.value("spin2160").toInt())
        ui->spin2160->setValue(capSettings.value("spin2160").toInt());
    else
        capSettings.setValue("spin2160", ui->spin2160->value());

    if(capSettings.value("spin1080").toInt())
        ui->spin1080->setValue(capSettings.value("spin1080").toInt());
    else
        capSettings.setValue("spin1080", ui->spin1080->value());

    if(capSettings.value("spin720").toInt())
        ui->spin720->setValue(capSettings.value("spin720").toInt());
    else
        capSettings.setValue("spin720", ui->spin720->value());

    if(capSettings.value("spin480").toInt())
        ui->spin480->setValue(capSettings.value("spin480").toInt());
    else
        capSettings.setValue("spin480", ui->spin480->value());

    if(capSettings.value("spin240").toInt())
        ui->spin240->setValue(capSettings.value("spin240").toInt());
    else
        capSettings.setValue("spin240", ui->spin240->value());
}

void MaxBitRateDialog::writeCapValues()
{
    capSettings.setValue("spin3840", ui->spin3480->value());
    capSettings.setValue("spin2160", ui->spin2160->value());
    capSettings.setValue("spin1080", ui->spin1080->value());
    capSettings.setValue("spin720", ui->spin720->value());
    capSettings.setValue("spin480", ui->spin480->value());
    capSettings.setValue("spin240", ui->spin240->value());
}

void MaxBitRateDialog::on_btnSave_clicked()
{
    writeCapValues();
    close();
}

void MaxBitRateDialog::on_btnCancel_clicked()
{
    close();
}

void MaxBitRateDialog::on_btnReset_clicked()
{
    ui->spin3480->setValue(20000);
    ui->spin2160->setValue(12000);
    ui->spin1080->setValue(6000);
    ui->spin720->setValue(3500);
    ui->spin480->setValue(1500);
    ui->spin240->setValue(700);
    capSettings.setValue("spin3840", 20000);
    capSettings.setValue("spin2160", 12000);
    capSettings.setValue("spin1080", 6000);
    capSettings.setValue("spin720", 3500);
    capSettings.setValue("spin480", 1500);
    capSettings.setValue("spin240", 700);
}
