#include <QProcess>
#include "mediainfo.h"
#include "addons.h"

/* Returns General details about the input file: FileName, FileExtensio, FileSize respectively*/
QStringList MediaInfo::getGeneralDetails(QString fileName)
{
    QProcess process;
    QString args = " --Output=file://" + Addons::MediaInfoArgGeneral() + " " + fileName;
    QStringList fileDetails;
    process.setNativeArguments(args);
    process.start(Addons::MediaInfoExe(), QIODevice::ReadWrite | QIODevice::Text);
    if(process.waitForFinished())
    {
        fileDetails << QString(process.readAllStandardOutput()).split('\n');
        process.kill();
    }
    return fileDetails;
}

/* Returns Video details for input file: Format, Width, Height respectively */
QStringList MediaInfo::getVideoDetails(QString fileName)
{
    QProcess process;
    QString args = " --Output=file://" + Addons::MediaInfoArgVideo() + " " + fileName;
    QStringList fileDetails;
    process.setNativeArguments(args);
    process.start(Addons::MediaInfoExe(), QIODevice::ReadWrite | QIODevice::Text);
    if(process.waitForFinished())
    {
        fileDetails << QString(process.readAllStandardOutput()).split('\n');
        process.kill();
    }
    return fileDetails;
}

/* Returns Audio details for the input file:Format, BitRate, SamplingRate, Channels respectively */
QStringList MediaInfo::getAudioDetails(QString fileName)
{
    QProcess process;
    QString args = " --Output=file://" + Addons::MediaInfoArgAudio() + " " + fileName;
    QStringList fileDetails;
    process.setNativeArguments(args);
    process.start(Addons::MediaInfoExe(), QIODevice::ReadWrite | QIODevice::Text);
    if(process.waitForFinished())
    {
        fileDetails << QString(process.readAllStandardOutput()).split('\n');
        process.kill();
    }
    return fileDetails;
}

/* Returns SubTitle details for the input file:Format */
QStringList MediaInfo::getSubDetails(QString fileName)
{
    QProcess process;
    QString args = " --Output=file://" + Addons::MediaInfoArgSub() + " " + fileName;
    QStringList fileDetails;
    process.setNativeArguments(args);
    process.start(Addons::MediaInfoExe(), QIODevice::ReadWrite | QIODevice::Text);
    if(process.waitForFinished())
    {
        fileDetails << QString(process.readAllStandardOutput()).split('\n');
        process.kill();
    }
    return fileDetails;
}
