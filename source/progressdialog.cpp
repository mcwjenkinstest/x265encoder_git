#include <QScrollBar>
#include "progressdialog.h"
#include "ui_progressdialog.h"
#include "encoder.h"

ProgressDialog::ProgressDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProgressDialog)
{
    ui->setupUi(this);
    ui->txtConsole->setStyleSheet("background:black;color:white;font-size:11px;");
    connect(ui->btnStop, SIGNAL(clicked()), parent, SLOT(btnStopClicked()));
    connect(ui->btnPause, SIGNAL(clicked()), parent, SLOT(btnPauseClicked()));
    connect(ui->btnResume, SIGNAL(clicked()), parent, SLOT(btnResumeClicked()));
    connect(ui->btnOutputFolder, SIGNAL(clicked()), parent, SLOT(btnOutputClicked()));
    ui->btnOutputFolder->hide();
}

void ProgressDialog::setMin(int min)
{
    ui->progressEnc->setMinimum(min);
    ui->progressEncFile->setMinimum(min);
}

void ProgressDialog::setMax(int max)
{
    ui->progressEnc->setMaximum(max);
}

void ProgressDialog::setMaxSingle(int max)
{
    ui->progressEncFile->setMaximum(max);
}

void ProgressDialog::setVal(int val)
{
    ui->progressEnc->setValue(val);
    Encoder::instance()->taskbarProgress->setValue(val);
}

void ProgressDialog::setValSingle(int val)
{
    ui->progressEncFile->setValue(val);
    //Encoder::instance()->taskbarProgress->setValue(val);
}

int ProgressDialog::getVal()
{
    return ui->progressEnc->value();
}

int ProgressDialog::getValSingle()
{
    return ui->progressEncFile->value();
}

void ProgressDialog::increment()
{
    setVal(ui->progressEnc->value() + 1);
}

void ProgressDialog::incrementSingle()
{
    setValSingle(ui->progressEncFile->value() + 1);
}

ProgressDialog::~ProgressDialog()
{
    delete ui;
}

void ProgressDialog::on_checkDetails_toggled(bool checked)
{
    if(checked)
    {
        ui->txtConsole->show();
        ui->outputQueue->show();
        ui->detailsLayout->addWidget(ui->txtConsole);
        ui->detailsLayout->addWidget(ui->outputQueue);
        this->resize(this->parentWidget()->width()/1.5, this->parentWidget()->height()/2);
    }
    else
    {
        ui->txtConsole->hide();
        ui->outputQueue->hide();
        ui->detailsLayout->removeWidget(ui->txtConsole);
        ui->detailsLayout->removeWidget(ui->outputQueue);
        this->adjustSize();
    }
}

void ProgressDialog::closeEvent(QCloseEvent *event)
{
    ui->outputQueue->setRowCount(0);
    ui->outputQueue->setColumnCount(0);
    Encoder::instance()->outputDestination.clear();
    Encoder::instance()->outputFile.clear();
    Encoder::instance()->outputEncoded.clear();
    event->accept();
}

/* print console output */
void ProgressDialog::printOutput(QString outputText)
{
    QTextEdit *console = ui->txtConsole;
    QScrollBar *scrollBar = console->verticalScrollBar();
    console->setHtml(console->toHtml() + "<font>" + outputText + "</font>");
    scrollBar->setValue(scrollBar->maximum());
}

void ProgressDialog::showPause(bool pause)
{
    if(pause)
    {
        ui->btnResume->hide();
        ui->btnPause->show();
    }
    else
    {
        ui->btnPause->hide();
        ui->btnResume->show();
    }
}

bool ProgressDialog::isPaused()
{
    return ui->btnPause->isHidden();
}

void ProgressDialog::setTitleBar(QString title)
{
    ui->lblProcess->setText(title);
}
