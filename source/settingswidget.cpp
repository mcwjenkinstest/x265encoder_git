#include <QFileDialog>
#include <QSettings>
#include <QStandardPaths>
#include <QDesktopServices>
#include <QPalette>
#include "math.h"
#include "filecontainer.h"
#include "settingswidget.h"
#include "ui_settingswidget.h"
#include "ui_yuvdialog.h"
#include "addons.h"
#include "maxbitratedialog.h"
#include "settingutils.h"
#include "systeminformation.h"
#include "mediainfo.h"
#include "yuvdialog.h"

#define DEFAULT_PRESET 4
#define DEFAULT_BIT_RATE 28
#define DEFAULT_UTILIZATION 100
#define MinCoreRequired 4

enum Settings {
    Basic, Advanced
};

SettingsWidget::SettingsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingsWidget)
{
    ui->setupUi(this);
    ui->lblHeader2->setStyleSheet("color:rgb(140,140,140);padding-bottom:50px;/*font-size:33px;*/");
    //ui->grpOutputFolder->setStyleSheet("QGroupBox{font-size:12px;}");
    connect(&txtBasic, SIGNAL(clicked()), this, SLOT( basicClicked()));
    connect(&txtAdvanced, SIGNAL(clicked()), this, SLOT( advancedClicked()));
    ui->stackedWidget->setCurrentIndex(0);
    ui->tabWidget->setCurrentIndex(0);
    ui->spinBitRate->hide();
    ui->spinQP->hide();
    ui->helpBitRate->hide();
    ui->helpQP->hide();
    ui->spinCustomSarWidth->setEnabled(false);
    ui->spinCustomSarHeight->setEnabled(false);
    setCropArea(false);
    QString bgColor = palette().color(QPalette::Normal, QPalette::Window).name();
    initCpuUsage();
    defaultSettings();
    selectedStyle = "QLabel{\
                            font-weight:bold;\
                            /*font-size:12px;*/\
                            border: 1px solid #bbb;\
                            border-bottom:none;\
                            background:#fff;\
                            border-bottom:0px;\
                            color:#000;\
                            padding: 7px 0px;\
                            qproperty-alignment:AlignCenter\
                         }";
   unselectedStyle = "QLabel{\
                             font-weight:bold;\
                             /*font-size:12px;*/\
                             border: 1px solid #bbb;\
                             border-bottom:none;\
                             background:#ddd;\
                             border-bottom:0px;\
                             color:#999;\
                             padding: 7px 0px;\
                             qproperty-alignment:AlignCenter\
                            }\
                       QLabel:hover{\
                             background:#bbb;\
                             color:black;}\
                       QToolTip {\
                             background: #000;\
                             padding: 1px;\
                             color:#fff;\
                             border-radius: 3px;\
                             opacity: 200;\
                            }";
    selecetedSettings = Basic;
    ui->layoutHSetting->addWidget(&txtBasic);
    ui->layoutHSetting->addWidget(&txtAdvanced);
    txtBasic.setStyleSheet(selectedStyle);
    txtBasic.setText("Basic Mode");
    txtAdvanced.setStyleSheet(unselectedStyle);
    txtAdvanced.setText("Advanced Mode");
    txtAdvanced.setToolTip("For expert users only");
    txtAdvanced.setToolTipDuration(-1);
    setStyleSheet("#frame{\
                    background:#bbb;\
                    margin-top:0px;\
                   }\
                   #grpOutputFolder{\
                    background:" + bgColor + "\
                   }\
                   #grpSettings{\
                    background:" + bgColor + "\
                   }");
    QString sliderStyle = "QSlider::groove:horizontal {\
                    border: 1px solid #999;\
                    height: 3px; \
                    background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #B1B1B1, stop:1 #c4c4c4);\
                    margin: 2px 0;\
                    }\
                    QSlider::handle:horizontal {\
                    background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\
                    border: 1px solid #5c5c5c;\
                    width: 8px;\
                    margin: -8px 0;\
                    border-radius: 2px;\
                    }";
    ui->sldBitRate->setStyleSheet(sliderStyle);
    ui->sldPreset->setStyleSheet(sliderStyle);
    ui->sldCpuUtilization->setStyleSheet(sliderStyle);
    ui->btnCap->setStyleSheet("QPushButton {\
                                  border: 1px solid #999;\
                                  border-radius: 5px;\
                                  background:none;\
                                  background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\
                                                              stop: 0 #f6f7fa, stop: 1 #dadbde);\
                                  /*font-size:11px;*/\
                                  min-height:25px;\
                                  min-width:95px;\
                               }\
                               QPushButton:pressed {\
                                  background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\
                                                              stop: 0 #dadbde, stop: 1 #f6f7fa);\
                               }\
                               QPushButton:flat {\
                                  border: none; \
                               }\
                               QPushButton:default {\
                                  border-color: navy; \
                               }");

    // preset warning message
    presetWarnMsg = new QMessageBox(this);
    presetWarnMsgCheck = new QCheckBox();
    presetWarnMsg->setCheckBox(presetWarnMsgCheck);
    presetWarnMsg->setWindowTitle("Warning");
    presetWarnMsg->setIcon(QMessageBox::Warning);
    presetWarnMsg->setText("Changing preset will affect settings in other tabs.");
    presetWarnMsg->setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    presetWarnMsgCheck->setText("Accept future changes?");
    presetWarnMsg->setModal(true);
    presetWarnMsg->setStyleSheet("QPushButton{background:none;}");

    // tune warning message
    tuneWarnMsg = new QMessageBox(this);
    tuneWarnMsgCheck = new QCheckBox();
    tuneWarnMsg->setCheckBox(tuneWarnMsgCheck);
    tuneWarnMsg->setWindowTitle("Warning");
    tuneWarnMsg->setIcon(QMessageBox::Warning);
    tuneWarnMsg->setText("Changing tune will affect settings in other tabs.");
    tuneWarnMsg->setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    tuneWarnMsgCheck->setText("Accept future changes?");
    tuneWarnMsg->setModal(true);
    tuneWarnMsg->setStyleSheet("QPushButton{background:none;}");
    connect(ui->comboPresetAdv, SIGNAL(currentIndexChanged(int)), this, SLOT(comboPresetAdv_currentIndexChanged(int)));
    connect(ui->comboTuneAdv, SIGNAL(currentIndexChanged(int)), this, SLOT(comboTuneAdv_currentIndexChanged(int)));
    enableBluRay = false;
}

SettingsWidget::~SettingsWidget()
{
    delete ui;
}

bool SettingsWidget::presetChecked = false;
bool SettingsWidget::tuneChecked = false;

int SettingsWidget::sldBitRateInvertedValue(int value) const
{
    static QSlider *sldBitRate = ui->sldBitRate;
    static int minBitRate = sldBitRate->minimum();
    static int maxBitRate = sldBitRate->maximum();
    return minBitRate + maxBitRate - value;
}

void SettingsWidget::initCpuUsage()
{
    //_SYSTEM_INFO sys;
    //GetNativeSystemInfo(&sys);
    //coresAvailable = sys.dwNumberOfProcessors;
    SystemInformation *sysInfo = new SystemInformation();
    coresAvailable = sysInfo->getLogicalProcessorCount();
    numaNodesCount = sysInfo->getNumaNodeCount();
    int step =  ceil(100/coresAvailable);
    int mincore = ceil(200.0/coresAvailable);
    int count = (100 - mincore)/step;
    for (int i = 0 ; i <= count; i++)
    {
        stepList << QString::number(100-step*i);
    }
    ui->sldCpuUtilization->setSliderPosition(DEFAULT_UTILIZATION);
    ui->lblCpuUtilization->setText(QString::number(DEFAULT_UTILIZATION) + "%");
    ui->sldCpuUtilization->setSingleStep(ceil(100 / coresAvailable));
    ui->sldCpuUtilization->setPageStep(ceil(100 / coresAvailable));
    if(coresAvailable < MinCoreRequired)
    {
        ui->sldCpuUtilization->setEnabled(false);
        ui->lblCpuMin->setEnabled(false);
        ui->lblCpuUsage->setEnabled(false);
        ui->lblCpuMax->setEnabled(false);
        ui->lblCpuUtilization->setEnabled(false);
    }
    else
    {
       int minCorePercent = ceil(200.0/coresAvailable);
       int pageStep = floor(100.0/coresAvailable);
       ui->lblCpuMin->setText(QString::number(minCorePercent) + "%");
       ui->sldCpuUtilization-> setMinimum(minCorePercent);
       ui->sldCpuUtilization->setPageStep(pageStep);
    }
}

/* open file dialog for selecting destination directory */
void SettingsWidget::on_btnDestination_clicked()
{
    QString dir = QFileDialog::getSaveFileName(this, tr("Save File"),
                                                 FileContainer::instance()->outputFolder + "/" +  ui -> txtFile -> text(),
                                                 tr("HEVC(*.mp4 *.mkv)"));
    if(dir.isEmpty() || dir.isNull())
        return;
//    if(!dir.endsWith(".mp4",Qt::CaseInsensitive))
//        dir += ".mp4";
    if(dir.endsWith(".mp4",Qt::CaseInsensitive))
            dir.remove(".mp4");
    else if(dir.endsWith(".mkv",Qt::CaseInsensitive))
            dir.remove(".mkv");
    int index = dir.lastIndexOf("/");
    FileContainer::instance()->outputFolder = dir.mid(0, index);
    ui->txtDest->setText(FileContainer::instance()->outputFolder);
    ui->txtFile->setText(dir.mid(index + 1, dir.length()));
    FileContainer::instance()->updateOutputFolder(FileContainer::instance()->outputFolder);
}

void SettingsWidget::on_btnLogDest_clicked()
{
    QString dir = QFileDialog::getSaveFileName(this, tr("Save File"),
                                             ui->txtLogFile->text(),
                                             tr("CSV(*.csv)"));
    if(dir.isEmpty() || dir.isNull())
        return;
    if(!dir.endsWith(".csv",Qt::CaseInsensitive))
        dir += ".csv";
    ui->txtLogFile->setText(dir);
}

void SettingsWidget::on_chkTransSkip_stateChanged(int value)
{
    if(value)
        ui->chkFastTransSkip->setEnabled(true);
    else
    {
        ui->chkFastTransSkip->setChecked(false);
        ui->chkFastTransSkip->setEnabled(false);
    }
}

void SettingsWidget::on_spinConsBFrame_valueChanged(int value)
{
    ui->spinLookAhead->setMinimum(value);
}

/* Maps the value of the preset slider to corrosponding preset*/
QString SettingsWidget::mapPreset(int value) const
{
    QString preset;
    switch (value)
    {
        case 0:
            preset = "superfast";
            break;
        case 1:
            preset = "veryfast";
            break;
        case 2:
            preset = "faster";
            break;
        case 3:
            preset = "fast";
            break;
        case 4:
            preset = "medium";
            break;
        case 5:
            preset = "slow";
            break;
        case 6:
            preset = "slower";
            break;
        case 7:
            preset = "veryslow";
            break;
        default:
            preset = "medium";
            break;
    }
    return preset;
}

void SettingsWidget::on_sldPreset_valueChanged(int value)
{
     ui->lblPresetVal->setText(mapPreset(value));
     ui->comboPresetAdv->setCurrentIndex(value + 1);
}

void SettingsWidget::on_sldBitRate_valueChanged(int value)
{
    ui->lblBitRateVal->setText(QString::number(sldBitRateInvertedValue(value)));
    ui->spinCRF->setValue(sldBitRateInvertedValue(value));
}

void SettingsWidget::on_comboRateControl_currentIndexChanged(int value)
{
   if(value == 0)
   {
       ui->spinCRF->show();
       ui->spinBitRate->hide();
       ui->spinQP->hide();
       ui->helpCRF->show();
       ui->helpBitRate->hide();
       ui->helpQP->hide();
       ui->groupVBV->setDisabled(false);
       ui->checkTwoPass->setEnabled(false);
       ui->checkTwoPass->setChecked(false);
       ui->checkSlowFirstPass->setEnabled(false);
       ui->checkSlowFirstPass->setChecked(false);
       ui->chkStrictCbr->setEnabled(false);
       ui->chkStrictCbr->setChecked(false);
       ui->spinQpMin->setEnabled(true);
       ui->spinQpMax->setEnabled(true);
   }
   else if(value == 1)
   {
       ui->spinCRF->hide();
       ui->spinBitRate->show();
       ui->spinQP->hide();
       ui->helpCRF->hide();
       ui->helpBitRate->show();
       ui->helpQP->hide();
       ui->groupVBV->setDisabled(false);
       ui->checkTwoPass->setEnabled(true);
       if(ui->spinVbvBufferSize->value() > 0)
            ui->chkStrictCbr->setEnabled(true);
       ui->spinQpMin->setEnabled(true);
       ui->spinQpMax->setEnabled(true);
   }
   else if(value == 2)
   {
       ui->spinCRF->hide();
       ui->spinBitRate->hide();
       ui->spinQP->show();
       ui->helpCRF->hide();
       ui->helpBitRate->hide();
       ui->helpQP->show();
       ui->groupVBV->setDisabled(true);
       ui->checkTwoPass->setEnabled(false);
       ui->checkTwoPass->setChecked(false);
       ui->checkSlowFirstPass->setEnabled(false);
       ui->checkSlowFirstPass->setChecked(false);
       ui->chkStrictCbr->setEnabled(false);
       ui->chkStrictCbr->setChecked(false);
       ui->spinQpMin->setEnabled(false);
       ui->spinQpMax->setEnabled(false);
   }
}

void SettingsWidget::on_comboOverScan_currentIndexChanged(int value)
{
    if(value == 2)
        setCropArea(true);
    else
        setCropArea(false);
}

void SettingsWidget::on_comboProfileAdv_currentIndexChanged(int value)
{
     if (FileContainer::instance()->count() == 0)
         return;
     int currIndex =  FileContainer::instance()->currentRow();
     int depth = FileContainer::instance()->videoDepth.at(currIndex).toInt();
     QString SubSampling =  FileContainer::instance()->subSampling.at(currIndex);
     if (SubSampling.compare("4:2:2", Qt::CaseInsensitive) == 0 && depth == 10 && (value ==2 || value == 5))
     {
         QMessageBox msg;
         msg.setText(ui->comboProfileAdv->currentText() + " profile is not compatible with i422 colorspace with bitdepth 10. Setting profile to main422-10");
         msg.setIcon(QMessageBox::Information);
         msg.exec();
         ui->comboProfileAdv->setCurrentIndex(3);
     }
     else if (SubSampling.compare("4:4:4", Qt::CaseInsensitive) == 0 && depth == 10 && (value == 2 || value == 3 || value == 5 || value == 6))
     {
         QMessageBox msg;
         msg.setText(ui->comboProfileAdv->currentText() + " profile is not compatible with i444 colorspace with bitdepth 10. Setting profile to main444-10");
         msg.setIcon(QMessageBox::Information);
         msg.exec();
         ui->comboProfileAdv->setCurrentIndex(4);
     }


}

void SettingsWidget::on_checkTwoPass_clicked()
{
    ui->checkSlowFirstPass->setDisabled(!ui->checkTwoPass->isChecked());
    if(ui->checkSlowFirstPass->isChecked())
        ui->checkSlowFirstPass->setChecked(ui->checkTwoPass->isChecked());
}

void SettingsWidget::on_checkCustomSAR_clicked()
{
    if(ui->checkCustomSAR->isChecked())
    {
        ui->spinCustomSarWidth->setEnabled(true);
        ui->spinCustomSarHeight->setEnabled(true);
        ui->comboSAR->setDisabled(true);
    }
    else
    {
        ui->spinCustomSarWidth->setEnabled(false);
        ui->spinCustomSarHeight->setEnabled(false);
        ui->comboSAR->setDisabled(false);
    }

}

void SettingsWidget::on_btnQPFile_clicked()
{
    QString fileDir;
    if(ui->txtQPFile->text().isEmpty())
        fileDir = ui->txtDest->text();
    else fileDir = ui->txtQPFile->text();
    if(fileDir.endsWith(":"))
        fileDir += "/";
    QString qpFile = QFileDialog::getOpenFileName(this,
                                                  "Add Custom QP Files",
                                                  fileDir,
                                                  "QP File (*.qp);;\
                                                   All Files(*.*)" );
    ui->txtQPFile->setText(qpFile);
}

void SettingsWidget::on_btnScalingFile_clicked()
{
    QString fileDir;
    if(ui->txtScalingFile->text().isEmpty())
        fileDir = ui->txtDest->text();
    else fileDir = ui->txtScalingFile->text();
    if(fileDir.endsWith(":"))
        fileDir += "/";
    QString scFile = QFileDialog::getOpenFileName(this,
                                                  "Add Scaling List Files",
                                                  fileDir,
                                                  "All Files(*.*)" );
    ui->txtScalingFile->setText(scFile);
}

void SettingsWidget::on_btnLambdaFile_clicked()
{
    QString fileDir;
    if(ui->txtLambdaFile->text().isEmpty())
        fileDir = ui->txtDest->text();
    else fileDir = ui->txtLambdaFile->text();
    if(fileDir.endsWith(":"))
        fileDir += "/";
    QString ldFile = QFileDialog::getOpenFileName(this,
                                                  "Add Lambda Files",
                                                  fileDir,
                                                  "All Files(*.*)" );
    ui->txtLambdaFile->setText(ldFile);
}

void SettingsWidget::basicClicked()
{
    if(selecetedSettings != Basic)
        toggleSettings();
}

void SettingsWidget::advancedClicked()
{
    if(selecetedSettings == Basic)
        toggleSettings();
}

void SettingsWidget::on_spinAQStrength_valueChanged(double value)
{
    if(value == 0.00)
    {
        ui->comboAdaptQuant->setCurrentIndex(0);
        ui->comboAdaptQuant->setEnabled(false);
    }
    else
        ui->comboAdaptQuant->setEnabled(true);
}

/* Set default settings on startup */
void SettingsWidget::defaultSettings()
{
    int bitRate;
    bitRate = sldBitRateInvertedValue(DEFAULT_BIT_RATE);
    ui->sldBitRate->setSliderPosition(DEFAULT_BIT_RATE);
    ui->sldBitRate->setSliderPosition(bitRate);
    ui->sldPreset->setSliderPosition(DEFAULT_PRESET);
    ui->lblPresetVal->setText("medium");
    ui->txtDest->setText(QStandardPaths::writableLocation(QStandardPaths::MoviesLocation));
    ui->stackedWidget->setStyleSheet("#stackedWidget{background:#fff; border: 1px solid #bbb;border-top:none;}");
    ui->tabWidget->setStyleSheet("/*font-size:12px;*/ background:none;");
    QGraphicsDropShadowEffect *capEffect = new QGraphicsDropShadowEffect();
    capEffect->setBlurRadius(1);
    capEffect->setOffset(1,1);
    ui->btnCap->setGraphicsEffect(capEffect);
    defaultAdvSettings();
}

void SettingsWidget::defaultAdvSettings()
{
    // Main page
    ui->comboPresetAdv->setCurrentIndex(5);
    ui->comboTuneAdv->setCurrentIndex(0);
    ui->comboLevelAdv->setCurrentIndex(0);
    //ui->comboProfileAdv->setCurrentIndex(1);
    ui->comboTierAdv->setCurrentIndex(0);
    ui->comboLogLevel->setCurrentIndex(2);
    ui->txtPools->setText("*");
    ui->spinFrameThreads->setValue(0);
    ui->chkWPP->setChecked(true);
    ui->chkParallelModeAnalysis->setChecked(false);
    ui->chkParallelMotionEst->setChecked(false);
    ui->spinLookAheadSlices->setValue(8);
    currPresetIndex = 5;
    currTuneIndex = 0;

    // Frames Page
    ui->spinFrames->setValue(0);
    ui->spinSeek->setValue(0);
    ui->spinMaxIntraPeriod->setValue(250);
    ui->spinMinGOPSize->setValue(0);
    ui->spinScenecut->setValue(40);
    ui->spinConsBFrame->setValue(4);
    ui->spinLookAhead->setMinimum(ui->spinConsBFrame->value());
    ui->spinLookAhead->setValue(20);
    ui->spinBFrameBias->setValue(0);
    ui->spinMaxRef->setValue(3);
    ui->spinLimitRef->setValue(3);
    ui->comboAdaptiveB->setCurrentIndex(2);
    ui->chkBRef->setChecked(true);
    ui->chkEnableGOP->setChecked(true);
    ui->comboPenaltyTU->setCurrentIndex(0);
    ui->chkTransSkip->setChecked(false);
    ui->chkFastTransSkip->setChecked(false);
    ui->chkConsIntra->setChecked(false);
    ui->chkStrongIntra->setChecked(true);
    ui->chkIntraInBFrames->setChecked(true);

    //RateControl page
    ui->chkStrictCbr->setEnabled(false);

    // Quantization page
    ui->comboAdaptQuant->setCurrentIndex(1);
    ui->spinAQStrength->setValue(1.0);
    ui->spinCbOffset->setValue(0);
    ui->spinCrOffset->setValue(0);
    ui->spinNoiseRedIntra->setValue(0);
    ui->spinNoiseRedInter->setValue(0);

    // Motion page
    ui->comboMotionMethod->setCurrentIndex(1);
    ui->spinMotionRange->setValue(57);
    ui->spinSubPixel->setValue(2);
    ui->chkRect->setChecked(false);
    ui->chkAssymetric->setChecked(false);
    ui->chkAssymetric->setEnabled(false);
    ui->spinMaxMerge->setValue(2);

    // Analysis page
    ui->spinRDO->setValue(3);
    ui->spinRDOQLevel->setValue(0);
    ui->chkEarlySkip->setChecked(false);
    ui->chkWeightedP->setChecked(true);
    ui->chkWeightedB->setChecked(false);
    ui->chkCULossless->setChecked(false);
    ui->chkHideSign->setChecked(true);
    ui->comboMinCUSize->setCurrentIndex(3);
    ui->comboMaxCUSize->setCurrentIndex(0);
    ui->chkCUTree->setChecked(true);
    ui->spinTUInterDepth->setValue(1);
    ui->spinTUIntraDepth->setValue(1);
    ui->spinPsyRDO->setValue(2.00);
    ui->spinPsyRDOQuant->setValue(1.00);
    ui->chkLimitModes->setChecked(false);
    ui->chkRskip->setChecked(true);

    // VUI page
    ui->comboSAR->setCurrentIndex(0);
    ui->checkCustomSAR->setChecked(false);
    ui->spinCustomSarWidth->setEnabled(false);
    ui->spinCustomSarHeight->setEnabled(false);
    ui->spinCustomSarWidth->setValue(1);
    ui->spinCustomSarHeight->setValue(1);
    ui->comboOverScan->setCurrentIndex(0);
    setCropArea(false);
    ui->comboVideoFormat->setCurrentIndex(0);
    ui->comboRange->setCurrentIndex(0);
    ui->comboColorPrim->setCurrentIndex(0);
    ui->comboTransferChar->setCurrentIndex(0);
    ui->comboColorMatrix->setCurrentIndex(0);
    ui->comboChromaloc->setCurrentIndex(0);

    // Misc page
    ui->chkRepeatHeader->setChecked(false);
    ui->chkEmitSEI->setChecked(true);
    ui->chkHRDSignal->setChecked(false);
    ui->chkAUDSignal->setChecked(false);
    ui->chkLoopFilter->setChecked(true);
    ui->chkSAO->setChecked(true);
    ui->radioNonDeblock->setChecked(false);
    ui->radioSkipRightBottom->setChecked(true);

    // Tools page
    ui->videoProcessing->setHidden(true);
    //QRegExpValidator* integer = new QRegExpValidator(QRegExp("\\d*"), this);
    QRegExpValidator* fp = new QRegExpValidator(QRegExp("[0-9]*\\.?[0-9]*"), this);
    //ui->Width->setValidator(integer);
    //ui->Height->setValidator(integer);
    ui->frameRate->setValidator(fp);
    //ui->chkDAR->setChecked(true);
    ui->chkAspectRatio->setChecked(true);
    ui->chkAspectRatio->setEnabled(false);
}

void SettingsWidget::writeSettings()
{

}

void SettingsWidget::setCropArea(bool enable)
{
    ui->spinCropLeft->setEnabled(enable);
    ui->spinCropTop->setEnabled(enable);
    ui->spinCropRight->setEnabled(enable);
    ui->spinCropBottom->setEnabled(enable);
}

//void SettingsWidget::on_sldCpuUtilization_valueChanged(int value)
//{
//    ui->lblCpuUtilization->setText(QString::number(value) + "%");
//}

QString SettingsWidget::getFfmpegArgs(int fileIndex)
{
    QString ffmpeg = Addons::FFmpegExe();
    if (ui->stackedWidget->currentIndex() == 0)
        return ffmpeg + getBasicFfmpegArgs(fileIndex);
    else
        return ffmpeg + getAdvFfmpegArgs(fileIndex);
}

QString SettingsWidget::getX265Args(int pass, QString frames)
{
    QString x265;
    if (!enableBluRay)
    {
        if(ui->comboProfileAdv->currentIndex() <=1)
            x265 = Addons::x2658BitExe();
        else if (ui->comboProfileAdv->currentIndex() >= 2 && ui->comboProfileAdv->currentIndex() <= 4)
            x265 = Addons::x26510BitExe();
        else if (ui->comboProfileAdv->currentIndex() >= 5)
            x265 = Addons::x26512BitExe();
    }
    else
    {
        x265 = Addons::x26510BitExe();
    }

    if (ui->stackedWidget->currentIndex() == 0)
        return Addons::x2658BitExe() + getBasicX265Args();
    else if (pass == 1)
        return x265 + getAdvX265ArgsPass1();
    else if (pass == 2)
        return x265 + getAdvX265ArgsPass2(frames);
    return x265;
}

QString SettingsWidget::getBasicFfmpegArgs(int fileIndex)
{
    QString file = QString("\"") +  FileContainer::instance()->item(fileIndex)->text() + QString("\"");
    QStringList generalInfo = MediaInfo::getGeneralDetails(QString("\"") + file + QString("\""));
    QString args;
    QString ffmpegThreads = "";
    if(ui->sldCpuUtilization->isEnabled())
    {
       int threads = ceil((ui->sldCpuUtilization->value()/100.0) * coresAvailable);
       if(threads == 1)
           ffmpegThreads = " -threads 1";
       else
       {
           int fthreads =  ceil((25/100.0)*  threads);
           ffmpegThreads = " -threads " + QString::number(fthreads);
       }
    }
    if(generalInfo.at(1).compare("YUV", Qt::CaseInsensitive) == 0)
    {

        int depth = FileContainer::instance()->videoDepth.at(0).toInt();
        //QString colorSpace = YuvDialog->ui->ColorSpace->currentText();
        float fps =  FileContainer::instance()->fps.at(0).toFloat();
        int height = FileContainer::instance()->videoHeight.at(0).toInt();
        int width = FileContainer::instance()->videoWidth.at(0).toInt();
        QString SubSampling =  FileContainer::instance()->subSampling.at(0);
        if (SubSampling.compare("4:4:4", Qt::CaseInsensitive) == 0 && depth == 8)
            SubSampling = QString("yuv444p");
        else if (SubSampling.compare("4:2:2", Qt::CaseInsensitive) == 0 && depth == 8)
            SubSampling = QString("yuv422p");
        else if (SubSampling.compare("4:2:0", Qt::CaseInsensitive) == 0 && depth == 8)
            SubSampling = QString("yuv420p");
        else if (SubSampling.compare("4:4:4", Qt::CaseInsensitive) == 0 && depth == 10)
            SubSampling = QString("yuv444p10");
        else if (SubSampling.compare("4:2:2", Qt::CaseInsensitive) == 0 && depth == 10)
            SubSampling = QString("yuv422p10");
        else if (SubSampling.compare("4:2:0", Qt::CaseInsensitive) == 0 && depth == 10)
            SubSampling = QString("yuv420p10");

        args =  " -s " +  QString::number( width) + "x" +  QString::number(height) + " -r " +
                QString::number(fps) + " -bits_per_raw_sample "  +  QString::number(depth) +
                " -pix_fmt " + SubSampling + " -i " + file + ffmpegThreads + " -strict -1 -f yuv4mpegpipe -";
    }
    else
    {
         args =  " -i " + file + ffmpegThreads + " -pix_fmt yuv420p -f yuv4mpegpipe -";
    }
    return args;
}

QString SettingsWidget::getBasicX265Args()
{
    QString args =  " --input - --y4m --preset " + ui->lblPresetVal->text();
    QString x265Threads, vbvSettings;
    int maxRate, bufSize, bitRate, height, cap;
    height = FileContainer::instance()->videoHeight.at(0).toInt();
    bitRate = FileContainer::instance()->bitRate.at(0).toFloat() / 1000;
    cap = getMaxRate(height);
    if(bitRate)
    {
        maxRate = bitRate < cap ? bitRate : cap;
    }
    else
        maxRate = cap;
    bufSize = 2 * maxRate;
    if(ui->sldCpuUtilization->isEnabled())
    {
       int threads = ceil((ui->sldCpuUtilization->value()/100.0) * coresAvailable);
       if(threads == 1)
           x265Threads = " --pools 1";
       else
       {
           int fthreads =  ceil((25/100.0)*  threads);
           int xthreads = threads - fthreads;
           int numaTemp = numaNodesCount;
           QString poolArgs = "";
           while(numaTemp)
           {
               poolArgs += QString::number(ceil(xthreads / numaNodesCount));
               if(numaTemp != 1)
                   poolArgs += ",";
               numaTemp--;
           }
           x265Threads = " --pools " + poolArgs;
       }
    }
    if (!FileContainer::instance()->totalFrames.at(0).isEmpty())
        args += " --frames " + FileContainer::instance()->totalFrames.at(0);

    vbvSettings = " --vbv-maxrate " + QString::number(maxRate);
    vbvSettings += " --vbv-bufsize " + QString::number(bufSize);
    vbvSettings += " --vbv-init 0.9";
    args += " --crf " + ui->lblBitRateVal->text() + x265Threads + vbvSettings;
    args += " --output " + QString("\"") + Path::getTempPath() + "/x265Encoder_video_out.hevc\"";
    return args;
}

QString SettingsWidget::getAdvFfmpegArgs(int fileIndex)
{
    QString file = QString("\"") +  FileContainer::instance()->item(fileIndex)->text() + QString("\"");
    QStringList generalInfo = MediaInfo::getGeneralDetails(QString("\"") + file + QString("\""));
    QString args = "";
    QString cropargs ="";
    int newWidth = ui->Width->value();
    int newHeight = ui->Height->value();
    float newFrameRate = ui->frameRate->text().toFloat();
    cropargs = " -vf " + QString("\"") + "crop=in_w-" + QString::number(ui->CropLeft->value()) + "-" +
                         QString::number(ui->CropRight->value()) + ":in_h-" + QString::number(ui->CropTop->value())
                         + "-" + QString::number(ui->CropBottom->value()) + ":" + QString::number(ui->CropLeft->value())
                         + ":" + QString::number(ui->CropTop->value()) +" , scale="+ QString::number(newWidth)+"x"+
                         QString::number(newHeight) + QString("\"");
    if (newFrameRate == FileContainer::instance()->frameRate.at(0).toFloat())
    {
        if(ui->spinFrames->value() > 0)
            args += " -frames " + QString::number(ui->spinFrames->value());
        else if(!FileContainer::instance()->totalFrames.at(0).isEmpty())
            args += " -frames " + FileContainer::instance()->totalFrames.at(0);
    }

    if(ui->spinSeek->value() > 0)
    {
        float seekSeconds;
        if(!FileContainer::instance()->frameRate.at(0).isEmpty())
        {
            seekSeconds = ui->spinSeek->value() / FileContainer::instance()->frameRate.at(0).toFloat();
            args += " -ss " + QString::number(seekSeconds);
        }
    }

    if(generalInfo.at(1).compare("YUV", Qt::CaseInsensitive) == 0)
    {
        int depth = FileContainer::instance()->videoDepth.at(0).toInt();
        //QString colorSpace = YuvDialog->ui->ColorSpace->currentText();
        float fps =  FileContainer::instance()->fps.at(0).toFloat();
        int height = FileContainer::instance()->videoHeight.at(0).toInt();
        int width = FileContainer::instance()->videoWidth.at(0).toInt();
        QString SubSampling =  FileContainer::instance()->subSampling.at(0);
        if (SubSampling.compare("4:4:4", Qt::CaseInsensitive) == 0 && depth == 8)
            SubSampling = QString("yuv444p");
        else if (SubSampling.compare("4:2:2", Qt::CaseInsensitive) == 0 && depth == 8)
            SubSampling = QString("yuv422p");
        else if (SubSampling.compare("4:2:0", Qt::CaseInsensitive) == 0 && depth == 8)
            SubSampling = QString("yuv420p");
        else if (SubSampling.compare("4:4:4", Qt::CaseInsensitive) == 0 && depth == 10)
            SubSampling = QString("yuv444p10");
        else if (SubSampling.compare("4:2:2", Qt::CaseInsensitive) == 0 && depth == 10)
            SubSampling = QString("yuv422p10");
        else if (SubSampling.compare("4:2:0", Qt::CaseInsensitive) == 0 && depth == 10)
            SubSampling = QString("yuv420p10");

        args = " -s " +  QString::number( width) + "x" +  QString::number(height) + " -r " +
                             QString::number(fps) + " -bits_per_raw_sample "  +  QString::number(depth) +
                             " -pix_fmt " + SubSampling + " -i " + file + args + cropargs +
                             " -r " + QString::number(newFrameRate) +" -strict -1 -f yuv4mpegpipe -";
    }
    else
    {
        args = " -i " + file + args + cropargs + " -r " + QString::number(newFrameRate) + " -pix_fmt yuv420p -f yuv4mpegpipe -";

    }
    return args;
}

QString SettingsWidget::getAdvX265ArgsPass1()
{
    QString args =  " --input - --y4m --preset " + ui->comboPresetAdv->currentText();

    // Main page
    if(ui->comboTuneAdv->currentIndex() != 0)
        args += " --tune " + ui->comboTuneAdv->currentText();
    if(ui->comboLevelAdv->currentIndex() != 0)
        args += " --level-idc " + ui->comboLevelAdv->currentText();
    if(ui->comboProfileAdv->currentIndex() != 0)
        args += " --profile " + ui->comboProfileAdv->currentText();
    if(ui->comboTierAdv->currentIndex() != 0)
        args += " --high-tier";
    if(ui->groupLog->isChecked() && !ui->checkTwoPass->isChecked())
    {
        args += " --log-level " + QString::number(ui->comboLogLevel->currentIndex());
        args += " --csv " + QString("\"") + ui->txtLogFile->text() + QString("\"");
        if(ui->chkCUStats->isChecked())
            args += " --cu-stats";
        args += " --psnr --ssim";
    }
    if(!ui->txtPools->text().isEmpty() && !ui->txtPools->text().isNull() && QString::compare(ui->txtPools->text(), "*") != 0)
        args += " --pools " + ui->txtPools->text();
    if(ui->spinFrameThreads->value() > 0)
        args += " --frame-threads " + QString::number(ui->spinFrameThreads->value());
    if(!ui->chkWPP->isChecked())
        args += " --no-wpp";
    if(ui->chkParallelModeAnalysis->isChecked())
        args += " --pmode";
    if(ui->chkParallelMotionEst->isChecked())
        args += " --pme";
    if(ui->spinLookAheadSlices->value() > 0)
        args += " --lookahead-slices " + QString::number(ui->spinLookAheadSlices->value());

    // Frames page
    /*if(ui->spinSeek->value() > 0)
        args += " --seek " + QString::number(ui->spinSeek->value());*/
    if(ui->spinMaxIntraPeriod->value() != 250)
        args += " --keyint " + QString::number(ui->spinMaxIntraPeriod->value());
    if(ui->spinMinGOPSize->value() != 0)
        args += " --min-keyint " + QString::number(ui->spinMinGOPSize->value());
    if(ui->spinScenecut->value() != 40)
        args += " --scenecut " + QString::number(ui->spinScenecut->value());
    if(ui->spinLookAhead->value() != 20)
        args += " --rc-lookahead " + QString::number(ui->spinLookAhead->value());
    if(ui->spinConsBFrame->value() != 4)
        args += " --bframes " + QString::number(ui->spinConsBFrame->value());
    if(ui->spinBFrameBias->value() != 0)
        args += " --bframe-bias " + QString::number(ui->spinBFrameBias->value());
    if(ui->spinMaxRef->value() != 3)
        args += " --ref " + QString::number(ui->spinMaxRef->value());
    if(ui->spinLimitRef->value() != 0)
        args += " --limit-refs " + QString::number(ui->spinLimitRef->value());
    if(ui->comboAdaptiveB->currentIndex() != 2)
        args += " --b-adapt " + QString::number(ui->comboAdaptiveB->currentIndex());
    if(!ui->chkBRef->isChecked())
        args += " --no-b-pyramid";
    if(!ui->chkEnableGOP->isChecked())
        args += " --no-open-gop";
    if(ui->comboPenaltyTU->currentIndex() != 0)
        args += " --rdpenalty " + QString::number(ui->comboPenaltyTU->currentIndex());
    if(!ui->chkIntraInBFrames->isChecked())
        args += " --no-b-intra";
    if(ui->chkTransSkip->isChecked())
        args += " --tskip";
    if(ui->chkFastTransSkip->isChecked())
        args += " --tskip-fast";
    if(ui->chkConsIntra->isChecked())
        args += " --constrained-intra";
    if(!ui->chkStrongIntra->isChecked())
        args += " --no-strong-intra-smoothing";
    if(ui->chkIntraRefresh->isChecked())
        args += " --intra-refresh";

    // Ratecontrol page
    if(ui->checkLossless->isChecked())
        args += " --lossless";
    else
    {
        args += " --ipratio " + QString::number(ui->spinIPRatio->value());
        args += " --pbratio " + QString::number(ui->spinPBRatio->value());
        args += " --qcomp " + QString::number(ui->spinQcomp->value());
        args += " --qpstep " + QString::number(ui->spinQpstep->value());
        args += " --qblur " + QString::number(ui->spinQblur->value());
        args += " --cplxblur " + QString::number(ui->spinCplxblur->value());
        if(ui->comboRateControl->currentIndex() != 2)
        {
            args += " --qpmin " + QString::number(ui->spinQpMin->value());
            args += " --qpmax " + QString::number(ui->spinQpMax->value());
        }
        if(ui->comboRateControl->currentIndex() == 0)
            args += " --crf " + QString::number(ui->spinCRF->value());
        else if(ui->comboRateControl->currentIndex() == 1)
            args += " --bitrate " + QString::number(ui->spinBitRate->value());
        else if(ui->comboRateControl->currentIndex() == 2)
            args += " --qp " + QString::number(ui->spinQP->value());

        if(ui->groupVBV->isEnabled())
        {
            args += " --vbv-bufsize " + QString::number(ui->spinVbvBufferSize->value());
            args += " --vbv-maxrate " + QString::number(ui->spinVbvMaxRate->value());
            args += " --vbv-init " + QString::number(ui->spinVbvInitBuffer->value());
        }

        if(ui->chkStrictCbr->isEnabled() && ui->chkStrictCbr->isChecked())
            args += " --strict-cbr";
    }

    // Quantization page
    if(ui->comboAdaptQuant->currentIndex() != 1)
        args += " --aq-mode " + QString::number(ui->comboAdaptQuant->currentIndex());
    if(ui->spinAQStrength->value() != 1.00)
        args += " --aq-strength " + QString::number(ui->spinAQStrength->value());
    if(ui->chkqgSize->isChecked())
        args += " --qg-size " + QString::number(ui->comboqgSize->currentText().toInt());
    if(ui->spinCbOffset->value() != 0)
        args += " --cbqpoffs " + QString::number(ui->spinCbOffset->value());
    if(ui->spinCrOffset->value() != 0)
        args += " --crqpoffs " + QString::number(ui->spinCrOffset->value());
    if(ui->spinNoiseRedIntra->value() > 0)
        args += " --nr-intra " + QString::number(ui->spinNoiseRedIntra->value());
    if(ui->spinNoiseRedInter->value() > 0)
        args += " --nr-inter " + QString::number(ui->spinNoiseRedInter->value());
    if(!ui->txtQPFile->text().isEmpty())
        args += " --qpfile " + QString("\"") + ui->txtQPFile->text() + QString("\"");
    if(!ui->txtScalingFile->text().isEmpty())
        args += " --scaling-list " + QString("\"") + ui->txtScalingFile->text() + QString("\"");
    if(!ui->txtLambdaFile->text().isEmpty())
        args += " --lambda-file " + QString("\"") + ui->txtLambdaFile->text() + QString("\"");

    // Motion page
    if(ui->comboMotionMethod->currentIndex() != 1)
        args += " --me " + QString::number(ui->comboMotionMethod->currentIndex());
    if(ui->spinMotionRange->value() != 57)
        args += " --merange " + QString::number(ui->spinMotionRange->value());
    if(ui->spinSubPixel->value() != 2)
        args += " --subme " + QString::number(ui->spinSubPixel->value());
    if(ui->spinMaxMerge->value() != 2)
        args += " --max-merge " + QString::number(ui->spinMaxMerge->value());
    if(ui->chkRect->isChecked())
    {
        args += " --rect";
        if(ui->chkAssymetric->isChecked())
            args += " --amp";
    }

    // Analysis page
    if(ui->spinRDO->value() != 3)
        args += " --rd " + QString::number(ui->spinRDO->value());
    if(ui->spinRDOQLevel->value() != 0)
        args += " --rdoq-level " + QString::number(ui->spinRDOQLevel->value());
    if(ui->spinLimitTU->value() != 0)
        args += " --limit-tu " + QString::number(ui->spinLimitTU->value());
    if(ui->chkEarlySkip->isChecked())
        args += " --early-skip";
    if(!ui->chkWeightedP->isChecked())
        args += " --no-weightp";
    if(ui->chkWeightedB->isChecked())
        args += " --weightb";
    if(ui->chkCULossless->isChecked())
        args += " --cu-lossless";
    if(!ui->chkHideSign->isChecked())
        args += " --no-signhide";
    if(!ui->chkCUTree->isChecked())
        args += " --no-cutree";
    if(!ui->chkRskip->isChecked())
        args += " --no-rskip";
    if(ui->spinTUIntraDepth->value() != 1)
        args += " --tu-intra-depth " + QString::number(ui->spinTUIntraDepth->value());
    if(ui->spinTUInterDepth->value() != 1)
        args += " --tu-inter-depth " + QString::number(ui->spinTUInterDepth->value());
    if(ui->comboMaxTUSize->currentIndex() != 0)
    {
        if(ui->comboMaxTUSize->currentIndex() == 1)
            args += " --max-tu-size 16";
        else if(ui->comboMaxTUSize->currentIndex() == 2)
            args += " --max-tu-size 8";
        else if(ui->comboMaxTUSize->currentIndex() == 3)
            args += " --max-tu-size 4";
    }
    if(ui->comboMinCUSize->currentIndex() != 3)
    {
        if(ui->comboMinCUSize->currentIndex() == 0)
            args += " --min-cu-size 64";
        else if(ui->comboMinCUSize->currentIndex() == 1)
            args += " --min-cu-size 32";
        else if(ui->comboMinCUSize->currentIndex() == 2)
            args += " --min-cu-size 16";
    }
    if(ui->comboMaxCUSize->currentIndex() != 0)
    {
        if(ui->comboMaxCUSize->currentIndex() == 1)
            args += " --ctu 32";
        else
            args += " --ctu 16";
    }
    if(ui->spinPsyRDO->value() != 0.00)
        args += " --psy-rd " + QString::number(ui->spinPsyRDO->value());
    if(ui->spinPsyRDOQuant->value() != 0.00)
        args += " --psy-rdoq " + QString::number(ui->spinPsyRDOQuant->value());
    if(ui->chkLimitModes->isChecked())
        args += " --limit-modes ";

    // VUI page
    if(ui->comboSAR->currentIndex() != 0 && !ui->checkCustomSAR->isChecked())
        args += " --sar " + ui->comboSAR->currentText();
    else if(ui->checkCustomSAR->isChecked())
        args += " --sar " + ui->spinCustomSarWidth->text() + ":" + ui->spinCustomSarHeight->text();
    if(ui->comboOverScan->currentIndex() != 0)
    {
        args += " --overscan " + ui->comboOverScan->currentText();
        if(ui->comboOverScan->currentIndex() == 2)
        {
            args += " --display-window " + QString::number(ui->spinCropLeft->value()) +
                    "," + QString::number(ui->spinCropTop->value()) +
                    "," + QString::number(ui->spinCropRight->value()) +
                    "," + QString::number(ui->spinCropBottom->value()) ;
        }
    }
    if(ui->comboVideoFormat->currentIndex() != 0)
        args += " --videoformat " + ui->comboVideoFormat->currentText();
    if(ui->comboRange->currentIndex() != 0)
        args += " --range " + ui->comboRange->currentText();
    if(ui->comboColorPrim->currentIndex() != 0)
        args += " --colorprim " + ui->comboColorPrim->currentText();
    if(ui->comboTransferChar->currentIndex() != 0)
        args += " --transfer " + ui->comboTransferChar->currentText();
    if(ui->comboColorMatrix->currentIndex() != 0)
        args += " --colormatrix " + ui->comboColorMatrix->currentText();
    if(ui->comboChromaloc->currentIndex() != 0)
        args += " --chromaloc " + ui->comboChromaloc->currentText();
    if(!ui->txtMasterDisplay->text().isEmpty())
        args += " --master-display " + QString("\"") + ui->txtMasterDisplay->text() + QString("\"");
    if(!ui->txtLightLevel->text().isEmpty())
        args += " --max-cll " + QString("\"") + ui->txtLightLevel->text() + QString("\"");

    //HDR page

    if (ui->spinMaxLuma->value() > 0)
        args += " --max-luma " + QString::number(ui->spinMaxLuma->value());
    if (ui->spinMinLuma->value() > 0)
        args += " --min-luma " + QString::number(ui->spinMinLuma->value());

    // Misc page
    if(ui->chkRepeatHeader->isChecked())
        args += " --repeat-headers";
    if(!ui->chkEmitSEI->isChecked())
        args += " --no-info";
    if(ui->chkHRDSignal->isChecked())
        args += " --hrd";
    if(ui->chkAUDSignal->isChecked())
        args += " --aud";
    if(ui->chkTemporalLayer->isChecked())
        args += " --temporal-layers";
    if(!ui->chkLoopFilter->isChecked())
        args += " --no-deblock";
    else
        args += " --deblock " + QString::number(ui->spinDeblocktC->value()) + "," + QString::number(ui->spinDeblockBeta->value());
    if(!ui->chkSAO->isChecked())
        args += " --no-sao";
    if(ui->radioNonDeblock->isChecked())
        args += " --sao-non-deblock";
    else
        args += " --no-sao-non-deblock";
    if(enableBluRay)
        args += " --uhd-bd";

    currX265Args = args; // save for two pass execution
    // for 2 pass

    // frame count
    float newFrameRate = ui->frameRate->text().toFloat();
    if (newFrameRate == FileContainer::instance()->frameRate.at(0).toFloat())
    {
        if(ui->spinFrames->value() > 0)
            args += " --frames " + QString::number(ui->spinFrames->value() - ui->spinSeek->value());
        else if(!FileContainer::instance()->totalFrames.at(0).isEmpty())
            args += " --frames " + FileContainer::instance()->totalFrames.at(0);
    }


    if(ui->checkTwoPass->isChecked())
    {
        args += " --pass 1";
        if (ui->checkSlowFirstPass->isChecked())
            args += " --slow-firstpass";
        args += " --stats " + QString("\"") + Path::getTempPath() + "/x265_2pass.log\"" + " --output NUL";
    }
    else
        args += " --output " + QString("\"") + Path::getTempPath() + "/x265Encoder_video_out.hevc\"";

    return args;
}

QString SettingsWidget::getAdvX265ArgsPass2(QString frames)
{
    QString args;
    args = currX265Args; // repeat the first pass command
    if(ui->groupLog->isChecked())
    {
        args += " --log-level " + QString::number(ui->comboLogLevel->currentIndex());
        args += " --csv " + QString("\"") + ui->txtLogFile->text() + QString("\"");
        if(ui->chkCUStats->isChecked())
            args += " --cu-stats";
        args += " --psnr --ssim";
    }
    if (!frames.isEmpty() && !frames.isNull())
        args += " --frames " + frames;
    args += " --pass 2 --stats " + QString("\"") + Path::getTempPath() + "/x265_2pass.log\"" + " --output " + QString("\"") + Path::getTempPath() + "/x265Encoder_video_out.hevc\"";
    return args;
}

void SettingsWidget::toggleSettings()
{
     if(selecetedSettings == Basic)
     {
         selecetedSettings = Advanced;
         txtAdvanced.setStyleSheet(selectedStyle);
         txtBasic.setStyleSheet(unselectedStyle);
     }
     else
     {
         selecetedSettings = Basic;
         txtAdvanced.setStyleSheet(unselectedStyle);
         txtBasic.setStyleSheet(selectedStyle);
     }
     ui->stackedWidget->setCurrentIndex(selecetedSettings);
}

int SettingsWidget::getMaxRate(int height)
{
    QSettings capSet;
    if(height <= 288)
    {
        return capSet.value("spin240").toInt();
    }
    else if(height > 288 && height <=576)
    {
        return capSet.value("spin480").toInt();
    }
    else if(height > 576 && height <= 720)
    {
        return capSet.value("spin720").toInt();
    }
    else if(height > 720 && height <= 1080)
    {
        return capSet.value("spin1080").toInt();
    }
    else if(height > 1080 && height <= 2160)
    {
        return capSet.value("spin2160").toInt();
    }
    else
    {
        return capSet.value("spin3840").toInt();
    }
}

void SettingsWidget::setSelectedSetting(int index)
{
    if(!index)
        selecetedSettings = Advanced;
    else
        selecetedSettings = Basic;
    toggleSettings();
}

void SettingsWidget::defaultSettingsUltrafast()
{
    ui->comboMaxCUSize->setCurrentIndex(1);
    ui->comboMinCUSize->setCurrentIndex(2);
    ui->spinConsBFrame->setValue(3);
    ui->comboAdaptiveB->setCurrentIndex(0);
    ui->spinLookAhead->setValue(5);
    ui->spinLookAheadSlices->setValue(8);
    ui->spinScenecut->setValue(0);
    ui->spinMaxRef->setValue(1);
    ui->spinLimitRef->setValue(0);
    ui->comboMotionMethod->setCurrentIndex(0);
    ui->spinMotionRange->setValue(57);
    ui->spinSubPixel->setValue(0);
    ui->chkRect->setChecked(false);
    ui->chkAssymetric->setEnabled(false);
    ui->chkAssymetric->setChecked(false);
    ui->spinMaxMerge->setValue(2);
    ui->chkEarlySkip->setChecked(true);
    //ui->chkFastCBF->setChecked(true);
    ui->chkIntraInBFrames->setChecked(false);
    ui->chkSAO->setChecked(false);
    ui->chkHideSign->setChecked(false);
    ui->chkWeightedP->setChecked(false);
    ui->chkWeightedB->setChecked(false);
    ui->comboAdaptQuant->setCurrentIndex(0);
    ui->chkCUTree->setChecked(true);
    ui->spinRDO->setValue(2);
    ui->spinRDOQLevel->setValue(0);
    //ui->chkLoopFilter->setChecked(false);
    ui->spinTUIntraDepth->setValue(1);
    ui->spinTUInterDepth->setValue(1);
    ui->chkLimitModes->setChecked(false);
    ui->chkRskip->setChecked(true);
}

void SettingsWidget::defaultSettingsSuperfast()
{
    ui->comboMaxCUSize->setCurrentIndex(1);
    ui->comboMinCUSize->setCurrentIndex(3);
    ui->spinConsBFrame->setValue(3);
    ui->comboAdaptiveB->setCurrentIndex(0);
    ui->spinLookAhead->setValue(10);
    ui->spinLookAheadSlices->setValue(8);
    ui->spinScenecut->setValue(40);
    ui->spinMaxRef->setValue(1);
    ui->spinLimitRef->setValue(0);
    ui->comboMotionMethod->setCurrentIndex(1);
    ui->spinMotionRange->setValue(57);
    ui->spinSubPixel->setValue(1);
    ui->chkRect->setChecked(false);
    ui->chkAssymetric->setEnabled(false);
    ui->chkAssymetric->setChecked(false);
    ui->spinMaxMerge->setValue(2);
    ui->chkEarlySkip->setChecked(true);
    //ui->chkFastCBF->setChecked(true);
    ui->chkIntraInBFrames->setChecked(false);
    ui->chkSAO->setChecked(true);
    ui->chkHideSign->setChecked(true);
    ui->chkWeightedP->setChecked(false);
    ui->chkWeightedB->setChecked(false);
    ui->comboAdaptQuant->setCurrentIndex(0);
    ui->chkCUTree->setChecked(true);
    ui->spinRDO->setValue(2);
    ui->spinRDOQLevel->setValue(0);
    //ui->chkLoopFilter->setChecked(true);
    ui->spinTUIntraDepth->setValue(1);
    ui->spinTUInterDepth->setValue(1);
    ui->chkLimitModes->setChecked(false);
    ui->chkRskip->setChecked(true);
}

void SettingsWidget::defaultSettingsVeryfast()
{
    ui->comboMaxCUSize->setCurrentIndex(0);
    ui->comboMinCUSize->setCurrentIndex(3);
    ui->spinConsBFrame->setValue(4);
    ui->comboAdaptiveB->setCurrentIndex(0);
    ui->spinLookAhead->setValue(15);
    ui->spinLookAheadSlices->setValue(8);
    ui->spinScenecut->setValue(40);
    ui->spinMaxRef->setValue(2);
    ui->spinLimitRef->setValue(3);
    ui->comboMotionMethod->setCurrentIndex(1);
    ui->spinMotionRange->setValue(57);
    ui->spinSubPixel->setValue(1);
    ui->chkRect->setChecked(false);
    ui->chkAssymetric->setEnabled(false);
    ui->chkAssymetric->setChecked(false);
    ui->spinMaxMerge->setValue(2);
    ui->chkEarlySkip->setChecked(true);
    //ui->chkFastCBF->setChecked(true);
    ui->chkIntraInBFrames->setChecked(false);
    ui->chkSAO->setChecked(true);
    ui->chkHideSign->setChecked(true);
    ui->chkWeightedP->setChecked(true);
    ui->chkWeightedB->setChecked(false);
    ui->comboAdaptQuant->setCurrentIndex(1);
    ui->chkCUTree->setChecked(true);
    ui->spinRDO->setValue(2);
    ui->spinRDOQLevel->setValue(0);
    //ui->chkLoopFilter->setChecked(true);
    ui->spinTUIntraDepth->setValue(1);
    ui->spinTUInterDepth->setValue(1);
    ui->chkLimitModes->setChecked(false);
    ui->chkRskip->setChecked(true);
}

void SettingsWidget::defaultSettingsFaster()
{
    ui->comboMaxCUSize->setCurrentIndex(0);
    ui->comboMinCUSize->setCurrentIndex(3);
    ui->spinConsBFrame->setValue(4);
    ui->comboAdaptiveB->setCurrentIndex(0);
    ui->spinLookAhead->setValue(15);
    ui->spinLookAheadSlices->setValue(8);
    ui->spinScenecut->setValue(40);
    ui->spinMaxRef->setValue(2);
    ui->spinLimitRef->setValue(3);
    ui->comboMotionMethod->setCurrentIndex(1);
    ui->spinMotionRange->setValue(57);
    ui->spinSubPixel->setValue(2);
    ui->chkRect->setChecked(false);
    ui->chkAssymetric->setEnabled(false);
    ui->chkAssymetric->setChecked(false);
    ui->spinMaxMerge->setValue(2);
    ui->chkEarlySkip->setChecked(true);
    //ui->chkFastCBF->setChecked(true);
    ui->chkSAO->setChecked(true);
    ui->chkIntraInBFrames->setChecked(false);
    ui->chkHideSign->setChecked(true);
    ui->chkWeightedP->setChecked(true);
    ui->chkWeightedB->setChecked(false);
    ui->comboAdaptQuant->setCurrentIndex(1);
    ui->chkCUTree->setChecked(true);
    ui->spinRDO->setValue(2);
    ui->spinRDOQLevel->setValue(0);
    //ui->chkLoopFilter->setChecked(true);
    ui->spinTUIntraDepth->setValue(1);
    ui->spinTUInterDepth->setValue(1);
    ui->chkLimitModes->setChecked(false);
    ui->chkRskip->setChecked(true);
}

void SettingsWidget::defaultSettingsFast()
{
    ui->comboMaxCUSize->setCurrentIndex(0);
    ui->comboMinCUSize->setCurrentIndex(3);
    ui->spinConsBFrame->setValue(4);
    ui->comboAdaptiveB->setCurrentIndex(0);
    ui->spinLookAhead->setValue(15);
    ui->spinLookAheadSlices->setValue(8);
    ui->spinScenecut->setValue(40);
    ui->spinMaxRef->setValue(3);
    ui->spinLimitRef->setValue(3);
    ui->comboMotionMethod->setCurrentIndex(1);
    ui->spinMotionRange->setValue(57);
    ui->spinSubPixel->setValue(2);
    ui->chkRect->setChecked(false);
    ui->chkAssymetric->setEnabled(false);
    ui->chkAssymetric->setChecked(false);
    ui->spinMaxMerge->setValue(2);
    ui->chkEarlySkip->setChecked(false);
    //ui->chkFastCBF->setChecked(false);
    ui->chkIntraInBFrames->setChecked(false);
    ui->chkSAO->setChecked(true);
    ui->chkHideSign->setChecked(true);
    ui->chkWeightedP->setChecked(true);
    ui->chkWeightedB->setChecked(false);
    ui->comboAdaptQuant->setCurrentIndex(1);
    ui->chkCUTree->setChecked(true);
    ui->spinRDO->setValue(2);
    ui->spinRDOQLevel->setValue(0);
    //ui->chkLoopFilter->setChecked(true);
    ui->spinTUIntraDepth->setValue(1);
    ui->spinTUInterDepth->setValue(1);
    ui->chkLimitModes->setChecked(false);
    ui->chkRskip->setChecked(true);
}

void SettingsWidget::defaultSettingsMedium()
{
    ui->comboMaxCUSize->setCurrentIndex(0);
    ui->comboMinCUSize->setCurrentIndex(3);
    ui->spinConsBFrame->setValue(4);
    ui->comboAdaptiveB->setCurrentIndex(2);
    ui->spinLookAhead->setValue(20);
    ui->spinLookAheadSlices->setValue(8);
    ui->spinScenecut->setValue(40);
    ui->spinMaxRef->setValue(3);
    ui->spinLimitRef->setValue(3);
    ui->comboMotionMethod->setCurrentIndex(1);
    ui->spinMotionRange->setValue(57);
    ui->spinSubPixel->setValue(2);
    ui->chkRect->setChecked(false);
    ui->chkAssymetric->setEnabled(false);
    ui->chkAssymetric->setChecked(false);
    ui->spinMaxMerge->setValue(2);
    ui->chkEarlySkip->setChecked(false);
    //ui->chkFastCBF->setChecked(false);
    ui->chkIntraInBFrames->setChecked(false);
    ui->chkSAO->setChecked(true);
    ui->chkHideSign->setChecked(true);
    ui->chkWeightedP->setChecked(true);
    ui->chkWeightedB->setChecked(false);
    ui->comboAdaptQuant->setCurrentIndex(1);
    ui->chkCUTree->setChecked(true);
    ui->spinRDO->setValue(3);
    ui->spinRDOQLevel->setValue(0);
    //ui->chkLoopFilter->setChecked(true);
    ui->spinTUIntraDepth->setValue(1);
    ui->spinTUInterDepth->setValue(1);
    ui->chkLimitModes->setChecked(false);
    ui->chkRskip->setChecked(true);
}

void SettingsWidget::defaultSettingsSlow()
{
    ui->comboMaxCUSize->setCurrentIndex(0);
    ui->comboMinCUSize->setCurrentIndex(3);
    ui->spinConsBFrame->setValue(4);
    ui->comboAdaptiveB->setCurrentIndex(2);
    ui->spinLookAhead->setValue(25);
    ui->spinLookAheadSlices->setValue(4);
    ui->spinScenecut->setValue(40);
    ui->spinMaxRef->setValue(4);
    ui->spinLimitRef->setValue(3);
    ui->comboMotionMethod->setCurrentIndex(3);
    ui->spinMotionRange->setValue(57);
    ui->spinSubPixel->setValue(3);
    ui->chkRect->setChecked(true);
    ui->chkAssymetric->setEnabled(true);
    ui->chkAssymetric->setChecked(false);
    ui->spinMaxMerge->setValue(3);
    ui->chkEarlySkip->setChecked(false);
    //ui->chkFastCBF->setChecked(false);
    ui->chkIntraInBFrames->setChecked(false);
    ui->chkSAO->setChecked(true);
    ui->chkHideSign->setChecked(true);
    ui->chkWeightedP->setChecked(true);
    ui->chkWeightedB->setChecked(false);
    ui->comboAdaptQuant->setCurrentIndex(1);
    ui->chkCUTree->setChecked(true);
    ui->spinRDO->setValue(4);
    ui->spinRDOQLevel->setValue(2);
    //ui->chkLoopFilter->setChecked(true);
    ui->spinTUIntraDepth->setValue(1);
    ui->spinTUInterDepth->setValue(1);
    ui->chkLimitModes->setChecked(true);
    ui->chkRskip->setChecked(true);
}

void SettingsWidget::defaultSettingsSlower()
{
    ui->comboMaxCUSize->setCurrentIndex(0);
    ui->comboMinCUSize->setCurrentIndex(3);
    ui->spinConsBFrame->setValue(8);
    ui->comboAdaptiveB->setCurrentIndex(2);
    ui->spinLookAhead->setValue(30);
    ui->spinLookAheadSlices->setValue(4);
    ui->spinScenecut->setValue(40);
    ui->spinMaxRef->setValue(4);
    ui->spinLimitRef->setValue(2);
    ui->comboMotionMethod->setCurrentIndex(3);
    ui->spinMotionRange->setValue(57);
    ui->spinSubPixel->setValue(3);
    ui->chkRect->setChecked(true);
    ui->chkAssymetric->setEnabled(true);
    ui->chkAssymetric->setChecked(true);
    ui->spinMaxMerge->setValue(3);
    ui->chkEarlySkip->setChecked(false);
    //ui->chkFastCBF->setChecked(false);
    ui->chkIntraInBFrames->setChecked(true);
    ui->chkSAO->setChecked(true);
    ui->chkHideSign->setChecked(true);
    ui->chkWeightedP->setChecked(true);
    ui->chkWeightedB->setChecked(true);
    ui->comboAdaptQuant->setCurrentIndex(1);
    ui->chkCUTree->setChecked(true);
    ui->spinRDO->setValue(6);
    ui->spinRDOQLevel->setValue(2);
    //ui->chkLoopFilter->setChecked(true);
    ui->spinTUIntraDepth->setValue(2);
    ui->spinTUInterDepth->setValue(2);
    ui->chkLimitModes->setChecked(true);
    ui->chkRskip->setChecked(true);
}

void SettingsWidget::defaultSettingsVeryslow()
{
    ui->comboMaxCUSize->setCurrentIndex(0);
    ui->comboMinCUSize->setCurrentIndex(3);
    ui->spinConsBFrame->setValue(8);
    ui->comboAdaptiveB->setCurrentIndex(2);
    ui->spinLookAhead->setValue(40);
    ui->spinLookAheadSlices->setValue(1);
    ui->spinScenecut->setValue(40);
    ui->spinMaxRef->setValue(5);
    ui->spinLimitRef->setValue(1);
    ui->comboMotionMethod->setCurrentIndex(3);
    ui->spinMotionRange->setValue(57);
    ui->spinSubPixel->setValue(4);
    ui->chkRect->setChecked(true);
    ui->chkAssymetric->setEnabled(true);
    ui->chkAssymetric->setChecked(true);
    ui->spinMaxMerge->setValue(4);
    ui->chkEarlySkip->setChecked(false);
    //ui->chkFastCBF->setChecked(false);
    ui->chkIntraInBFrames->setChecked(true);
    ui->chkSAO->setChecked(true);
    ui->chkHideSign->setChecked(true);
    ui->chkWeightedP->setChecked(true);
    ui->chkWeightedB->setChecked(true);
    ui->comboAdaptQuant->setCurrentIndex(1);
    ui->chkCUTree->setChecked(true);
    ui->spinRDO->setValue(6);
    ui->spinRDOQLevel->setValue(2);
    //ui->chkLoopFilter->setChecked(true);
    ui->spinTUIntraDepth->setValue(3);
    ui->spinTUInterDepth->setValue(3);
    ui->chkLimitModes->setChecked(true);
    ui->chkRskip->setChecked(false);
}

void SettingsWidget::defaultSettingsPlacebo()
{
    ui->comboMaxCUSize->setCurrentIndex(0);
    ui->comboMinCUSize->setCurrentIndex(3);
    ui->spinConsBFrame->setValue(8);
    ui->comboAdaptiveB->setCurrentIndex(2);
    ui->spinLookAhead->setValue(60);
    ui->spinLookAheadSlices->setValue(1);
    ui->spinScenecut->setValue(40);
    ui->spinMaxRef->setValue(5);
    ui->spinLimitRef->setValue(0);
    ui->comboMotionMethod->setCurrentIndex(3);
    ui->spinMotionRange->setValue(92);
    ui->spinSubPixel->setValue(5);
    ui->chkRect->setChecked(true);
    ui->chkAssymetric->setEnabled(true);
    ui->chkAssymetric->setChecked(true);
    ui->spinMaxMerge->setValue(5);
    ui->chkEarlySkip->setChecked(false);
    //ui->chkFastCBF->setChecked(false);
    ui->chkIntraInBFrames->setChecked(true);
    ui->chkSAO->setChecked(true);
    ui->chkHideSign->setChecked(true);
    ui->chkWeightedP->setChecked(true);
    ui->chkWeightedB->setChecked(true);
    ui->comboAdaptQuant->setCurrentIndex(1);
    ui->chkCUTree->setChecked(true);
    ui->spinRDO->setValue(6);
    ui->spinRDOQLevel->setValue(2);
    //ui->chkLoopFilter->setChecked(true);
    ui->spinTUIntraDepth->setValue(4);
    ui->spinTUInterDepth->setValue(4);
    ui->chkLimitModes->setChecked(false);
    ui->chkRskip->setChecked(false);
}

void SettingsWidget::on_checkLossless_clicked()
{
    if (ui->checkLossless->isChecked())
    {
        ui->groupRateControl->setDisabled(true);
        ui->groupVBV->setDisabled(true);
    }
    else
    {
        ui->groupRateControl->setDisabled(false);
        ui->groupVBV->setDisabled(false);
    }
}

void SettingsWidget::on_chkRect_clicked()
{
    if(ui->chkRect->isChecked())
        ui->chkAssymetric->setEnabled(true);
    else
    {
        ui->chkAssymetric->setChecked(false);
        ui->chkAssymetric->setEnabled(false);
    }
}

void SettingsWidget::comboPresetAdv_currentIndexChanged(int index)
{
    if(!presetWarnMsgCheck->isChecked() && ui->stackedWidget->currentIndex() && !presetChecked)
    {
        presetWarnMsg->exec();
        if(presetWarnMsg->standardButton(presetWarnMsg->clickedButton()) == QMessageBox::Cancel)
        {
            presetWarnMsgCheck->setChecked(false);
            disconnect(ui->comboPresetAdv, SIGNAL(currentIndexChanged(int)), this, SLOT(comboPresetAdv_currentIndexChanged(int)));
            ui->comboPresetAdv->setCurrentIndex(currPresetIndex);
            connect(ui->comboPresetAdv, SIGNAL(currentIndexChanged(int)), this, SLOT(comboPresetAdv_currentIndexChanged(int)));
            return;
        }
    }
    currPresetIndex = index;
    switch(index)
    {
        case 0:
            defaultSettingsUltrafast();
            break;
        case 1:
            defaultSettingsSuperfast();
            break;
        case 2:
            defaultSettingsVeryfast();
            break;
        case 3:
            defaultSettingsFaster();
            break;
        case 4:
            defaultSettingsFast();
            break;
        case 5:
            defaultSettingsMedium();
            break;
        case 6:
            defaultSettingsSlow();
            break;
        case 7:
            defaultSettingsSlower();
            break;
        case 8:
            defaultSettingsVeryslow();
            break;
        case 9:
            defaultSettingsPlacebo();
            break;
        default:
            defaultSettingsMedium();
            break;
    }
    if(presetWarnMsgCheck->isChecked())
        presetChecked = true;
}

void SettingsWidget::comboTuneAdv_currentIndexChanged(int index)
{
    if(!tuneWarnMsgCheck->isChecked() && !tuneChecked)
    {
        tuneWarnMsg->exec();
        if(tuneWarnMsg->standardButton(tuneWarnMsg->clickedButton()) == QMessageBox::Cancel)
        {
            tuneWarnMsgCheck->setChecked(false);
            disconnect(ui->comboTuneAdv, SIGNAL(currentIndexChanged(int)), this, SLOT(comboTuneAdv_currentIndexChanged(int)));
            ui->comboTuneAdv->setCurrentIndex(currTuneIndex);
            connect(ui->comboTuneAdv, SIGNAL(currentIndexChanged(int)), this, SLOT(comboTuneAdv_currentIndexChanged(int)));
            return;
        }
    }
    currTuneIndex = index;
    switch(index)
    {
        case 0:
            break;
        case 1:
            ui->comboAdaptQuant->setCurrentIndex(0);
            ui->spinPsyRDO->setValue(0.0);
            ui->spinPsyRDOQuant->setValue(0.0);
            ui->chkCUTree->setChecked(false);
            break;
        case 2:
            ui->comboAdaptQuant->setCurrentIndex(2);
            ui->spinPsyRDO->setValue(0.0);
            ui->spinPsyRDOQuant->setValue(0.0);
            break;
        case 3:
            ui->spinPsyRDO->setValue(0.5);
            ui->spinRDOQLevel->setValue(0);
            ui->spinPsyRDOQuant->setValue(0);
            ui->spinAQStrength->setValue(0.3);
            ui->spinIPRatio->setValue(1.1);
            ui->spinPBRatio->setValue(1.1);
            break;
        case 4:
            ui->spinLookAhead->setValue(0);
            ui->spinConsBFrame->setValue(0);
            ui->chkCUTree->setChecked(false);
            break;
        case 5:
            ui->chkLoopFilter->setChecked(false);
            ui->chkWeightedB->setChecked(false);
            ui->chkWeightedP->setChecked(false);
            ui->chkIntraInBFrames->setChecked(false);
            break;
        default:
            break;
    }
    if(tuneWarnMsgCheck->isChecked())
        tuneChecked = true;
}

void SettingsWidget::on_helpPreset_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-preset";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpTune_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-tune";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}


void SettingsWidget::on_helpLevel_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-level-idc";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpProfile_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-profile";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpTier_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-high-tier";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpLogLevel_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-log-level";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpLogFile_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-csv";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpEmitCu_clicked()
{
    QString link = "http://x265.readthedocs.io/en/1.7/cli.html#cmdoption-cu-stats";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpPME_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-pme";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpPMA_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-pmode";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpThreads_clicked()
{
    QString link = "http://x265.readthedocs.org/en/default/cli.html#cmdoption-pools";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpFrameThreads_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-frame-threads";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpWPP_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-wpp";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpFrames_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-frames";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpSeek_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-seek";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpMaxIntra_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-keyint";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpMinGOP_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-min-keyint";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpScenecut_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-scenecut";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpLookAhead_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-rc-lookahead";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpMaxB_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-bframes";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpBBias_clicked()
{
    QString link = "http://x265.readthedocs.org/en/default/cli.html#cmdoption-bframe-bias";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpMaxReference_clicked()
{
    QString link = "http://x265.readthedocs.org/en/default/cli.html#cmdoption-ref";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpAdaptiveB_clicked()
{
    QString link = "http://x265.readthedocs.org/en/default/cli.html#cmdoption-b-adapt";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpBRef_clicked()
{
    QString link = "http://x265.readthedocs.org/en/default/cli.html#cmdoption-b-pyramid";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpEnableGop_clicked()
{
    QString link = "http://x265.readthedocs.org/en/default/cli.html#cmdoption-open-gop";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpISlicePenalty_clicked()
{
    QString link = "http://x265.readthedocs.org/en/default/cli.html#cmdoption-rdpenalty";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpTransformSkip_clicked()
{
    QString link = "http://x265.readthedocs.org/en/default/cli.html#cmdoption-tskip";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpConstrainedIntra_clicked()
{
    QString link = "http://x265.readthedocs.org/en/default/cli.html#cmdoption-constrained-intra";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpStrongIntra_clicked()
{
    QString link = "http://x265.readthedocs.org/en/default/cli.html#cmdoption-strong-intra-smoothing";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpFastTransformSkip_clicked()
{
    QString link = "http://x265.readthedocs.org/en/default/cli.html#cmdoption-tskip-fast";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpUseIntaB_clicked()
{
    QString link = "http://x265.readthedocs.org/en/default/cli.html#cmdoption-b-intra";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpCRF_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-crf";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpBitRate_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-bitrate";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpQP_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-qp";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpIPRatio_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-ipratio";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpPBRatio_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-pbratio";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpEnableTwoPass_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-pass";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpSlowFirstPass_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-slow-firstpass";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpVBVMaxBitRate_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-vbv-maxrate";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpVBVBuffSize_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-vbv-bufsize";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpVBVInitial_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-vbv-init";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpLossless_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-lossless";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpAdaptQuant_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-aq-mode";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpQuantStrength_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-aq-strength";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpChromaOffsetCb_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-cbqpoffs";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpChromaOffsetCb_2_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-crqpoffs";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpNoiseRedStrength_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-nr-intra";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpNoiseRedInter_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-nr-intra";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpQPFile_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-qpfile";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpScalingFile_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-scaling-list";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpLambdaFile_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-lambda-file";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpMotionSearch_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-me";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpMotionSearchRange_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-merange";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpSubPixelRef_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-subme";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpRect_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-rect";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpAssemetric_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-amp";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpMaxMerge_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-max-merge";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpRateDisortion_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-rd";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpRDOQLevel_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-rdoq-level";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpEarlySkip_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-early-skip";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpWeightP_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-weightp";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpWeightB_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-weightb";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpCULossless_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-cu-lossless";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpHideSign_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-signhide";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpCUTree_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-cutree";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpMaxCU_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-ctu";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpTUIntra_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-tu-intra-depth";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpInter_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-tu-inter-depth";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpRDO_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-psy-rd";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpRDOQuant_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-psy-rdoq";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpSAR_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-sar";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpVideoFormat_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-videoformat";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpRange_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-range";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpColorPrim_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-colorprim";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpTransChar_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-transfer";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpColorMatrix_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-colormatrix";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpChromaSamp_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-chromaloc";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpOverScan_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-overscan";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpCrop_clicked()
{
    QString link = "http://x265.readthedocs.io/en/latest/cli.html#cmdoption-display-window";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpRepeatHeaders_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-repeat-headers";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpHRDSignalling_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-hrd";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpSEI_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-info";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpAUDSignalling_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-aud";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpDeblockLoopFilter_clicked()
{
    QString link = "http://x265.readthedocs.io/en/latest/cli.html#cmdoption-deblock";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpSampleAdapOffset_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-sao";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpSAONonDeblock_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-sao-non-deblock";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}


void SettingsWidget::on_helpSAOSkip_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-sao-non-deblock";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

//void SettingsWidget::on_helpAnalysisMode_clicked()
//{
//    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption--analysis-mode";
//    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
//}

//void SettingsWidget::on_helpAnalysisFile_clicked()
//{
//    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption--analysis-file";
//    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
//}

void SettingsWidget::on_helpBasicRateFactor_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-crf";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpBasicPreset_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-preset";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpEnableStrictCbr_clicked()
{
    QString link = "http://x265.readthedocs.org/en/default/cli.html#cmdoption-strict-cbr";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpMinCU_clicked()
{
    QString link = "http://x265.readthedocs.org/en/default/cli.html#cmdoption-min-cu-size";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpLookAheadSlices_clicked()
{
    QString link = "http://x265.readthedocs.org/en/default/cli.html#cmdoption-lookahead-slices";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpMaxTU_clicked()
{
    QString link = "http://x265.readthedocs.org/en/default/cli.html#cmdoption-max-tu-size";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpTemporalLayer_clicked()
{
    QString link = "http://x265.readthedocs.io/en/default/cli.html#cmdoption-temporal-layers-no-temporal-layers";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpLimitReference_clicked()
{
    QString link = "http://x265.readthedocs.org/en/default/cli.html#cmdoption-limit-refs";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpIntraRefresh_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-intra-refresh";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpQcomp_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-qcomp";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpQpstep_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-qpstep";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpQblur_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-qblur";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpCplxblur_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-cplxblur";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpMasterDisplay_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-master-display";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpLightLevel_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-max-cll";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpqgSize_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-qg-size";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpMaxLuma_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-max-luma";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpMinLuma_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-min-luma";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpLimitModes_clicked()
{
    QString link = "http://x265.readthedocs.org/en/latest/cli.html#cmdoption-limit-modes";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpUhdBd_clicked()
{
    QString link = "http://x265.readthedocs.io/en/latest/cli.html#cmdoption-uhd-bd";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpRSkip_clicked()
{
    QString link = "http://x265.readthedocs.io/en/latest/cli.html#cmdoption-rskip";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpQpMin_clicked()
{
    QString link = "http://x265.readthedocs.io/en/latest/cli.html#cmdoption-qpmin";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpQpMax_clicked()
{
    QString link = "http://x265.readthedocs.io/en/latest/cli.html#cmdoption-qpmax";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_helpLimitTU_clicked()
{
    QString link = "http://x265.readthedocs.io/en/latest/cli.html#cmdoption-limit-tu";
    QDesktopServices::openUrl(QUrl(link,QUrl::TolerantMode));
}

void SettingsWidget::on_btnCap_clicked()
{
    MaxBitRateDialog *newDialog = new MaxBitRateDialog(this);
    newDialog->layout()->setSizeConstraint(QLayout::SetFixedSize);
    newDialog->setModal(true);
    newDialog->show();
}

void SettingsWidget::on_btnLoadSet_clicked()
{
    if(lastOpenDir.isEmpty())
        lastOpenDir = QStandardPaths::writableLocation(QStandardPaths::MoviesLocation);
    QString fileName = QFileDialog::getOpenFileName(
                             this,
                             "Load Settings",
                             lastOpenDir,
                             "x265 Settings (*.xml);;");
    if (fileName.length() == 0)
        return;
    QList<SettingsWidget *> setList;
    setList.insert(0, this);
    settingutils setUtils;
    setUtils.loadsettings(setList, fileName, 0);
    lastOpenDir = fileName.left(fileName.lastIndexOf('/') + 1);
}

void SettingsWidget::on_btnSaveSet_clicked()
{
    QString filePath = QFileDialog::getSaveFileName(this, tr("Save Settings"),
                                             lastOpenDir,
                                             tr("XML(*.xml)"));
    if(filePath.length()==0)
        return;
    settingutils setUtils;
    QList<SettingsWidget *> setList;
    setList.insert(0, this);
    setUtils.saveSettings(setList, filePath, 0);
    lastOpenDir = filePath.left(filePath.lastIndexOf('/') + 1);
}

void SettingsWidget::DefLoadSet(SettingsWidget * widget)
{
    QString fileName;
    QFile file (QDir::tempPath() + "/x265Encoder_DefSet.xml");
    fileName = file.fileName();
    QList<SettingsWidget *> setList;
    setList.insert(0, widget);
    settingutils setUtils;
    setUtils.loadsettings(setList, fileName, 0);
}

void SettingsWidget::DefSaveSet(SettingsWidget *widget)
{
    QFile file (QDir::tempPath() + "/x265Encoder_DefSet.xml");
    QString filePath = file.fileName();
    settingutils setUtils;
    QList<SettingsWidget *> setList;
    setList.insert(0, widget);
    setUtils.saveSettings(setList, filePath, 0);
}

void SettingsWidget::on_spinVbvBufferSize_valueChanged(int value)
{
    if(value > 0 && ui->comboRateControl->currentIndex() == 1)
        ui->chkStrictCbr->setEnabled(true);
    else
        ui->chkStrictCbr->setEnabled(false);
}



void SettingsWidget::on_CropLeft_valueChanged(int arg1)
{
    if(FileContainer::instance()->count() > 0)
    {
        int Width =  FileContainer::instance()->videoWidth.at(FileContainer::instance()->currentIndex().row()).toInt() - ui->CropLeft->value() - ui->CropRight->value();
        ui->Width->setValue(Width);
    }
}

void SettingsWidget::on_CropRight_valueChanged(int arg1)
{
    if(FileContainer::instance()->count() > 0)
    {
        int Width = FileContainer::instance()->videoWidth.at(FileContainer::instance()->currentIndex().row()).toInt() - ui->CropLeft->value() - ui->CropRight->value();
        ui->Width->setValue(Width);
    }
}

void SettingsWidget::on_CropTop_valueChanged(int arg1)
{
     if(FileContainer::instance()->count() > 0)
     {
         int Height = FileContainer::instance()->videoHeight.at(FileContainer::instance()->currentIndex().row()).toInt() - ui->CropTop->value() - ui->CropBottom->value();
         ui->Height->setValue(Height);
     }
}

void SettingsWidget::on_CropBottom_valueChanged(int arg1)
{
    if(FileContainer::instance()->count() > 0)
    {
        int Height = FileContainer::instance()->videoHeight.at(FileContainer::instance()->currentIndex().row()).toInt() - ui->CropTop->value() - ui->CropBottom->value();
        ui->Height->setValue(Height);
    }
}

void SettingsWidget::on_preset_currentIndexChanged(int index)
{
    if(!FileContainer::instance()->loadxml)
    {
        QStringList Resolution = ui->preset->currentText().split( "x" );
        QStringList defResolution = ui->preset->itemText(0).split( "x" );

        if(Resolution.at(0).compare("Custom", Qt::CaseInsensitive) != 0)
        {
            ui->Width->setValue(Resolution.at(0).toInt());
            ui->Height->setValue(Resolution.at(1).toInt());
            ui->Width->setDisabled(true);
        }
        else
        {
            if(defResolution.at(0).compare("Custom", Qt::CaseInsensitive) != 0)
            {
                ui->Width->setValue(defResolution.at(0).toInt());
                ui->Height->setValue(defResolution.at(1).toInt());
                ui->Width->setDisabled(false);
            }
        }
    }
}

void SettingsWidget::on_Width_valueChanged(int arg1)
{
    if (ui->chkResize->isChecked()&& ui->chkAspectRatio->isChecked())
    {
       float aspectRatio;
       QStringList defResolution = ui->preset->itemText(0).split( "x" );
       if(defResolution.at(0).compare("Custom", Qt::CaseInsensitive) != 0)
       {
          aspectRatio = defResolution.at(0).toFloat() / defResolution.at(1).toFloat();
          int height = ui->Width->value() / aspectRatio;
          height = (height % 2 == 0) ? height : (height + 1);
          ui->Height->setValue(height);
       }
    }

}

void SettingsWidget::on_chkResize_stateChanged(int arg1)
{

    if(arg1 == 2)
    {
        ui->preset->setEnabled(true);
        ui->chkAspectRatio->setEnabled(true);
        if (ui->CropLeft->value() > 0 || ui->CropRight->value() > 0 || ui->CropTop->value() > 0 || ui->CropBottom->value() > 0)
        {
            QStringList WidthPreset, HeightPreset;
            float aspectRatio;
            int height;
            WidthPreset << ui->Width->text();
            HeightPreset << ui->Height->text();
            aspectRatio = ui->Width->text().toFloat() / ui->Height->text().toFloat();
            while (WidthPreset.at(WidthPreset.size() -1).toInt() >= 320)
            {
                WidthPreset << QString::number(WidthPreset.at(WidthPreset.size() - 1).toInt() - 320);
                height = WidthPreset.at(WidthPreset.size() -1).toInt() / aspectRatio;
                height = (height % 2 == 0) ? height : (height + 1);
                HeightPreset << QString::number(height);
            }
            int presetCount =   ui->preset->count();
            for (int i = 0; i < presetCount -1; ++i)
            {
                ui->preset->removeItem(0);
            }
            for (int i = 0; i < WidthPreset.size() -1; ++i)
            {
                ui->preset->insertItem(i,WidthPreset.at(i) + " x " + HeightPreset.at(i) );
            }

            ui->preset->setCurrentIndex(0);
        }
        else
        {
            QStringList WidthPreset, HeightPreset;
            float aspectRatio;
            int height;
            WidthPreset << FileContainer::instance()->videoWidth.at(FileContainer::instance()->currentIndex().row());
            HeightPreset << FileContainer::instance()->videoHeight.at(FileContainer::instance()->currentIndex().row());
            aspectRatio = FileContainer::instance()->videoWidth.at(FileContainer::instance()->currentIndex().row()).toFloat()
                          / FileContainer::instance()->videoHeight.at(FileContainer::instance()->currentIndex().row()).toFloat();
            while (WidthPreset.at(WidthPreset.size() -1).toInt() >= 320)
            {
                WidthPreset << QString::number(WidthPreset.at(WidthPreset.size() - 1).toInt() - 320);
                height = WidthPreset.at(WidthPreset.size() -1).toInt() / aspectRatio;
                height = (height % 2 == 0) ? height : (height + 1);
                HeightPreset << QString::number(height);

            }
            int presetCount =   ui->preset->count();

            for (int i = 0; i < presetCount -1; ++i)
            {
                ui->preset->removeItem(0);
            }

            for (int i = 0; i < WidthPreset.size() -1; ++i)
            {
                ui->preset->insertItem(i,WidthPreset.at(i) + " x " + HeightPreset.at(i) );
            }

            ui->preset->setCurrentIndex(0);
        }

    }
    else
    {
        ui->chkAspectRatio->setEnabled(false);
        ui->chkAspectRatio->setChecked(true);
        ui->preset->setEnabled(false);
        ui->Width->setDisabled(true);
        QStringList defResolution = ui->preset->itemText(0).split( "x" );
        ui->Width->setValue(defResolution.at(0).toInt());
        ui->Height->setValue(defResolution.at(1).toInt());
    }

}



void SettingsWidget::on_chkAspectRatio_stateChanged(int arg1)
{
    if(arg1 == 0)
    {
        ui->preset->setEnabled(false);
        ui->Width->setEnabled(true);
        ui->Height->setEnabled(true);
    }
    else
    {
        ui->preset->setEnabled(true);
        QStringList Resolution = ui->preset->currentText().split( "x" );
        if(Resolution.at(0).compare("Custom", Qt::CaseInsensitive) == 0)
        {
          ui->Height->setEnabled(false);
        }
        else
        {
            ui->Width->setEnabled(false);
            ui->Height->setEnabled(false);
        }
    }
}

void SettingsWidget::on_Width_editingFinished()
{
    int width = ui->Width->value();
    width = (width % 2 == 0) ? width : (width + 1);
    ui->Width->setValue(width);
}

void SettingsWidget::on_Height_editingFinished()
{
    int height = ui->Height->value();
    height = (height % 2 == 0) ? height : (height + 1);
    ui->Height->setValue(height);
}


void SettingsWidget::on_chkLoopFilter_clicked(bool checked)
{
    if(!checked)
    {
       ui->spinDeblocktC->setVisible(false);
       ui->spinDeblockBeta->setVisible(false);
       ui->commaDeblock->setVisible(false);
    }
    else
    {
        ui->spinDeblocktC->setVisible(true);
        ui->spinDeblockBeta->setVisible(true);
        ui->commaDeblock->setVisible(true);
    }
}

void SettingsWidget::on_chkUhdBd_clicked(bool checked)
{
    QString subSampling = FileContainer::instance()->subSampling.at(FileContainer::instance()->currentRow());
    int depth =FileContainer::instance()->videoDepth.at(FileContainer::instance()->currentRow()).toInt();
    if(checked)
    {
       if(QString::compare(subSampling, "4:2:0", Qt::CaseInsensitive) == 0  && depth == 10)
           enableBluRay = true;
       else
           enableBluRay = false;
    }
    else
    {
       enableBluRay = false;
    }
}

void SettingsWidget::on_chkqgSize_clicked(bool checked)
{
   if(checked)
       ui->comboqgSize->setEnabled(true);
   else
       ui->comboqgSize->setEnabled(false);

}

void SettingsWidget::on_sldCpuUtilization_sliderMoved(int position)
{
   ui->lblCpuUtilization->setText(QString::number(position) + "%");
}

void SettingsWidget::on_sldCpuUtilization_actionTriggered(int action)
{
    int value, finalValue;
    QStringList finalList;
    if(action == 1 || action == 3)
        value = ui->sldCpuUtilization->value() + ui->sldCpuUtilization->singleStep();
    else if (action == 2 || action == 4)
        value = ui->sldCpuUtilization->value() - ui->sldCpuUtilization->singleStep();
    for(int i = 0; i < stepList.count(); i++)
    {
        if (action == 1 || action == 2 || action == 3 || action == 4)
        {
            finalValue = abs(value - stepList.at(i).toInt());
            finalList << QString::number(finalValue);
        }
    }
    if (action == 1 || action == 2 || action == 3 || action == 4)
    {
        int Min = finalList.at(0).toInt();
        for (int i = 1; i < finalList.count(); i++)
        {
           if (finalList.at(i).toInt() < Min)
             Min = finalList.at(i).toInt();
        }
        ui->lblCpuUtilization->setText(stepList.at(finalList.indexOf(QString::number(Min))) + "%");
        ui->sldCpuUtilization->setValue(stepList.at(finalList.indexOf(QString::number(Min))).toInt());
    }
}

void SettingsWidget::on_comboContainer_currentIndexChanged(int index)
{
    if(ui->txtFile->text().contains(".mp4"))
    {
        QString txtFile = ui->txtFile->text();
        txtFile.remove(".mp4");
        ui->txtFile->setText(txtFile);
    }
    else if (ui->txtFile->text().contains(".mkv"))
    {
        QString txtFile = ui->txtFile->text();
        txtFile.remove(".mkv");
        ui->txtFile->setText(txtFile);
    }
}
