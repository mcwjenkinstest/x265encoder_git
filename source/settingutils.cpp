#include <QFile>
#include <QXmlSimpleReader>
#include <QXmlStreamAttributes>
#include "settingutils.h"
#include "encoder.h"
#include "ui_settingswidget.h"
#include <QMap>

#define DEFAULT_PRESET 4
#define DEFAULT_BIT_RATE 28
#define DEFAULT_UTILIZATION 100
#define MinCoreRequired 4

void settingutils::writeSettings(SettingsWidget *setting, int all)
{
    if (all) {
        /*write the basic settings */
        if (setting->ui->sldBitRate->value() !=
                setting->sldBitRateInvertedValue(DEFAULT_BIT_RATE)) {
            writer.writeStartElement("setting");
            writer.writeAttribute("name" , "BasicBitRate");
            writer.writeTextElement("label",setting->ui->lblBitRateVal->text());
            writer.writeTextElement("value",QString::number(setting->ui->sldBitRate->value()));
            writer.writeEndElement();
        }
        if (setting->ui->sldPreset->value() != DEFAULT_PRESET) {
            writer.writeStartElement("setting");
            writer.writeAttribute("name" , "BasicPreset");
            writer.writeTextElement("label",setting->ui->lblPresetVal->text());
            writer.writeTextElement("value",QString::number(setting->ui->sldPreset->value()));
            writer.writeEndElement();
        }
        if (setting->ui->sldCpuUtilization->value() != DEFAULT_UTILIZATION) {
            writer.writeStartElement("setting");
            writer.writeAttribute("name" , "BasicCpuUtilization");
            writer.writeTextElement("label",setting->ui->lblCpuUtilization->text());
            writer.writeTextElement("value",QString::number(setting->ui->sldCpuUtilization->value()));
            writer.writeEndElement();
        }
        /* end of basic */
    }

    /* Determines the mode of Settings */
    if (setting->ui->stackedWidget->currentIndex() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "StackedWidget");
        writer.writeTextElement("index",QString::number(setting->ui->stackedWidget->currentIndex()));
        writer.writeEndElement();
    }

    /* Advance Settings main tab */
    if (setting->ui->comboPresetAdv->currentIndex() != 5) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "AdvancePreset");
        writer.writeTextElement("index",QString::number(setting->ui->comboPresetAdv->currentIndex()));
        writer.writeEndElement();
    }
    if (setting->ui->comboTuneAdv->currentIndex() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "Tune");
        writer.writeTextElement("index",QString::number(setting->ui->comboTuneAdv->currentIndex()));
        writer.writeEndElement();
    }
    if (setting->ui->comboLevelAdv->currentIndex() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "Level");
        writer.writeTextElement("index",QString::number(setting->ui->comboLevelAdv->currentIndex()));
        writer.writeEndElement();
    }
    if (setting->ui->comboProfileAdv->currentIndex() != 1) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "Profile");
        writer.writeTextElement("index",QString::number(setting->ui->comboProfileAdv->currentIndex()));
        writer.writeEndElement();
    }
    if (setting->ui->comboTierAdv->currentIndex() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "Tier");
        writer.writeTextElement("index",QString::number(setting->ui->comboTierAdv->currentIndex()));
        writer.writeEndElement();
    }
    if (setting->ui->groupLog->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "GroupLog");
        writer.writeTextElement("check",QString::number(setting->ui->groupLog->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (setting->ui->comboLogLevel->currentIndex() != 2) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "LogLevel");
        writer.writeTextElement("index",QString::number(setting->ui->comboLogLevel->currentIndex()));
        writer.writeEndElement();
    }
    if (setting->ui->txtLogFile->text() != "") {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "LogFile");
        writer.writeTextElement("value",setting->ui->txtLogFile->text());
        writer.writeEndElement();
    }
    if (setting->ui->chkCUStats->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "CUStatus");
        writer.writeTextElement("check",QString::number(setting->ui->chkCUStats->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (setting->ui->chkParallelMotionEst->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "ParallelMotionEstimation");
        writer.writeTextElement("check",QString::number(setting->ui->chkParallelMotionEst->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (setting->ui->chkParallelModeAnalysis->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "ParallelModeAnalaysis");
        writer.writeTextElement("check",QString::number(setting->ui->chkParallelModeAnalysis->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (setting->ui->spinFrameThreads->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "FrameThreads");
        writer.writeTextElement("value",QString::number(setting->ui->spinFrameThreads->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinLookAheadSlices->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "LookaheadSlices");
        writer.writeTextElement("value",QString::number(setting->ui->spinLookAheadSlices->value()));
        writer.writeEndElement();
    }
    if (QString::compare(setting->ui->txtPools->text(), "*") != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "NumaPools");
        writer.writeTextElement("value",setting->ui->txtPools->text());
        writer.writeEndElement();
    }
    if (!setting->ui->chkWPP->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "WavefrontParallelProcessing");
        writer.writeTextElement("check",QString::number(setting->ui->chkWPP->isChecked()?1:0));
        writer.writeEndElement();
    }
    /* end of main */

    /* Advance Setting Frame Tab */
    if (setting->ui->spinFrames->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "Frames");
        writer.writeTextElement("value",QString::number(setting->ui->spinFrames->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinSeek->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "Seek");
        writer.writeTextElement("value",QString::number(setting->ui->spinSeek->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinMaxIntraPeriod->value() != 250) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "MaximumIntraPeriod");
        writer.writeTextElement("value",QString::number(setting->ui->spinMaxIntraPeriod->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinMinGOPSize->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "MinimumGOPSize");
        writer.writeTextElement("value",QString::number(setting->ui->spinMinGOPSize->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinScenecut->value() != 40) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "Scenecut");
        writer.writeTextElement("value",QString::number(setting->ui->spinScenecut->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinLookAhead->value() != 20) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "LookAheadDepth");
        writer.writeTextElement("value",QString::number(setting->ui->spinLookAhead->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinConsBFrame->value() != 4) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "MaxConsecutiveBFrames");
        writer.writeTextElement("value",QString::number(setting->ui->spinConsBFrame->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinBFrameBias->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "BFramesBias");
        writer.writeTextElement("value",QString::number(setting->ui->spinBFrameBias->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinMaxRef->value() != 3) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "MaxReference");
        writer.writeTextElement("value",QString::number(setting->ui->spinMaxRef->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinLimitRef->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "LimitReference");
        writer.writeTextElement("value",QString::number(setting->ui->spinLimitRef->value()));
        writer.writeEndElement();
    }
    if (!setting->ui->chkBRef->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "BFrameRef");
        writer.writeTextElement("check",QString::number(setting->ui->chkBRef->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (!setting->ui->chkEnableGOP->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "OpenGOP");
        writer.writeTextElement("check",QString::number(setting->ui->chkEnableGOP->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (setting->ui->chkIntraRefresh->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "IntraRefresh");
        writer.writeTextElement("check",QString::number(setting->ui->chkIntraRefresh->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (setting->ui->comboPenaltyTU->currentIndex() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "PenaltyTU");
        writer.writeTextElement("index",QString::number(setting->ui->comboPenaltyTU->currentIndex()));
        writer.writeEndElement();
    }
    if (setting->ui->chkTransSkip->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "TransformSkip");
        writer.writeTextElement("check",QString::number(setting->ui->chkTransSkip->isChecked()?1:0));
        writer.writeEndElement();
        if (setting->ui->chkFastTransSkip->isChecked()) {
            writer.writeStartElement("setting");
            writer.writeAttribute("name" , "FastTransformSkip");
            writer.writeTextElement("check",QString::number(setting->ui->chkFastTransSkip->isChecked()?1:0));
            writer.writeEndElement();
        }
    }
    if (setting->ui->chkConsIntra->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "ConstraintIntra");
        writer.writeTextElement("check",QString::number(setting->ui->chkConsIntra->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (!setting->ui->chkStrongIntra->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "StrongIntra");
        writer.writeTextElement("check",QString::number(setting->ui->chkStrongIntra->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (!setting->ui->chkIntraInBFrames) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "IntraModeBFrames");
        writer.writeTextElement("check",QString::number(setting->ui->chkIntraInBFrames->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (setting->ui->comboAdaptiveB->currentIndex() != 2) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "ComboAdaptiveB");
        writer.writeTextElement("index",QString::number(setting->ui->comboAdaptiveB->currentIndex()));
        writer.writeEndElement();
    }
    /* end of Frame */

    /* Advance Settings Rate Control */
    if (setting->ui->comboRateControl->currentIndex() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "AdvanceRateControl");
        writer.writeTextElement("index",QString::number(setting->ui->comboRateControl->currentIndex()));
        writer.writeEndElement();
    }
    if (setting->ui->spinCRF->value() != 28.0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "CRF");
        writer.writeTextElement("value",QString::number(setting->ui->spinCRF->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinBitRate->value() != 1000) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "BitRate");
        writer.writeTextElement("value",QString::number(setting->ui->spinBitRate->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinQP->value() != 26) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "QP");
        writer.writeTextElement("value",QString::number(setting->ui->spinQP->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinIPRatio->value() != 1.4) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "IPRatio");
        writer.writeTextElement("value",QString::number(setting->ui->spinIPRatio->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinPBRatio->value() != 1.3) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "PBRatio");
        writer.writeTextElement("value",QString::number(setting->ui->spinPBRatio->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinQcomp->value() != 0.6) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "Qcomp");
        writer.writeTextElement("value",QString::number(setting->ui->spinQcomp->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinQpstep->value() != 4.0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "Qpstep");
        writer.writeTextElement("value",QString::number(setting->ui->spinQpstep->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinQblur->value() != 0.5) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "Qblur");
        writer.writeTextElement("value",QString::number(setting->ui->spinQblur->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinCplxblur->value() != 20.0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "Cplxblur");
        writer.writeTextElement("value",QString::number(setting->ui->spinCplxblur->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinQpMin->value() != 0.0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "QpMin");
        writer.writeTextElement("value",QString::number(setting->ui->spinQpMin->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinQpMax->value() != 69.0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "QpMax");
        writer.writeTextElement("value",QString::number(setting->ui->spinQpMax->value()));
        writer.writeEndElement();
    }
    if (setting->ui->checkTwoPass->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "TwoPass");
        writer.writeTextElement("check",QString::number(setting->ui->checkTwoPass->isChecked()?1:0));
        writer.writeEndElement();
        if (setting->ui->checkSlowFirstPass->isChecked()) {
            writer.writeStartElement("setting");
            writer.writeAttribute("name" , "SlowFirstPass");
            writer.writeTextElement("check",QString::number(setting->ui->checkSlowFirstPass->isChecked()?1:0));
            writer.writeEndElement();
        }
    }
    if (setting->ui->chkStrictCbr->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "StrictCBR");
        writer.writeTextElement("check",QString::number(setting->ui->chkStrictCbr->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (setting->ui->spinVbvMaxRate->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "VBVMaxRate");
        writer.writeTextElement("value",QString::number(setting->ui->spinVbvMaxRate->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinVbvBufferSize->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "VBVBufferSize");
        writer.writeTextElement("value",QString::number(setting->ui->spinVbvBufferSize->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinVbvInitBuffer->value() != 0.9) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "VBVinitBuffer");
        writer.writeTextElement("value",QString::number(setting->ui->spinVbvInitBuffer->value()));
        writer.writeEndElement();
    }
    if (setting->ui->checkLossless->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "Lossless");
        writer.writeTextElement("check",QString::number(setting->ui->checkLossless->isChecked()?1:0));
        writer.writeEndElement();
    }
    /* end of RateControl */

    /* Advance Settings Quantization */
    if (setting->ui->comboAdaptQuant->currentIndex() != 2) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "AdaptiveQuantization");
        writer.writeTextElement("index",QString::number(setting->ui->comboAdaptQuant->currentIndex()));
        writer.writeEndElement();
    }
    if (setting->ui->spinAQStrength->value() != 1.00) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "AdaptiveQuantizationStrength");
        writer.writeTextElement("value",QString::number(setting->ui->spinAQStrength->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinCbOffset->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "ChromaOffsetCB");
        writer.writeTextElement("value",QString::number(setting->ui->spinCbOffset->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinCrOffset->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "ChromaOffsetCR");
        writer.writeTextElement("value",QString::number(setting->ui->spinCrOffset->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinNoiseRedIntra->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "NoiseReductionIntra");
        writer.writeTextElement("value",QString::number(setting->ui->spinNoiseRedIntra->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinNoiseRedInter->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "NoiseReductionInter");
        writer.writeTextElement("value",QString::number(setting->ui->spinNoiseRedInter->value()));
        writer.writeEndElement();
    }
    if (setting->ui->txtQPFile->text() != "") {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "QPFile");
        writer.writeTextElement("value",setting->ui->txtQPFile->text());
        writer.writeEndElement();
    }
    if (setting->ui->txtScalingFile->text() != "") {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "ScalingFile");
        writer.writeTextElement("value",setting->ui->txtScalingFile->text());
        writer.writeEndElement();
    }
    if (setting->ui->txtLambdaFile->text() != "") {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "LambdaFile");
        writer.writeTextElement("value",setting->ui->txtLambdaFile->text());
        writer.writeEndElement();
    }
    if (setting->ui->chkqgSize->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "QGSize");
        writer.writeTextElement("check",QString::number(setting->ui->chkqgSize->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (setting->ui->comboqgSize->currentIndex() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "ComboQgsize");
        writer.writeTextElement("index",QString::number(setting->ui->comboqgSize->currentIndex()));
        writer.writeEndElement();
    }
    /* end of Quantization */

    /* Advance Settings Motion */
    if (setting->ui->comboMotionMethod->currentIndex() != 1) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "MotionMethod");
        writer.writeTextElement("index",QString::number(setting->ui->comboMotionMethod->currentIndex()));
        writer.writeEndElement();
    }
    if (setting->ui->spinMotionRange->value() != 57) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "MotionSearchRange");
        writer.writeTextElement("value",QString::number(setting->ui->spinMotionRange->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinSubPixel->value() != 2) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "SubPixelRefinement");
        writer.writeTextElement("value",QString::number(setting->ui->spinSubPixel->value()));
        writer.writeEndElement();
    }
    if (setting->ui->chkRect->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "InterModesRectangular");
        writer.writeTextElement("check",QString::number(setting->ui->chkRect->isChecked()?1:0));
        writer.writeEndElement();
        if (setting->ui->chkAssymetric->isChecked()) {
            writer.writeStartElement("setting");
            writer.writeAttribute("name" , "InterModesAsymmetric");
            writer.writeTextElement("check",QString::number(setting->ui->chkAssymetric->isChecked()?1:0));
            writer.writeEndElement();
        }
    }
    if (setting->ui->spinMaxMerge->value() != 2) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "MaxMergeCandidates");
        writer.writeTextElement("value",QString::number(setting->ui->spinMaxMerge->value()));
        writer.writeEndElement();
    }
    /* end of Motion */

    /* Advance Settings Analysis */
    if (setting->ui->spinRDO->value() != 3) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "RateDisortionOptimization");
        writer.writeTextElement("value",QString::number(setting->ui->spinRDO->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinRDOQLevel->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "RDOQLevel");
        writer.writeTextElement("value",QString::number(setting->ui->spinRDOQLevel->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinLimitTU->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "LimitTU");
        writer.writeTextElement("value",QString::number(setting->ui->spinLimitTU->value()));
        writer.writeEndElement();
    }
    if (setting->ui->chkEarlySkip->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "EarlySkip");
        writer.writeTextElement("check",QString::number(setting->ui->chkEarlySkip->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (setting->ui->chkRskip->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "RecursionSkip");
        writer.writeTextElement("check",QString::number(setting->ui->chkRskip->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (!setting->ui->chkWeightedP->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "WrightPredictioninP");
        writer.writeTextElement("check",QString::number(setting->ui->chkWeightedP->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (setting->ui->chkWeightedB->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "WeightPredictioninB");
        writer.writeTextElement("check",QString::number(setting->ui->chkWeightedB->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (setting->ui->chkCULossless->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "CULossless");
        writer.writeTextElement("check",QString::number(setting->ui->chkCULossless->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (!setting->ui->chkHideSign->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "HideSignBit");
        writer.writeTextElement("check",QString::number(setting->ui->chkHideSign->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (!setting->ui->chkCUTree->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "CUTree");
        writer.writeTextElement("check",QString::number(setting->ui->chkCUTree->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (setting->ui->comboMinCUSize->currentIndex() != 3) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "MinCUSize");
        writer.writeTextElement("index",QString::number(setting->ui->comboMinCUSize->currentIndex()));
        writer.writeEndElement();
    }
    if (setting->ui->comboMaxCUSize->currentIndex() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "MaxCUSize");
        writer.writeTextElement("index",QString::number(setting->ui->comboMaxCUSize->currentIndex()));
        writer.writeEndElement();
    }
    if (setting->ui->comboMaxTUSize->currentIndex() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "MaxTUSize");
        writer.writeTextElement("index",QString::number(setting->ui->comboMaxTUSize->currentIndex()));
        writer.writeEndElement();
    }
    if (setting->ui->spinTUInterDepth->value() != 1) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "TUInterDepth");
        writer.writeTextElement("value",QString::number(setting->ui->spinTUInterDepth->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinTUIntraDepth->value() != 1) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "TUIntraDepth");
        writer.writeTextElement("value",QString::number(setting->ui->spinTUIntraDepth->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinPsyRDO->value() != 0.00) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "PSYRDO");
        writer.writeTextElement("value",QString::number(setting->ui->spinPsyRDO->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinPsyRDOQuant->value() != 0.00) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "PSYRDOQuant");
        writer.writeTextElement("value",QString::number(setting->ui->spinPsyRDOQuant->value()));
        writer.writeEndElement();
    }
    if (setting->ui->chkLimitModes->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "LimitModes");
        writer.writeTextElement("check",QString::number(setting->ui->chkLimitModes->isChecked()?1:0));
        writer.writeEndElement();
    }
    /* end of Analysis */

    /* Advance Settings VUI */
    if (setting->ui->comboSAR->currentIndex() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "SampleAspectRatio");
        writer.writeTextElement("index",QString::number(setting->ui->comboSAR->currentIndex()));
        writer.writeEndElement();
    }
    if (setting->ui->checkCustomSAR->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "CustomSAREnable");
        writer.writeTextElement("check",QString::number(setting->ui->checkCustomSAR->isChecked()?1:0));
        writer.writeEndElement();
        if (setting->ui->spinCustomSarWidth->value() != 1) {
            writer.writeStartElement("setting");
            writer.writeAttribute("name" , "CustomSARWidth");
            writer.writeTextElement("value",QString::number(setting->ui->spinCustomSarWidth->value()));
            writer.writeEndElement();
        }
        if (setting->ui->spinCustomSarHeight->value() != 1) {
            writer.writeStartElement("setting");
            writer.writeAttribute("name" , "CustomSARHeight");
            writer.writeTextElement("value",QString::number(setting->ui->spinCustomSarHeight->value()));
            writer.writeEndElement();
        }
    }
    if (setting->ui->comboVideoFormat->currentIndex() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "VideoFormat");
        writer.writeTextElement("index",QString::number(setting->ui->comboVideoFormat->currentIndex()));
        writer.writeEndElement();
    }
    if (setting->ui->comboRange->currentIndex() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "Range");
        writer.writeTextElement("index",QString::number(setting->ui->comboRange->currentIndex()));
        writer.writeEndElement();
    }
    if (setting->ui->comboColorPrim->currentIndex() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "ColorPrimitive");
        writer.writeTextElement("index",QString::number(setting->ui->comboColorPrim->currentIndex()));
        writer.writeEndElement();
    }
    if (setting->ui->comboTransferChar->currentIndex() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "TransferCharacteristics");
        writer.writeTextElement("index",QString::number(setting->ui->comboTransferChar->currentIndex()));
        writer.writeEndElement();
    }
    if (setting->ui->comboColorMatrix->currentIndex() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "ColorMatrix");
        writer.writeTextElement("index",QString::number(setting->ui->comboColorMatrix->currentIndex()));
        writer.writeEndElement();
    }
    if (setting->ui->comboChromaloc->currentIndex() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "ChromaSampleLocation");
        writer.writeTextElement("index",QString::number(setting->ui->comboChromaloc->currentIndex()));
        writer.writeEndElement();
    }
    if (setting->ui->comboOverScan->currentIndex() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "OverScan");
        writer.writeTextElement("index",QString::number(setting->ui->comboOverScan->currentIndex()));
        writer.writeEndElement();
    }
    if (setting->ui->spinCropLeft->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "CropLeft");
        writer.writeTextElement("value",QString::number(setting->ui->spinCropLeft->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinCropTop->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "CropTop");
        writer.writeTextElement("value",QString::number(setting->ui->spinCropTop->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinCropRight->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "CropRight");
        writer.writeTextElement("value",QString::number(setting->ui->spinCropRight->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinCropTop->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "CropBottom");
        writer.writeTextElement("value",QString::number(setting->ui->spinCropBottom->value()));
        writer.writeEndElement();
    }
    if (setting->ui->txtMasterDisplay->text() != "") {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "MasterDisplay");
        writer.writeTextElement("value",setting->ui->txtMasterDisplay->text());
        writer.writeEndElement();
    }
    if (setting->ui->txtLightLevel->text() != "") {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "LightLevel");
        writer.writeTextElement("value",setting->ui->txtLightLevel->text());
        writer.writeEndElement();
    }
    if (setting->ui->spinMaxLuma->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "MaxLuma");
        writer.writeTextElement("value",QString::number(setting->ui->spinMaxLuma->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinMinLuma->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "MinLuma");
        writer.writeTextElement("value",QString::number(setting->ui->spinMinLuma->value()));
        writer.writeEndElement();
    }
    /* end of VUI */

    /* Advance Settings MISC */
    if (setting->ui->chkRepeatHeader->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "RepeatHeaders");
        writer.writeTextElement("check",QString::number(setting->ui->chkRepeatHeader->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (setting->ui->chkHRDSignal->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "HRDSignaling");
        writer.writeTextElement("check",QString::number(setting->ui->chkHRDSignal->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (!setting->ui->chkEmitSEI->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "EmitSEI");
        writer.writeTextElement("check",QString::number(setting->ui->chkEmitSEI->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (setting->ui->chkAUDSignal->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "AUDSignaling");
        writer.writeTextElement("check",QString::number(setting->ui->chkAUDSignal->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (setting->ui->chkTemporalLayer->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "TemporalLayer");
        writer.writeTextElement("check",QString::number(setting->ui->chkTemporalLayer->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (!setting->ui->chkLoopFilter->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "LoopFilter");
        writer.writeTextElement("check",QString::number(setting->ui->chkLoopFilter->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (setting->ui->chkLoopFilter->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "DeblockFilter");
        writer.writeTextElement("check",QString::number(setting->ui->chkLoopFilter->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (setting->ui->spinDeblocktC->value() !=0 ) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "DeblocktC");
        writer.writeTextElement("value",QString::number(setting->ui->spinDeblocktC->value()));
        writer.writeEndElement();
    }
    if (setting->ui->spinDeblockBeta->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "DeblockBeta");
        writer.writeTextElement("value",QString::number(setting->ui->spinDeblockBeta->value()));
        writer.writeEndElement();
    }
    if (!setting->ui->chkSAO->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "SampleAdaptiveOffset");
        writer.writeTextElement("check",QString::number(setting->ui->chkSAO->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (setting->ui->radioNonDeblock->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "SAODeblock");
        writer.writeTextElement("radio",QString::number(setting->ui->radioNonDeblock->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (!setting->ui->radioSkipRightBottom->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "SAORightBottom");
        writer.writeTextElement("radio",QString::number(setting->ui->radioSkipRightBottom->isChecked()?1:0));
        writer.writeEndElement();
    }
    if (setting->ui->chkUhdBd->isChecked()) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "UHDBluray");
        writer.writeTextElement("check",QString::number(setting->ui->chkUhdBd->isChecked()?1:0));
        writer.writeEndElement();
    }
    /* end of MISC */

    /* Advance Settings TOOLS */
    if (setting->ui->Width->text().toInt() !=0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name", "ScaleWidth");
        writer.writeTextElement("value",QString::number(setting->ui->Width->text().toInt()));
        writer.writeEndElement();
    }
    if (setting->ui->Height->text().toInt() !=0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name", "ScaleHeight");
        writer.writeTextElement("value",QString::number(setting->ui->Height->text().toInt()));
        writer.writeEndElement();
    }
    if (setting->ui->frameRate->text().toFloat() !=0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name", "ConvertFrameRate");
        writer.writeTextElement("value",QString::number(setting->ui->frameRate->text().toFloat()));
        writer.writeEndElement();
    }
    if (setting->ui->CropLeft->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "VideoCropLeft");
        writer.writeTextElement("value",QString::number(setting->ui->CropLeft->value()));
        writer.writeEndElement();
    }
    if (setting->ui->CropRight->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "VideoCropRight");
        writer.writeTextElement("value",QString::number(setting->ui->CropRight->value()));
        writer.writeEndElement();
    }
    if (setting->ui->CropTop->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "VideoCropTop");
        writer.writeTextElement("value",QString::number(setting->ui->CropTop->value()));
        writer.writeEndElement();
    }
    if (setting->ui->CropBottom->value() != 0) {
        writer.writeStartElement("setting");
        writer.writeAttribute("name" , "VideoCropBottom");
        writer.writeTextElement("value",QString::number(setting->ui->CropBottom->value()));
        writer.writeEndElement();
    }
    /* end of TOOLS */
}

void settingutils::readSettings(SettingsWidget *setting, QString filename)
{
    reader.readNext();
    QStringList videoInfo = MediaInfo::getVideoDetails(QString("\"") + filename + QString("\""));
    QStringList generalInfo = MediaInfo::getGeneralDetails(QString("\"") + filename + QString("\""));
    while(!(reader.isEndElement() && reader.name() == "file")) {
        if(reader.isStartElement() && reader.name() == "setting") {
           while(!(reader.isEndElement() && reader.name() == "setting")) {
               QXmlStreamAttributes attrs = reader.attributes();
               if((videoInfo.at(0).isEmpty() || videoInfo.at(0).isNull()) && generalInfo.at(1).compare("YUV", Qt::CaseInsensitive) == 0)
               {
                    if (attrs.value("name").toString() == "Width") {
                        reader.readNextStartElement();
                        if(reader.isStartElement() && reader.name().toString() == "value")
                           filecontainer->videoWidth << QString::number(reader.readElementText().toInt());
                    }
                    else if (attrs.value("name").toString() == "Height") {
                        reader.readNextStartElement();
                        if(reader.isStartElement() && reader.name().toString() == "value")
                           filecontainer->videoHeight << QString::number(reader.readElementText().toInt());
                    }
                    else if (attrs.value("name").toString() == "Depth") {
                        reader.readNextStartElement();
                        if(reader.isStartElement() && reader.name().toString() == "value")
                           filecontainer->videoDepth << QString::number(reader.readElementText().toInt());
                    }
                    else if (attrs.value("name").toString() == "Framerate") {
                        reader.readNextStartElement();
                        if(reader.isStartElement() && reader.name().toString() == "value")
                           filecontainer->frameRate << QString::number(reader.readElementText().toFloat());
                    }
                    else if (attrs.value("name").toString() == "Colorspace") {
                        reader.readNextStartElement();
                        if(reader.isStartElement() && reader.name().toString() == "value")
                           filecontainer->videoColorSpace << reader.readElementText();
                    }
                    else if (attrs.value("name").toString() == "Subsampling") {
                        reader.readNextStartElement();
                        if(reader.isStartElement() && reader.name().toString() == "value")
                           filecontainer->subSampling << reader.readElementText();
                    }
                    else if (attrs.value("name").toString() == "Totalframes") {
                        reader.readNextStartElement();
                        if(reader.isStartElement() && reader.name().toString() == "value")
                           filecontainer->totalFrames << QString::number(reader.readElementText().toInt());
                    }
                    else if (attrs.value("name").toString() == "Fps") {
                        reader.readNextStartElement();
                        if(reader.isStartElement() && reader.name().toString() == "value")
                           filecontainer->fps << QString::number(reader.readElementText().toFloat());
                    }
               }
               int num = SettingsMap[attrs.value("name").toString()];
               switch(num)
               {
                  case 1 :   reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "label")
                                setting->ui->lblBitRateVal->setText(reader.readElementText());
                             reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->sldBitRate->setValue(reader.readElementText().toInt());
                             break;
                  case 2 :   reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "label")
                                setting->ui->lblPresetVal->setText(reader.readElementText());
                             reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->sldPreset->setValue(reader.readElementText().toInt());
                             break;
                  case 3 :   reader.readNextStartElement();
                             if(reader.isStartElement() &&  reader.name().toString() == "label")
                                setting->ui->lblCpuUtilization->setText(reader.readElementText());
                             reader.readNextStartElement();
                             if(reader.isStartElement() &&  reader.name().toString() == "value")
                             setting->ui->sldCpuUtilization->setValue(reader.readElementText().toInt());
                             break;

                  /* Sets the mode while saving */
                  case 4 :   reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->stackedWidget->setCurrentIndex(reader.readElementText().toInt());
                             break;
                  /* Retrives and set the value in main page */
                  case 5 :   reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->comboPresetAdv->setCurrentIndex(reader.readElementText().toInt());
                             break;
                  case 6 :   reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->comboTuneAdv->setCurrentIndex(reader.readElementText().toInt());
                             break;
                  case 7 :   reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->comboLevelAdv->setCurrentIndex(reader.readElementText().toInt());
                             break;
                  case 8 :   reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->comboProfileAdv->setCurrentIndex(reader.readElementText().toInt());
                             break;
                  case 9 :   reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->comboTierAdv->setCurrentIndex(reader.readElementText().toInt());
                             break;
                  case 10 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->groupLog->setChecked(reader.readElementText().toInt()==1?true:false);
                             break;
                  case 11 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->comboLogLevel->setCurrentIndex(reader.readElementText().toInt());
                             break;
                  case 12 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->txtLogFile->setText(reader.readElementText());
                             break;
                  case 13 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkCUStats->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 14 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkParallelMotionEst->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 15 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkParallelModeAnalysis->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 16 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinFrameThreads->setValue(reader.readElementText().toFloat());
                             break;
                  case 17 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinLookAheadSlices->setValue(reader.readElementText().toInt());
                             break;
                  case 18 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->txtPools->setText(reader.readElementText());
                             break;
                  case 19 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkWPP->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  /* end of main page */

                  /* Advance Setting Frame Tab */
                  case 20 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinFrames->setValue(reader.readElementText().toFloat());
                             break;
                  case 21 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinSeek->setValue(reader.readElementText().toFloat());
                             break;
                  case 22 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinMaxIntraPeriod->setValue(reader.readElementText().toFloat());
                             break;
                  case 23 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinMinGOPSize->setValue(reader.readElementText().toFloat());
                             break;
                  case 24 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinScenecut->setValue(reader.readElementText().toFloat());
                             break;
                  case 25 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinLookAhead->setValue(reader.readElementText().toFloat());
                             break;
                  case 26 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinConsBFrame->setValue(reader.readElementText().toFloat());
                             break;
                  case 27 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinBFrameBias->setValue(reader.readElementText().toFloat());
                             break;
                  case 28 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinMaxRef->setValue(reader.readElementText().toFloat());
                             break;
                  case 29 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinLimitRef->setValue(reader.readElementText().toInt());
                             break;
                  case 30 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkBRef->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 31 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkEnableGOP->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 32 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkIntraRefresh->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 33 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->comboPenaltyTU->setCurrentIndex(reader.readElementText().toInt());
                             break;
                  case 34 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                 setting->ui->chkTransSkip->setChecked(reader.readElementText().toInt()?true:false);
                             if (setting->ui->chkTransSkip->isChecked())
                                 setting->ui->chkFastTransSkip->setEnabled(true);
                             break;
                  case 35 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkConsIntra->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 36 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkStrongIntra->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 37 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkFastTransSkip->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 38 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkIntraInBFrames->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  /* end of Frame Tab */

                  /* Advance Setting Rate Control */
                  case 39 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->comboRateControl->setCurrentIndex(reader.readElementText().toInt());
                             if (setting->ui->comboRateControl->currentIndex() == 2)
                                 setting->ui->checkTwoPass->setEnabled(true);
                             else if (setting->ui->comboRateControl->currentIndex() == 3) {
                                 setting->ui->spinVbvMaxRate->setDisabled(true);
                                 setting->ui->spinVbvBufferSize->setDisabled(true);
                                 setting->ui->spinVbvInitBuffer->setDisabled(true);
                             }
                             break;
                  case 40 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinCRF->setValue(reader.readElementText().toFloat());
                             break;
                  case 41 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinBitRate->setValue(reader.readElementText().toFloat());
                             break;
                  case 42 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinQP->setValue(reader.readElementText().toFloat());
                             break;
                  case 43 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinIPRatio->setValue(reader.readElementText().toFloat());
                             break;
                  case 44 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinPBRatio->setValue(reader.readElementText().toFloat());
                             break;
                  case 45 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinQcomp->setValue(reader.readElementText().toFloat());
                             break;
                  case 46 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                               setting->ui->spinQpstep->setValue(reader.readElementText().toFloat());
                             break;
                  case 47 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinQblur->setValue(reader.readElementText().toFloat());
                             break;
                  case 48 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinCplxblur->setValue(reader.readElementText().toFloat());
                             break;
                  case 49 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                 setting->ui->checkTwoPass->setChecked(reader.readElementText().toInt()?true:false);
                             if (setting->ui->checkTwoPass->isChecked())
                                 setting->ui->checkSlowFirstPass->setEnabled(true);
                             break;
                  case 50 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->checkSlowFirstPass->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 51 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkStrictCbr->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 52 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinVbvMaxRate->setValue(reader.readElementText().toFloat());
                             break;
                  case 53 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinVbvBufferSize->setValue(reader.readElementText().toFloat());
                             break;
                  case 54 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinVbvInitBuffer->setValue(reader.readElementText().toFloat());
                             break;
                  case 55 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->checkLossless->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  /* end of Rate Control */

                  /* Advance Mode Quantization */
                  case 56 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->comboAdaptQuant->setCurrentIndex(reader.readElementText().toInt());
                             break;
                  case 57 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinAQStrength->setValue(reader.readElementText().toFloat());
                             break;
                  case 58 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinCbOffset->setValue(reader.readElementText().toFloat());
                             break;
                  case 59 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinCrOffset->setValue(reader.readElementText().toFloat());
                             break;
                  case 60 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinNoiseRedIntra->setValue(reader.readElementText().toFloat());
                             break;
                  case 61 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinNoiseRedInter->setValue(reader.readElementText().toFloat());
                             break;
                  case 62 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->txtQPFile->setText(reader.readElementText());
                             break;
                  case 63 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->txtScalingFile->setText(reader.readElementText());
                             break;
                  case 64 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->txtLambdaFile->setText(reader.readElementText());
                             break;
                  /* end of Quantization */

                  /* Advance Setting Motion Page */
                  case 65 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->comboMotionMethod->setCurrentIndex(reader.readElementText().toInt());
                             break;
                  case 66 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinMotionRange->setValue(reader.readElementText().toFloat());
                             break;
                  case 67 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinSubPixel->setValue(reader.readElementText().toFloat());
                             break;
                  case 68 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                 setting->ui->chkRect->setChecked(reader.readElementText().toInt()?true:false);
                             if (setting->ui->chkRect->isChecked())
                                 setting->ui->chkAssymetric->setEnabled(true);
                             break;
                  case 69 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkAssymetric->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 70 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinMaxMerge->setValue(reader.readElementText().toFloat());
                             break;
                  /* end of Motion Page */

                  /* Advance Settings Analysis */
                  case 71 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinRDO->setValue(reader.readElementText().toFloat());
                             break;
                  case 72 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinRDOQLevel->setValue(reader.readElementText().toInt());
                             break;
                  case 73 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkTransSkip->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 74 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkWeightedP->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 75 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkWeightedB->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 76 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkCULossless->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 77 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkHideSign->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 78 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkCUTree->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 79 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->comboMinCUSize->setCurrentIndex(reader.readElementText().toInt());
                             break;
                  case 80 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->comboMaxCUSize->setCurrentIndex(reader.readElementText().toInt());
                             break;
                  case 81 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->comboMaxTUSize->setCurrentIndex(reader.readElementText().toInt());
                             break;
                  case 82 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinTUInterDepth->setValue(reader.readElementText().toFloat());
                             break;
                  case 84 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinTUIntraDepth->setValue(reader.readElementText().toFloat());
                             break;
                  case 85 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinPsyRDO->setValue(reader.readElementText().toFloat());
                             break;
                  case 86 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinPsyRDOQuant->setValue(reader.readElementText().toFloat());
                             break;
                  /* end of Analysis Settings */

                  /* Advance Setting VUI */
                  case 87 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->comboSAR->setCurrentIndex(reader.readElementText().toInt());
                             break;
                  case 88 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                 setting->ui->checkCustomSAR->setChecked(reader.readElementText().toInt()?true:false);
                             if (setting->ui->checkCustomSAR->isChecked()) {
                                 setting->ui->comboSAR->setDisabled(true);
                                 setting->ui->spinCustomSarWidth->setEnabled(true);
                                 setting->ui->spinCustomSarHeight->setEnabled(true);
                             }
                             break;
                  case 89 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinCustomSarWidth->setValue(reader.readElementText().toFloat());
                             break;
                  case 90 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinCustomSarHeight->setValue(reader.readElementText().toFloat());
                             break;
                  case 91 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->comboVideoFormat->setCurrentIndex(reader.readElementText().toInt());
                             break;
                  case 92 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->comboRange->setCurrentIndex(reader.readElementText().toInt());
                             break;
                  case 93 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->comboColorPrim->setCurrentIndex(reader.readElementText().toInt());
                             break;
                  case 94 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->comboTransferChar->setCurrentIndex(reader.readElementText().toInt());
                             break;
                  case 95 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->comboColorMatrix->setCurrentIndex(reader.readElementText().toInt());
                             break;
                  case 96 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->comboChromaloc->setCurrentIndex(reader.readElementText().toInt());
                             break;
                  case 97 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->comboOverScan->setCurrentIndex(reader.readElementText().toInt());
                             if(setting->ui->comboOverScan->currentIndex() == 2) {
                                setting->ui->spinCropLeft->setEnabled(true);
                                setting->ui->spinCropTop->setEnabled(true);
                                setting->ui->spinCropRight->setEnabled(true);
                                setting->ui->spinCropBottom->setEnabled(true);
                             }
                             break;
                  case 98 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinCropLeft->setValue(reader.readElementText().toFloat());
                             break;
                  case 99 :  reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinCropTop->setValue(reader.readElementText().toFloat());
                             break;
                  case 100 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinCropRight->setValue(reader.readElementText().toFloat());
                             break;
                  case 101 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinCropBottom->setValue(reader.readElementText().toFloat());
                             break;
                  case 102 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->txtMasterDisplay->setText(reader.readElementText());
                             break;
                  case 103 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->txtLightLevel->setText(reader.readElementText());
                             break;
                  /* end of VUI */

                  /* Advance Setting MISC */
                  case 104 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkRepeatHeader->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 105 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkHRDSignal->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 106 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkEmitSEI->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 107 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkAUDSignal->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 108 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkTemporalLayer->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 109 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkLoopFilter->setChecked(reader.readElementText().toInt()?true:false);
                             setting->ui->spinDeblocktC->setVisible(false);
                             setting->ui->commaDeblock->setVisible(false);
                             setting->ui->spinDeblockBeta->setVisible(false);
                             break;
                  case 110 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkSAO->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 111 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "radio")
                                setting->ui->radioNonDeblock->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 112 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "radio")
                                setting->ui->radioSkipRightBottom->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  /* end of MISC */

                  /* Advance Setting TOOLS */
                  case 113 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->Width->setValue(reader.readElementText().toInt());
                             break;
                  case 114 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->Height->setValue(reader.readElementText().toInt());
                             break;
                  case 115 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->CropLeft->setValue(reader.readElementText().toFloat());
                             break;
                  case 116 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->CropRight->setValue(reader.readElementText().toFloat());
                             break;
                  case 117 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->CropTop->setValue(reader.readElementText().toFloat());
                             break;
                  case 118 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->CropBottom->setValue(reader.readElementText().toFloat());
                             break;
                  case 119 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkqgSize->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 120 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkLimitModes->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 121 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinMaxLuma->setValue(reader.readElementText().toFloat());
                             break;
                  case 122 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinMinLuma->setValue(reader.readElementText().toFloat());
                             break;
                  case 123 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->comboqgSize->setCurrentIndex(reader.readElementText().toInt());
                             if(setting->ui->chkqgSize->isChecked())
                                 setting->ui->comboqgSize->setEnabled(true);
                             break;
                  case 124 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkLoopFilter->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 125 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinDeblocktC->setValue(reader.readElementText().toInt());
                             break;
                  case 126 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinDeblockBeta->setValue(reader.readElementText().toInt());
                             break;
                  case 127 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "index")
                                setting->ui->comboAdaptiveB->setCurrentIndex(reader.readElementText().toInt());
                             break;
                  case 128 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkUhdBd->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 129 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "check")
                                setting->ui->chkRskip->setChecked(reader.readElementText().toInt()?true:false);
                             break;
                  case 130 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinQpMin->setValue(reader.readElementText().toFloat());
                             break;
                  case 131 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinQpMax->setValue(reader.readElementText().toFloat());
                             break;
                  case 132 : reader.readNextStartElement();
                             if(reader.isStartElement() && reader.name().toString() == "value")
                                setting->ui->spinLimitTU->setValue(reader.readElementText().toInt());
                          break;
                  default :
                            break;

               }
               reader.readNext();
            }
        }
        reader.readNext();
    }
}


settingutils::settingutils()
{
   filecontainer = FileContainer::instance();
   SettingsMap.insert("BasicBitRate", 1);
   SettingsMap.insert("BasicPreset", 2);
   SettingsMap.insert("BasicCpuUtilization", 3);
   SettingsMap.insert("StackedWidget", 4);
   SettingsMap.insert("AdvancePreset", 5);
   SettingsMap.insert("Tune", 6);
   SettingsMap.insert("Level", 7);
   SettingsMap.insert("Profile", 8);
   SettingsMap.insert("Tier", 9);
   SettingsMap.insert("GroupLog", 10);
   SettingsMap.insert("LogLevel", 11);
   SettingsMap.insert("LogFile", 12);
   SettingsMap.insert("CUStatus", 13);
   SettingsMap.insert("ParallelMotionEstimation", 14);
   SettingsMap.insert("ParallelModeAnalaysis", 15);
   SettingsMap.insert("FrameThreads", 16);
   SettingsMap.insert("LookaheadSlices", 17);
   SettingsMap.insert("NumaPools", 18);
   SettingsMap.insert("WavefrontParallelProcessing", 19);
   SettingsMap.insert("Frames", 20);
   SettingsMap.insert("Seek", 21);
   SettingsMap.insert("MaximumIntraPeriod", 22);
   SettingsMap.insert("MinimumGOPSize", 23);
   SettingsMap.insert("Scenecut", 24);
   SettingsMap.insert("LookAheadDepth", 25);
   SettingsMap.insert("MaxConsecutiveBFrames", 26);
   SettingsMap.insert("BFramesBias", 27);
   SettingsMap.insert("MaxReference", 28);
   SettingsMap.insert("LimitReference", 29);
   SettingsMap.insert("BFrameRef", 30);
   SettingsMap.insert("OpenGOP", 31);
   SettingsMap.insert("IntraRefresh", 32);
   SettingsMap.insert("PenaltyTU", 33);
   SettingsMap.insert("TransformSkip", 34);
   SettingsMap.insert("ConstraintIntra", 35);
   SettingsMap.insert("StrongIntra", 36);
   SettingsMap.insert("FastTransformSkip", 37);
   SettingsMap.insert("InterModeBFrames", 38);
   SettingsMap.insert("AdvanceRateControl", 39);
   SettingsMap.insert("CRF", 40);
   SettingsMap.insert("Bitrate", 41);
   SettingsMap.insert("QP", 42);
   SettingsMap.insert("IPRatio", 43);
   SettingsMap.insert("PBRatio", 44);
   SettingsMap.insert("Qcomp", 45);
   SettingsMap.insert("Qpstep", 46);
   SettingsMap.insert("Qblur", 47);
   SettingsMap.insert("Cplxblur", 48);
   SettingsMap.insert("TwoPass", 49);
   SettingsMap.insert("SlowFirstPass", 50);
   SettingsMap.insert("StrictCBR", 51);
   SettingsMap.insert("VBVMaxRate", 52);
   SettingsMap.insert("VBVBufferSize", 53);
   SettingsMap.insert("VBVinitBuffer", 54);
   SettingsMap.insert("Lossless", 55);
   SettingsMap.insert("AdaptiveQuantization", 56);
   SettingsMap.insert("AdaptiveQuantizationStrength", 57);
   SettingsMap.insert("ChromaOffsetCB", 58);
   SettingsMap.insert("ChromaOffsetCR", 59);
   SettingsMap.insert("NoiseReductionIntra", 60);
   SettingsMap.insert("NoiseReductionInter", 61);
   SettingsMap.insert("QPFile", 62);
   SettingsMap.insert("ScalingFile", 63);
   SettingsMap.insert("LambdaFile", 64);
   SettingsMap.insert("MotionMethod", 65);
   SettingsMap.insert("MotionSearchRange", 66);
   SettingsMap.insert("SubPixelRefinement", 67);
   SettingsMap.insert("InterModesRectangular", 68);
   SettingsMap.insert("InterModesAsymmetric", 69);
   SettingsMap.insert("MaxMergeCandidates", 70);
   SettingsMap.insert("RateDisortionOptimization", 71);
   SettingsMap.insert("RDOQLevel", 72);
   SettingsMap.insert("TransformSkip", 73);
   SettingsMap.insert("WrightPredictioninP", 74);
   SettingsMap.insert("WeightPredictioninB", 75);
   SettingsMap.insert("CULossless", 76);
   SettingsMap.insert("HideSignBit", 77);
   SettingsMap.insert("CUTree", 78);
   SettingsMap.insert("MinCUSize", 79);
   SettingsMap.insert("MaxCUSize", 80);
   SettingsMap.insert("MaxTUSize", 81);
   SettingsMap.insert("MaxCUSize", 82);
   SettingsMap.insert("TUInterDepth", 83);
   SettingsMap.insert("TUIntraDepth", 84);
   SettingsMap.insert("PSYRDO", 85);
   SettingsMap.insert("PSYRDOQuant", 86);
   SettingsMap.insert("SampleAspectRatio", 87);
   SettingsMap.insert("CustomSAREnable", 88);
   SettingsMap.insert("CustomSARWidth", 89);
   SettingsMap.insert("CustomSARHeight", 90);
   SettingsMap.insert("VideoFormat", 91);
   SettingsMap.insert("Range", 92);
   SettingsMap.insert("ColorPrimitive", 93);
   SettingsMap.insert("TransferCharacteristics", 94);
   SettingsMap.insert("ColorMatrix", 95);
   SettingsMap.insert("ChromaSampleLocation", 96);
   SettingsMap.insert("OverScan", 97);
   SettingsMap.insert("CropLeft", 98);
   SettingsMap.insert("CropTop", 99);
   SettingsMap.insert("CropRight", 100);
   SettingsMap.insert("CropBottom", 101);
   SettingsMap.insert("MasterDisplay", 102);
   SettingsMap.insert("LightLevel", 103);
   SettingsMap.insert("RepeatHeaders", 104);
   SettingsMap.insert("HRDSignaling", 105);
   SettingsMap.insert("EmitSEI", 106);
   SettingsMap.insert("AUDSignaling", 107);
   SettingsMap.insert("TemporalLayer", 108);
   SettingsMap.insert("LoopFilter", 109);
   SettingsMap.insert("SampleAdaptiveOffset", 110);
   SettingsMap.insert("SAODeblock", 111);
   SettingsMap.insert("SAORightBottom", 112);
   SettingsMap.insert("ScaleWidth", 113);
   SettingsMap.insert("ScaleHeight", 114);
   SettingsMap.insert("VideoCropLeft", 115);
   SettingsMap.insert("VideoCropRight", 116);
   SettingsMap.insert("VideoCropTop", 117);
   SettingsMap.insert("VideoCropBottom", 118);
   SettingsMap.insert("QGSize", 119);
   SettingsMap.insert("LimitModes", 120);
   SettingsMap.insert("MaxLuma", 121);
   SettingsMap.insert("MinLuma", 122);
   SettingsMap.insert("ComboQgsize", 123);
   SettingsMap.insert("DeblockFilter", 124);
   SettingsMap.insert("DeblocktC", 125);
   SettingsMap.insert("DeblockBeta", 126);
   SettingsMap.insert("ComboAdaptiveB", 127);
   SettingsMap.insert("UHDBluray", 128);
   SettingsMap.insert("RecursionSkip", 129);
   SettingsMap.insert("QpMin", 130);
   SettingsMap.insert("QpMax", 131);
   SettingsMap.insert("LimitTU", 132);
}

void settingutils::loadsettings(QList<SettingsWidget *> settList,
                                QString filepath, int all)
{
    filecontainer->loadxml = true;
    QMessageBox msg;
    msg.setIcon(QMessageBox::Critical);
    msg.setDefaultButton(QMessageBox::Ok);
    msg.setModal(true);
    QFile file(filepath);
    if (!file.open(QFile::ReadOnly|QFile::Text)) {
        msg.setText("Not a valid file");
        msg.exec();
        return;
    }
    QXmlInputSource source(&file);
    QXmlSimpleReader xmlsimplereader;
    if(!xmlsimplereader.parse(source)) {
        msg.setText("Errors in xml");
        msg.exec();
        return;
    }
    file.seek(0);
    reader.setDevice(&file);
    if(reader.hasError()) {
        msg.setText(reader.errorString());
        msg.exec();
        return;
    }
    reader.readNextStartElement();
    if(reader.name() != "x265Encoder") {
        msg.setText("Not a valid x265Encoder Settings");
        msg.exec();
        return;
    }
    if (!all){
        filecontainer->loadxml = false;
        readSettings(settList.at(0), "");
        return;
    }
    while(reader.readNextStartElement()){
        if (reader.name()=="file" ) {
          QXmlStreamAttributes attrs = reader.attributes();
          if (!attrs.hasAttribute("path") ||
                  !attrs.hasAttribute("destdir") ||
                  !attrs.hasAttribute("destfile")) {
              msg.setText("Cannot proceed.\nErrors in settings found.");
              msg.exec();
              return;
          }
          QFile videofile(attrs.value("path").toString());
          if (videofile.exists()) {
              SettingsWidget *setting = new SettingsWidget();
              readSettings(setting, videofile.fileName());
              setting->ui->txtDest->setText(attrs.value("destdir").toString());
              setting->ui->txtFile->setText(attrs.value("destfile").toString());
              setting->ui->comboContainer->setCurrentText(attrs.value("destContainer").toString());
              Encoder::instance()->setList.append(setting);
              filecontainer->queueAdd(attrs.value("path").toString(),false);
              setting->setSelectedSetting(setting->ui->stackedWidget->currentIndex());
          }
          else  {
              msg.setText("Some of the files were removed from the location");
              msg.setIcon(QMessageBox::Warning);
              msg.exec();
              msg.setIcon(QMessageBox::Critical);
          }
        }
        else {
            msg.setText("File tag not found");
            msg.exec();
            return;
        }
    }
}

void settingutils::saveSettings(QList<SettingsWidget *> settList,
                                QString filepath, int all)
{
    QFile file(filepath);
    if (!file.open(QFile::WriteOnly)) {
        QMessageBox msg;
        msg.setIcon(QMessageBox::Critical);
        msg.setDefaultButton(QMessageBox::Ok);
        msg.setText("Unable to write to the file");
        msg.setModal(true);
        msg.exec();
        return;
    }
    writer.setDevice(&file);
    writer.setAutoFormatting(true);
    writer.setAutoFormattingIndent(2);
    writer.writeStartDocument("1.0",true);
    writer.writeStartElement("x265Encoder");
    int i = 0;
    foreach (SettingsWidget * setting, settList){
        writer.writeStartElement("file");
        if (all) {
            writer.writeAttribute("path",filecontainer->item(i)->text());
            writer.writeAttribute("destdir",setting->ui->txtDest->text());
            writer.writeAttribute("destfile",setting->ui->txtFile->text());
            writer.writeAttribute("destContainer",setting->ui->comboContainer->currentText());
            QStringList videoInfo = MediaInfo::getVideoDetails(QString("\"") + filecontainer->item(i)->text() + QString("\""));
            QStringList generalInfo = MediaInfo::getGeneralDetails(QString("\"") + filecontainer->item(i)->text() + QString("\""));
            if((videoInfo.at(0).isEmpty() || videoInfo.at(0).isNull()) && generalInfo.at(1).compare("YUV", Qt::CaseInsensitive) == 0)
            {
                writer.writeStartElement("setting");
                writer.writeAttribute("name" , "Width");
                writer.writeTextElement("value",filecontainer->videoWidth.at(i));
                writer.writeEndElement();
                writer.writeStartElement("setting");
                writer.writeAttribute("name" , "Height");
                writer.writeTextElement("value",filecontainer->videoHeight.at(i));
                writer.writeEndElement();
                writer.writeStartElement("setting");
                writer.writeAttribute("name" , "Depth");
                writer.writeTextElement("value",filecontainer->videoDepth.at(i));
                writer.writeEndElement();
                writer.writeStartElement("setting");
                writer.writeAttribute("name" , "Framerate");
                writer.writeTextElement("value",filecontainer->frameRate.at(i));
                writer.writeEndElement();
                writer.writeStartElement("setting");
                writer.writeAttribute("name" , "Colorspace");
                writer.writeTextElement("value",filecontainer->videoColorSpace.at(i));
                writer.writeEndElement();
                writer.writeStartElement("setting");
                writer.writeAttribute("name" , "Subsampling");
                writer.writeTextElement("value",filecontainer->subSampling.at(i));
                writer.writeEndElement();
                writer.writeStartElement("setting");
                writer.writeAttribute("name" , "Totalframes");
                writer.writeTextElement("value",filecontainer->totalFrames.at(i));
                writer.writeEndElement();
                writer.writeStartElement("setting");
                writer.writeAttribute("name" , "Fps");
                writer.writeTextElement("value",filecontainer->fps.at(i));
                writer.writeEndElement();
            }
        }
        writeSettings(setting, all);
        writer.writeEndElement();
        i++;
    }
    writer.writeEndElement();
    writer.writeEndDocument();
}
