#include "update.h"
#include <QFile>
#include <QDir>
#include <QUrl>
#include <QUrlQuery>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>
#include <QStandardPaths>
#include <QSettings>
#include <QDomDocument>
#include <QMessageBox>
#include <QTextStream>
#include <QDesktopServices>

Update::Update()
{

}

Update::~Update()
{

}

Update *Update::instance()
{
    static Update *upd = NULL;
    if(upd != NULL)
        upd = new Update();
    return upd;
}

/* Check if an update exists */
int Update::chkUpdate(QString *downloadVersion, QString *downloadUrl, QString *checksum)
{
    int update;
    QString version_url = "https://x265.com/update/update.txt";
    QNetworkAccessManager NAManager;
    QUrl url(version_url);
    QNetworkRequest request(url);
    QUrlQuery data;
    request.setHeader(QNetworkRequest::ContentTypeHeader,
                      "application/x-www-form-urlencoded");
    QNetworkReply *reply = NAManager.post(request, data.toString(QUrl::FullyEncoded).toUtf8());
    QEventLoop eventLoop;
    QObject::connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    eventLoop.exec();
    if (reply->error())
    {
        return 2;
    }
    QString response = reply->readAll();
    if ( response.length() == 0)
        return 2;
    QStringList downloadList = response.split("-");
    *downloadVersion = downloadList.at(0);
    *downloadUrl = downloadList.at(1);
    *checksum = downloadList.at(2);
    QString Folder = QDir(qApp->applicationDirPath()).absolutePath();
    QString path = Folder.remove(QDir(qApp->applicationDirPath()).dirName())+ "version.txt";
    QFile file(path);
    file.open(QFile::ReadOnly);
    QString installedVersion = QString(file.readAll());
    file.close();
    QStringList latest = downloadList.at(0).split (".");
    QStringList install = installedVersion.split (".");
    if (latest.at(0) > install.at(0))
        update = 1;
    else if (latest.at(0) == install.at(0))
    {
        if (latest.at(1) > install.at(1))
            update = 1;
        else if (latest.at(1) == install.at(1))
        {
            if (latest.at(2) > install.at(2) )
                update = 1;
            else if (latest.at(2) == install.at(2))
                update = 0;
            else
                update = 0;
        }
        else
            update = 0;
    }
    else
        update = 0;

    return update;
}

/* Check if a newer version is already downloaded and not installed */
bool Update::chkDownload()
{
    QString downloadVersion, installedVersion;
    bool update;
    QString Folder = QDir(qApp->applicationDirPath()).absolutePath();
    QString path = Folder.remove(QDir(qApp->applicationDirPath()).dirName())+ "version.txt";
    QFile installedFile(path);
    QFile downloadFile (QDir::tempPath() + "/x265HEVCUpgrade_update.txt");
    QFile exeFile (QDir::tempPath() + "/x265HEVCUpgrade_update.exe");
    if(!exeFile.exists())
        return false;
    if(installedFile.exists())
    {
        installedFile.open(QFile::ReadOnly);
        installedVersion = QString(installedFile.readAll());
        installedFile.close();
    }
    else
        return false;

    if(downloadFile.exists())
    {
        downloadFile.open(QFile::ReadOnly);
        downloadVersion = downloadFile.readAll();
        downloadFile.close();
        QStringList download = downloadVersion.split (".");
        QStringList install = installedVersion.split (".");

        if (download.at(0) > install.at(0))
            update = true;
        else if (download.at(0) == install.at(0))
        {
            if (download.at(1) > install.at(1))
                update = true;
            else if (download.at(1) == install.at(1))
            {
                if (download.at(2) > install.at(2) )
                    update = true;
                else if (download.at(2) == install.at(2))
                    update = false;
                else
                    update = false;
            }
            else
                update = false;
        }
        else
            update = false;

        return update;
    }
    else
        return false;
}

/* check for redundant download */
bool Update::chkRedundant()
{
    QString downloadVersion, remoteVersion;
    QFile downloadFile (QDir::tempPath() + "/x265HEVCUpgrade_update.txt");
    QString version_url = "https://x265.com/update/update.txt";
    QNetworkAccessManager NAManager;
    QUrl url(version_url);
    QNetworkRequest request(url);
    QUrlQuery data;
    request.setHeader(QNetworkRequest::ContentTypeHeader,
                      "application/x-www-form-urlencoded");
    QNetworkReply *reply = NAManager.post(request, data.toString(QUrl::FullyEncoded).toUtf8());
    QEventLoop eventLoop;
    QObject::connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    eventLoop.exec();
    if (reply->error())
    {
        return false;
    }
    QString response = reply->readAll();
    if ( response.length() == 0)
        return false;
    QStringList downloadList = response.split("-");
    remoteVersion = downloadList.at(0);
    if(downloadFile.exists())
    {
        downloadFile.open(QFile::ReadOnly);
        downloadVersion = downloadFile.readAll();
        downloadFile.close();
        QStringList download = downloadVersion.split (".");
        QStringList install = remoteVersion.split (".");

        if ((download.at(0) == install.at(0)) &&
                (download.at(1) == install.at(1)) &&
                (download.at(2) == install.at(2)) )
            return true;
        else
            return false;
    }
    else
        return false;
}

/* Install the update */
void Update::installUpdate()
{
    QString path = QDir::tempPath() + "/x265HEVCUpgrade_update.exe";
    if (!path.isEmpty()) {
        QString url = path;

        if (url.startsWith ("/"))
            url = "file://" + url;

        else
            url = "file:///" + url;

        QDesktopServices::openUrl (url);
        qApp->closeAllWindows();
    }
}
