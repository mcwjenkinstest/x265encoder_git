#include <QString>
#include <QtMath>
#include "y4mutils.h"

static const Y4MUtils::x265_cli_csp x265_cli_csps[] =
{
    { 1, { 0, 0, 0 }, { 0, 0, 0 } }, /* i400 */
    { 3, { 0, 1, 1 }, { 0, 1, 1 } }, /* i420 */
    { 3, { 0, 1, 1 }, { 0, 0, 0 } }, /* i422 */
    { 3, { 0, 0, 0 }, { 0, 0, 0 } }, /* i444 */
    { 2, { 0, 0 },    { 0, 1 } },    /* nv12 */
    { 2, { 0, 0 },    { 0, 0 } },    /* nv16 */
};

Y4MUtils::Y4MUtils()
{
}

/* Returns the struct index for the corresponding video type */
int Y4MUtils::getColorSpaceIndex(QString colorSpace)
{
   if(colorSpace.compare("4:2:0") == 0)
       return 1;
   else if(colorSpace.compare("4:2:2") == 0)
       return 2;
   else if(colorSpace.compare("4:4:4") == 0)
       return 3;
   else if(colorSpace.compare("nv12", Qt::CaseInsensitive) == 0 )
       return 4;
   else if(colorSpace.compare("nv16", Qt::CaseInsensitive) == 0 )
       return 5;
   return 0;
}

Y4MUtils *Y4MUtils::instance()
{
    static Y4MUtils *util = NULL;
    if(util != NULL)
        util = new Y4MUtils();
    return util;
}

/* Calculates the frame count using the parameter */
int Y4MUtils::frameCount(int width, int height, int64_t size, int depth, QString colorSpace)
{
    QString frame = "FRAME";
    unsigned long long int  frameSize = frame.length();
    int bytesPerPixel = depth > 8 ? 2 : 1;
    int colorSpaceIndex = getColorSpaceIndex(colorSpace);
    int temp;
    for (int i = 0; i < x265_cli_csps[colorSpaceIndex].planes; i++)
    {
        temp = (unsigned int)(width >> x265_cli_csps[colorSpaceIndex].width[i]) * bytesPerPixel;
        temp = (unsigned int)(temp * (height >> x265_cli_csps[colorSpaceIndex].height[i]));
        frameSize += temp;
    }
    return ceil(((size)/frameSize));
}

/* Calculates the frame count considering default value for Depth and Colorspace */
int Y4MUtils::frameCount(int width, int height, int64_t size)
{
    int defaultDepth = 8;
    QString defaultColorSpace = "4:2:0";
    return frameCount(width, height, size, defaultDepth, defaultColorSpace);
}

/* Calculates the frame count consideing default value for Depth */
int Y4MUtils::frameCount(int width, int height, int64_t size, QString colorSpace)
{
    int defaultDepth = 8;
    return frameCount(width, height, size, defaultDepth, colorSpace);
}

/* Calculates the frame count considering default value for Colorspace */
int Y4MUtils::frameCount(int width, int height, int64_t size, int depth)
{
    QString defaultColorSpace = "4:2:0";
    return frameCount(width, height, size, depth, defaultColorSpace);
}
