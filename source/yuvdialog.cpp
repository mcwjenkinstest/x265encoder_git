#include <QMessageBox>
#include <QCloseEvent>
#include "yuvdialog.h"
#include "ui_yuvdialog.h"

YUVDialog::YUVDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::YUVDialog)
{
    ui->setupUi(this);

    ui->Subsampling->setCurrentIndex(3);
    ui->Resolution->setCurrentIndex(15);
}

YUVDialog::~YUVDialog()
{
    delete ui;
}



YUVDialog *YUVDialog::instance()
{
    static YUVDialog *yd = NULL;
    if(!yd) {
        yd = new YUVDialog();

    }
    return yd;
}


void YUVDialog::on_Ok_clicked()
{
   if (this->subSampling == 0)
   {
       QMessageBox msg;
       msg.setText("Please Select a valid Subsampling");
       msg.setIcon(QMessageBox::Information);
       msg.exec();
   }
   int Width = ui->Width->text().toInt();
   int Height = ui->Height->text().toInt();
   if(!(ui->Height->text().toInt()) || (Width <= 0) || !(ui->Width->text().toInt()) || (Height <= 0))
   {
       QMessageBox msg;
       msg.setText("Please Select a valid Width and Height");
       msg.setIcon(QMessageBox::Information);
       msg.exec();
   }
   if (this->subSampling > 0 && ui->Height->text().toInt() && Width > 0 && ui->Width->text().toInt() && Height > 0)
   {
       this->Ok = true;
       this->close();
   }
}

void YUVDialog::closeEvent(QCloseEvent *event)
{

    if(this->Ok || this->Cancel)
        event->accept();
    else
    {
        this->Cancel = true;
        event->accept();
    }

}



void YUVDialog::on_Subsampling_currentIndexChanged(int index)
{
    if (index >= 0)
        this->subSampling = index;

}

void YUVDialog::on_Resolution_currentIndexChanged(int index)
{
    switch (index) {
    case 1:
        ui->Width->setText("176");
        ui->Height->setText("144");
        break;
    case 2:
        ui->Width->setText("352");
        ui->Height->setText("240");
        break;
    case 3:
        ui->Width->setText("352");
        ui->Height->setText("288");
        break;
    case 4:
        ui->Width->setText("640");
        ui->Height->setText("480");
        break;
    case 5:
        ui->Width->setText("700");
        ui->Height->setText("525");
        break;
    case 6:
        ui->Width->setText("720");
        ui->Height->setText("576");
        break;
    case 7:
        ui->Width->setText("780");
        ui->Height->setText("586");
        break;
    case 8:
        ui->Width->setText("1080");
        ui->Height->setText("720");
        break;
    case 9:
        ui->Width->setText("1920");
        ui->Height->setText("1080");
        break;
    case 10:
        ui->Width->setText("2048");
        ui->Height->setText("1080");
        break;
    case 11:
        ui->Width->setText("3840");
        ui->Height->setText("2160");
        break;
    case 12:
        ui->Width->setText("4096");
        ui->Height->setText("2160");
        break;
    case 13:
        ui->Width->setText("4520");
        ui->Height->setText("2540");
        break;
    case 14:
        ui->Width->setText("7680");
        ui->Height->setText("4320");
        break;
    case 15:
        ui->Width->clear();
        ui->Height->clear();
        break;
    default:
        break;
    }
}

void YUVDialog::on_Cancel_clicked()
{
    this->Cancel = true;
    this->close();
}

void YUVDialog::on_Width_textChanged(const QString &arg1)
{

    int Width = arg1.toInt();
    int Height = ui->Height->text().toInt();
    if(Width == 176 && Height == 144)
        ui->Resolution->setCurrentIndex(1);
    else if(Width == 352 && Height == 240)
        ui->Resolution->setCurrentIndex(2);
    else if(Width == 352 && Height == 288)
        ui->Resolution->setCurrentIndex(3);
    else if(Width == 640 && Height == 480)
        ui->Resolution->setCurrentIndex(4);
    else if(Width == 700 && Height == 525)
        ui->Resolution->setCurrentIndex(5);
    else if(Width == 720 && Height == 576)
        ui->Resolution->setCurrentIndex(6);
    else if(Width == 780 && Height == 586)
        ui->Resolution->setCurrentIndex(7);
    else if(Width == 1080 && Height == 720)
        ui->Resolution->setCurrentIndex(8);
    else if(Width == 1920 && Height == 1080)
        ui->Resolution->setCurrentIndex(9);
    else if(Width == 2048 && Height == 1080)
        ui->Resolution->setCurrentIndex(10);
    else if(Width == 3840 && Height == 2160)
        ui->Resolution->setCurrentIndex(11);
    else if(Width == 4096 && Height == 2160)
        ui->Resolution->setCurrentIndex(12);
    else if(Width == 4520 && Height == 2540)
        ui->Resolution->setCurrentIndex(13);
    else if(Width == 7680 && Height == 4320)
        ui->Resolution->setCurrentIndex(14);
}

void YUVDialog::on_Height_textChanged(const QString &arg1)
{
    int Height = arg1.toInt();
    int Width = ui->Width->text().toInt();
    if(Width == 176 && Height == 144)
        ui->Resolution->setCurrentIndex(1);
    else if(Width == 352 && Height == 240)
        ui->Resolution->setCurrentIndex(2);
    else if(Width == 352 && Height == 288)
        ui->Resolution->setCurrentIndex(3);
    else if(Width == 640 && Height == 480)
        ui->Resolution->setCurrentIndex(4);
    else if(Width == 700 && Height == 525)
        ui->Resolution->setCurrentIndex(5);
    else if(Width == 720 && Height == 576)
        ui->Resolution->setCurrentIndex(6);
    else if(Width == 780 && Height == 586)
        ui->Resolution->setCurrentIndex(7);
    else if(Width == 1080 && Height == 720)
        ui->Resolution->setCurrentIndex(8);
    else if(Width == 1920 && Height == 1080)
        ui->Resolution->setCurrentIndex(9);
    else if(Width == 2048 && Height == 1080)
        ui->Resolution->setCurrentIndex(10);
    else if(Width == 3840 && Height == 2160)
        ui->Resolution->setCurrentIndex(11);
    else if(Width == 4096 && Height == 2160)
        ui->Resolution->setCurrentIndex(12);
    else if(Width == 4520 && Height == 2540)
        ui->Resolution->setCurrentIndex(13);
    else if(Width == 7680 && Height == 4320)
        ui->Resolution->setCurrentIndex(14);
    //else
       // ui->Resolution->setCurrentIndex(15);
}
