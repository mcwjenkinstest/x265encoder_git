#-------------------------------------------------
#
# Project created by QtCreator 2014-08-12T18:52:25
#
#-------------------------------------------------

QT       += core gui winextras xml network
DEFINES  += NOTDEMO

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = x265Encoder
TEMPLATE = app
#CONFIG += static
DEFINES += NOMINMAX
INCLUDEPATH += headers License

Debug:LIBS += -Llibs\NetLicensing_Debug -lnetlicensing
Release:LIBS += -Llibs\NetLicensing_Release -lnetlicensing

LIBS += -Llibs -llibcurl_a

SOURCES += source/addons.cpp source/encoder.cpp source/filecontainer.cpp source/main.cpp \
           source/mediainfo.cpp source/progressdialog.cpp \
    source/y4mutils.cpp \
    source/aboutdialog.cpp \
    source/settingswidget.cpp \
    source/settingutils.cpp \
    source/clickablelabel.cpp \
    source/maxbitratedialog.cpp\
    License/McwLicensePro.cpp \
    License/McwLicenseCon.cpp \
    source/systeminformation.cpp \
    source/licensedialog.cpp \
    source/yuvdialog.cpp \
    source/downloaddialog.cpp \
    source/update.cpp \
    source/licenseeinputdialog.cpp \
    source/McwModifyRegistrySecurity.cpp    

HEADERS += headers/addons.h headers/encoder.h headers/filecontainer.h headers/mediainfo.h \
           headers/progressdialog.h \
    headers/y4mutils.h \
    headers/aboutdialog.h \
    headers/settingswidget.h \
    headers/settingutils.h \
    headers/clickablelabel.h \
    License/Utils.h \
    License/Bitanswer.h \
    License/McwLicensePro.h \
    License/McwLicenseCon.h \
    License/json/json.h \
    License/json/json-forwards.h \
    License/netlicensing/constants.h \
    License/netlicensing/context.h \
    License/netlicensing/converters.h \
    License/netlicensing/datatypes.h \
    License/netlicensing/entity.h \
    License/netlicensing/exception.h \
    License/netlicensing/info.h \
    License/netlicensing/licensee.h \
    License/netlicensing/mapper.h \
    License/netlicensing/netlicensing.h \
    License/netlicensing/product.h \
    License/netlicensing/service.h \
    License/netlicensing/traversal.h \
    License/netlicensing/validation_result.h \
    License/netlicensing/version.h \
    headers/maxbitratedialog.h \
    headers/systeminformation.h \
    headers/licensedialog.h \
    headers/yuvdialog.h \
    headers/downloaddialog.h \
    headers/update.h \
    headers/licenseeinputdialog.h \
    headers/McwModifyRegistrySecurity.h

FORMS   += ui/encoder.ui ui/progressdialog.ui \
    ui/aboutdialog.ui \
    ui/settingswidget.ui \
    ui/maxbitratedialog.ui \
    ui/licensedialog.ui \
    ui/yuvdialog.ui \
    ui/downloaddialog.ui \
    ui/licenseeinputdialog.ui

RESOURCES += Resources.qrc


win32:RC_ICONS += Images/x265Logo_128alpha.ico
